<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new data;
include('banners.php');

$id_dica = (int) $_GET['dica'];

$dirfoto = $patchARQ . 'dicasvideos/';

//dados
$query = $sql->Consulta("SELECT * FROM dicasvideos WHERE id='$id_dica' AND estado='1' LIMIT 1");
$linha = mysql_fetch_array($query);
VerificaVar($linha);

if ($linha['cartaz']) {
    $linha['cartaz'] = '<div><span>Cartaz:</span><a href="' . $dirfoto . $linha['cartaz'] . '" title="Ver detalhes" class="bordacartaz fancybox"><img src="' . $dirfoto . 'mini/' . $linha['cartaz'] . '" style="float: none;" title="Ampliar cartaz" /></a></div>';
}
if ($linha['classificacao']) {
    $linha['classificacao'] = '<div><span>Classifica��o:</span>' . $linha['classificacao'] . '</div>';
}
if ($linha['direcao']) {
    $linha['direcao'] = '<div><span>Dire��o:</span>' . $linha['direcao'] . '</div>';
}
if ($linha['atores']) {
    $linha['atores'] = '<div><span>Elenco:</span>' . nl2br($linha['atores']) . '</div>';
}
if ($linha['sinopse']) {
    $linha['sinopse'] = '<div><span>Sinopse:</span>' . nl2br($linha['sinopse']) . '</div>';
}
if ($linha['locadora']) {
    $linha['locadora'] = '<div><span>Locadora:</span>' . $linha['locadora'] . '</div>';
}
?><!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Dicas de v�deos - <?= $linha['titulo']; ?> - <?= $config['nomesite']; ?></title>
        <meta charset="iso-8859-1">
        <link rel="icon" href="<?= $patchIMG ?>favicon.ico" type="image/x-icon" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">        -->
        <link rel="stylesheet" href="<?= $patchCSS ?>/reset.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/text.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/960_16_col.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/style.css">
        <link href="<?= $patchCSS ?>/dicasvideos.css" rel="stylesheet" type="text/css" />
        <link href="<?= $patchCSS ?>/amplia.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css" />
    </head>
    <body>

        <!-- topo -->
        <?php include('topo.php'); ?>
        <div class="container_16 box preto">

            <ul class="breadcrumb grid_16">
                <li><a href="<?= $host ?>" >Inicial</a> <span class="divider">></span></li>
                <li><a href="dicasVideo.php" class="linkcaminho">Dicas de v�deos</a> <span class="divider">></span></li>
                <li class="active">Visualizar detalhes</li>
            </ul>

            <?php include('sidebar.php'); ?>
            <div class="grid_13">
                <h2><span class="icon25 filmes"></span>Dicas de v�deos</h2>
                <div class="content">
                    <!--- corpo -->
                    <div class="fundocentro">
                        <div class="topobotaointerno" style="height: 22px; margin-top: 0px;display: none;">
                            <div class="descricaobotao"><a href="dicasVideo.php">Ver todas as dicas de v�deos</a></div>
                        </div>
                        <a href="http://casablancavideos.com.br/" target="blank" title="Casablanca Videolocadora"><img src="<?=$patchIMG?>casablanca.png" alt="Casablanca Videolocadora" style="top: 35px;right: 20px; position: absolute;"></a>
                        <div class="titulodica"><?= $linha['titulo']; ?></div>
                        <div class="dicavideoborda">
                            <div><span>G�nero:</span><?= $linha['genero']; ?></div>
                            <?= $linha['classificacao']; ?>
                            <?= $linha['direcao']; ?>
                            <?= $linha['locadora']; ?>
                            <?= $linha['atores']; ?>
                            <?= $linha['sinopse']; ?>
                            <?= $linha['cartaz']; ?>
                        </div>

                        <div class="clear"></div>
                        <div class="ads largo push_2" title="Anuncie" style="margin-top: 20px;">
                            <?php
                            if (isset($banners['9'])) {
                                print $banners['9'];
                            }
                            ?>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('rodape.php'); ?>
        <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>
    <script type="text/javascript" src="<?= $patchJS ?>geral.js"></script>
    <script type="text/javascript" src="<?= $patchJS ?>amplia.js"></script>
</body>
</html>
