<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new Data;
$id_evento = (int) $_GET['evento'];

include('banners_eventos.php');

//dados do evento
$query = $sql->Consulta("SELECT galerias.*,estados.uf,cat_galeria.categoria
FROM galerias
LEFT JOIN estados ON estados.id=galerias.id_estado
LEFT JOIN cat_galeria ON cat_galeria.id=galerias.id_categoria
WHERE galerias.id='$id_evento' LIMIT 1");
$evento = mysql_fetch_array($query);
VerificaVar($evento);
$id_categoria = $evento['id_categoria'];

//atualiza acesso
$sql->Consulta("UPDATE galerias SET acessos=(acessos + 1) WHERE id='$id_evento' LIMIT 1");

if($evento['fotografo']){ $evento['fotografo'] = '<br />Fot�grafo: '.$evento['fotografo'];}
if($evento['local']){ $evento['local'] = '<br />Local: '.$evento['local'];}
if($evento['cidade']){ $evento['cidade'] = '<br />Cidade: '.$evento['cidade'];}
if($evento['uf']){ $evento['cidade'] .= ' - '.$evento['uf'];}
if($evento['descricao']){ $evento['descricao'] = '<br /><br />'.$evento['descricao'];}

//fotos deste evento
$diretorio = 'arquivos/galerias/'.$evento['pasta'].'/';
$fotos = array();

$aberto = @opendir($diretorio.'mini/');//abre o diretorio 
while($arq = @readdir($aberto)) {//le o diretorio
	if(($arq != '.') && ($arq != '..')) {//desconsidera subdiretorios
		$ext = explode('.',$arq); //verifica extens�o da imagens
		if(($ext[1] == 'jpg') || ($ext[1] == 'JPG')){
			$fotos[] = $arq;
		}
	}
}
@closedir($aberto);
//ordena o vetor
@array_multisort($fotos);

//galeria v4
$resul = '{fotos: [';
$temp = array();
$totalfotos = sizeof($fotos);
for($i = 0; $i < $totalfotos; $i++){ 
	$temp[] =  '{img:"'.$fotos[$i].'"}';
			
}
$resul .= implode(',',$temp);
$resul .= ']}';
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=$evento['categoria'];?> - <?=$evento['titulo'];?>  - <?=$config['nomesite'];?></title>
<link href="css/geral.css" rel="stylesheet" type="text/css" />
<link href="css/eventoVisualiza.css" rel="stylesheet" type="text/css" />
<link href="css/paginacao.css" rel="stylesheet" type="text/css" />
<link href="css/galeriav4.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
	var totalFotos = <?=$totalfotos;?>;
	var diretorio = '<?=$diretorio;?>';
	var pasta = '';
	var pastaMiniatura = 'mini/';
	var fotos = <?=$resul;?>;
	var fotoAtual = 0;
	var icload = new Image();
	icload.src = 'load28.gif';
</script>
<script type="text/javascript" src="js/galeriav4.js"></script>
<script type="text/javascript" src="js/geral.js"></script>
</head>

<body>
<table width="750" border="0" cellspacing="0" cellpadding="0" align="center" class="fundosite">
  <tr>
    <td height="180">
		<!-- topo -->
		<?php @include('topo.php'); ?>		</td>
  </tr>
  <tr>
    <td align="center" valign="top" class="corpo">
		<!-- menu -->
		<div class="fundomenuh">
			<a href="index.php" class="linkmenuh">Inicial</a>
			<a href="muralfotos.php" class="linkmenuh">Mural de fotos</a>
			<a href="mural.php" class="linkmenuh">Mural de recados</a>
			<a href="galeranet.php" class="linkmenuh">Galera net</a>
			<a href="videos.php" class="linkmenuh">V�deos em Foco</a>
			<a href="agenda.php" class="linkmenuh">Agenda de eventos</a>
			<a href="javascript:Mostra('maisg');" class="linkmaisg">Mais galerias</a>
			<div id="maisg" class="fundomaisg" style="display: none;">
				<div class="maigexibindo">Exibindo as 5 galerias mais recentes em <?=$evento['categoria'];?></div>
				<?php
				//listagem
				$cont = 1;
				$query = $sql->Consulta("SELECT galerias.*,estados.uf 
				FROM galerias
				LEFT JOIN estados ON estados.id=galerias.id_estado
				WHERE galerias.estado='1' AND id_categoria='$id_categoria' ORDER BY galerias.id DESC LIMIT 5");
				while($linha = mysql_fetch_array($query)){ 
					print '<a href="eventoVisualiza.php?evento='.$linha['id'].'" class="titulomaisg">'.$cont.' - '.$linha['titulo'].' - '.$data->MysqlData($linha['data']).'</a>';
					$cont++;
				}
				?>
				<div class="fundovertodas">
					<a href="eventos.php?cat=<?=$evento['id_categoria'];?>">+ Ver todas as galerias em <?=$evento['categoria'];?></a>
				</div>
				<a href="javascript:Mostra('maisg');" title="Fechar este menu">Fechar</a>
			</div>
		</div>
		
		<!--- corpo -->
		<div class="caminho" style="padding-left: 3px;">
			<a href="index.php" class="linkcaminho">Inicial</a><img src="icones/setacaminho.jpg" />
			<a href="eventos.php?cat=<?=$evento['id_categoria'];?>" class="linkcaminho"><?=$evento['categoria'];?></a><img src="icones/setacaminho.jpg" />
			<?=$evento['titulo'];?>
		</div>

		<div class="fundocentro" align="left" style="padding: 5px; position: relative; z-index: 0;">
			<div class="bannertopo" style="top: 0px;"><?php if(isset($banners['1'])) { print $banners['1']; } ?></div>
			<div class="bannertopo" style="top: 80px;"><?php if(isset($banners['2'])) { print $banners['2']; } ?></div>
			<br />
			<div class="tituloevento"><?=$evento['titulo'];?></div>
			<div class="qtdfotos"><?=sizeof($fotos);?> fotos</div>
			<div class="fundodescricao">
				Data: <?=$data->MysqlData($evento['data']);?>
				<?=$evento['local'];?>
				<?=$evento['cidade'];?>
				<?=$evento['fotografo'];?>
				<?=$evento['descricao'];?>
			</div>
		</div>
		<div style="position: static;"><?php include('galeriav4.php'); ?></div>
		<div style="clear: both;"></div>
		<div class="bannerrodape"><?php if(isset($banners['3'])) { print $banners['3']; } ?></div>
		<div class="bannerrodape"><?php if(isset($banners['4'])) { print $banners['4']; } ?></div>
		</td>
  </tr>
  <tr>
    <td><?php include('rodape.php'); ?></td>
  </tr>
</table>
</body>
</html>