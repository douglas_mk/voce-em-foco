<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new data;
include('banners.php');

$dexibe = 'none';
$denviado = 'none';
$erro = false;
$liberado = 0; //0 - requer liberção, 1 - liberado
$dirsmiles = 'icones/smiles/';
$dirfoto = 'arquivos/muralfotos/';
$tempo_envio = 20;

if(!isset($post['nome'])){ $post['nome'] = NULL; }
if(!isset($post['cidade'])){ $post['cidade'] = NULL; }
if(!isset($post['email'])){ $post['email'] = NULL; }
if(!isset($post['comentario'])){ $post['comentario'] = NULL; }

if(isset($_POST['EnviaRecado'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['nome'],'texto','Nome');
	$form->Valida($post['cidade'],'texto','Cidade');
	$form->Valida($post['email'],'email','E-mail');
	$form->Valida($post['comentario'],'texto','Comentário');
	$form->Tamanho($post['comentario'],2,500,'Comentário');

	$erro = $form->getErro();
	
	session_start();
	if(isset($_SESSION['tempoform'])){
		if($_SESSION['tempoform'] > time()){
			$tempo = $_SESSION['tempoform'] - time();
			$erro = 'Aguarde '.$tempo.' segundos para enviar novamente';
		}
	}
	
	if(!$erro){
		$img = new UploadImg($_FILES['foto']);
		$img->setQuali(85);
		$img->setLargura(400);
		$img->setAltura(300);
		$img->setFixa('altura');
		$img->setNome(date('dmYhis'));
		$maior = $img->Gera($dirfoto);	
	
		$img->setLargura(100);
		$img->setAltura(75);
		$mini = $img->Gera($dirfoto.'mini/');	
		
		if((!$maior) || (!$mini)){
			$erro = $img->getErro();
		}else{
		
			$foto =  $img->getNome();	
			$ip = Ip();
			
			//smiles
			//for($i = 1; $i < 40; $i++){
			//	$post[recado] = str_replace(' :'.$i.': ','<img src="'.$dirsmiles.$i.'" />',$post[recado]);
			//}		
			
			$sql->Consulta("INSERT INTO muralf
			(estado,nome,cidade,email,comentario,foto,data,hora,ip)
			VALUES
			('$liberado','$post[nome]','$post[cidade]','$post[email]','$post[comentario]','$foto',NOW(),NOW(),'$ip')");
				
			$denviado = 'block';
			$post = NULL;
			$_SESSION['tempoform'] = time() + $tempo_envio;
		}
	}
}

if($erro){
	$dexibe = 'block';
}

//paginacao
$totar = $sql->Totalreg("SELECT * FROM muralf WHERE estado='1'");
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);
$pag->setRegistro(6);
$pag->setTotal($totar); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('');
$pagatual = $pag->getAtual();

if($totar > 0){
	$exmax = $min + $max;
	if($totar < ($min + $max)){ $exmax = $totar; }
	$exibindo = 'Exibindo '.($min + 1).' - '.$exmax.' de um total de '.$totar; 
}else{
	$exibindo = 'Nenhuma foto encontrada';
}

//listagem
$query = $sql->Consulta("SELECT * FROM muralf WHERE estado='1' ORDER BY id DESC LIMIT $min,$max");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Mural de fotos  - <?=$config['nomesite'];?></title>
<link href="css/geral.css" rel="stylesheet" type="text/css" />
<link href="css/mural.css" rel="stylesheet" type="text/css" />
<link href="css/paginacao.css" rel="stylesheet" type="text/css" />
<link href="css/amplia.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/muralfotos.js"></script>
<script type="text/javascript" src="js/geral.js"></script>
<script type="text/javascript" src="js/amplia.js"></script>
</head>

<body>
<table width="750" border="0" cellspacing="0" cellpadding="0" align="center" class="fundosite">
  <tr>
    <td height="180" colspan="2">
		<!-- topo -->
		<?php @include('topo.php'); ?>
		</td>
  </tr>
  <tr>
    <td width="180" align="center" valign="top">
		<!-- menu -->
		<?php include('menu.php'); ?>
	</td>
    <td width="560" align="center" valign="top" class="corpo">
		<!--- corpo -->
		<div class="bannercentral"><?php if(isset($banners['8'])) { print $banners['8']; } ?></div>
		<div class="caixatopointerno">Mural de fotos</div>
		<div class="caminho">
			<a href="index.php" class="linkcaminho">Inicial</a><img src="icones/setacaminho.jpg" />Mural de fotos
		</div>
		<!-- recado enviado -->
		<div class="msgaviso" style="display: <?=$denviado;?>; ">Sua foto foi enviada com sucesso, aguarde a liberação pelo administrador do sistema</div>

		<div class="fundocentro">
			<!-- envio -->
			<div class="topobotaointerno">
				<div style="float: left;"><a href="javascript:ExibeForm();" title="Envie sua foto"><img src="icones/enviarfoto.jpg" alt="Envie sua foto" border="0" /></a></div>
				<div class="descricaobotao"></div>
			</div>
			
			<div style="position: relative; width: 555px; height: auto;">
				<div class="muralcaixaform" style="display: <?=$dexibe;?>;" id="formrecado">
					<div class="msgerro" style="display: <?=$dexibe;?>;"><?=$erro;?></div>
					<form action="muralfotos.php" method="post" name="recado" id="recado" style="margin: 0px;" enctype="multipart/form-data" onsubmit="ExibeAguarde()">
						Nome:<br />
						<input name="nome" type="text" class="muralform" maxlength="40" value="<?=$post['nome'];?>" /><br />
						Cidade:<br />
						<input name="cidade" type="text" class="muralform" maxlength="40" value="<?=$post['cidade'];?>" /><br />
						E-mail:<br />
						<input name="email" type="text" class="muralform" maxlength="60" value="<?=$post['email'];?>" /><br />
						Foto:<br />
						<input name="foto" type="file" class="muralform" /><br />
						Comentário:<br />
						<textarea name="comentario" id="trecado" class="muralform" style="height: 70px;" onfocus="ContaC(this)"><?=$post['comentario'];?></textarea>
						<div class="muralcaracteres">Caracteres restantes: <span id="carac">500</span></div>
						
						<!-- <a href="javascript:ExibeSmiles();" class="muralbotsmile" title="Exibir smiles">Exibir smiles</a> -->
						<!-- <div id="smiles" style="display: none;" class="muralfundosmiles"> -->
						<?php
						//for($i = 1; $i < 40; $i++){
						//	print '<img src="'.$dirsmiles.$i.'" class="muralsmile" onclick="AdicionaSmile('.$i.')" />';
						//}
						?>
						<!-- </div> -->
						
						<input type="submit" name="EnviaRecado" value="Enviar foto" class="muralbotaoform" title="Enviar foto" />
						<input type="button" name="Cancelar" value="Cancelar" class="muralbotaoform" onclick="FechaForm()" title="Cancelar envio" />
					</form>
					<div class="muralload" id="aguarde"><img src="icones/aguarde.gif" alt="Aguarde" /></div>
				</div>
			</div>
			
			<div class="fundoexibindo"><span class="exibindo"><?=$exibindo;?></span></div>
			
			<!-- listagem -->
			<?php 
			while($linha = mysql_fetch_array($query)){ 
				$totalcoment = $sql->Totalreg("SELECT * FROM coment_muralf WHERE id_recado='$linha[id]' AND estado='1'");
				$coment = 'Nenhum comentário';
				if($totalcoment > 0){ $coment = $totalcoment.' Comentário(s)'; }
			?>
			<div class="muralbordalista">
				<a href="javascript:Amplia('<?=$dirfoto.$linha['foto'];?>');" title="Ampliar foto" class="bordaimg"><img src="<?=$dirfoto.'mini/'.$linha['foto'];?>" /></a>
				<div class="muralbordatitulolista">
						<div class="muralcaixacomentario">
							<a href="muralfotosComent.php?recado=<?=$linha['id'];?>" class="linkcomentarios">Ler e enviar comentários</a>
							<?=$coment;?>
						</div>
						
						<span class="muraldescricaotitulo">Nome:</span> <?=$linha['nome'];?><br />
						<span class="muraldescricaotitulo">Cidade:</span> <?=$linha['cidade'];?><br />
						<div class="muraldata"><?=$data->MYsqlData($linha['data']);?> - <?=$linha['hora'];?></div>
						<div class="muralemail">
							<a href="javascript:MostraEmail(<?=$linha['id'];?>)" title="Ver e-mail"><img src="icones/email.jpg" alt="Ver e-email" border="0" /></a>&nbsp;<span id="e<?=$linha['id'];?>" style="display: none;"><?=$linha['email'];?></span>
						</div>
				</div>
				<?=nl2br($linha['comentario']);?>&nbsp;
			</div>
			<?php } ?>
			
			<!-- paginacao -->
			<div class="fundopaginacao">
				<?=$paginas;?>
			</div>
			
			<div class="bannercentral" style="margin-top: 15px;"><?php if(isset($banners['9'])) { print $banners['9']; } ?></div>
		</div>
	</td>
  </tr>
  <tr>
    <td colspan="2"><?php include('rodape.php'); ?></td>
  </tr>
</table>
</body>
</html>