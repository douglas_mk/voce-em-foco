<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new data;
include('banners.php');

$id_recado = (int) $_GET['recado'];

$dexibe = 'none';
$denviado = 'none';
$erro = false;
$liberado = 0; //0 - requer liberção, 1 - liberado
$dirsmiles = 'icones/smiles/';
$dirfoto = 'arquivos/muralfotos/';
$tempo_envio = 20;

//dados da foto
$query = $sql->Consulta("SELECT * FROM muralf WHERE id='$id_recado' LIMIT 1");
$reg = mysql_fetch_array($query);
VerificaVar($reg);

if($reg['estado'] != 1)
{
	header('Location:index.php');
	exit();
}

if(!isset($post['nome'])){ $post['nome'] = NULL; }
if(!isset($post['cidade'])){ $post['cidade'] = NULL; }
if(!isset($post['email'])){ $post['email'] = NULL; }
if(!isset($post['comentario'])){ $post['comentario'] = NULL; }

if(isset($_POST['EnviaComentario'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['nome'],'texto','Nome');
	$form->Valida($post['cidade'],'texto','Cidade');
	$form->Valida($post['email'],'email','E-mail');
	$form->Valida($post['comentario'],'texto','Comentário');
	$form->Tamanho($post['comentario'],2,500,'Comentário');

	$erro = $form->getErro();
	
	session_start();
	if(isset($_SESSION['tempoform'])){
		if($_SESSION['tempoform'] > time()){
			$tempo = $_SESSION['tempoform'] - time();
			$erro = 'Aguarde '.$tempo.' segundos para enviar novamente';
		}
	}
	
	if(!$erro){
		$foto = NULL;
		$img = new UploadImg($_FILES['foto']);
		if($img->getTamanho() > 0){
			$img->setQuali(85);
			$img->setLargura(400);
			$img->setAltura(300);
			$img->setFixa('altura');
			$img->setNome(date('dmYhis'));
			$maior = $img->Gera($dirfoto);	
		
			$img->setLargura(100);
			$img->setAltura(75);
			$mini = $img->Gera($dirfoto.'mini/');	
			
			if((!$maior) || (!$mini)){
				$erro = $img->getErro();
			}
		}
		
		
		if(!$erro){
		
			$foto =  $img->getNome();	
			$ip = Ip();
			
			//smiles
			//for($i = 1; $i < 40; $i++){
			//	$post[recado] = str_replace(' :'.$i.': ','<img src="'.$dirsmiles.$i.'" />',$post[recado]);
			//}		
			
			$sql->Consulta("INSERT INTO coment_muralf
			(id_recado,estado,nome,cidade,email,comentario,foto,data,hora,ip)
			VALUES
			('$id_recado','$liberado','$post[nome]','$post[cidade]','$post[email]','$post[comentario]','$foto',NOW(),NOW(),'$ip')");
				
			$denviado = 'block';
			$post = NULL;
			$_SESSION['tempoform'] = time() + $tempo_envio;
		}
	}
}

if($erro){
	$dexibe = 'block';
}

//paginacao
$totac = $sql->Totalreg("SELECT * FROM coment_muralf WHERE estado='1' AND id_recado='$id_recado'");
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);
$pag->AddLink('recado',$id_recado);
$pag->setRegistro(5);
$pag->setTotal($totac); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('');

if($totac > 0){
	$exmax = $min + $max;
	if($totac < ($min + $max)){ $exmax = $totac; }
	$exibindo = 'Exibindo '.($min + 1).' - '.$exmax.' de um total de '.$totac;
	$coment = $totac.' Comentários';
	
	if($totac == 1){ $coment = $totac.' Comentário'; }
}else{
	$coment = 'Nenhum comentário'; 
	$exibindo = 'Nenhum comentário para esta foto';
	
}

//listagem
$query = $sql->Consulta("SELECT * FROM coment_muralf WHERE estado='1' AND id_recado='$id_recado' ORDER BY id DESC LIMIT $min,$max");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Comentar foto  - <?=$config['nomesite'];?></title>
<link href="css/geral.css" rel="stylesheet" type="text/css" />
<link href="css/comentario.css" rel="stylesheet" type="text/css" />
<link href="css/paginacao.css" rel="stylesheet" type="text/css" />
<link href="css/amplia.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/muralfotos.js"></script>
<script type="text/javascript" src="js/geral.js"></script>
<script type="text/javascript" src="js/amplia.js"></script>
</head>

<body>
<table width="750" border="0" cellspacing="0" cellpadding="0" align="center" class="fundosite">
  <tr>
    <td height="180" colspan="2">
		<!-- topo -->
		<?php @include('topo.php'); ?>
		</td>
  </tr>
  <tr>
    <td width="180" align="center" valign="top">
		<!-- menu -->
		<?php include('menu.php'); ?>
	</td>
    <td width="560" align="center" valign="top" class="corpo">
		<!--- corpo -->
		<div class="bannercentral"><?php if(isset($banners['8'])) { print $banners['8']; } ?></div>
		<div class="caixatopointerno">Mural de fotos - Comentar foto</div>
		<div class="caminho">
			<a href="index.php" class="linkcaminho">Inicial</a><img src="icones/setacaminho.jpg" />
			<a href="muralfotos.php" class="linkcaminho">Mural de fotos</a><img src="icones/setacaminho.jpg" />Comentar foto
		</div>
		
		<!-- recado enviado -->
		<div class="msgaviso" style="display: <?=$denviado;?>; ">Seu comentário foi enviado com sucesso, aguarde a liberação pelo administrador do sistema</div>
		<div class="fundocentro">
			<!-- envio -->
			<div class="topobotaointerno">
				<div style="float: left;"><a href="javascript:ExibeForm();" title="Envie seu comentário"><img src="icones/comentarfoto.jpg" alt="Envie seu comentário" border="0" /></a></div>
				<div class="descricaobotao"><a href="muralfotos.php">Voltar para Mural de Fotos</a></div>
			</div>
			
			<div style="position: relative; width: 555px; height: auto;">
				<div class="comcaixaform" style="display: <?=$dexibe;?>;" id="formrecado">
					<div class="msgerro" style="display: <?=$dexibe;?>;"><?=$erro;?></div>
					<form action="muralfotosComent.php?recado=<?=$id_recado;?>" method="post" name="recado" id="recado" style="margin: 0px;" enctype="multipart/form-data" onsubmit="ExibeAguarde()">
						Nome:<br />
						<input name="nome" type="text" class="comform" maxlength="40" value="<?=$post['nome'];?>" /><br />
						Cidade:<br />
						<input name="cidade" type="text" class="comform" maxlength="40" value="<?=$post['cidade'];?>" /><br />
						E-mail:<br />
						<input name="email" type="text" class="comform" maxlength="60" value="<?=$post['email'];?>" /><br />
						Foto: (Opcional)<br />
						<input name="foto" type="file" class="comform" /><br />
						Comentário:<br />
						<textarea name="comentario" id="trecado" class="comform" style="height: 70px;" onfocus="ContaC(this)"><?=$post['comentario'];?></textarea>
						<div class="comcaracteres">Caracteres restantes: <span id="carac">500</span></div>
						
						<input type="submit" name="EnviaComentario" value="Enviar coment&aacute;rio" class="combotaoform" title="Enviar coment&aacute;rio" />
						<input type="button" name="Cancelar" value="Cancelar" class="combotaoform" onclick="FechaForm()" title="Cancelar envio" />
					</form>
					<div class="comload" id="aguarde"><img src="icones/aguarde.gif" alt="Aguarde" /></div>
				</div>
			</div>
			
			<!-- comentario -->
			<div class="fundocomentado">
				<div style="float: left;">
					<a href="javascript:Amplia('<?=$dirfoto.$reg['foto'];?>');" title="Ampliar foto" class="bordaimg">
					<img src="<?=$dirfoto.$reg['foto'];?>" height="200" border="0" style="float: none;" /></a>
					<div style="margin-top: 5px;"><a href="javascript:Amplia('<?=$dirfoto.$reg['foto'];?>');" title="Ampliar foto">+ Ampliar foto</a></div>
				</div>
				<div class="titulocomentado"><?=$reg['nome'];?></div><br />
				<strong>E-mail:<span class="destaquecomentado"> <?=$reg['email'];?></span> </strong><br />
				<strong>Cidade:<span class="destaquecomentado"> <?=$reg['cidade'];?></span> </strong><br />
				<div class="comdata">Enviado em <?=$data->MYsqlData($reg['data']);?> - <?=$reg['hora'];?></div>
				<div class="descricaocomentado"><?=nl2br($reg['comentario']);?></div>
			</div>

			<div class="caixatopointerno" style="margin-top: 20px; margin-bottom: 10px; position: static;"><?=$coment;?></div>
			
			<div class="fundoexibindo"><span class="exibindo"><?=$exibindo;?></span></div>
			
			<!-- listagem -->
			<?php while($linha = mysql_fetch_array($query)){ 
			if($linha['foto']){
				$linha['foto'] = '<a href="javascript:Amplia(\''.$dirfoto.$linha['foto'].'\');" title="Ampliar foto" class="bordaimg"><img src="'.$dirfoto.'mini/'.$linha['foto'].'" /></a>';
			}
			?>
			
			<div class="combordalista">
				<?=$linha['foto'];?>
				<div class="combordatitulolista">	
						<div class="comcaixacomentario">
							<div class="comdata"><?=$data->MYsqlData($linha['data']);?> - <?=$linha['hora'];?></div>
						</div>	
										
						<span class="comdescricaotitulo">Nome:</span> <?=$linha['nome'];?><br />
						<span class="comdescricaotitulo">Cidade:</span> <?=$linha['cidade'];?><br />
						<div class="comemail">
							<a href="javascript:MostraEmail(<?=$linha['id'];?>)" title="Ver e-mail"><img src="icones/email.jpg" alt="Ver e-email" border="0" /></a>&nbsp;&nbsp;<span id="e<?=$linha['id'];?>" style="display: none;"><?=$linha['email'];?></span>
						</div>
				</div>
				<?=nl2br($linha['comentario']);?>&nbsp;
			</div>
			<?php } ?>
			
			<!-- paginacao -->
			<div class="fundopaginacao">
				<?=$paginas;?>
			</div>
			
			<div class="bannercentral" style="margin-top: 15px;"><?php if(isset($banners['9'])) { print $banners['9']; } ?></div>
		</div>
	</td>
  </tr>
  <tr>
    <td colspan="2"><?php include('rodape.php'); ?></td>
  </tr>
</table>
</body>
</html>