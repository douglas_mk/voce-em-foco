<?php
date_default_timezone_set('America/Sao_Paulo');

$config['nomesite'] = 'Voc� em Foco';

$base = 'sistema/';

require($base.'includes/class.email.php');
require($base.'includes/class.data.php');
require($base.'includes/class.mysql.php');
//require($base.'includes/class.template.php');
require($base.'includes/class.validaform.php');
require($base.'includes/class.uploadimg.php');
require($base.'includes/inc.funcoes.php');
require('class.paginacao.php');
?>
