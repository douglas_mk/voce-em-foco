<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new data;
include('banners.php');

$dirfoto = 'arquivos/galerias/foto_capa/';

if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);

$id_categoria = (int) $_GET['cat'];

//dados da categoria
$query = $sql->Consulta("SELECT * FROM cat_galeria WHERE id='$id_categoria' LIMIT 1");
$cat = mysql_fetch_array($query);
VerificaVar($cat);

//busca
if(!isset($_GET['busca'])){ $_GET['busca'] = NULL; }
$busca = Sql_inject($_GET['busca']);
$titulob = 'Todas as galerias';
$vertodos = NULL;
$and = NULL;

if($busca){
	$and = " AND (galerias.titulo LIKE '%$busca%' OR galerias.local LIKE '%$busca%') AND galerias.id_categoria='$id_categoria' ";
	$pag->AddLink('busca',$busca);
	$titulob = 'Resultados da busca por: '.$busca;
}

//paginacao
$totaln = $sql->Totalreg("SELECT galerias.* FROM galerias WHERE estado='1' AND id_categoria='$id_categoria' $and");

if(($busca) && ($totaln == 0)){ 
	$vertodos = '<a href="eventos.php?cat='.$id_categoria.'" title="Ver todas as galerias em '.$cat['categoria'].'">Todas as galerias em '.$cat['categoria'].'</a>';
}


$pag->setRegistro(7);
$pag->AddLink('cat',$id_categoria);
$pag->setTotal($totaln); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('');
$pagatual = $pag->getAtual();

$totalgaleria = NULL;
if($totaln > 0){
	$exmax = $min + $max;
	if($totaln < ($min + $max)){ $exmax = $totaln; }
	$exibindo = '&nbsp;&nbsp;I&nbsp;&nbsp;Exibindo '.($min + 1).' - '.$exmax.' de um total de '.$totaln; 
	$totalgaleria = ' ('.$totaln.' galeria'; 
	if($totaln > 1){ $totalgaleria.= 's'; }
	$totalgaleria .= ')';
}else{
	$exibindo = '&nbsp;&nbsp;I&nbsp;&nbsp;Nenhuma galeria encontrada';
}

//listagem
$query = $sql->Consulta("SELECT galerias.*,estados.uf 
FROM galerias
LEFT JOIN estados ON estados.id=galerias.id_estado
WHERE galerias.estado='1' AND id_categoria='$id_categoria' $and ORDER BY galerias.id DESC LIMIT $min,$max");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title><?=$cat['categoria'];?> - <?=$config['nomesite'];?></title>
<link href="css/geral.css" rel="stylesheet" type="text/css" />
<link href="css/eventos.css" rel="stylesheet" type="text/css" />
<link href="css/paginacao.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/geral.js"></script>
</head>

<body>
<table width="750" border="0" cellspacing="0" cellpadding="0" align="center" class="fundosite">
  <tr>
    <td height="180" colspan="2">
		<!-- topo -->
		<?php @include('topo.php'); ?>
		</td>
  </tr>
  <tr>
    <td width="180" align="center" valign="top">
		<!-- menu -->
		<?php include('menu.php'); ?>
	</td>
    <td width="560" align="center" valign="top" class="corpo">
		<!--- corpo -->
		<div class="bannercentral"><?php if(isset($banners['8'])) { print $banners['8']; } ?></div>
		<div class="caixatopointerno"><?=$cat['categoria'];?><?=$totalgaleria;?></div>
		<div class="caminho">
			<a href="index.php" class="linkcaminho">Inicial</a><img src="icones/setacaminho.jpg" /><?=$cat['categoria'];?>
		</div>


		<div class="fundocentro">
			<!-- busca -->
			<div class="fundobusca">
				<div style="float: left;"><a href="javascript:ExibeBusca();" title="Exibir busca"><img src="icones/buscar.jpg" alt="Buscar" border="0" /></a>
					<div id="formbusca" class="caixabusca" style="display: none; width: 240px;">
					<form action="eventos.php" method="get" name="busca" id="busca" style="margin: 0px; display: block;">
						<input type="hidden" name="cat" value="<?=$id_categoria;?>" />
						<input type="text" name="busca" maxlength="25" class="formbusca" value="<?=$busca;?>" title="Informe um termo para buscar" />
						<input type="submit" value="Buscar galeria" name="BuscaT" class="botaobusca" style="width: 100px;"> 
					</form>
					</div>
				</div>
				<div class="vertodosbusca"><?=$vertodos;?></div>
			</div>
		
			<div class="fundoexibindo"><?=$titulob;?><span class="exibindo"><?=$exibindo;?></span></div>
			
			<!-- listagem -->
			<table width="550" border="0" cellspacing="0" cellpadding="5">
			<?php 
			while($linha = mysql_fetch_array($query)){ 
			if($linha['foto_capa']){
				$foto = $dirfoto.'mini/'.$linha['foto_capa'];
			}else{
				$foto = 'icones/fotogaleria.jpg';
			}
			$linha['foto_capa'] = '<a href="eventoVisualiza.php?evento='.$linha['id'].'" title="Ver fotos" class="bordaimg"><img src="'.$foto.'" style="float: none; margin: 10px;" /></a>';
			
			if($linha['local']){ $linha['local'] = '<div class="eventodescricao">Local: '.$linha['local'].'</div>'; }
			if($linha['uf']){ $linha['uf'] = ' - '.$linha['uf']; }
			if($linha['cidade']){ $linha['cidade'] = '<div class="eventodescricao">Cidade: '.$linha['cidade'].$linha['uf'].'</div>'; }
			?>
			  <tr>
				<td width="90" class="bordafundofoto" align="center"><?=$linha['foto_capa'];?></td>
				<td align="left" valign="top" class="bordafundodados">
					<div class="eventodata"><?=$data->MYsqlData($linha['data']);?></div>
					<a href="eventoVisualiza.php?evento=<?=$linha['id'];?>" class="eventotitulolista"><?=$linha['titulo'];?></a>
					
					<?=$linha['local'];?>
					<?=$linha['cidade'];?>
				</td>
			  </tr>
			  <tr>
				<td height="15" colspan="2"></td>
			  </tr>
			<?php } ?>
			</table>
			
			<!-- paginacao -->
			<div class="fundopaginacao">
				<?=$paginas;?>
			</div>
			
			<div class="bannercentral" style="margin-top: 15px;"><?php if(isset($banners['9'])) { print $banners['9']; } ?></div>
		</div>
	</td>
  </tr>
  <tr>
    <td colspan="2"><?php include('rodape.php'); ?></td>
  </tr>
</table>
</body>
</html>
