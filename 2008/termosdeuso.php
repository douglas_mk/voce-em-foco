<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new data;
include('banners.php');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Termos de uso  - <?=$config['nomesite'];?></title>
<link href="css/geral.css" rel="stylesheet" type="text/css" />
<link href="css/paginacao.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/geral.js"></script>
</head>

<body>
<table width="750" border="0" cellspacing="0" cellpadding="0" align="center" class="fundosite">
  <tr>
    <td height="180" colspan="2">
		<!-- topo -->
		<?php @include('topo.php'); ?>
		</td>
  </tr>
  <tr>
    <td width="180" align="center" valign="top">
		<!-- menu -->
		<?php include('menu.php'); ?>
	</td>
    <td width="560" align="center" valign="top" class="corpo">
		<!--- corpo -->
		<div class="bannercentral"><?php if(isset($banners['8'])) { print $banners['8']; } ?></div>
		<div class="caixatopointerno">Termos de uso</div>
		<div class="caminho">
			<a href="index.php" class="linkcaminho">Inicial</a><img src="icones/setacaminho.jpg" />Termos de uso
		</div>

		<div class="fundocentro">
			<div class="termos">
			  Para que todos possam usufruir dos servi�os oferecidos pelo voceemfoco.com.br
			  pedimos que leiam atentamente os termos de uso:<br>
			  <br>
			  1)&nbsp;Voc&ecirc; aceita n&atilde;o colocar qualquer mensagem abusiva, 
			  obscena, vulgar, insultuosa, de &oacute;dio, amea&ccedil;adora, sexualmente 
			  tendenciosa ou qualquer outro material que possa violar qualquer lei 
			  aplic&aacute;vel nos espa�os para envio de mensagens.Os endere&ccedil;os de IP de todas as mensagens s&atilde;o 
			  registrados para ajudar a implementar essas condi&ccedil;&otilde;es. 
			  <br>
			  <br>
			  2)&nbsp;Voc&ecirc; concorda que quem faz e mant&eacute;m estas p&aacute;ginas, 
			  administradores e diretores t&ecirc;m o direito de remover e editar 
			  qualquer mensagem em qualquer momento que eles assim o decidam e seja 
			  impl&iacute;cito. <br>
			  <br>
			  3)&nbsp;Como Usu&aacute;rio voc&ecirc; aceita que qualquer informa&ccedil;&atilde;o 
			  seja guardada num Banco de Dados. Apesar dessa informa&ccedil;&atilde;o 
			  n&atilde;o ser fornecida a terceiros sem a sua autoriza&ccedil;&atilde;o, 
			  o encarregado das p&aacute;ginas, administradores ou moderadores n&atilde;o 
			  podem assumir a responsabilidade por qualquer tentativa ou ato de 'hacking', 
			  intromiss&atilde;o for&ccedil;ada e ilegal que conduza a exposi&ccedil;&atilde;o 
			  dessa informa&ccedil;&atilde;o.<br>
			  <br>
			  4)&nbsp;O Usu&aacute;rio reconhece e concorda que o voceemfoco.com.br poder&aacute; 
			  preservar o Conte&uacute;do do Usu&aacute;rio, mas tamb&eacute;m poder&aacute; 
			  divulg&aacute;-lo se desta forma determinar a Lei, ou, de boa f&eacute;, 
			  acreditar que a preserva&ccedil;&atilde;o ou a revela&ccedil;&atilde;o 
			  seja necess&aacute;ria para:<br>
			  <br>
			  &nbsp;&nbsp;(a) cumprir com algum procedimento legal;<br>
			  &nbsp;&nbsp;(b) para responder reclama&ccedil;&otilde;es de que tal 
			  Conte&uacute;do viole Direitos de terceiros; <br>
			  &nbsp;&nbsp;(c) para proteger direitos, propriedades, interesses ou 
			  manter a seguran&ccedil;a do voceemfoco.com.br, dos Usu&aacute;rios 
			  e do p&uacute;blico em geral.<br>
			  <br>
			  5)&nbsp;O Usu&aacute;rio expressamente concorda e est&aacute; ciente 
			  de que o voceemfoco.com.br n&atilde;o tem qualquer responsabilidade por quaisquer 
			  danos patrimoniais ou morais causados pelo mal uso ou incapacidade de usar os espa�os envio de mensagens.<br>
			  <br>
		  	  6)&nbsp; As not�cias publicadas na sess�o "not�cias" s�o de interia responsabilidade
		  	  do Di�rio de Sapezal, sendo assim o voceemfoco.com.br n�o possui nenhum v�nculo com o conte�do destas not�cias.<br />
			  <br />
			  7) Ao estar utilizando estes servi�os voc� indica que leu e concordou, mesmo que tacitamente, com os termos de uso contidos nesta p�gina.
			</div>
			<div class="bannercentral" style="margin-top: 15px;"><?php if(isset($banners['9'])) { print $banners['9']; } ?></div>
		</div>
	</td>
  </tr>
  <tr>
    <td colspan="2"><?php include('rodape.php'); ?></td>
  </tr>
</table>
</body>
</html>