<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new data;
include('banners.php');

$dexibe = 'none';
$denviado = 'none';
$erro = false;
$liberado = 0; //0 - requer liber��o, 1 - liberado
$dirsmiles = 'icones/smiles/';
$tempo_envio = 20;

if(!isset($post['de'])){ $post['de'] = NULL; }
if(!isset($post['para'])){ $post['para'] = NULL; }
if(!isset($post['email'])){ $post['email'] = NULL; }
if(!isset($post['recado'])){ $post['recado'] = NULL; }

if(isset($_POST['EnviaRecado'])){
	$post = array_map('Sql_injectTag',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['de'],'texto','De');
	$form->Valida($post['para'],'texto','Para');
	$form->Valida($post['email'],'email','E-mail');
	$form->Valida($post['recado'],'texto','Recado');
	$form->Tamanho($post['recado'],2,500,'Recado');

	$erro = $form->getErro();
	
	session_start();
	if(isset($_SESSION['tempoform'])){
		if($_SESSION['tempoform'] > time()){
			$tempo = $_SESSION['tempoform'] - time();
			$erro = 'Aguarde '.$tempo.' segundos para enviar novamente';
		}
	}
	
	if(!$erro){
		$ip = Ip();
		
		//smiles
		for($i = 1; $i < 40; $i++){
			$post['recado'] = str_replace(' :'.$i.': ','<img src="'.$dirsmiles.$i.'.gif" />',$post['recado']);
		}		
		
		$sql->Consulta("INSERT INTO muralr
		(estado,de,para,email,recado,data,hora,ip)
		VALUES
		('$liberado','$post[de]','$post[para]','$post[email]','$post[recado]',NOW(),NOW(),'$ip')");
			
		$denviado = 'block';
		$post = NULL;
		$_SESSION['tempoform'] = time() + $tempo_envio;
	}
}

if($erro){
	$dexibe = 'block';
}

//paginacao
//$totar = (int) $_GET['totalr'];
$totar = $sql->Totalreg("SELECT * FROM muralr WHERE estado='1'");
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);
$pag->setRegistro(6);
$pag->setTotal($totar); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('');

if($totar > 0){
	$exmax = $min + $max;
	if($totar < ($min + $max)){ $exmax = $totar; }
	$exibindo = 'Exibindo '.($min + 1).' - '.$exmax.' de um total de '.$totar; 
}else{
	$exibindo = 'Nenhum recado encontrado';
}

//listagem
$query = $sql->Consulta("SELECT * FROM muralr WHERE estado='1' ORDER BY id DESC LIMIT $min,$max");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Mural de recados  - <?=$config['nomesite'];?></title>
<link href="css/geral.css" rel="stylesheet" type="text/css" />
<link href="css/mural.css" rel="stylesheet" type="text/css" />
<link href="css/paginacao.css" rel="stylesheet" type="text/css" />
<link href="css/amplia.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/mural.js"></script>
<script type="text/javascript" src="js/geral.js"></script>
<script type="text/javascript" src="js/amplia.js"></script>
</head>

<body>
<table width="750" border="0" cellspacing="0" cellpadding="0" align="center" class="fundosite">
  <tr>
    <td height="180" colspan="2">
		<!-- topo -->
		<?php @include('topo.php'); ?>
		</td>
  </tr>
  <tr>
    <td width="180" align="center" valign="top">
		<!-- menu -->
		<?php include('menu.php'); ?>
	</td>
    <td width="560" align="center" valign="top" class="corpo">
		<!--- corpo -->
		<div class="bannercentral"><?php if(isset($banners['8'])) { print $banners['8']; } ?></div>
		<div class="caixatopointerno">Mural de recados</div>
		<div class="caminho">
			<a href="index.php" class="linkcaminho">Inicial</a><img src="icones/setacaminho.jpg" />Mural de recados
		</div>
		<!-- recado enviado -->
		<div class="msgaviso" style="display: <?=$denviado;?>; ">Seu recado foi enviado com sucesso, aguarde a libera��o pelo administrador do sistema</div>

		<div class="muralfundo">
			<!-- envio -->
			<div class="topobotaointerno">
				<div style="float: left;"><a href="javascript:ExibeForm();" title="Envie sua foto"><img src="icones/enviarecado.jpg" alt="Enviar recado" border="0" /></a></div>
				<div class="descricaobotao"></div>
			</div>
			<div style="position: relative; width: 555px; height: auto;">
				<div class="muralcaixaform" style="display: <?=$dexibe;?>;" id="formrecado">
					<div class="msgerro" style="display: <?=$dexibe;?>;"><?=$erro;?></div>
					<form action="mural.php" method="post" name="recado" id="recado" style="margin: 0px;" onsubmit="ExibeAguarde()">
						De:<br />
						<input name="de" type="text" class="muralform" maxlength="60" value="<?=$post['de'];?>" /><br />
						Para:<br />
						<input name="para" type="text" class="muralform" maxlength="60" value="<?=$post['para'];?>" /><br />
						E-mail:<br />
						<input name="email" type="text" class="muralform" maxlength="60" value="<?=$post['email'];?>" /><br />
						Recado:<br />
						<textarea name="recado" id="trecado" class="muralform" style="height: 70px;" onfocus="ContaC(this)"><?=$post['recado'];?></textarea>
						<div class="muralcaracteres">Caracteres restantes: <span id="carac">500</span></div>
						
						<a href="javascript:ExibeSmiles();" class="muralbotsmile" title="Exibir smiles">Exibir smiles</a>
						<div id="smiles" style="display: none;" class="muralfundosmiles">
						<?php
						for($i = 1; $i < 40; $i++){
							print '<img src="'.$dirsmiles.$i.'.gif" class="muralsmile" onclick="AdicionaSmile('.$i.')" />';
						}
						?>
						</div>
						
						<input type="submit" name="EnviaRecado" value="Enviar recado" class="muralbotaoform" title="Enviar recado" />
						<input type="button" name="Cancelar" value="Cancelar" class="muralbotaoform" onclick="FechaForm()" title="Cancelar envio" />
					</form>
					<div class="muralload" id="aguarde"><img src="icones/aguarde.gif" alt="Aguarde" /></div>
				</div>
			</div>
			
			<div class="fundoexibindo"><span class="exibindo"><?=$exibindo;?></span></div>
			
			<!-- listagem -->
			<?php while($linha = mysql_fetch_array($query)){ ?>
			<div class="muralbordalista">
				<div class="muralbordatitulolista">
						<div class="muralcaixacomentario">
							<div class="muraldata"><?=$data->MYsqlData($linha['data']);?> - <?=$linha['hora'];?></div>
						</div>
						
						<span class="muraldescricaotitulo">De:</span> <?=$linha['de'];?><br />
						<span class="muraldescricaotitulo">Para:</span> <?=$linha['para'];?><br />
						<div class="muralemail">
							<a href="javascript:MostraEmail(<?=$linha['id'];?>)" title="Ver e-mail"><img src="icones/email.jpg" alt="Ver e-email" border="0" /></a>&nbsp;<span id="e<?=$linha['id'];?>" style="display: none;"><?=$linha['email'];?></span>
						</div>
				</div>
				<?=nl2br($linha['recado']);?>&nbsp;
			</div>
			<?php } ?>
			
			<!-- paginacao -->
			<div class="fundopaginacao">
				<?=$paginas;?>
			</div>
			
			<div class="bannercentral" style="margin-top: 15px;"><?php if(isset($banners['9'])) { print $banners['9']; } ?></div>
		</div>
	</td>
  </tr>
  <tr>
    <td colspan="2"><?php include('rodape.php'); ?></td>
  </tr>
</table>
</body>
</html>
