<div class="restante">Aguarde as miniaturas serem carregadas: <span id="restante">(<?=sizeof($fotos);?> faltando )</span></div>

<!-- fundo -->
<div class="fundoopaco" id="fundoopaco" onclick="FechaFoto()" title="Clique para fechar"></div>

<!-- menu modo -->
<div class="fundomodo" id="barramodo">
	<a href="javascript:AlteraModo(2);" class="linkmodo" id="lkmodo2">Ver todas as miniaturas</a>
	<a href="javascript:AlteraModo(1);" class="linkmodoover" id="lkmodo1">Ver miniaturas em lista</a>
</div>

<div style="width: auto; padding-left: 20px; text-align: left; padding-right: 20px;">

<!-- visualizar fotos -->
<div class="fundoexibe" id="fundoexibe">
	<div class="fundonfotos" id="exibindo">Exibindo foto 1 de <?=sizeof($fotos);?> (<?php if(isset($fotos[0])) { print $fotos[0]; } ?>)</div>
	
	<!-- exibe foto-->
	<div class="exibefoto">
		<div class="exibe" id="exibe1"><img src="<?=$diretorio.$fotos[0];?>" border="0"  alt="<?php if(isset($fotos[0])) { print $fotos[0]; } ?>" /></div>
		<div class="exibe" id="exibe2"></div>
	</div>
	
	<!-- menu -->
	<div class="fundomenu">
		<div class="fundoload" id="load"></div>
		<a href="javascript:Proxima(1);" title="Pr�xima foto (Atalho: Seta para a direita)">Pr�xima</a>
		<a href="javascript:Proxima(0);" title="Foto anterior (Atalho: Seta para a esquerda)">Anterior</a>
		<a href="javascript:IniciaSlide();" id="botslide" title="Apresenta��o em slides (Atalho: s)">Iniciar slide</a>
		<a href="javascript:FechaFoto();" id="botfecha" title="Fechar foto (Atalho: Esc)">Fechar foto</a>
	</div>
</div>

<!-- miniaturas -->
<div class="fundominiatura" id="miniaturas">
	<?php 
		for($i = 0; $i < sizeof($fotos); $i++){ 
		$foto = $i + 1;
	?>
	<a href="javascript:Amplia(<?=$foto;?>);" title="Ampliar"><img src="<?=$diretorio.'mini/'.$fotos[$i];?>" border="0" height="75" onload="Restante()" id="foto<?=$foto;?>" alt="Ampliar foto (<?=$fotos[$i];?>)" /></a>
	<?php } ?>
</div>

<!-- atalhos -->

</div>