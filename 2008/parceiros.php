<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new data;
include('banners.php');

$dirparceiro = 'arquivos/parceiros/';

//paginacao
$totalr = $sql->Totalreg("SELECT * FROM parceiros WHERE estado='1'");
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);
$pag->setRegistro(5);
$pag->setTotal($totalr); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('');
$pagatual = $pag->getAtual();

if($totalr > 0){
	//$exibindo = 'Clique sobre o nome para mais detalhes'; 
	$exmax = $min + $max;
	if($totalr < ($min + $max)){ $exmax = $totalr; }
	$exibindo = 'Exibindo '.($min + 1).' - '.$exmax.' de um total de '.$totalr; 
}else{
	$exibindo = 'Nenhum cadastro encontrado';
}

//listagem
$query = $sql->Consulta("SELECT parceiros.*,estados.uf
FROM parceiros 
LEFT JOIN estados ON estados.id=parceiros.id_estado
WHERE parceiros.estado='1' ORDER BY parceiros.nome ASC LIMIT $min,$max");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Parceiros  - <?=$config['nomesite'];?></title>
<link href="css/geral.css" rel="stylesheet" type="text/css" />
<link href="css/parceiros.css" rel="stylesheet" type="text/css" />
<link href="css/paginacao.css" rel="stylesheet" type="text/css" />
<link href="css/amplia.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/geral.js"></script>
<script type="text/javascript" src="js/amplia.js"></script>
</head>

<body>
<table width="750" border="0" cellspacing="0" cellpadding="0" align="center" class="fundosite">
  <tr>
    <td height="180" colspan="2">
		<!-- topo -->
		<?php @include('topo.php'); ?>
		</td>
  </tr>
  <tr>
    <td width="180" align="center" valign="top">
		<!-- menu -->
		<?php include('menu.php'); ?>
	</td>
    <td width="560" align="center" valign="top" class="corpo">
		<!--- corpo -->
		<div class="bannercentral"><?php if(isset($banners['8'])) { print $banners['8']; } ?></div>
		<div class="caixatopointerno">Nossos parceiros</div>
		<div class="caminho">
			<a href="index.php" class="linkcaminho">Inicial</a><img src="icones/setacaminho.jpg" />Parceiros
		</div>

		<div class="fundocentro">
			<div class="fundoexibindo" style="margin-top: 6px;"><?=$exibindo;?></div>
			
			<!-- listagem -->
			<?php 
			while($linha = mysql_fetch_array($query)){ 
				if($linha['foto']){
					$linha['foto'] = '<a href="javascript:Amplia(\''.$dirparceiro.$linha['foto'].'\');" title="Ampliar foto" class="bordaimg"><img src="'.$dirparceiro.'mini/'.$linha['foto'].'" /></a>';
				}
				if($linha['endereco']){ $linha['endereco'] = '<span class="parceirodescricaotitulo">Endere�o:&nbsp;</span> '.$linha['endereco'].'<br />'; }
				if($linha['telefone']){ $linha['telefone'] = '<span class="parceirodescricaotitulo">Telefone:&nbsp;</span> '.FormataTel($linha['telefone']).'<br />'; }
				if($linha['email']){ $linha['email'] = '<span class="parceirodescricaotitulo">E-mail:&nbsp;</span> '.$linha['email'].'<br />'; }
				if($linha['site']){ $linha['site'] = '<span class="parceirodescricaotitulo">Site:&nbsp;</span ><a href="'.$linha['site'].'" target="_blank">'.$linha['site'].'</a><br />'; }
			?>
			<div class="parceirobordalista">
				<a href="javascript:Mostra('p<?=$linha['id'];?>');" class="parceironome" title="Ver / Ocultar detalhes"><?=$linha['nome'];?></a>
				<div style="display: block;" id="p<?=$linha['id'];?>" class="parceirooculto">
				<?=$linha['foto'];?>
				<div class="parceirobordatitulolista">
						<span class="parceirodescricaotitulo">Cidade:&nbsp;</span> <?=$linha['cidade'];?> - <?=$linha['uf'];?><br />
						<?=$linha['endereco'];?>
						<?=$linha['telefone'];?>
						<?=$linha['email'];?>
						<?=$linha['site'];?>
				</div>
				<?=nl2br($linha['informacoes']);?>&nbsp;
				</div>
			</div>
			<?php } ?>
			
			<!-- paginacao -->
			<div class="fundopaginacao">
				<?=$paginas;?>
			</div>
			
			<div class="bannercentral" style="margin-top: 15px;"><?php if(isset($banners['9'])) { print $banners['9']; } ?></div>
		</div>
	</td>
  </tr>
  <tr>
    <td colspan="2"><?php include('rodape.php'); ?></td>
  </tr>
</table>
</body>
</html>