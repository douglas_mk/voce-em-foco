<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new data;
include('banners.php');

$dirfoto = 'arquivos/galeranet/';

//paginacao
$mes = date('m');
$totar = $sql->Totalreg("SELECT * FROM galeranet WHERE estado='1' AND EXTRACT(MONTH FROM nascimento)='$mes'");
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);
$pag->setRegistro(6);
$pag->setTotal($totar); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('');
$pagatual = $pag->getAtual();

if($totar > 0){
	$exmax = $min + $max;
	if($totar < ($min + $max)){ $exmax = $totar; }
	$exibindo = 'Exibindo '.($min + 1).' - '.$exmax.' de um total de '.$totar; 
}else{
	$exibindo = 'Nenhum registro encontrado';
}

//listagem
$query = $sql->Consulta("SELECT galeranet.*,estados.uf,EXTRACT(DAY FROM galeranet.nascimento) AS dia
FROM galeranet 
LEFT JOIN estados ON estados.id=galeranet.id_estado
WHERE galeranet.estado='1' AND EXTRACT(MONTH FROM galeranet.nascimento)='$mes' ORDER BY galeranet.nascimento ASC LIMIT $min,$max");

$mesex = $data->MesExtenso($mes);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Galera net  - Aniversariantes de <?=$mesex;?> - <?=$config['nomesite'];?></title>
<link href="css/geral.css" rel="stylesheet" type="text/css" />
<link href="css/galeranet.css" rel="stylesheet" type="text/css" />
<link href="css/paginacao.css" rel="stylesheet" type="text/css" />
<link href="css/amplia.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/galeranet.js"></script>
<script type="text/javascript" src="js/geral.js"></script>
<script type="text/javascript" src="js/amplia.js"></script>
</head>

<body>
<table width="750" border="0" cellspacing="0" cellpadding="0" align="center" class="fundosite">
  <tr>
    <td height="180" colspan="2">
		<!-- topo -->
		<?php @include('topo.php'); ?>
	</td>
  </tr>
  <tr>
    <td width="180" align="center" valign="top">
		<!-- menu -->
		<?php include('menu.php'); ?>
	</td>
    <td width="560" align="center" valign="top" class="corpo">
		<!--- corpo -->
		<div class="bannercentral"><?php if(isset($banners['8'])) { print $banners['8']; } ?></div>
		<div class="caixatopointerno">Galera net - Aniversariantes de <?=$mesex;?> </div>
		<div class="caminho">
			<a href="index.php" class="linkcaminho">Inicial</a><img src="icones/setacaminho.jpg" />
			<a href="galeranet.php" class="linkcaminho">Galera net</a><img src="icones/setacaminho.jpg" />
			Aniversariantes de <?=$mesex;?>
		</div>

		<div class="fundocentro">
			<!-- envio -->
			<div class="topobotaointerno">
				<div style="float: left;"><a href="galeranetCadastra.php" title="Cadastre-se"><img src="icones/galeracadastra.jpg" alt="Cadastre seu anivers�rio" border="0" /></a></div>
				<div class="descricaobotao"></div>
			</div>
			
			<div class="fundoexibindo"><span class="exibindo"><?=$exibindo;?></span></div>
			
			<!-- listagem -->
			<table width="550" border="0" cellspacing="0" cellpadding="5">
			<?php 
			while($linha = mysql_fetch_array($query)){ 
			if($linha['foto']){
				$linha['foto'] = '<a href="javascript:Amplia(\''.$dirfoto.$linha['foto'].'\');" title="Ampliar foto"><img src="'.$dirfoto.'mini/'.$linha['foto'].'" class="bordaminiatura" style="float: none; margin: 10px;" /></a>';
			}else{
				$linha['foto'] = '<img src="icones/fotogaleranet.jpg" class="bordaminiatura" style="float: none; margin: 10px;" alt="Foto n�o cadastrada" />';
			}
			if($linha['msn']){ $linha['msn'] = '<div class="galeramsn" title="Msn">'.$linha['msn'].'</div>'; }
			if($linha['siter']){ $linha['siter'] = '<div class="galeraorkut"><a href="'.$linha['siter'].'" target="_blank"><img src="icones/gorkut.gif" alt="Ver perfil do orkut" border="0" /></a></div>'; }
			?>
			  <tr>
				<td width="90" class="bordafundofoto" align="center"><?=$linha['foto'];?></td>
				<td align="left" valign="top" class="bordafundodados">
					<div class="galeradianiver" title="Data de anivers�rio de <?=$linha['nome'];?>"> Dia <?=$linha['dia'];?></div>
					<div class="galeranome"><?=$linha['nome'];?></div>
					<div class="galeracidade"><?=$linha['cidade'];?> - <?=$linha['uf'];?></div>
					<div class="galeraemail" title="E-mail"><?=$linha['email'];?></div>
					<?=$linha['msn'];?>
					<?=$linha['siter'];?>
				</td>
			  </tr>
			  <tr>
				<td height="15" colspan="2"></td>
			  </tr>
			<?php } ?>
		  </table>
			
			<!-- paginacao -->
			<div class="fundopaginacao">
				<?=$paginas;?>
			</div>
			
			<div class="bannercentral" style="margin-top: 15px;"><?php if(isset($banners['9'])) { print $banners['9']; } ?></div>
		</div>
	</td>
  </tr>
  <tr>
    <td colspan="2"><?php include('rodape.php'); ?></td>
  </tr>
</table>
</body>
</html>