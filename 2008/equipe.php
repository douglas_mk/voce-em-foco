<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new data;
include('banners.php');

$direquipe = 'arquivos/equipe/';

//paginacao
$totalr = $sql->Totalreg("SELECT * FROM equipe WHERE estado='1'");
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);
$pag->setRegistro(5);
$pag->setTotal($totalr); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('');
$pagatual = $pag->getAtual();

if($totalr > 0){
	//$exibindo = 'Clique sobre o nome para mais detalhes'; 
	$exmax = $min + $max;
	if($totalr < ($min + $max)){ $exmax = $totalr; }
	$exibindo = 'Exibindo '.($min + 1).' - '.$exmax.' de um total de '.$totalr; 
}else{
	$exibindo = 'Nenhum cadastro encontrado';
}

//listagem
$query = $sql->Consulta("SELECT equipe.*
FROM equipe
WHERE equipe.estado='1' ORDER BY equipe.nome ASC LIMIT $min,$max");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Equipe  - <?=$config['nomesite'];?></title>
<link href="css/geral.css" rel="stylesheet" type="text/css" />
<link href="css/equipe.css" rel="stylesheet" type="text/css" />
<link href="css/paginacao.css" rel="stylesheet" type="text/css" />
<link href="css/amplia.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/geral.js"></script>
<script type="text/javascript" src="js/amplia.js"></script>
</head>

<body>
<table width="750" border="0" cellspacing="0" cellpadding="0" align="center" class="fundosite">
  <tr>
    <td height="180" colspan="2">
		<!-- topo -->
		<?php @include('topo.php'); ?>
		</td>
  </tr>
  <tr>
    <td width="180" align="center" valign="top">
		<!-- menu -->
		<?php include('menu.php'); ?>
	</td>
    <td width="560" align="center" valign="top" class="corpo">
		<!--- corpo -->
		<div class="bannercentral"><?php if(isset($banners['8'])) { print $banners['8']; } ?></div>
		<div class="caixatopointerno">Equipe do Voc� em Foco</div>
		<div class="caminho">
			<a href="index.php" class="linkcaminho">Inicial</a><img src="icones/setacaminho.jpg" />Equipe
		</div>

		<div class="fundocentro">
			<div class="fundoexibindo" style="margin-top: 6px;"><?=$exibindo;?></div>
			
			<!-- listagem -->
			<?php 
			while($linha = mysql_fetch_array($query)){ 
				if($linha['foto']){
					$linha['foto'] = '<a href="javascript:Amplia(\''.$direquipe.$linha['foto'].'\');" title="Ampliar foto" class="bordaimg"><img src="'.$direquipe.'mini/'.$linha['foto'].'" style="float: right;" /></a>';
				}
				if($linha['telefonec']){ $linha['telefonec'] = '<span class="equipedescricaotitulo">Telefone comercial:&nbsp;</span> '.FormataTel($linha['telefonec']).'<br />'; }
				if($linha['telefoner']){ $linha['telefoner'] = '<span class="equipedescricaotitulo">Telefone residencial:&nbsp;</span> '.FormataTel($linha['telefoner']).'<br />'; }
				if($linha['celular']){ $linha['celular'] = '<span class="equipedescricaotitulo">Telefone celular:&nbsp;</span> '.FormataTel($linha['celular']).'<br />'; }
				if($linha['email']){ $linha['email'] = '<span class="equipedescricaotitulo">E-mail:&nbsp;</span> '.$linha['email'].'<br />'; }
				if($linha['msn']){ $linha['msn'] = '<span class="equipedescricaotitulo">Msn:&nbsp;</span> '.$linha['msn'].'<br />'; }
				if($linha['cargo']){ $linha['cargo'] = '<div class="equipecargo">'.$linha['cargo'].'</div>'; }
			?>
			<div class="equipebordalista">	
				<div class="equipenome"><?=$linha['nome'];?></div>
				<?=$linha['foto'];?>
				<div class="equipeoculto">
					<div class="equipebordatitulolista">
							<?=$linha['cargo'];?>
							<?=$linha['telefonec'];?>
							<?=$linha['telefoner'];?>
							<?=$linha['celular'];?>
							<?=$linha['email'];?>
							<?=$linha['msn'];?>
					</div>
				<?=nl2br($linha['informacoes']);?>&nbsp;
				</div>
			</div>
			<?php } ?>
			
			<!-- paginacao -->
			<div class="fundopaginacao">
				<?=$paginas;?>
			</div>
			
			<div class="bannercentral" style="margin-top: 15px;"><?php if(isset($banners['9'])) { print $banners['9']; } ?></div>
		</div>
	</td>
  </tr>
  <tr>
    <td colspan="2"><?php include('rodape.php'); ?></td>
  </tr>
</table>
</body>
</html>