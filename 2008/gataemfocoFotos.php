<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new data;
include('banners_gata.php');

$id_gata = (int) $_GET['id'];

$dirfoto = 'arquivos/emfoco/';

//dados
$query = $sql->Consulta("SELECT gataemfoco.*,estados.uf,EXTRACT(YEAR FROM nascimento) AS ano,EXTRACT(MONTH FROM nascimento) AS mes,EXTRACT(DAY FROM nascimento) AS dia
FROM gataemfoco 
LEFT JOIN estados ON estados.id=gataemfoco.id_estado
WHERE gataemfoco.id='$id_gata' AND gataemfoco.estado='1' LIMIT 1");
$linha = mysql_fetch_array($query);
VerificaVar($linha);

if($linha['foto_capa']){
	$linha['foto_capa'] = '<img src="'.$dirfoto.'foto_capa/mini/'.$linha['foto_capa'].'" border="0" alt="'.$linha['nome'].'" />';
}

//fotos desta gata
$diretorio = 'arquivos/emfoco/'.$linha['pasta'].'/';
$fotos = array();

$aberto = @opendir($diretorio.'mini/');//abre o diretorio 
while($arq = @readdir($aberto)) {//le o diretorio
	if(($arq != '.') && ($arq != '..')) {//desconsidera subdiretorios
		$ext = explode('.',$arq); //verifica extensão da imagens
		if(($ext[1] == 'jpg') || ($ext[1] == 'JPG')){
			$fotos[] = $arq;
		}
	}
}
@closedir($aberto);
//ordena o vetor
@array_multisort($fotos);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Gata em foco - <?=$linha['nome'];?> - <?=$config['nomesite'];?></title>
<link href="css/geral.css" rel="stylesheet" type="text/css" />
<link href="css/gataemfoco.css" rel="stylesheet" type="text/css" />
<link href="css/paginacao.css" rel="stylesheet" type="text/css" />
<link href="css/galeriav3_1.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/geral.js"></script>
<script type="text/javascript">
	var totalfotos = '<?=sizeof($fotos);?>';
	var dir = '';
	var pasta = '<?=$diretorio;?>';
</script>
<script type="text/javascript" src="js/galeriav3_1.js"></script>
</head>

<body>
<table width="750" border="0" cellspacing="0" cellpadding="0" align="center" class="fundosite">
  <tr>
    <td height="180" colspan="2">
		<!-- topo -->
		<?php @include('topo.php'); ?>
	</td>
  </tr>
  <tr>
    <td width="180" align="center" valign="top">
		<!-- menu -->
		<?php include('menu.php'); ?>
	</td>
    <td width="560" align="center" valign="top" class="corpo">
		<!--- corpo -->
		<div class="bannercentral"><?php if(isset($banners['8'])) { print $banners['8']; } ?></div>
		<div class="caixatopointerno">Gata em foco - Visualizar fotos </div>
		<div class="caminho">
			<a href="index.php" class="linkcaminho">Inicial</a><img src="icones/setacaminho.jpg" />
			<a href="emfoco.php" class="linkcaminho">Gata/Gato em foco</a><img src="icones/setacaminho.jpg" />
			Visualizar fotos </div>

		<div class="fundocentro">
			<div class="topobotaointerno" style="height: 22px; margin-top: 0px;">
				<div class="descricaobotao"><a href="gataemfoco.php">Ver todas as gatas em foco</a></div>
			</div>
			
			<div class="nomegata">
				<div class="bordafotogata">
					<?=$linha['foto_capa'];?>
				</div>
				<div class="bordadescricao">Fotos</div>
				<div class="bordadescricaosub">Fotos</div>
				<?=$linha['nome'];?>
			</div>
			
			<div class="barraperfil">
				<a href="gataemfocoPerfil.php?id=<?=$id_gata;?>" class="linkbarradesat" title="Veja o perfil de <?=$linha['nome'];?>">Perfil</a>
				<a href="gataemfocoFotos.php?id=<?=$id_gata;?>" class="linkbarra" title="Veja as fotos de <?=$linha['nome'];?>">Fotos</a>
			</div>
			
			<div class="qtdfotos">Fotos por: <?=$linha['fotografo'];?></div>
			<div class="fotosgataborda">
				<?php include('galeriav3_1.php'); ?>	
			</div>
			
			<div class="bannercentral" style="margin-top: 15px;"><?php if(isset($banners['9'])) { print $banners['9']; } ?></div>
			<!-- atalhos -->
			<div class="fundoatalhos">
				<div>Teclas de atalho</div>
				<span>Seta para a direita:</span> Exibir pr&oacute;xima foto<br />
				<span>Seta para a esquerda:</span> Exibir foto anterior<br />
				<span>S:</span> Iniciar / Parar apresentação em slides<br />
			</div>
		</div>
	</td>
  </tr>
  <tr>
    <td colspan="2"><?php include('rodape.php'); ?></td>
  </tr>
</table>
</body>
</html>