<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new data;
include('banners.php');

$dirsmiles = 'icones/smiles/';
$dirfoto = 'arquivos/galeranet/';
$tempo_envio = 20;

if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);

//busca
if(!isset($_GET['busca'])){ $_GET['busca'] = NULL; }
$busca = Sql_inject($_GET['busca']);

if(!isset($_GET['tipo'])){ $_GET['tipo'] = NULL; }
$tipo = (int) $_GET['tipo'];
$titulob = 'Todos os cadastros';
$and = NULL;

if($busca){
	if($tipo == 1){
		$and = " AND (galeranet.nome LIKE '%$busca%' OR galeranet.cidade LIKE '%$busca%')";
		$titulob = 'Resultados da busca por: '.$busca;
	}else{
		$and = " AND galeranet.nome LIKE '$busca%'";
		$titulob = 'Busca por cadastros iniciados com a letra '.$busca;
	}
	$pag->AddLink('busca',$busca);
	$pag->AddLink('tipo',$tipo);
}

//paginacao
$totalr = $sql->Totalreg("SELECT * FROM galeranet WHERE estado='1' $and");
$pag->setRegistro(6);
$pag->setTotal($totalr); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('');
$pagatual = $pag->getAtual();

if($totalr > 0){
	$exmax = $min + $max;
	if($totalr < ($min + $max)){ $exmax = $totalr; }
	$exibindo = '&nbsp;&nbsp;I&nbsp;&nbsp;Exibindo '.($min + 1).' - '.$exmax.' de um total de '.$totalr; 
}else{
	$exibindo = '&nbsp;&nbsp;I&nbsp;&nbsp;Nenhum cadastro encontrado';
}

//listagem
$query = $sql->Consulta("SELECT galeranet.* ,estados.uf
FROM galeranet 
LEFT JOIN estados ON estados.id=galeranet.id_estado
WHERE galeranet.estado='1' $and ORDER BY galeranet.id DESC LIMIT $min,$max");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Galera net  - <?=$config['nomesite'];?></title>
<link href="css/geral.css" rel="stylesheet" type="text/css" />
<link href="css/galeranet.css" rel="stylesheet" type="text/css" />
<link href="css/paginacao.css" rel="stylesheet" type="text/css" />
<link href="css/amplia.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/galeranet.js"></script>
<script type="text/javascript" src="js/geral.js"></script>
<script type="text/javascript" src="js/amplia.js"></script>
</head>

<body>
<table width="750" border="0" cellspacing="0" cellpadding="0" align="center" class="fundosite">
  <tr>
    <td height="180" colspan="2">
		<!-- topo -->
		<?php @include('topo.php'); ?>
	</td>
  </tr>
  <tr>
    <td width="180" align="center" valign="top">
		<!-- menu -->
		<?php include('menu.php'); ?>
	</td>
    <td width="560" align="center" valign="top" class="corpo">
		<!--- corpo -->
		<div class="bannercentral"><?php if(isset($banners['8'])) { print $banners['8']; } ?></div>
		<div class="caixatopointerno">Galera net - Buscar </div>
		<div class="caminho">
			<a href="index.php" class="linkcaminho">Inicial</a><img src="icones/setacaminho.jpg" />
			<a href="galeranet.php" class="linkcaminho">Galera net</a><img src="icones/setacaminho.jpg" />Buscar
		</div>

		<div class="fundocentro">
			<!-- envio -->
			<div class="topobotaointerno" style="height: 22px;">
				<div style="float: left; margin-top: 5px;">
				<form action="galeraBusca.php" method="get" name="busca" id="busca" style="margin: 0px; display: block;">
					<input type="text" name="busca" maxlength="25" class="formbusca" value="<?=$busca;?>" title="Informe um termo para buscar" />
					<input type="submit" value="Buscar" name="BuscaT" class="botaobusca" style="width: 100px;"> 
					<input type="hidden" name="tipo" value="1" />
				</form>
				</div>
				<div class="descricaobotao"><a href="galeranet.php" title="Exibir todos os cadastros">Exibir todos os cadastros</a></div>
			</div>

			<div class="fundoexibindo" style="margin-bottom: 5px;">Filtrar por letra:&nbsp;&nbsp;&nbsp; 
			<span class="exibindo">
				<a href="galeraBusca.php?busca=A&tipo=0">A</a> . 
				<a href="galeraBusca.php?busca=B&tipo=0">B</a> . 
				<a href="galeraBusca.php?busca=C&tipo=0">C</a> . 
				<a href="galeraBusca.php?busca=D&tipo=0">D</a> . 
				<a href="galeraBusca.php?busca=E&tipo=0">E</a> . 
				<a href="galeraBusca.php?busca=F&tipo=0">F</a> . 
				<a href="galeraBusca.php?busca=G&tipo=0">G</a> . 
				<a href="galeraBusca.php?busca=H&tipo=0">H</a> . 
				<a href="galeraBusca.php?busca=I&tipo=0">I</a> . 
				<a href="galeraBusca.php?busca=J&tipo=0">J</a> . 
				<a href="galeraBusca.php?busca=K&tipo=0">K</a> . 
				<a href="galeraBusca.php?busca=L&tipo=0">L</a> . 
				<a href="galeraBusca.php?busca=M&tipo=0">M</a> . 
				<a href="galeraBusca.php?busca=N&tipo=0">N</a> . 
				<a href="galeraBusca.php?busca=O&tipo=0">O</a> . 
				<a href="galeraBusca.php?busca=P&tipo=0">P</a> . 
				<a href="galeraBusca.php?busca=Q&tipo=0">Q</a> . 
				<a href="galeraBusca.php?busca=R&tipo=0">R</a> . 
				<a href="galeraBusca.php?busca=S&tipo=0">S</a> . 
				<a href="galeraBusca.php?busca=T&tipo=0">T</a> . 
				<a href="galeraBusca.php?busca=U&tipo=0">U</a> . 
				<a href="galeraBusca.php?busca=V&tipo=0">V</a> . 
				<a href="galeraBusca.php?busca=W&tipo=0">W</a> . 
				<a href="galeraBusca.php?busca=X&tipo=0">X</a> . 
				<a href="galeraBusca.php?busca=Y&tipo=0">Y</a> . 
				<a href="galeraBusca.php?busca=Z&tipo=0">Z</a>
			</span>
			</div>
			<div class="fundoexibindo"><?=$titulob;?><span class="exibindo"><?=$exibindo;?></span></div>
			
			<!-- listagem -->
			<table width="550" border="0" cellspacing="0" cellpadding="5">
			<?php 
			while($linha = mysql_fetch_array($query)){ 
			if($linha['foto']){
				$linha['foto'] = '<a href="javascript:Amplia(\''.$dirfoto.$linha['foto'].'\');" title="Ampliar foto"><img src="'.$dirfoto.'mini/'.$linha['foto'].'" class="bordaminiatura" style="float: none; margin: 10px;" /></a>';
			}else{
				$linha['foto'] = '<img src="icones/fotogaleranet.jpg" class="bordaminiatura" style="float: none; margin: 10px;" alt="Foto n�o informada" />';
			}
			if($linha['msn']){ $linha['msn'] = '<div class="galeramsn" title="Msn">'.$linha['msn'].'</div>'; }
			?>
			  <tr>
				<td width="90" class="bordafundofoto" align="center"><?=$linha['foto'];?></td>
				<td align="left" valign="top" class="bordafundodados">
					<div class="galeranome"><?=$linha['nome'];?></div>
					<div class="galeracidade"><?=$linha['cidade'];?> - <?=$linha['uf'];?></div>
					<div class="galeraemail" title="E-mail"><?=$linha['email'];?></div>
					<?=$linha['msn'];?>
				</td>
			  </tr>
			  <tr>
				<td height="15" colspan="2"></td>
			  </tr>
			<?php } ?>
		  </table>
			
			<!-- paginacao -->
			<div class="fundopaginacao">
				<?=$paginas;?>
			</div>
			
			<div class="bannercentral" style="margin-top: 15px;"><?php if(isset($banners['9'])) { print $banners['9']; } ?></div>
		</div>
	</td>
  </tr>
  <tr>
    <td colspan="2"><?php include('rodape.php'); ?></td>
  </tr>
</table>
</body>
</html>