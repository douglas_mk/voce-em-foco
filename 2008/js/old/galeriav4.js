var idAtual = 0;
var exibe = 0;
var zIndex = 2;
var liberaProxima = true;
var timeLoad = false;

var slide = false;
var esperaTmp = false;
var esperaImg = false;
var tmpSlide;

function StatusMin(img){
	document.getElementById('minst').innerHTML = img.getAttribute('alt'); 	
	img.onmouseout = function(){
		document.getElementById('minst').innerHTML = '';	
	}
}

function StatusFoto(img){
	document.getElementById('fotost').innerHTML = img.getAttribute('alt'); 	
	img.onmouseout = function(){
		document.getElementById('fotost').innerHTML = '';	
	}
}


function ProximaMin(tipo){	
	if(parseInt(tipo) == 1){
		fotoAtual = fotoAtual + 10;
		if(fotoAtual > totalFotos){
			fotoAtual = 0;	
		}
	}else{
		fotoAtual = fotoAtual - 10;
		if(fotoAtual < 0){
			fotoAtual = 0;	
		}
	}

	 var limite = fotoAtual + 10;
	 if(limite > totalFotos){
		limite = totalFotos;		 
	 }
	
	document.getElementById('miniaturas').innerHTML = '';
	for(var i = fotoAtual; i < limite; i++){
		var foto = diretorio+pastaMiniatura+fotos.fotos[i].img;
		document.getElementById('miniaturas').innerHTML += '<a href="javascript:Amplia(' +i+ ')"><img src="' +foto+ '" border="0" alt="Ampliar foto" /></a>';
	}
	
	document.getElementById('totalmin').innerHTML = 'Miniaturas ' +(fotoAtual + 1)+ '-' +limite+ ' de ' +totalFotos;
}

function Amplia(id){
	ParaSlide();
	if(liberaProxima == true){
		var foto = diretorio+fotos.fotos[id].img;
		idAtual = id;
		
		if(exibe == 0){
			//exibe = 1;	
			document.getElementById('exibe1').innerHTML = '<img src="' +foto+ '" border="0" onload="ExibeFoto()" onerror="ExibeFoto()" />';
		}else{
			//exibe = 0;	
			document.getElementById('exibe0').innerHTML = '<img src="' +foto+ '" border="0" onload="ExibeFoto()" onerror="ExibeFoto()" />';
		}
		
		timeLoad = setTimeout('ExibeLoad()',20);
	}
}

function ExibeFoto(){
	zIndex++;
	clearTimeout(timeLoad);
	liberaProxima = false;
	
	if(exibe == 1){
		exibe = 0;
		setOpacidade(0,'exibe0');	
		document.getElementById('exibe0').style.zIndex = zIndex;
		setFade('exibe0',0,100,500);
	}else{
		exibe = 1;
		setOpacidade(0,'exibe1');	
		document.getElementById('exibe1').style.zIndex = zIndex;
		setFade('exibe1',0,100,500);
	}

	document.getElementById('fotoatual').innerHTML = idAtual + 1;
	document.getElementById('load').style.display = 'none';
	setTimeout('LiberaProxima()',510);
}

function ExibeLoad(){
	document.getElementById('load').style.display = 'block';
}

function LiberaProxima(){
	liberaProxima = true;	
	document.getElementById('load').style.display = 'none';
	Slide();
}

function ProximaFoto(tipo){
	if(liberaProxima == true){
		if(parseInt(tipo) == 1){
			idAtual = idAtual + 1;
			if(idAtual >= totalFotos){
				idAtual = 0;	
				ProximaMin(1);
			}
			if(idAtual == (fotoAtual + 10)){
				 ProximaMin(1);	
			}
		}else{
			idAtual = idAtual - 1;
			if(idAtual < 0){
				idAtual = 0;	
			}
			if(idAtual == (fotoAtual - 1)){
				 ProximaMin(0);	
			}
		}
		Amplia(idAtual);
	}
}

//slide -----------------------------------------
function IniciaSlide(){
	if(slide == false){
		slide = true;
		Slide();
		document.getElementById('btslide').alt = 'Parar slide';
		document.getElementById('btslide').src = 'slidestop.jpg';
	}else{
		ParaSlide();
	}
}

function Slide(){
	if(slide == true){
		esperaTmp = false;
		esperaImg = false;
		
		idAtual = idAtual + 1;
		if(idAtual >= totalFotos){
			idAtual = 0;
			ParaSlide();
		}
		
		var foto = diretorio+fotos.fotos[idAtual].img;
			
		if(exibe == 0){
			//exibe = 1;	
			document.getElementById('exibe1').innerHTML = '<img src="' +foto+ '" border="0" onload="LiberaImg()" onerror="LiberaImg()" />';
		}else{
			//exibe = 0;	
			document.getElementById('exibe0').innerHTML = '<img src="' +foto+ '" border="0" onload="LiberaImg()" onerror="LiberaImg()" />';
		}	
		tmpSlide = setTimeout('LiberaSlide()',4000);
	}
}

function LiberaImg(){
	esperaImg = true;
	if(esperaTmp == true){
		ExibeFoto();	
	}	
}

function LiberaSlide(){
	esperaTmp = true;
	if(esperaImg == true){
		ExibeFoto();
	}
}

function ParaSlide(){
	slide = false;	
	clearTimeout(tmpSlide);	
	document.getElementById('btslide').alt = 'Iniciar slide';
	document.getElementById('btslide').src = 'slide.jpg';
}

//---------------------------------------

function getTecla(e){
	var tecla = 0;
	
	if(window.event){
		tecla = window.event.keyCode;
	}else if(e.keyCode){
		tecla = e.keyCode;
	}
	
	switch(tecla){
		case 37: ProximaFoto(0); break;	
		case 39: ProximaFoto(1); break;	
	}
}

function setFade(id,inicio,fim,tempo){
	
 	var velocidade = Math.round(tempo / 100); 
    var timer = 0; 	
	
	if(inicio > fim){
        for(i = inicio; i >= fim; i--) { 
            setTimeout('setOpacidade(' + i + ',\'' + id + '\')',(timer * velocidade)); 
            timer++; 
        } 
	}
	if(fim > inicio){
        for(i = inicio; i <= fim; i++) { 
            setTimeout('setOpacidade(' + i + ',\'' + id + '\')',(timer * velocidade)); 
            timer++; 
        } 
	}
}

function setOpacidade(opacidade,id) { 
    var obj = document.getElementById(id).style; 
    obj.opacity = (opacidade / 100); 
    obj.MozOpacity = (opacidade / 100); 
    obj.KhtmlOpacity = (opacidade / 100); 
    obj.filter = 'alpha(opacity=' + opacidade + ')'; 
} 

document.onkeydown = getTecla;