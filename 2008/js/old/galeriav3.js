var idatual = 1;
var exibeatual = 1;
var tamy = 0;
var tamy = 0;
var topo = 0;
var zindex = 10;
var liberaprox = true;
var sa = 0;
var sc = 0;
var slide = false;
var esperatmp = false;
var esperaimg = false;
var tmpslide;
var modo = 1;

function TamanhoTela(){
		tamy = document.documentElement.clientHeight;
		tamx = document.documentElement.clientWidth;
		topo = document.documentElement.scrollTop;
		
		if (window.opera){ 
			tamy = document.body.clientHeight;
			tamx = document.body.clientWidth;
			topo = document.body.scrollTop;
		}
}

function Amplia(id){
	if(liberaprox == true){
		id = parseInt(id);	
		
		if(modo == 1){		
			sa = document.getElementById('miniaturas').scrollTop; 
			sc = ((id - 1) * 89) - 200;
			//Scroll(); 
			//PosScroll(id);
		}else{
			Reposiciona();
			document.getElementById('fundoopaco').style.display = 'block';	
			document.getElementById('fundoexibe').style.display = 'block';	
		}
		
		var foto = document.getElementById('foto' +id);
		var caminho = foto.src.split('/');
		foto = caminho[(caminho.length - 1)];
		
		idatual = id;
		
		document.getElementById('exibindo').innerHTML = 'Carregando foto...';
		document.getElementById('load').style.display = 'block';
		
		if(exibeatual == 1){
			document.getElementById('exibe2').innerHTML = '<img src="' +dir+pasta+foto+ '" border="0" onload="FechaLoad(this)" onerror="FechaLoad(this)" alt="' +foto+ '" />';	
		}else{
			document.getElementById('exibe1').innerHTML = '<img src="' +dir+pasta+foto+ '" border="0" onload="FechaLoad(this)" onerror="FechaLoad(this)" alt="' +foto+ '" />';				
		}
		liberaprox = false;
	}
}

function FechaLoad(foto){;
	var caminho = foto.src.split('/');
	nomefoto = caminho[(caminho.length - 1)];
	
	document.getElementById('load').style.display = 'none';	
	document.getElementById('exibindo').innerHTML = 'Exibindo foto ' +idatual+ ' de ' +totalfotos+ ' (' +nomefoto+ ')';
	
	zindex++;
	if(exibeatual == 1){
		setOpacidade(0,'exibe2');	
		document.getElementById('exibe2').style.zIndex = zindex;
		setFade('exibe2',0,100,500);
		exibeatual = 2;
	}else{
		setOpacidade(0,'exibe1');	
		document.getElementById('exibe1').style.zIndex = zindex;
		setFade('exibe1',0,100,500);
		exibeatual = 1;	
	}
	setTimeout('LiberaProxima()',510);
}

function LiberaProxima(){
	liberaprox = true;	
	Slide();
}

function setFade(id,inicio,fim,tempo){
	
 	var velocidade = Math.round(tempo / 100); 
    var timer = 0; 	
	
	if(inicio > fim){
        for(i = inicio; i >= fim; i--) { 
            setTimeout('setOpacidade(' + i + ',\'' + id + '\')',(timer * velocidade)); 
            timer++; 
        } 
	}
	if(fim > inicio){
        for(i = inicio; i <= fim; i++) { 
            setTimeout('setOpacidade(' + i + ',\'' + id + '\')',(timer * velocidade)); 
            timer++; 
        } 
	}
}

function setOpacidade(opacidade,id) { 
    var obj = document.getElementById(id).style; 
    obj.opacity = (opacidade / 100); 
    obj.MozOpacity = (opacidade / 100); 
    obj.KhtmlOpacity = (opacidade / 100); 
    obj.filter = 'alpha(opacity=' + opacidade + ')'; 
} 

function Proxima(tipo){
	ParaSlide();
	if(parseInt(tipo) == 1){
		var id = idatual + 1;
		if(id > totalfotos){ id = totalfotos; }
	}else{
		var id = idatual - 1;	
		if(id < 1){ id = 1; }
	}	
	Amplia(id);
}

function ProximaSlide(){
	var id = idatual + 1;
	if(id > totalfotos){ id = totalfotos; }
	Amplia(id);
}

function Scroll(){
	if(sa < sc){
		sa = sa + 8;
		if(sa > sc){sa = sc; }
		document.getElementById('miniaturas').scrollTop = sa;
		setTimeout('Scroll()',1);
	}
	if(sa > sc){
		sa = sa - 8;
		if(sa < sc){sa = sc; }
		document.getElementById('miniaturas').scrollTop = sa;
		setTimeout('Scroll()',1);
	}
}

function PosScroll(id){
	id = parseInt(id);	
	sc = ((id - 1) * 89) - 168;	
	document.getElementById('miniaturas').scrollTop = sc;
}

function AlteraModo(modoa){
	modoa = parseInt(modoa);
	ParaSlide();
	BarraModoOver(modoa);
	modo = modoa;
	if(modoa == 1){
		FechaFoto();
		document.getElementById('miniaturas').className = 'fundominiatura';	
		document.getElementById('fundoexibe').className = 'fundoexibe';	
		document.getElementById('botfecha').style.display = 'none';
		document.getElementById('fundoexibe').style.display = 'block';	
		PosScroll(idatual);
	}else{
		document.getElementById('miniaturas').className = 'fundominiaturamodo';	
		document.getElementById('fundoexibe').className = 'fundoexibemodo';	
		document.getElementById('botfecha').style.display = 'block';
		document.getElementById('fundoexibe').style.display = 'none';	
	}	
}

function BarraModoOver(modoa){
	var lks = document.getElementById('barramodo').getElementsByTagName('a');	
	for(var i =0; i < lks.length; i++){
		lks[i].className = 'linkmodo';	
		if(lks[i].id == 'lkmodo' +modoa){
			lks[i].className = 'linkmodoover';	
		}
	}
}

function FechaFoto(){
	document.getElementById('fundoexibe').style.display = 'none';	
	document.getElementById('fundoopaco').style.display = 'none';
	ParaSlide();
}

function Reposiciona(){
	if(modo == 2){
		TamanhoTela();
		ExibeFundo();	
		document.getElementById('fundoexibe').style.left = (tamx - 480) / 2 +'px';	
		document.getElementById('fundoexibe').style.top = ((tamy - 426) / 2) +topo +'px';
	}
}

function ExibeFundo(){
	var sch = document.documentElement.scrollHeight;
	var scw = document.documentElement.scrollWidth;
		
	if(tamy > sch){ sch = tamy; }
	if(tamx > scw){ scw = tamx; }
		
	document.getElementById('fundoopaco').style.width = scw +'px';
	document.getElementById('fundoopaco').style.height = sch +'px';
	setOpacidade(85,'fundoopaco');
}

function RolaJanela(){
	if(modo == 2){
		TamanhoTela();
		document.getElementById('fundoexibe').style.top = ((tamy - 476) / 2) +topo +'px';
	}
}

// slide ----------------------------------------------------------------------------

function IniciaSlide(){
	if(slide == false){
		slide = true;
		document.getElementById('botslide').innerHTML = 'Parar slide';
		if(modo == 1){
			document.getElementById('miniaturas').style.visibility = 'hidden';
		}
		Slide();
	}else{
		ParaSlide();
	}	
}

function ParaSlide(){
	slide = false;
	document.getElementById('botslide').innerHTML = 'Iniciar slide';
	clearTimeout(tmpslide);	
	document.getElementById('miniaturas').style.visibility = 'visible';
}

function Slide(){
	if(slide == true){
		esperatmp = false;
		esperaimg = false;
		
		//precarrega
		if((idatual + 1) <= totalfotos){	
			var foto = document.getElementById('foto' +(idatual + 1));
			var caminho = foto.src.split('/');
			foto = caminho[(caminho.length - 1)];
			
			if(exibeatual == 1){
				document.getElementById('exibe2').innerHTML = '<img src="' +dir+pasta+foto+ '" border="0" onload="LiberaImg()" onerror="LiberaImg()" alt="' +foto+ '" />';	
			}else{
				document.getElementById('exibe1').innerHTML = '<img src="' +dir+pasta+foto+ '" border="0" onload="LiberaImg()" onerror="LiberaImg()" alt="' +foto+ '" />';				
			}		
			tmpslide = setTimeout('LiberaSlide()',4000);	
		}else{
			ParaSlide();	
		}
	}
}

function LiberaSlide(){
	esperatmp = true;
	if(esperaimg == true){
		ProximaSlide();	
	}
}

function LiberaImg(){
	esperaimg = true;
	if(esperatmp == true){
		ProximaSlide();	
	}
}

//-----------------------------------------------------------------------------------

function Teclap(e){
	var tecla = 0;
	
	if(window.event){ // IE
		tecla = window.event.keyCode;
	}else if(e.keyCode){ // Netscape/Firefox/Opera
		tecla = e.keyCode;
	}
	
	switch(tecla){
		case 37: Proxima(0); break;	
		case 39: Proxima(1); break;	
		case 27: 
			if(modo == 2){
				FechaFoto();	
			}
		break;	
		case 83: IniciaSlide();  break;
		case 77: 
			if(modo == 1){
				AlteraModo(2);
			}else{
				AlteraModo(1);	 
			}
		break;	
	}
}

//restantes
function Restante(){
	resta--;
	if(document.getElementById('restante')){
		if(resta == 0){
			document.getElementById('restante').innerHTML =  'Conclu�do';	
		}else{
			document.getElementById('restante').innerHTML =  '(' +resta+ ' faltando)';	
		}
	}
}

document.onkeydown = Teclap;
window.onresize = Reposiciona;
window.onscroll = RolaJanela;