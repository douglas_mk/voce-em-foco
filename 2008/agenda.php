<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new Data;
include('banners.php');

$diragenda = 'arquivos/agenda/';

//paginacao
$totalr = $sql->Totalreg("SELECT * FROM agenda WHERE estado='1' AND data >= NOW()");
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);
$pag->setRegistro(5);
$pag->setTotal($totalr); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('');
$pagatual = $pag->getAtual();

if($totalr > 0){
	$exmax = $min + $max;
	if($totalr < ($min + $max)){ $exmax = $totalr; }
	$exibindo = 'Exibindo '.($min + 1).' - '.$exmax.' de um total de '.$totalr; 
}else{
	$exibindo = 'Nenhum evento encontrado';
}

//listagem
$query = $sql->Consulta("SELECT agenda.*,estados.uf
FROM agenda 
LEFT JOIN estados ON estados.id=agenda.id_estado
WHERE agenda.estado='1' AND data >= NOW() ORDER BY agenda.data ASC LIMIT $min,$max");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Agenda de eventos  - <?=$config['nomesite'];?></title>
<link href="css/geral.css" rel="stylesheet" type="text/css" />
<link href="css/agenda.css" rel="stylesheet" type="text/css" />
<link href="css/paginacao.css" rel="stylesheet" type="text/css" />
<link href="css/amplia.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/geral.js"></script>
<script type="text/javascript" src="js/amplia.js"></script>
</head>

<body>
<table width="750" border="0" cellspacing="0" cellpadding="0" align="center" class="fundosite">
  <tr>
    <td height="180" colspan="2">
		<!-- topo -->
		<?php @include('topo.php'); ?>
		</td>
  </tr>
  <tr>
    <td width="180" align="center" valign="top">
		<!-- menu -->
		<?php include('menu.php'); ?>
	</td>
    <td width="560" align="center" valign="top" class="corpo">
		<!--- corpo -->
		<div class="bannercentral"><?php if(isset($banners['8'])) { print $banners['8']; } ?></div>
		<div class="caixatopointerno">Agenda de eventos</div>
		<div class="caminho">
			<a href="index.php" class="linkcaminho">Inicial</a><img src="icones/setacaminho.jpg" />Agenda de eventos
		</div>

		<div class="fundocentro">
			<div class="fundoexibindo" style="margin-top: 6px;"><span class="exibindo"><?=$exibindo;?></span></div>
			
			<!-- listagem -->
			<?php 
			while($linha = mysql_fetch_array($query)){ 
				$infodata = $data->Diferenca($data->MysqlData($linha['data']),date('d/m/Y'));
				if($infodata > 0){
					$infodata = ' - Faltam '.$infodata.' dia(s)';	
				}else{
					$infodata = ' - <strong>Hoje!</strong>';
				}
				if($linha['cartaz']){
					$linha['cartaz'] = '<a href="javascript:Amplia(\''.$diragenda.$linha['cartaz'].'\');" title="Ampliar cartaz" class="bordaimg"><img src="'.$diragenda.'mini/'.$linha['cartaz'].'" /></a>';
				}
				if($linha['animacao']){ $linha['animacao'] = '<span class="agendadescricaotitulo">Anima��o:</span> '.$linha['animacao'].'<br />'; }
				if($linha['realizacao']){ $linha['realizacao'] = '<span class="agendadescricaotitulo">Realizacao:</span> '.$linha['realizacao'].'<br />'; }
				if($linha['descricao']){$linha['descricao'] = '<div class="agendacoment">'.$linha['descricao'].'</div>'; }
				
			?>
			<div class="agendabordalista">
				<div class="agendadata"><?=$data->MysqlData($linha['data']);?> <span class="agendainfodata"><?=$infodata;?></span></div>
				<?=$linha['cartaz'];?>
				<div class="agendabordatitulolista">
						<div class="agendatitulo"><?=$linha['evento'];?></div>
						<span class="agendadescricaotitulo">Local:</span> <?=$linha['local'];?><br />
						<span class="agendadescricaotitulo">Cidade:</span> <?=$linha['cidade'];?> - <?=$linha['uf'];?><br />
						<?=$linha['animacao'];?>
						<?=$linha['realizacao'];?>
				</div>
				<?=nl2br($linha['descricao']);?>&nbsp;
			</div>
			<?php } ?>
			
			<!-- paginacao -->
			<div class="fundopaginacao">
				<?=$paginas;?>
			</div>
			
			<div class="bannercentral" style="margin-top: 15px;"><?php if(isset($banners['9'])) { print $banners['9']; } ?></div>
		</div>
	</td>
  </tr>
  <tr>
    <td colspan="2"><?php include('rodape.php'); ?></td>
  </tr>
</table>
</body>
</html>