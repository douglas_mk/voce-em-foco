<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new data;
include('banners.php');

$erro = false;
$denviado = 'none';
$class = 'msgerro';
$tempo_envio = 20;

if(!isset($post['nome'])){ $post['nome'] = NULL; }
if(!isset($post['cidade'])){ $post['cidade'] = NULL; }
if(!isset($post['email'])){ $post['email'] = NULL; }
if(!isset($post['uf'])){ $post['uf'] = NULL; }
if(!isset($post['msg'])){ $post['msg'] = NULL; }
if(!isset($post['telefone'])){ $post['telefone'] = NULL; }

if(isset($_POST['EnviaContato'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['nome'],'texto','Nome');
	$form->Valida($post['email'],'email','E-mail');
	$form->Valida($post['cidade'],'texto','Cidade');
	$form->Valida($post['uf'],'texto','Estado');
	$form->Valida($post['msg'],'texto','Mensagem');
	if($post['telefone']){
		$form->Valida($post['telefone'],'numerico','Telefone');
	}

	$erro = $form->getErro();
	session_start();
	if(isset($_SESSION['tempoform'])){
		if($_SESSION['tempoform'] > time()){
			$tempo = $_SESSION['tempoform'] - time();
			$erro = 'Aguarde '.$tempo.' segundos para enviar novamente';
		}
	}
	
	if(!$erro){
		$email = new Email;
		$email->setPara('contato@voceemfoco.com.br');
		$email->setDe($post['email']);
		$email->setAssunto('Contato Voc� em foco: '.$post['nome']);
		
		$msg = 'Nome: '.$post['nome'];
		$msg .= '<br>E-mail: '.$post['email'];
		$msg .= '<br>Telefone: '.Vazio($post['telefone']);
		$msg .= '<br>Cidade: '.$post['cidade'].' - '.$post['uf'];
		$msg .= '<br><br>'.$post['msg'];
		$msg .= '<hr>';
		$msg .= 'Enviado em '.date('d/m/Y').' �s '.date('h:i:s');
		$msg .= '<br>Ip: '.Ip();
		

		$email->setTexto($msg);

		if(!$email->Envia()){
			$erro = 'Falha ao enviar email, tente novamente mais tarde';
		}else{
			$erro = 'Seu e-mail foi enviado com sucesso. Em breve entraremos em contato.';
			$class = 'msgaviso';
			$post = NULL;
			$_SESSION['tempoform'] = time() + $tempo_envio;
		}
	}
	$denviado = 'block';
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Contato  - <?=$config['nomesite'];?></title>
<link href="css/geral.css" rel="stylesheet" type="text/css" />
<link href="css/contato.css" rel="stylesheet" type="text/css" />
<link href="css/paginacao.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/geral.js"></script>
</head>

<body>
<table width="750" border="0" cellspacing="0" cellpadding="0" align="center" class="fundosite">
  <tr>
    <td height="180" colspan="2">
		<!-- topo -->
		<?php @include('topo.php'); ?>
		</td>
  </tr>
  <tr>
    <td width="180" align="center" valign="top">
		<!-- menu -->
		<?php include('menu.php'); ?>
	</td>
    <td width="560" align="center" valign="top" class="corpo">
		<!--- corpo -->
		<div class="bannercentral"><?php if(isset($banners['8'])) { print $banners['8']; } ?></div>
		<div class="caixatopointerno">Contato</div>
		<div class="caminho">
			<a href="index.php" class="linkcaminho">Inicial</a><img src="icones/setacaminho.jpg" />Contato
		</div>

		<div class="fundocentro">
			<div class="tituloforma">Contato via e-mail</div>	
			<!-- email enviado -->
			<div class="<?=$class;?>" style="display: <?=$denviado;?>; "><?=$erro;?></div>
			
			<form id="form1" name="form1" method="post" action="contato.php">	
			<table width="436" border="0" cellspacing="0" cellpadding="1" class="tabelacontato">
			  <tr>
			    <td>&nbsp;</td>
			    <td>&nbsp;</td>
		      </tr>
			  <tr>
				<td width="91" align="right">* Nome:</td>
				<td width="341">
			    <input type="text" name="nome" class="formcontato" maxlength="60" value="<?=$post['nome'];?>" />				</td>
			  </tr>
			  <tr>
				<td align="right">* E-mail:</td>
				<td><input type="text" name="email" class="formcontato" maxlength="60" value="<?=$post['email'];?>" /></td>
			  </tr>
			  <tr>
				<td align="right">* Cidade:</td>
				<td><input type="text" name="cidade" class="formcontato" maxlength="60" value="<?=$post['cidade'];?>" /></td>
			  </tr>
			  <tr>
				<td align="right">* Estado:</td>
				<td>
				<select name="uf" class="formcontato">
					<option value="0">Selecione seu estado</option>
					<?php
					$querye = $sql->Consulta("SELECT * FROM estados WHERE estado='1' ORDER BY nome_estado ASC");
					while($linha = mysql_fetch_array($querye)){
						$sel = NULL;
						if($post['uf'] == $linha['uf']){ $sel = ' selected="selected"'; }
						print '<option value="'.$linha['uf'].'"'.$sel.'>'.$linha['nome_estado'].'</option>';
					}
					?>
				</select>				</td>
			  </tr>
			  <tr>
				<td align="right">Telefone:</td>
				<td><input type="text" name="telefone" class="formcontato" maxlength="11" value="<?=$post['telefone'];?>" /></td>
			  </tr>
			  <tr>
				<td align="right">* Mensagem:</td>
				<td><textarea name="msg" cols="35" rows="5" class="formcontato" style="height:100px; width: 250px;"><?=$post['msg'];?></textarea></td>
			  </tr>
			  <tr>
				<td height="31">&nbsp;</td>
				<td class="campoobr" valign="top">* Campos obrigat&oacute;rios </td>
			  </tr>
			  <tr>
			    <td colspan="2" align="center"><input type="submit" name="EnviaContato" value="Enviar" class="contatobotaoform" /></td>
		      </tr>
			</table>
			</form>
			<div class="separadorform"></div>
			<div class="bannercentral" style="margin-top: 15px;"><?php if(isset($banners['9'])) { print $banners['9']; } ?></div>
		</div>
	</td>
  </tr>
  <tr>
    <td colspan="2"><?php include('rodape.php'); ?></td>
  </tr>
</table>
</body>
</html>