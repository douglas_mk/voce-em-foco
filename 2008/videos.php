<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new data;
include('banners.php');

$dirvideo = 'arquivos/videos/';

if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);

//busca
if(!isset($_GET['busca'])){ $_GET['busca'] = NULL; }
$busca = Sql_inject($_GET['busca']);
$titulob = 'Todos os v�deos';
$and = NULL;
$vertodos = NULL;

if($busca){
	$and = " AND (videos.titulo LIKE '%$busca%' OR videos.descricao LIKE '%$busca%')";
	$pag->AddLink('busca',$busca);
	$titulob = 'Resultados da busca por: '.$busca;
}

//paginacao
$totalv = $sql->Totalreg("SELECT * FROM videos WHERE estado='1' $and");

if(($busca) && ($totalv == 0)){ 
	$vertodos = '<a href="videos.php" title="Ver todos os v�deos">Todos os v�deos</a>';
}

$pag->setRegistro(7);
$pag->setTotal($totalv); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('');
$pagatual = $pag->getAtual();

if($totalv > 0){
	$exmax = $min + $max;
	if($totalv < ($min + $max)){ $exmax = $totalv; }
	$exibindo = '&nbsp;&nbsp;I&nbsp;&nbsp;Exibindo '.($min + 1).' - '.$exmax.' de um total de '.$totalv; 
}else{
	$exibindo = '&nbsp;&nbsp;I&nbsp;&nbsp;Nenhum v�deo encontrado';
}

//listagem
$query = $sql->Consulta("SELECT * FROM videos WHERE estado='1' $and ORDER BY id DESC LIMIT $min,$max");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>V�deos em Foco - <?=$config['nomesite'];?></title>
<link href="css/geral.css" rel="stylesheet" type="text/css" />
<link href="css/videos.css" rel="stylesheet" type="text/css" />
<link href="css/paginacao.css" rel="stylesheet" type="text/css" />
<link href="css/amplia.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/geral.js"></script>
<script type="text/javascript" src="js/amplia.js"></script>
</head>

<body>
<table width="750" border="0" cellspacing="0" cellpadding="0" align="center" class="fundosite">
  <tr>
    <td height="180" colspan="2">
		<!-- topo -->
		<?php @include('topo.php'); ?>
	</td>
  </tr>
  <tr>
    <td width="180" align="center" valign="top">
		<!-- menu -->
		<?php include('menu.php'); ?>
	</td>
    <td width="560" align="center" valign="top" class="corpo">
		<!--- corpo -->
		<div class="bannercentral"><?php if(isset($banners['8'])) { print $banners['8']; } ?></div>
		<div class="caixatopointerno">V�deos em Foco</div>
		<div class="caminho">
			<a href="index.php" class="linkcaminho">Inicial</a><img src="icones/setacaminho.jpg" />V�deos em foco
		</div>
		
		<!-- busca -->
		<div class="fundobusca">
			<div style="float: left;"><a href="javascript:ExibeBusca();" title="Exibir busca"><img src="icones/buscar.jpg" alt="Buscar" border="0" /></a>
				<div id="formbusca" class="caixabusca" style="display: none; width: 240px;">
				<form action="videos.php" method="get" name="busca" id="busca" style="margin: 0px; display: block;">
					<input type="text" name="busca" maxlength="25" class="formbusca" value="<?=$busca;?>" title="Informe um termo para buscar" />
					<input type="submit" value="Buscar V&iacute;deo" name="BuscaT" class="botaobusca" style="width: 100px;"> 
				</form>
				</div>
			</div>
			<div class="vertodosbusca"><?=$vertodos;?></div>
		</div>
	
		<div class="fundoexibindo"><?=$titulob;?><span class="exibindo"><?=$exibindo;?></span></div>
		
		<div class="fundocentro">
			<!-- listagem -->
			<?php while($linha = mysql_fetch_array($query)){ 
			if($linha['miniatura']){
				$linha['miniatura'] = '<a href="videoVisualiza.php?video='.$linha['id'].'" title="Visualizar v�deo" class="bordaimg"><img src="'.$dirvideo.'mini/'.$linha['miniatura'].'" /></a>';
			}
			?>
			<div class="videobordalista">
				<?=$linha['miniatura'];?>
				<div class="videocontlista">
						<a href="videoVisualiza.php?video=<?=$linha['id'];?>" class="videocomentario" title="Visualizar v�deo">Visualizar v�deo</a>
						<a href="videoVisualiza.php?video=<?=$linha['id'];?>" title="Visualizar v�deo" class="videotitulo"><?=$linha['titulo'];?></a><br />
						<div class="videodata"><?=$data->MYsqlData($linha['data']);?></div>
				</div>
				<?=nl2br($linha['descricao']);?>&nbsp;
			</div>
			<?php } ?>
			
			<!-- paginacao -->
			<div class="fundopaginacao">
				<?=$paginas;?>
			</div>
			
			<div class="bannercentral" style="margin-top: 15px;"><?php if(isset($banners['9'])) { print $banners['9']; } ?></div>
		</div>
	</td>
  </tr>
  <tr>
    <td colspan="2"><?php include('rodape.php'); ?></td>
  </tr>
</table>
</body>
</html>