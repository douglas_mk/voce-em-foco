<?php
require_once './recaptchalib.php';
include('inc.includes.php');
$sql = new Mysql;
$data = new data;
include('banners.php');

$dexibe = 'none';
$denviado = 'none';
$erro = false;
$liberado = 0; //0 - requer liber��o, 1 - liberado
$dirsmiles = 'icones/smiles/';
$tempo_envio = 20;

if (!isset($post['de'])) {
    $post['de'] = NULL;
}
if (!isset($post['para'])) {
    $post['para'] = NULL;
}
if (!isset($post['email'])) {
    $post['email'] = NULL;
}
if (!isset($post['recado'])) {
    $post['recado'] = NULL;
}

if (isset($_POST['EnviaRecado'])) {
    $post = array_map('Sql_injectTag', $_POST);
//die(print_r($post));
    $form = new ValidaForm;
    $form->Valida($post['de'], 'texto', 'De');
    $form->Valida($post['para'], 'texto', 'Para');
    $form->Valida($post['email'], 'email', 'E-mail');
    $form->Valida($post['recado'], 'texto', 'Recado');
    $form->Tamanho($post['recado'], 2, 500, 'Recado');
    $form->ReCaptcha($post['recaptcha_challenge_field'], $post['recaptcha_response_field']);


    $erro = $form->getErro();

    session_start();
    if (isset($_SESSION['tempoform'])) {
        if ($_SESSION['tempoform'] > time()) {
            $tempo = $_SESSION['tempoform'] - time();
            $erro = 'Aguarde ' . $tempo . ' segundos para enviar novamente';
        }
    }

    if (!$erro) {
        $ip = Ip();

        //smiles
        for ($i = 1; $i < 40; $i++) {
            $post['recado'] = str_replace(' :' . $i . ': ', '<img src="' . $dirsmiles . $i . '.gif" />', $post['recado']);
        }

        $sql->Consulta("INSERT INTO muralr
		(estado,de,para,email,recado,data,hora,ip)
		VALUES
		('$liberado','$post[de]','$post[para]','$post[email]','$post[recado]',NOW(),NOW(),'$ip')");

        $denviado = 'block';
        $post = NULL;
        $_SESSION['tempoform'] = time() + $tempo_envio;
    }
}

if ($erro) {
    $dexibe = 'block';
}

//paginacao
//$totar = (int) $_GET['totalr'];
$totar = $sql->Totalreg("SELECT * FROM muralr WHERE estado='1'");
if (!isset($_GET['inicio'])) {
    $_GET['inicio'] = 0;
}
$pag = new Paginacao($_GET['inicio']);
$pag->setRegistro(6);
$pag->setTotal($totar);
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('');

if ($totar > 0) {
    $exmax = $min + $max;
    if ($totar < ($min + $max)) {
        $exmax = $totar;
    }
    $exibindo = 'Exibindo ' . ($min + 1) . ' - ' . $exmax . ' de um total de ' . $totar;
} else {
    $exibindo = 'Nenhum recado encontrado';
}

//listagem
$query = $sql->Consulta("SELECT * FROM muralr WHERE estado='1' ORDER BY id DESC LIMIT $min,$max");
?><!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Mural de recados  - <?= $config['nomesite']; ?></title>
        <meta charset="iso-8859-1">
        <link rel="icon" href="<?= $patchIMG ?>favicon.ico" type="image/x-icon" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">        -->
        <link rel="stylesheet" href="<?= $patchCSS ?>/reset.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/text.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/960_16_col.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/style.css">
        <link href="<?= $patchCSS ?>/mural.css" rel="stylesheet" type="text/css" />
        <link href="<?= $patchCSS ?>/paginacao.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <!-- topo -->
        <?php include('topo.php'); ?>
        <div class="container_16 box verde">
            <ul class="breadcrumb grid_16">
                <li><a href="<?= $host ?>" >Inicial</a> <span class="divider">></span></li>
                <li class="active">Mural de recados</li>
            </ul>
            <?php include('sidebar.php'); ?>
            <div class="grid_13 verde">
                <h2><span class="icon25 recados"></span>Mural de recados</h2>
                <div class="content">
                    <!--- corpo -->
                    <!-- recado enviado -->
                    <div class="msgaviso" style="display: <?= $denviado; ?>; ">Seu recado foi enviado com sucesso, aguarde a libera��o pelo administrador do sistema</div>

                    <div class="muralfundo">
                        <!-- envio -->
                        <div class="topobotaointerno">
                            <div style="float: left;">
                                <a href="javascript:ExibeForm();" title="Envie seu recado" class="btn left verde grid_3">Envie seu recado</a>
                            </div>
                            <div class="descricaobotao"></div>
                        </div>
                        <div style="position: relative; width: 555px; height: auto;">
                            <div class="muralcaixaform" style="display: <?= $dexibe; ?>;" id="formrecado">
                                <div class="msgerro" style="display: <?= $dexibe; ?>;"><?= $erro; ?></div>
                                <form action="" method="post" name="recado" id="recado" style="margin: 0px;" onsubmit="ExibeAguarde()">
                                    De:<br />
                                    <input name="de" type="text" class="muralform" maxlength="60" value="<?= $post['de']; ?>" /><br />
                                    Para:<br />
                                    <input name="para" type="text" class="muralform" maxlength="60" value="<?= $post['para']; ?>" /><br />
                                    E-mail:<br />
                                    <input name="email" type="text" class="muralform" maxlength="60" value="<?= $post['email']; ?>" /><br />
                                    Recado:<br />
                                    <textarea name="recado" id="trecado" class="muralform" style="height: 70px;" onfocus="ContaC(this)"><?= $post['recado']; ?></textarea>
                                    <div class="muralcaracteres">Caracteres restantes: <span id="carac">500</span></div>

                                    <a href="javascript:ExibeSmiles();" class="muralbotsmile" title="Exibir smiles">Exibir smiles</a>
                                    <div id="smiles" style="display: none;" class="muralfundosmiles">
                                        <?php
                                        for ($i = 1; $i < 40; $i++) {
                                            print '<img src="' . $dirsmiles . $i . '.gif" class="muralsmile" onclick="AdicionaSmile(' . $i . ')" />';
                                        }
                                        ?>
                                    </div>

                                    Digite os caracteres abaixo:<br />
                                    <?php
                                    require_once('recaptchalib.php');
                                    $publickey = "6LdSw-cSAAAAAJJCzQ9i5FSUplr9cxKhAuHJZGiD"; // you got this from the signup page
                                    echo recaptcha_get_html($publickey);
                                    ?>
                                    <input type="submit" name="EnviaRecado" value="Enviar recado" class="muralbotaoform" title="Enviar recado" />
                                    <input type="button" name="Cancelar" value="Cancelar" class="muralbotaoform" onclick="FechaForm()" title="Cancelar envio" />
                                </form>
                                <div class="muralload" id="aguarde"><img src="<?= $patchICONES ?>aguarde.gif" alt="Aguarde" /></div>
                            </div>
                        </div>
                        <div class="clear"></div>

                        <div class="textCinza text11 grid_12"><?= $exibindo; ?></div>
                        <ul>
                            <!-- listagem -->
                            <?php while ($linha = mysql_fetch_array($query)) { ?>
                                <li class="list">
                                    <p class="data textCinza text10"><?= $data->MYsqlData($linha['data']); ?> - <?= $linha['hora']; ?></p>
                                    <p class="textCinza text12">De: <b><?= $linha['de']; ?></b>&nbsp;<a href="javascript:MostraEmail(<?= $linha['id']; ?>)" title="Ver e-mail"><img src="<?= $patchICONES ?>email.jpg" alt="Ver e-email" border="0" /></a>&nbsp;<span id="e<?= $linha['id']; ?>" style="display: none;" ><?= strtolower($linha['email']); ?></span></p>
                                    <p class="textCinza text12">Para: <b><?= $linha['para']; ?></b></p>

                                    <p class="chatBaloom text12"><?= nl2br($linha['recado']); ?></p>
                                    <hr>
                                </li>
                            <?php } ?>
                        </ul>
                        <!-- paginacao -->
                        <div class="grid_16">
                            <?= $paginas; ?>
                        </div>
                        <div class="clear"></div>
                        <div class="ads largo push_2" title="Anuncie" style="margin-top: 20px;">
                            <?php
                            if (isset($banners['9'])) {
                                print $banners['9'];
                            }
                            ?>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include('rodape.php'); ?>
        <script type="text/javascript" src="<?= $patchJS ?>mural.js"></script>
    </body>
</html>
