<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new Data;
$id_evento = (int) $_GET['evento'];

include('banners_eventos.php');

//dados do evento
$query = $sql->Consulta("SELECT galerias.*,estados.uf,cat_galeria.categoria
FROM galerias
LEFT JOIN estados ON estados.id=galerias.id_estado
LEFT JOIN cat_galeria ON cat_galeria.id=galerias.id_categoria
WHERE galerias.id='$id_evento' LIMIT 1");
$evento = mysql_fetch_array($query);
VerificaVar($evento);
$id_categoria = $evento['id_categoria'];

//atualiza acesso
$sql->Consulta("UPDATE galerias SET acessos=(acessos + 1) WHERE id='$id_evento' LIMIT 1");

if ($evento['fotografo']) {
    $evento['fotografo'] = '<br />Fot�grafo: ' . $evento['fotografo'];
}
if ($evento['local']) {
    $evento['local'] = '<br />Local: ' . $evento['local'];
}
if ($evento['cidade']) {
    $evento['cidade'] = '<br />Cidade: ' . $evento['cidade'];
}
if ($evento['uf']) {
    $evento['cidade'] .= ' - ' . $evento['uf'];
}
if ($evento['descricao']) {
    $evento['descricao'] = '<br /><br />' . $evento['descricao'];
}

//fotos deste evento
$diretorio = $arquivos . 'galerias/' . $evento['pasta'] . '/';
$fotos = array();

//'../arquivos/galerias/'
$aberto = opendir($diretorio . 'mini/'); //abre o diretorio
$diretorio = $patchARQ . 'galerias/' . $evento['pasta'] . '/';
while ($arq = @readdir($aberto)) {//le o diretorio
    if (($arq != '.') && ($arq != '..')) {//desconsidera subdiretorios
        $ext = explode('.', $arq); //verifica extens�o da imagens
        if (($ext[1] == 'jpg') || ($ext[1] == 'JPG')) {
            $fotos[] = $arq;
        }
    }
}
@closedir($aberto);
//ordena o vetor
@array_multisort($fotos);

//galeria v4
$resul = '{fotos: [';
$temp = array();
$totalfotos = sizeof($fotos);
for ($i = 0; $i < $totalfotos; $i++) {
    $temp[] = '{img:"' . $fotos[$i] . '"}';
}
$resul .= implode(',', $temp);
$resul .= ']}';
?><!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $evento['categoria']; ?> - <?= $evento['titulo']; ?>  - <?= $config['nomesite']; ?></title>
        <meta charset="iso-8859-1">
        <link rel="icon" href="<?= $patchIMG ?>favicon.ico" type="image/x-icon" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="<?= $patchCSS ?>/reset.css" />
        <link rel="stylesheet" type="text/css" href="<?= $patchCSS ?>/text.css" />
        <link rel="stylesheet" type="text/css" href="<?= $patchCSS ?>/960_16_col.css" />
        <link rel="stylesheet" type="text/css" href="<?= $patchCSS ?>/style.css">
        <link rel="stylesheet" type="text/css" href="<?= $patchCSS ?>/eventoVisualiza.css" />
        <link rel="stylesheet" type="text/css" href="<?= $patchCSS ?>/paginacao.css" />
        <link rel="stylesheet" type="text/css" href="<?= $patchCSS ?>/galeriav5.css" />
        <!--script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script-->

        <script type="text/javascript" src="<?= $patchJS ?>/galeriav5.js"></script>
        <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>

        <script type="text/javascript">
            var totalFotos = <?= $totalfotos; ?>;
            var diretorio = '<?= $diretorio; ?>';
            var pasta = '';
            var pastaMiniatura = 'mini/';
            var fotos = <?= $resul; ?>;
            var fotoAtual = 0;
            var icload = new Image();
            icload.src = '<?= $patchICONES ?>load28.gif';
        </script>
    </head>
    <body>

        <!-- topo -->
        <?php include('topo.php'); ?>
        <div class="container_16 box vermelho">
            <ul class="breadcrumb grid_16">
                <li><a href="<?= $host ?>" >Inicial</a> <span class="divider">></span></li>
                <li><a href="eventos.php?cat=<?= $evento['id_categoria']; ?>"><?= $evento['categoria']; ?></a> <span class="divider">></span></li>
                <li class="active"><?= $evento['titulo']; ?></li>
            </ul>
            <div class="grid_16">
                <h2><span class="icon25 coberturas"></span><?= $evento['titulo']; ?></h2>
                <div class="content">
                    <!--- corpo -->
                    <div class="barraperfil" style="display: none;">
                        <a href="javascript:void(0);"  onclick="$('#descricao').slideDown();
                                $('#galeria').slideUp();" class="linkbarra" title="Veja o perfil de <?= $linha['nome']; ?>">Descri��o</a>
                        <a href="javascript:void(0);" onclick="$('#descricao').slideUp();
                                $('#galeria').slideDown();" class="linkbarradesat" title="Veja as fotos de <?= $linha['nome']; ?>">Fotos</a>
                    </div>


                    <div class="clear"></div>

                    <p>Data: <?= $data->MysqlData($evento['data']); ?>
                        <?= $evento['local']; ?>
                        <?= $evento['cidade']; ?>
                        <?= $evento['fotografo']; ?>
                        <?= $evento['descricao']; ?></p>
                    <div class="grid_7 alpha push_3" style="margin-top: 20px; margin-bottom: 20px;"><?php
                        if (isset($banners['1'])) {
                            print $banners['1'];
                        }
                        ?>
                    </div>
                    <div class="grid_6 omega push_1" style="margin-top: 20px; margin-bottom: 20px;"><?php
                        if (isset($banners['2'])) {
                            print $banners['2'];
                        }
                        ?>
                    </div>
                    <div class="clear"></div>
                    <?php
                    if ($totalfotos > 0) {
                        include('galeriav5.php');
                    } else {
                        ?>
                        <h3>Nenhuma foto cadastrada.</h3>
                        <?php
                    }
                    ?>

                    <div class="grid_9" style="display: none;">

                        <h3 class="textLaranja"  style="margin: 10px 0px;">Mais <?= $evento['categoria']; ?></h3>
                        <ul class="list">
                            <?php
                            //listagem
                            $cont = 1;
                            $query = $sql->Consulta("SELECT galerias.*,estados.uf
				FROM galerias
				LEFT JOIN estados ON estados.id=galerias.id_estado
				WHERE galerias.estado='1' AND id_categoria='$id_categoria' ORDER BY galerias.id DESC LIMIT 5");
                            while ($linha = mysql_fetch_array($query)) {
                                print '<li><a href="eventoVisualiza.php?evento=' . $linha['id'] . '" class="titulomaisg">' . $cont . ' - ' . $linha['titulo'] . ' - ' . $data->MysqlData($linha['data']) . '</a></li>';
                                $cont++;
                            }
                            ?>
                        </ul>
                        <a href="eventos.php?cat=<?= $evento['id_categoria']; ?>" class="textLaranja text11">+ Ver todas as <?= $evento['categoria']; ?></a>
                    </div>
                    <div class="clear"></div>

                    <div class="bannerrodape" style="margin-left: 100px;"><?php
                        if (isset($banners['3'])) {
                            print $banners['3'];
                        }
                        ?>
                    </div>
                    <div class="bannerrodape"><?php
                        if (isset($banners['4'])) {
                            print $banners['4'];
                        }
                        ?>
                    </div>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <?php include('rodape.php'); ?>
        <script type="text/javascript" src="./js/geral.js"></script>
        <script type="text/javascript">
            var totalfotos = '<?= sizeof($fotos); ?>';
            var dir = '';
            var pasta = '<?= $diretorio; ?>';
            $(".barraperfil a").click(function() {
                $(".barraperfil a").toggleClass("linkbarra")
                $(".barraperfil a").toggleClass("linkbarradesat")
            });


            $(document).ready(function() {

                tmp = 0;

                $('.fancybox-thumbs').fancybox({
                    padding: 5,
                    closeBtn  : true,
                    nextClick : true,

                    openEffect : 'elastic',
                    openSpeed  : 1,

                    closeEffect : 'elastic',
                    closeSpeed  : 1,

                    helpers : {
                        title:  null,
                        /*thumbs : {
                            width  : 50,
                            height : 50
                        }*/
                    },

                    loop: false,
                    afterShow: function(){
                        if((this.index  >= this.group.length - 5) && ($('#btnCarregarMaisFotos').length>0)){
                            ProximaMin(1);

                            if(this.index != tmp){
                                $('.fancybox-thumbs:nth-child('+(this.index + 1)+')').trigger('click');
                                tmp = this.index;
                            }
                        }
                    }
                });
            });
        </script>

    </body>
</html>