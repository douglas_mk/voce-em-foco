<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new data;
include('banners.php');

$dirvideo = $arquivos . 'videos/';

if (!isset($_GET['inicio'])) {
    $_GET['inicio'] = 0;
}
$pag = new Paginacao($_GET['inicio']);

//busca
if (!isset($_GET['busca'])) {
    $_GET['busca'] = NULL;
}
$busca = Sql_inject($_GET['busca']);
$titulob = 'Todos os v�deos';
$and = NULL;
$vertodos = NULL;

if ($busca) {
    $and = " AND (videos.titulo LIKE '%$busca%' OR videos.descricao LIKE '%$busca%')";
    $pag->AddLink('busca', $busca);
    $titulob = 'Resultados da busca por: ' . $busca;
}

//paginacao
$totalv = $sql->Totalreg("SELECT * FROM videos WHERE estado='1' $and");

if (($busca) && ($totalv == 0)) {
    $vertodos = '<a href="videos.php" title="Ver todos os v�deos">Todos os v�deos</a>';
}

$pag->setRegistro(7);
$pag->setTotal($totalv);
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('');
$pagatual = $pag->getAtual();

if ($totalv > 0) {
    $exmax = $min + $max;
    if ($totalv < ($min + $max)) {
        $exmax = $totalv;
    }
    $exibindo = '&nbsp;&nbsp;I&nbsp;&nbsp;Exibindo ' . ($min + 1) . ' - ' . $exmax . ' de um total de ' . $totalv;
} else {
    $exibindo = '&nbsp;&nbsp;I&nbsp;&nbsp;Nenhum v�deo encontrado';
}

//listagem
$query = $sql->Consulta("SELECT * FROM videos WHERE estado='1' $and ORDER BY id DESC LIMIT $min,$max");
?><!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>V�deos - <?= $config['nomesite']; ?></title>
        <meta charset="iso-8859-1">
        <link rel="icon" href="<?= $patchIMG ?>favicon.ico" type="image/x-icon" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">        -->
        <link rel="stylesheet" href="<?= $patchCSS ?>/reset.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/text.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/960_16_col.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/style.css">
        <link href="<?= $patchCSS ?>/videos.css" rel="stylesheet" type="text/css" />
        <link href="<?= $patchCSS ?>/paginacao.css" rel="stylesheet" type="text/css" />
        <link href="<?= $patchCSS ?>/galeriav4.css" rel="stylesheet" type="text/css" />
    </head>
    <body>

        <!-- topo -->
        <?php include('topo.php'); ?>
        <div class="container_16 box vermelho">

            <ul class="breadcrumb grid_16">
                <li><a href="<?= $host ?>" >Inicial</a> <span class="divider">></span></li>
                <li class="active">V�deos</li>
            </ul>

            <?php include('sidebar.php'); ?>
            <div class="grid_13">
                <h2><span class="icon25 videos"></span>V�deos</h2>
                <div class="content">
                    <!--- corpo -->

                    <!-- busca -->
                    <div class="fundobusca grid_12">
                        <div style="float: left;"><a href="javascript:ExibeBusca();" title="Exibir busca" class="btn left vermelho grid_2">Buscar</a>
                            <div id="formbusca" class="chatBaloom" style="display: none; width: 240px;">
                                <form action="videos.php" method="get" name="busca" id="busca" style="margin: 0px; display: block;">
                                    <input type="text" name="busca" maxlength="25" class="formbusca" value="<?= $busca; ?>" title="Informe um termo para buscar" />
                                    <input type="submit" value="Buscar V&iacute;deo" name="BuscaT" class="botaobusca" style="width: 100px;">
                                </form>
                            </div>
                        </div>
                        <div class="vertodosbusca"><?= $vertodos; ?></div>
                    </div>

                    <div class="fundoexibindo"><?= $titulob; ?><span class="exibindo"><?= $exibindo; ?></span></div>

                    <ul class="list" style="margin-bottom: 20px;">
                        <!-- listagem -->
                        <?php
                        while ($linha = mysql_fetch_array($query)) {
                            if ($linha['miniatura']) {
                                $linha['miniatura'] = '<a href="videoVisualiza.php?video=' . $linha['id'] . '" title="Visualizar v�deo" class="bordaimg"><img src="' . $dirvideo . 'mini/' . $linha['miniatura'] . '" /></a>';
                            }
                            ?>
                            <li class="videobordalista">
                                <?= $linha['miniatura']; ?>
                                <div class="videocontlista">
                                    <a href="<?=$hotst . $linha['id'] . '/videos/' . titulo2url($linha['titulo'])?>" class="videocomentario" title="Visualizar v�deo">Visualizar v�deo</a>
                                    <a href="<?=$hotst . $linha['id'] . '/videos/' . titulo2url($linha['titulo'])?>" title="Visualizar v�deo" class="videotitulo"><h3><?= $linha['titulo']; ?></h3></a>
                                    <div class="videodata"><?= $data->MYsqlData($linha['data']); ?></div>
                                </div>
                                <?= nl2br($linha['descricao']); ?>&nbsp;
                            </li>
                        <?php } ?>
                    </ul>
                    <div class="clear"></div>
                    <!-- paginacao -->
                    <div class="grid_16">
                        <?= $paginas; ?>
                    </div>
                    <div class="clear"></div>
                    <div class="ads largo push_2" title="Anuncie" style="margin-top: 20px;">
                        <?php
                        if (isset($banners['9'])) {
                            print $banners['9'];
                        }
                        ?>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
        <?php include('rodape.php'); ?>
        <script type="text/javascript" src="<?=$patchJS ?>geral.js"></script>
    </body>
</html>
