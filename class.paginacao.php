<?php
class Paginacao{
	var $total = 0;
	var $reg_pag = 10;
	var $inicio = 0;
	var $atual;
	var $anterior;
	var $links;
	
	function Paginacao($inicio){	
		$this->inicio = (int) $inicio;
		$this->anterior = $this->inicio;
    }
	
	function setTotal($total){
		$this->total = $total; 
	}

	function getTotal(){
		return $this->total;
	}
	
	function getMin(){
		return $this->inicio;
	}
	
	function getMax(){
		return $this->reg_pag;
	}
	
	function getAtual(){
		return $this->atual;
	}
	
	function setRegistro($regpag){
		$this->reg_pag = (int) $regpag;
	}
	
	function AddLink($var,$valor){
		$this->links .= '&amp;'.$var.'='.$valor;	
	}
	
	//exibe a pagina��o
	function getPaginas($semreg){
		
		if($this->total == 0){
			$monta = '<div class="page_texto">'.$semreg.'</div>';
		}else{
			//monta link
			//'&amp;regpag='.$this->reg_pag.
			$this->links = '&amp;totalr='.$this->total.$this->links;
			
			//paginas
			$monta = '<div class="page_paginas" align="center" title="Total de p�ginas">'.ceil($this->total / $this->reg_pag).' P�gina(s)</div>';
			//anterior e primeira
			if($this->inicio > 0){
				$monta .= '<a href="'.$_SERVER['PHP_SELF'].'?inicio=0'.$this->links.'" class="page" title="Primeira p�gina">Primeira</a>';
				$monta .= '<a href="'.$_SERVER['PHP_SELF'].'?inicio='.($this->anterior - $this->reg_pag).$this->links.'" class="page" title="P�gina anterior">Anterior</a>';
			}
			
			//pagina atual  ((0 + 16) / 16) = 1 , ((10 + 16) / 16) = 2 ...
			$this->atual = floor(($this->inicio + $this->reg_pag) / $this->reg_pag);
			//inicio (0 - (5 * 16)) = -80 , (128 - (5 * 16)) = 48 ...			
			$this->inicio = floor($this->inicio - (3 * $this->reg_pag));
			//paginacao
			for($i = ($this->atual - 3); $i <= ($this->atual + 3); $i++){
				if(($this->inicio < $this->total) && ($i > 0)){
					if($this->atual == $i){
						$monta .= '<div class="page_atual" title="P�gina atual">'.$i.'</div>';
					}else{
						$monta .= '<a href="'.$_SERVER['PHP_SELF'].'?inicio='.$this->inicio.$this->links.'" class="page" title="Ir � p�gina '.$i.'">'.$i.'</a>';
					}
				}
				$this->inicio = $this->inicio + $this->reg_pag;								
			}
			//proxima e ultima
			if($this->total > ($this->anterior + $this->reg_pag)){
				$monta .= '<a href="'.$_SERVER['PHP_SELF'].'?inicio='.($this->anterior + $this->reg_pag).$this->links.'" class="page" title="Pr�xima p�gina">Pr�xima</a>';
				$monta .= '<a href="'.$_SERVER['PHP_SELF'].'?inicio='.((ceil($this->total / $this->reg_pag) * $this->reg_pag) - $this->reg_pag).$this->links.'" class="page" title="�ltima p�gina">�ltima</a>';	
			}
		}
		return $monta;
	}
}
?>