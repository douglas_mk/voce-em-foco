<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new data;
include('banners.php');

$dirfoto = 'arquivos/noticias/';

//listagem
$query = $sql->Consulta("SELECT noticias.*,cat_noticia.categoria
FROM noticias 
LEFT JOIN cat_noticia ON cat_noticia.id=noticias.id_categoria
WHERE noticias.estado='1' ORDER BY noticias.acessos DESC LIMIT 20");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Not�cias - <?=$config['nomesite'];?></title>
<link href="<?= $patchCSS ?>/geral.css" rel="stylesheet" type="text/css" />
<link href="css/noticias.css" rel="stylesheet" type="text/css" />
<link href="<?= $patchCSS ?>/paginacao.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?=$patchJS ?>geral.js"></script>
</head>

<body>
<table width="750" border="0" cellspacing="0" cellpadding="0" align="center" class="fundosite">
  <tr>
    <td height="180" colspan="2">
		<!-- topo -->
		<?php @include('topo.php'); ?>
		</td>
  </tr>
  <tr>
    <td width="180" align="center" valign="top">
		<!-- menu -->
		<?php include('menu.php'); ?>
	</td>
    <td width="560" align="center" valign="top" class="corpo">
		<!--- corpo -->
		<div class="bannercentral"><?php if(isset($banners['8'])) { print $banners['8']; } ?></div>
		<div class="caixatopointerno">Not&iacute;cias - Mais lidas </div>
		<div class="caminho">
			<a href="<?= $host ?>" class="linkcaminho">Inicial</a><img src="<?=$patchICONES?>setacaminho.jpg" />
			<a href="noticias.php" class="linkcaminho">Not�cias</a><img src="<?=$patchICONES?>setacaminho.jpg" />
			Not�cias mais lidas
		</div>
		
		<div class="fundocentro">
			<div class="fundoexibindo" style="margin-top: 6px;">Exibindo as 20 not�cias mais lidas</div>
			
			<!-- listagem -->
			<?php while($linha = mysql_fetch_array($query)){ ?>
			<div class="noticiabordalista">
				<div class="noticiabordatitulolista">
					<span class="noticiadescricao"><?=$linha['categoria'];?></span><br />
					<span><a href="noticiaVisualiza.php?noticia=<?=$linha['id'];?>" class="noticiatitulolista"><?=$linha['titulo'];?></a></span> <br />
					<div class="noticiadata"><?=$data->MYsqlData($linha['data']);?> - <?=$linha['hora'];?></div>
					<div class="noticiadata"><?=$linha['acessos'];?> visualiza��es</div>
				</div>
			</div>
			<?php } ?>
			
			<!-- paginacao -->
			<div class="fundopaginacao">
				<?=$paginas;?>
			</div>
			
			<div class="bannercentral" style="margin-top: 15px;"><?php if(isset($banners['9'])) { print $banners['9']; } ?></div>
		</div>
	</td>
  </tr>
  <tr>
    <td colspan="2"><?php include('rodape.php'); ?></td>
  </tr>
</table>
</body>
</html>
