<?php
header('Access-Control-Allow-Origin: http://www.voceemfoco.com.br');
error_reporting(0);
date_default_timezone_set('America/Sao_Paulo');
ini_set('allow_url_fopen ', 'on');

$config['nomesite'] = 'Voc� em Foco';

$host = 'http://' . $_SERVER["HTTP_HOST"] . "/";

$patchCSS = $host . "theme/";
$patchJS = $host . "js/";
$patchIMG = $host . "theme/img/";
$patchICONES = $host . "icones/";
$patchARQ = 'http://' . $_SERVER["HTTP_HOST"] . "/arquivos/";
$arquivos = "arquivos/";

$base = 'sistema/';
//$arquivos = '../arquivos/';
require($base . 'includes/class.email.php');
require($base . 'includes/class.data.php');
require($base . 'includes/class.mysql.php');
//require($base.'includes/class.template.php');
require($base . 'includes/class.validaform.php');
require($base . 'includes/class.uploadimg.php');
require($base . 'includes/inc.funcoes.php');
require('class.paginacao.php');
?>
