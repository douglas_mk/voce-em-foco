<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new data;
include('banners.php');

$dirparceiro = $arquivos . 'parceiros/';

//paginacao
$totalr = $sql->Totalreg("SELECT * FROM parceiros WHERE estado='1'");
if (!isset($_GET['inicio'])) {
    $_GET['inicio'] = 0;
}
$pag = new Paginacao($_GET['inicio']);
$pag->setRegistro(5);
$pag->setTotal($totalr);
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('');
$pagatual = $pag->getAtual();

if ($totalr > 0) {
    //$exibindo = 'Clique sobre o nome para mais detalhes';
    $exmax = $min + $max;
    if ($totalr < ($min + $max)) {
        $exmax = $totalr;
    }
    $exibindo = 'Exibindo ' . ($min + 1) . ' - ' . $exmax . ' de um total de ' . $totalr;
} else {
    $exibindo = 'Nenhum cadastro encontrado';
}

//listagem
$query = $sql->Consulta("SELECT parceiros.*,estados.uf
FROM parceiros
LEFT JOIN estados ON estados.id=parceiros.id_estado
WHERE parceiros.estado='1' ORDER BY parceiros.nome ASC LIMIT $min,$max");
?><!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Parceiros  - <?= $config['nomesite']; ?></title>
        <meta charset="iso-8859-1">
        <link rel="icon" href="<?= $patchIMG ?>favicon.ico" type="image/x-icon" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">        -->
        <link rel="stylesheet" href="<?= $patchCSS ?>/reset.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/text.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/960_16_col.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/style.css">
        <link href="<?= $patchCSS ?>/parceiros.css" rel="stylesheet" type="text/css" />
        <link href="<?= $patchCSS ?>/paginacao.css" rel="stylesheet" type="text/css" />
        <link href="<?= $patchCSS ?>/amplia.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <!-- topo -->
        <?php include('topo.php'); ?>
        <div class="container_16 box laranja">
            <ul class="breadcrumb grid_16">
                <li><a href="<?= $host ?>" >Inicial</a> <span class="divider">></span></li>
                <li class="active">Parceiros</li>
            </ul>
            <?php include('sidebar.php'); ?>
            <div class="grid_13">
                <h2><span class="icon25 galera"></span>Parceiros</h2>
                <div class="content">
                    <!--- corpo -->

                    <div class="fundocentro">
                        <div class="fundoexibindo" style="margin-top: 6px;"><?= $exibindo; ?></div>

                        <!-- listagem -->
                        <?php
                        while ($linha = mysql_fetch_array($query)) {
                            if ($linha['foto']) {
                                $linha['foto'] = '<a href="javascript:Amplia(\'' . $dirparceiro . $linha['foto'] . '\');" title="Ampliar foto" class="bordaimg"><img src="' . $dirparceiro . 'mini/' . $linha['foto'] . '" /></a>';
                            }
                            if ($linha['endereco']) {
                                $linha['endereco'] = '<span class="parceirodescricaotitulo">Endere�o:&nbsp;</span> ' . $linha['endereco'] . '<br />';
                            }
                            if ($linha['telefone']) {
                                $linha['telefone'] = '<span class="parceirodescricaotitulo">Telefone:&nbsp;</span> ' . FormataTel($linha['telefone']) . '<br />';
                            }
                            if ($linha['email']) {
                                $linha['email'] = '<span class="parceirodescricaotitulo">E-mail:&nbsp;</span> ' . $linha['email'] . '<br />';
                            }
                            if ($linha['site']) {
                                $linha['site'] = '<span class="parceirodescricaotitulo">Site:&nbsp;</span ><a href="' . $linha['site'] . '" target="_blank">' . $linha['site'] . '</a><br />';
                            }
                            ?>
                            <div class="parceirobordalista">
                                <a href="javascript:Mostra('p<?= $linha['id']; ?>');" class="parceironome" title="Ver / Ocultar detalhes"><?= $linha['nome']; ?></a>
                                <div style="display: block;" id="p<?= $linha['id']; ?>" class="parceirooculto">
                                    <?= $linha['foto']; ?>
                                    <div class="parceirobordatitulolista">
                                        <span class="parceirodescricaotitulo">Cidade:&nbsp;</span> <?= $linha['cidade']; ?> - <?= $linha['uf']; ?><br />
                                        <?= $linha['endereco']; ?>
                                        <?= $linha['telefone']; ?>
                                        <?= $linha['email']; ?>
                                        <?= $linha['site']; ?>
                                    </div>
                                    <?= nl2br($linha['informacoes']); ?>&nbsp;
                                </div>
                            </div>
                        <?php } ?>

                        <!-- paginacao -->
                        <div class="grid_16">
                            <?= $paginas; ?>
                        </div>
                        <div class="clear"></div>
                        <div class="ads largo push_2" title="Anuncie" style="margin-top: 20px;">
                            <?php
                            if (isset($banners['9'])) {
                                print $banners['9'];
                            }
                            ?>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('rodape.php'); ?>
    <script type="text/javascript" src="<?=$patchJS ?>amplia.js"></script>
    <script type="text/javascript" src="<?=$patchJS ?>geral.js"></script>
</body>
</html>
