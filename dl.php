<?php
// Force download of image file specified in URL query string and which
// is in the same directory as the download.php script.
if(empty($_GET['img'])) {
   header("HTTP/1.0 404 Not Found");
   return;
}

$basename = split('_._',basename($_GET['img']));
if (sizeof($basename) != 2) {
   header("HTTP/1.0 404 Not Found");
   return;
}

if (substr($basename[1],-3,3) !== 'jpg' && substr($basename[1],-4,4) !== 'jpeg') {
   header("HTTP/1.0 404 Not Found");
   return;
}

$filename = __DIR__ . '/arquivos/galerias/' . $basename[0].'/'.$basename[1]; // don't accept other directories

if (!file_exists($filename)) {
   header("HTTP/1.0 404 Not Found");
   return;
}
$mime = ($mime = getimagesize($filename)) ? $mime['mime'] : $mime;
$size = filesize($filename);
$fp   = fopen($filename, "rb");
if (!($mime && $size && $fp)) {
  // Error.
  return;
}

header("Content-type: " . $mime);
header("Content-Length: " . $size);
header("Content-Disposition: attachment; filename=" . $basename[1]);
header('Content-Transfer-Encoding: binary');
header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
fpassthru($fp);