<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new data;
include('banners.php');

$denviado = 'none';
$class = 'msgerro';
$erro = false;
$liberado = 0; //0 - requer liber��o, 1 - liberado
$dirfoto = 'arquivos/galeranet/';
$tempo_envio = 20;

if (!isset($post['nome'])) {
    $post['nome'] = NULL;
}
if (!isset($post['nascimento'])) {
    $post['nascimento'] = NULL;
}
if (!isset($post['email'])) {
    $post['email'] = NULL;
}
if (!isset($post['cidade'])) {
    $post['cidade'] = NULL;
}
if (!isset($post['uf'])) {
    $post['uf'] = NULL;
}
if (!isset($post['siter'])) {
    $post['siter'] = NULL;
}
if (!isset($post['msn'])) {
    $post['msn'] = NULL;
}

if (isset($_POST['EnviaCadastro'])) {
    $post = array_map('Sql_inject', $_POST);

    $form = new ValidaForm;
    $form->Valida($post['nome'], 'texto', 'Nome');
    $form->Valida($post['email'], 'email', 'E-mail');
    $form->Valida($post['nascimento'], 'data', 'Data de nascimento');
    $form->Valida($post['cidade'], 'texto', 'Cidade');
    $form->Valida($post['uf'], 'texto', 'Estado');

    $erro = $form->getErro();
    if ($post['siter']) {
        /*if (substr_count($post['siter'], 'http://www.orkut.com/Profile.aspx?uid=') != 1) {
            $erro = 'Este link n�o � de um perfil do orkut';
        }*/
    }

    session_start();
    if (isset($_SESSION['tempoform'])) {
        if ($_SESSION['tempoform'] > time()) {
            $tempo = $_SESSION['tempoform'] - time();
            $erro = 'Aguarde ' . $tempo . ' segundos para enviar novamente';
        }
    }

    if (!$erro) {
        $img = new UploadImg($_FILES['foto']);
        //if($img->getTamanho() > 0){
        $img->setQuali(85);
        $img->setLargura(400);
        $img->setAltura(300);
        $img->setFixa('altura');
        $img->setNome(date('dmYhis'));
        $maior = $img->Gera($dirfoto);

        $img->setLimite(true);
        $img->setLargura(100);
        $img->setAltura(75);
        $mini = $img->Gera($dirfoto . 'mini/');

        if ((!$maior) || (!$mini)) {
            $erro = $img->getErro();
            if ($erro == 'Selecione uma imagem') {
                $erro = 'Selecione uma foto (jpg, png ou gif)';
            }
        }
        //}

        $totalemail = $sql->Totalreg("SELECT * FROM galeranet WHERE email='$post[email]'LIMIT 1");
        if ($totalemail > 0) {
            $erro = 'Este e-mail j� est� cadastrado';
        }

        if (!$erro) {
            $foto = $img->getNome();
            $ip = Ip();
            $post['nascimento'] = $data->DataMysql($post['nascimento']);

            //smiles
            //for($i = 1; $i < 40; $i++){
            //	$post[recado] = str_replace(' :'.$i.': ','<img src="'.$dirsmiles.$i.'" />',$post[recado]);
            //}

            $sql->Consulta("INSERT INTO galeranet
			(id_estado,estado,nome,nascimento,cidade,email,msn,siter,foto,data,hora,ip)
			VALUES
			('$post[uf]','$liberado','$post[nome]','$post[nascimento]','$post[cidade]','$post[email]','$post[msn]','$post[siter]','$foto',NOW(),NOW(),'$ip')");

            $erro = 'Seu cadastro foi efetuado com sucesso, aguarde a libera��o pelo administrador do sistema';
            $class = 'msgaviso';
            $post = NULL;
            $_SESSION['tempoform'] = time() + $tempo_envio;
        }
    }
    $denviado = 'block';
}
?><!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Galera net - Efetuar cadastro - <?= $config['nomesite']; ?></title>
        <meta charset="iso-8859-1">
        <link rel="icon" href="<?= $patchIMG ?>favicon.ico" type="image/x-icon" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">        -->
        <link rel="stylesheet" href="<?= $patchCSS ?>/reset.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/text.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/960_16_col.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/style.css">
        <link href="<?= $patchCSS ?>/galeranet.css" rel="stylesheet" type="text/css" />
        <link href="<?= $patchCSS ?>/mural.css" rel="stylesheet" type="text/css" />
        <link href="<?= $patchCSS ?>/paginacao.css" rel="stylesheet" type="text/css" />
    </head>
    <body>
        <!-- topo -->
        <?php include('topo.php'); ?>
        <div class="container_16 box azul">
            <ul class="breadcrumb grid_16">
                <li><a href="<?= $host ?>" >Inicial</a> <span class="divider">></span></li>
                <li><a href="galeranet.php">Galera net</a> <span class="divider">></span></li>
                <li class="active">Efetuar cadastro</li>
            </ul>
            <?php include('sidebar.php'); ?>
            <div class="grid_13">
                <h2><span class="icon25 recados"></span>Galera net - Efetuar cadastro</h2>
                <div class="content">
                    <!--- corpo -->

                    <!-- cadastro efetuado -->
                    <div class="<?= $class; ?>" style="display: <?= $denviado; ?>; "><?= $erro; ?></div>

                    <form id="form1" name="form1" method="post" action="" enctype="multipart/form-data" onsubmit="DesativaBot('bot')">
                        <table width="476" border="0" cellspacing="0" cellpadding="1" class="tabelacadastro">
                            <tr>
                                <td width="150" align="right">* Nome:</td>
                                <td width="326">
                                    <input type="text" name="nome" class="formgalera" maxlength="40" value="<?= $post['nome']; ?>" /></td>
                            </tr>
                            <tr>
                                <td align="right">* E-mail:</td>
                                <td><input type="text" name="email" class="formgalera" maxlength="60" value="<?= $post['email']; ?>" /></td>
                            </tr>
                            <tr>
                                <td align="right">Link da sua Rede social:</td>
                                <td><input type="text" name="siter" class="formgalera" maxlength="80" value="<?= $post['siter']; ?>" /></td>
                            </tr>
                            <tr>
                                <td align="right">* Data de nascimento: </td>
                                <td class="obsform"><input type="text" name="nascimento" onkeypress="Mdata(this)" class="formgalera" maxlength="10" style="width: 80px;" value="<?= $post['nascimento']; ?>" /> DD/MM/AAAA</td>
                            </tr>
                            <tr>
                                <td align="right">* Cidade:</td>
                                <td><input type="text" name="cidade" class="formgalera" maxlength="60" value="<?= $post['cidade']; ?>" /></td>
                            </tr>
                            <tr>
                                <td align="right">* Estado:</td>
                                <td>
                                    <select name="uf" class="formgalera">
                                        <option value="0">Selecione seu estado</option>
                                        <?php
                                        $querye = $sql->Consulta("SELECT * FROM estados WHERE estado='1' ORDER BY nome_estado ASC");
                                        while ($linha = mysql_fetch_array($querye)) {
                                            $sel = NULL;
                                            if ($post['uf'] == $linha['id']) {
                                                $sel = ' selected="selected"';
                                            }
                                            print '<option value="' . $linha['id'] . '"' . $sel . '>' . $linha['nome_estado'] . '</option>';
                                        }
                                        ?>
                                    </select>
                                </td>
                            </tr>
                            <tr>
                                <td align="right">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td align="right">* Foto:</td>
                                <td><input name="foto" type="file" class="formgalera" /></td>
                            </tr>
                            <tr>
                                <td height="31">&nbsp;</td>
                                <td class="campoobr" valign="top">* Campos obrigat&oacute;rios </td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center"><input type="submit" name="EnviaCadastro" value="Enviar" id="bot" class="galerabotaoform" /></td>
                            </tr>
                        </table>
                    </form>


                    <div class="clear"></div>
                    <div class="ads largo push_2" title="Anuncie" style="margin-top: 20px;">
                        <?php
                        if (isset($banners['9'])) {
                            print $banners['9'];
                        }
                        ?>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
        <?php include('rodape.php'); ?>
        <script type="text/javascript" src="<?=$patchJS ?>galeranet.js"></script>
    </body>
</html>
