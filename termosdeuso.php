<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new data;
include('banners.php');
?><!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Termos de uso  - <?= $config['nomesite']; ?></title>
        <meta charset="iso-8859-1">
        <link rel="icon" href="<?= $patchIMG ?>favicon.ico" type="image/x-icon" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">        -->
        <link rel="stylesheet" href="<?= $patchCSS ?>/reset.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/text.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/960_16_col.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/style.css">
        <link href="<?= $patchCSS ?>/dicasvideos.css" rel="stylesheet" type="text/css" />
        <link href="<?= $patchCSS ?>/paginacao.css" rel="stylesheet" type="text/css" />
        <link href="<?= $patchCSS ?>/galeriav4.css" rel="stylesheet" type="text/css" />
    </head>
    <body>

        <!-- topo -->
        <?php include('topo.php'); ?>
        <div class="container_16 box preto">

            <ul class="breadcrumb grid_16">
                <li><a href="<?= $host ?>" >Inicial</a> <span class="divider">></span></li>
                <li class="active">Termos de uso</li>
            </ul>

            <?php include('sidebar.php'); ?>
            <div class="grid_13">
                <h2>Termos de uso</h2>
                <div class="content">

                    <div class="fundocentro">
                        <div class="termos">
                            Para que todos possam usufruir dos servi�os oferecidos pelo voceemfoco.com.br
                            pedimos que leiam atentamente os termos de uso:<br>
                            <br>
                            1)&nbsp;Voc&ecirc; aceita n&atilde;o colocar qualquer mensagem abusiva,
                            obscena, vulgar, insultuosa, de &oacute;dio, amea&ccedil;adora, sexualmente
                            tendenciosa ou qualquer outro material que possa violar qualquer lei
                            aplic&aacute;vel nos espa�os para envio de mensagens.Os endere&ccedil;os de IP de todas as mensagens s&atilde;o
                            registrados para ajudar a implementar essas condi&ccedil;&otilde;es.
                            <br>
                            <br>
                            2)&nbsp;Voc&ecirc; concorda que quem faz e mant&eacute;m estas p&aacute;ginas,
                            administradores e diretores t&ecirc;m o direito de remover e editar
                            qualquer mensagem em qualquer momento que eles assim o decidam e seja
                            impl&iacute;cito. <br>
                            <br>
                            3)&nbsp;Como Usu&aacute;rio voc&ecirc; aceita que qualquer informa&ccedil;&atilde;o
                            seja guardada num Banco de Dados. Apesar dessa informa&ccedil;&atilde;o
                            n&atilde;o ser fornecida a terceiros sem a sua autoriza&ccedil;&atilde;o,
                            o encarregado das p&aacute;ginas, administradores ou moderadores n&atilde;o
                            podem assumir a responsabilidade por qualquer tentativa ou ato de 'hacking',
                            intromiss&atilde;o for&ccedil;ada e ilegal que conduza a exposi&ccedil;&atilde;o
                            dessa informa&ccedil;&atilde;o.<br>
                            <br>
                            4)&nbsp;O Usu&aacute;rio reconhece e concorda que o voceemfoco.com.br poder&aacute;
                            preservar o Conte&uacute;do do Usu&aacute;rio, mas tamb&eacute;m poder&aacute;
                            divulg&aacute;-lo se desta forma determinar a Lei, ou, de boa f&eacute;,
                            acreditar que a preserva&ccedil;&atilde;o ou a revela&ccedil;&atilde;o
                            seja necess&aacute;ria para:<br>
                            <br>
                            &nbsp;&nbsp;(a) cumprir com algum procedimento legal;<br>
                            &nbsp;&nbsp;(b) para responder reclama&ccedil;&otilde;es de que tal
                            Conte&uacute;do viole Direitos de terceiros; <br>
                            &nbsp;&nbsp;(c) para proteger direitos, propriedades, interesses ou
                            manter a seguran&ccedil;a do voceemfoco.com.br, dos Usu&aacute;rios
                            e do p&uacute;blico em geral.<br>
                            <br>
                            5)&nbsp;O Usu&aacute;rio expressamente concorda e est&aacute; ciente
                            de que o voceemfoco.com.br n&atilde;o tem qualquer responsabilidade por quaisquer
                            danos patrimoniais ou morais causados pelo mal uso ou incapacidade de usar os espa�os envio de mensagens.<br>
                            <br>
                            6)&nbsp; As not�cias publicadas na sess�o "not�cias" s�o de interia responsabilidade
                            do Di�rio de Sapezal, sendo assim o voceemfoco.com.br n�o possui nenhum v�nculo com o conte�do destas not�cias.<br />
                            <br />
                            7) Ao estar utilizando estes servi�os voc� indica que leu e concordou, mesmo que tacitamente, com os termos de uso contidos nesta p�gina.
                        </div>
                        <div class="clear"></div>
                        <div class="ads largo push_2" title="Anuncie" style="margin-top: 20px;">
                            <?php
                            if (isset($banners['9'])) {
                                print $banners['9'];
                            }
                            ?>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include('rodape.php'); ?>
        <script type="text/javascript" src="<?=$patchJS ?>geral.js"></script>
    </body>
</html>
