<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new data;
include('banners.php');

$dirfoto = $patchARQ . 'galeranet/';

//paginacao
$totar = $sql->Totalreg("SELECT * FROM galeranet WHERE estado='1'");
if (!isset($_GET['inicio'])) {
    $_GET['inicio'] = 0;
}
$pag = new Paginacao($_GET['inicio']);
$pag->setRegistro(6);
$pag->setTotal($totar);
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('');
$pagatual = $pag->getAtual();

if ($totar > 0) {
    $exmax = $min + $max;
    if ($totar < ($min + $max)) {
        $exmax = $totar;
    }
    $exibindo = 'Exibindo ' . ($min + 1) . ' - ' . $exmax . ' de um total de ' . $totar;
} else {
    $exibindo = 'Nenhum registro encontrado';
}

//listagem
$query = $sql->Consulta("SELECT galeranet.*,estados.uf
FROM galeranet
LEFT JOIN estados ON estados.id=galeranet.id_estado
WHERE galeranet.estado='1' ORDER BY galeranet.id DESC LIMIT $min,$max");
?><!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Galera net  - <?= $config['nomesite']; ?></title>
        <meta charset="iso-8859-1">
        <link rel="icon" href="<?= $patchIMG ?>favicon.ico" type="image/x-icon" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">        -->
        <link rel="stylesheet" href="<?= $patchCSS ?>/reset.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/text.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/960_16_col.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/style.css">
        <link href="<?= $patchCSS ?>/galeranet.css" rel="stylesheet" type="text/css" />
        <link href="<?= $patchCSS ?>/mural.css" rel="stylesheet" type="text/css" />
        <link href="<?= $patchCSS ?>/paginacao.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css" />
    </head>
    <body>
        <!-- topo -->
        <?php include('topo.php'); ?>
        <div class="container_16 box azul">
            <ul class="breadcrumb grid_16">
                <li><a href="<?= $host ?>" >Inicial</a> <span class="divider">></span></li>
                <li class="active">Galera net</li>
            </ul>
            <?php include('sidebar.php'); ?>
            <div class="grid_13">
                <h2><span class="icon25 galera"></span>Galera net</h2>
                <div class="content">
                    <!--- corpo -->

                    <a href="<?=$host?>galeranet/cadastro" title="Cadastre-se" class="btn  azul grid_2">Cadastre-se</a>
                    <a href="galeraBusca.php" title="Buscar galera" class="btn  azul grid_2">Buscar</a>


                    <div class="textCinza text11 grid_12"><?= $exibindo; ?></div>

                    <!-- listagem -->
                    <table width="700" border="0" cellspacing="0" cellpadding="5">
                        <?php
                        while ($linha = mysql_fetch_array($query)) {
                            if ($linha['foto']) {
                                $linha['foto'] = '<a href="' . $dirfoto . $linha['foto'] . '" title="Ampliar foto" class="bordaimg fancybox"><img src="' . $dirfoto . 'mini/' . $linha['foto'] . '" style="float: none; margin: 10px;" /></a>';
                            } else {
                                $linha['foto'] = '<img src="<?=$patchICONES?>fotogaleranet.jpg" class="bordaminiatura" style="float: none; margin: 10px;" alt="Foto n�o cadastrada" />';
                            }
                            if ($linha['msn']) {
                                $linha['msn'] = '<div class="galeramsn" title="Msn">' . $linha['msn'] . '</div>';
                            }
                            if ($linha['siter']) {
                                if (stripos($linha['siter'], 'orkut.com')>0) {
                                    $linha['siter'] = '<div class="galeraorkut"><a href="' . $linha['siter'] . '" target="_blank"><img src="<?=$patchICONES?>gorkut.gif" alt="Ver perfil do orkut" border="0" /></a></div>';
                                }elseif (stripos($linha['siter'], 'facebook.com')>0) {
                                    $linha['siter'] = '<div class="galeraorkut"><a href="' . $linha['siter'] . '" target="_blank"><img src="<?=$patchICONES?>facebook.png" alt="Ver perfil do facebook" border="0" /></a></div>';
                                }elseif (stripos($linha['siter'], 'plus.google.com')>0) {
                                    $linha['siter'] = '<div class="galeraorkut"><a href="' . $linha['siter'] . '" target="_blank"><img src="<?=$patchICONES?>googleplus.png" alt="Ver perfil do google+" border="0" /></a></div>';
                                }else{
                                    $linha['siter'] = '<div class="galeraorkut"><a href="' . $linha['siter'] . '" target="_blank">Ver meu perfil</a></div>';
                                }
                            }
                            ?>
                            <tr>
                                <td width="90" class="bordafundofoto" align="center"><?= $linha['foto']; ?></td>
                                <td align="left" valign="top" class="bordafundodados">
                                    <div class="galeranome"><?= $linha['nome']; ?></div>
                                    <div class="galeracidade"><?= $linha['cidade']; ?> - <?= $linha['uf']; ?></div>
                                    <div class="galeraemail" title="E-mail"><?= $linha['email']; ?></div>
                                    <?= $linha['msn']; ?>
                                    <?= $linha['siter']; ?>
                                </td>
                            </tr>
                            <tr>
                                <td height="15" colspan="2"></td>
                            </tr>
                        <?php } ?>
                    </table>

                    <!-- paginacao -->
                    <div class="grid_16">
                        <?= $paginas; ?>
                    </div>
                    <div class="clear"></div>
                    <div class="ads largo push_2" title="Anuncie" style="margin-top: 20px;">
                        <?php
                        if (isset($banners['9'])) {
                            print $banners['9'];
                        }
                        ?>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include('rodape.php'); ?>
        <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>
    <script type="text/javascript" src="<?=$patchJS ?>galeranet.js"></script>
</body>
</html>
