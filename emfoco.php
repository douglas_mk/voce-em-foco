<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new Data;
include('banners.php');

$dirgata = 'arquivos/emfoco/foto_capa/';

//paginacao
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);

//busca
if(!isset($_GET['busca'])){ $_GET['busca'] = NULL; }
$busca = Sql_inject($_GET['busca']);
$titulob = 'Todos os cadastros';
$vertodos = NULL;
$and = NULL;

if($busca){
	$and = " AND (gataemfoco.nome LIKE '%$busca%' OR gataemfoco.cidade LIKE '%$busca%')";
	$pag->AddLink('busca',$busca);
	$titulob = 'Resultados da busca por: '.$busca;
}

$totalr = $sql->Totalreg("SELECT * FROM gataemfoco WHERE estado=1 $and");
if(($busca) && ($totalr == 0)){ 
	$vertodos = '<a href="gataemfoco.php" title="Ver todas as gatas em foco">Todas as gatas em foco</a>';
}

$pag->setRegistro(8);
$pag->setTotal($totalr); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('');
$pagatual = $pag->getAtual();

if($totalr > 0){
	$exmax = $min + $max;
	if($totalr < ($min + $max)){ $exmax = $totalr; }
	$exibindo = '&nbsp;&nbsp;I&nbsp;&nbsp;Exibindo '.($min + 1).' - '.$exmax.' de um total de '.$totalr; 
}else{
	$exibindo = '&nbsp;&nbsp;I&nbsp;&nbsp;Nenhum cadastro encontrado';
}

//listagem
$query = $sql->Consulta("SELECT * FROM gataemfoco WHERE estado=1 $and ORDER BY id DESC LIMIT $min,$max");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Gata/Gato em foco  - <?=$config['nomesite'];?></title>
<link href="<?= $patchCSS ?>/geral.css" rel="stylesheet" type="text/css" />
<link href="css/gataemfoco.css" rel="stylesheet" type="text/css" />
<link href="<?= $patchCSS ?>/paginacao.css" rel="stylesheet" type="text/css" />
<link href="<?= $patchCSS ?>/amplia.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?=$patchJS ?>geral.js"></script>
<script type="text/javascript" src="<?=$patchJS ?>amplia.js"></script>
</head>

<body>
<table width="750" border="0" cellspacing="0" cellpadding="0" align="center" class="fundosite">
  <tr>
    <td height="180" colspan="2">
		<!-- topo -->
		<?php @include('topo.php'); ?>
		</td>
  </tr>
  <tr>
    <td width="180" align="center" valign="top">
		<!-- menu -->
		<?php include('menu.php'); ?>
	</td>
    <td width="560" align="center" valign="top" class="corpo">
		<!--- corpo -->
		<div class="bannercentral"><?php if(isset($banners['8'])) { print $banners['8']; } ?></div>
		<div class="caixatopointerno">Gata/Gato em foco</div>
		<div class="caminho">
			<a href="<?= $host ?>" class="linkcaminho">Inicial</a><img src="<?=$patchICONES?>setacaminho.jpg" />Gata/Gato em foco
		</div>

		<div class="fundocentro">
			<!-- busca -->
			<div class="fundobusca">
				<div style="float: left;"><a href="javascript:ExibeBusca();" title="Exibir busca"><img src="<?=$patchICONES?>buscar.jpg" alt="Buscar" border="0" /></a>
					<div id="formbusca" class="caixabusca" style="display: none; width: 240px;">
					<form action="emfoco.php" method="get" name="busca" id="busca" style="margin: 0px; display: block;">
						<input type="text" name="busca" maxlength="25" class="formbusca" value="<?=$busca;?>" title="Informe um termo para buscar" />
						<input type="submit" value="Buscar" name="BuscaT" class="botaobusca" style="width: 100px;"> 
					</form>
					</div>
				</div>
				<div class="vertodosbusca"><?=$vertodos;?></div>
			</div>
			
			<div class="fundoexibindo"><?=$titulob;?><span class="exibindo"><?=$exibindo;?></span></div>
			
			<!-- listagem -->
			<?php 
			while($linha = mysql_fetch_array($query)){ 
				$nomegata = explode(' ',$linha['nome']);
				$url = $linha['tipo'] == 1 ? 'gataemfocoPerfil.php' : 'gatoemfocoPerfil.php';
			?>
				<div class="bordagatalista">
					<img src="<?=$dirgata.$linha['foto_capa'];?>" alt="<?=$linha['nome'];?>" />
					<div class="barragata"><a href="<?php print $url; ?>?id=<?=$linha['id'];?>">Veja as<br /> fotos</a><div><?=$nomegata[0];?></div></div>
				</div>
			<?php 
			} 
			?>
			<div style="clear: both;"></div>
			
			<!-- paginacao -->
			<div class="fundopaginacao">
				<?=$paginas;?>
			</div>
			
			<div class="bannercentral" style="margin-top: 15px;"><?php if(isset($banners['9'])) { print $banners['9']; } ?></div>
		</div>
	</td>
  </tr>
  <tr>
    <td colspan="2"><?php include('rodape.php'); ?></td>
  </tr>
</table>
</body>
</html>