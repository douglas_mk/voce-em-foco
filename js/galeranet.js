function ExibeForm(){
	//document.getElementById('formrecado').style.display = 'block';
	if(document.getElementById('formrecado').style.display == 'none'){
		document.getElementById('formrecado').style.display = 'block';
	}else{
			document.getElementById('formrecado').style.display = 'none';
	}
}

function FechaForm(){
	document.getElementById('formrecado').style.display = 'none';
}

function ContaCaracteres(obj){
	var maximo = 500;
 	var recado = obj.value;
	var qtd = maximo - recado.length;
	if (qtd > 0){
	   document.getElementById('carac').innerHTML = qtd;
	}else {
	  document.getElementById('carac').innerHTML = 0;
	  obj.value = recado.substr(0,maximo);
	}
}

function ContaC(obj){
	ContaCaracteres(obj);
	obj.onkeyup = function(){
		 ContaCaracteres(obj);
	}
}

function ExibeAguarde(){
	document.getElementById('aguarde').style.display = 'block';
}

function ExibeSmiles(){
	var elem = document.getElementById('smiles');

	if(elem.style.display == 'none'){
		elem.style.display = 'block';
	}else{
		elem.style.display = 'none';
	}
}

function AdicionaSmile(img){
	var recado = document.getElementById('trecado').value;
	recado += ' :' +img+ ': ';
	document.getElementById('trecado').value = recado;
}

function MostraEmail(id){
	if(document.getElementById('e' +id).style.display == 'none'){
		document.getElementById('e' +id).style.display = 'inline';
	}else{
			document.getElementById('e' +id).style.display = 'none';
	}
}
$(document).ready(function() {
	$(".fancybox").fancybox();
});