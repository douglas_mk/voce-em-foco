var idAtual = 0;
var exibe = 0;
var zIndex = 2;
var liberaProxima = true;
var timeLoad = false;

var slide = false;
var esperaTmp = false;
var esperaImg = false;
var tmpSlide;

function StatusMin(img){
	document.getElementById('minst').innerHTML = img.getAttribute('alt');
	img.onmouseout = function(){
		document.getElementById('minst').innerHTML = '';
	}
}

function StatusFoto(img){
	document.getElementById('fotost').innerHTML = img.getAttribute('alt');
	img.onmouseout = function(){
		document.getElementById('fotost').innerHTML = '';
	}
}


function ProximaMin(tipo){
    console.info('Load More');
	if(parseInt(tipo) == 1){
		fotoAtual = fotoAtual + 15;
		if(fotoAtual > totalFotos){
			fotoAtual = 0;
		}
	}else{
		fotoAtual = fotoAtual - 15;
		if(fotoAtual < 0){
			fotoAtual = 0;
		}
	}

	 var limite = fotoAtual + 15;
	 if(limite > totalFotos){
		limite = totalFotos;
	 }

	//document.getElementById('miniaturas').innerHTML = '';
	for(var i = fotoAtual; i < limite; i++){
        var fotoMin = diretorio+pastaMiniatura+fotos.fotos[i].img;
        var foto = diretorio+fotos.fotos[i].img;
        var link = document.createElement('a');
        link.setAttribute('href',foto);
        link.setAttribute('class','fancybox-thumbs');
        link.setAttribute('data-fancybox-group','thumb');
        var img = document.createElement('img');
        img.setAttribute('src',fotoMin);
        img.setAttribute('alt','Visualizar');

        link.appendChild(img);

		document.getElementById('miniaturas').appendChild(link);
	}

    if(fotoAtual+15 >= totalFotos){
        document.getElementById('btnCarregarMaisFotos').remove();
    }
}
