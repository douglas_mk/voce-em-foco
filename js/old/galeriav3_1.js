var idatual = 1;
var exibeatual = 1;
var zindex = 10;
var liberaprox = true;
var slide = false;
var esperatmp = false;
var esperaimg = false;
var tmpslide;
var tmpsc;

function Amplia(id){
	if(liberaprox == true){
		id = parseInt(id);	
		
		var foto = document.getElementById('foto' +id);
		var caminho = foto.src.split('/');
		foto = caminho[(caminho.length - 1)];
		
		idatual = id;
		Scroll(id);
		
		document.getElementById('exibindo').innerHTML = 'Carregando foto...';
		document.getElementById('load').style.display = 'block';
		
		if(exibeatual == 1){
			document.getElementById('exibe2').innerHTML = '<img src="' +dir+pasta+foto+ '" border="0" onload="FechaLoad(this)" onerror="FechaLoad(this)" alt="' +foto+ '" />';	
		}else{
			document.getElementById('exibe1').innerHTML = '<img src="' +dir+pasta+foto+ '" border="0" onload="FechaLoad(this)" onerror="FechaLoad(this)" alt="' +foto+ '" />';				
		}
		liberaprox = false;
	}
}

function FechaLoad(foto){;
	var caminho = foto.src.split('/');
	nomefoto = caminho[(caminho.length - 1)];
	
	document.getElementById('load').style.display = 'none';	
	document.getElementById('exibindo').innerHTML = 'Exibindo foto ' +idatual+ ' de ' +totalfotos+ ' (' +nomefoto+ ')';
	
	zindex++;
	if(exibeatual == 1){
		setOpacidade(0,'exibe2');	
		document.getElementById('exibe2').style.zIndex = zindex;
		setFade('exibe2',0,100,500);
		exibeatual = 2;
	}else{
		setOpacidade(0,'exibe1');	
		document.getElementById('exibe1').style.zIndex = zindex;
		setFade('exibe1',0,100,500);
		exibeatual = 1;	
	}
	setTimeout('LiberaProxima()',510);
}

function LiberaProxima(){
	liberaprox = true;	
	Slide();
}

function setFade(id,inicio,fim,tempo){
	
 	var velocidade = Math.round(tempo / 100); 
    var timer = 0; 	
	
	if(inicio > fim){
        for(i = inicio; i >= fim; i--) { 
            setTimeout('setOpacidade(' + i + ',\'' + id + '\')',(timer * velocidade)); 
            timer++; 
        } 
	}
	if(fim > inicio){
        for(i = inicio; i <= fim; i++) { 
            setTimeout('setOpacidade(' + i + ',\'' + id + '\')',(timer * velocidade)); 
            timer++; 
        } 
	}
}

function setOpacidade(opacidade,id) { 
    var obj = document.getElementById(id).style; 
    obj.opacity = (opacidade / 100); 
    obj.MozOpacity = (opacidade / 100); 
    obj.KhtmlOpacity = (opacidade / 100); 
    obj.filter = 'alpha(opacity=' + opacidade + ')'; 
} 

function Proxima(tipo){
	ParaSlide();
	if(parseInt(tipo) == 1){
		var id = idatual + 1;
		if(id > totalfotos){ id = totalfotos; }
	}else{
		var id = idatual - 1;	
		if(id < 1){ id = 1; }
	}	
	Amplia(id);
}

function ProximaSlide(){
	var id = idatual + 1;
	if(id > totalfotos){ id = totalfotos; }
	Amplia(id);
}

function Scroll(id){
	document.getElementById('miniaturas').scrollLeft = (id * 84) - (84 * 3);
}

function PosScroll(tipo){
	
	var sc = document.getElementById('miniaturas').scrollLeft;
	if(tipo == 1){
		if(sc < ((totalfotos * 84) - (84 * 3))){
			document.getElementById('miniaturas').scrollLeft = sc + 4;
			tmpsc = setTimeout('PosScroll(' +tipo+')',10);
		}
	}else{
		if(sc > 0){
			document.getElementById('miniaturas').scrollLeft = sc - 4;
			tmpsc = setTimeout('PosScroll(' +tipo+')',10);
		}	
	}
}

function MaisMin(tipo,obj){
	tipo = parseInt(tipo);
	PosScroll(tipo);		
	obj.onmouseout = function(){
		clearTimeout(tmpsc);
	}
}

// slide ----------------------------------------------------------------------------

function IniciaSlide(){
	if(slide == false){
		slide = true;
		document.getElementById('botslide').innerHTML = 'Parar slide';
		document.getElementById('controle').style.visibility = 'hidden';
		Slide();
	}else{
		ParaSlide();
	}	
}

function ParaSlide(){
	slide = false;
	document.getElementById('botslide').innerHTML = 'Iniciar slide';
	clearTimeout(tmpslide);	
	document.getElementById('controle').style.visibility = 'visible';
}

function Slide(){
	if(slide == true){
		esperatmp = false;
		esperaimg = false;
		
		//precarrega
		if((idatual + 1) <= totalfotos){	
			var foto = document.getElementById('foto' +(idatual + 1));
			var caminho = foto.src.split('/');
			foto = caminho[(caminho.length - 1)];
			
			if(exibeatual == 1){
				document.getElementById('exibe2').innerHTML = '<img src="' +dir+pasta+foto+ '" border="0" onload="LiberaImg()" onerror="LiberaImg()" alt="' +foto+ '" />';	
			}else{
				document.getElementById('exibe1').innerHTML = '<img src="' +dir+pasta+foto+ '" border="0" onload="LiberaImg()" onerror="LiberaImg()" alt="' +foto+ '" />';				
			}		
			tmpslide = setTimeout('LiberaSlide()',4000);	
		}else{
			ParaSlide();	
		}
	}
}

function LiberaSlide(){
	esperatmp = true;
	if(esperaimg == true){
		ProximaSlide();	
	}
}

function LiberaImg(){
	esperaimg = true;
	if(esperatmp == true){
		ProximaSlide();	
	}
}

//-----------------------------------------------------------------------------------

function Teclap(e){
	var tecla = 0;
	
	if(window.event){ // IE
		tecla = window.event.keyCode;
	}else if(e.keyCode){ // Netscape/Firefox/Opera
		tecla = e.keyCode;
	}
	
	switch(tecla){
		case 37: Proxima(0); break;	
		case 39: Proxima(1); break;	
		case 83: IniciaSlide();  break;
		break;	
	}
}

document.onkeydown = Teclap;