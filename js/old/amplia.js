function Amplia(img){
	
	var aimg = document.createElement('div');

	aimg.setAttribute('id','loadimg'); 
	document.body.appendChild(aimg);
	
	document.getElementById('loadimg').className = 'bordaload';
	
	var topo = document.documentElement.scrollTop;
	
	var insere = '<div class="imgload">Carregando...</div><img src="' +img+ '" width="1" height="1" border="0" style="visibility: hidden;" onload="FechaAmplia(this)" />';
	
	document.getElementById('loadimg').innerHTML = insere;
	document.getElementById('loadimg').style.left = (posx + 15) + 'px';		
	document.getElementById('loadimg').style.top = (posy + topo) - 15 + 'px';

	document.getElementById('loadimg').style.display = 'block';
}

function FechaAmplia(img){
	//fecha load
	var destroi = document.getElementById('loadimg');
	document.body.removeChild(destroi);	
	
	//amplia imagem
	var foto = new Image();
	foto.src = img.src;
	
	var imgx = (foto.width + 30);
	var imgy = (foto.height + 30) + 21;
	
	//tamanho tela
	var tamy = document.documentElement.clientHeight;
	var tamx = document.documentElement.clientWidth;	
	if (window.opera){ 
		var tamy = document.body.clientHeight; 
		var tamx = document.body.clientWidth;	
	} 
	var topo = document.documentElement.scrollTop;
	
	//cria janela
	if(!document.getElementById('amplia')){
		var amplia = document.createElement('div');

		amplia.setAttribute('id','amplia'); 
		document.body.appendChild(amplia);
		
		document.getElementById('amplia').className = 'bordaamplia';
			
		document.getElementById('amplia').onclick = function(){
			FechaImg();	
		}
	}	
	
	//document.getElementById('amplia').style.height = imgy +'px';
	document.getElementById('amplia').style.width = imgx +'px';
	document.getElementById('amplia').style.left = (tamx - imgx) / 2 +'px';	
	document.getElementById('amplia').style.top = ((tamy - imgy) / 2) +topo +'px';
	
	document.getElementById('amplia').innerHTML = '<div class="topoamplia">Clique na foto para fechar</div><img src="' +foto.src+ '" border="0" title="Clique para fechar" class="imgamplia" />';
	document.getElementById('amplia').style.visibility = 'visible';
}

function FechaImg(){
	document.getElementById('amplia').style.visibility = 'hidden';	
}