var idAtual = 0;
var exibe = 0;
var zIndex = 2;
var liberaProxima = true;
var timeLoad = false;

var slide = false;
var esperaTmp = false;
var esperaImg = false;
var tmpSlide;

function StatusMin(img){
	document.getElementById('minst').innerHTML = img.getAttribute('alt');
	img.onmouseout = function(){
		document.getElementById('minst').innerHTML = '';
	}
}

function StatusFoto(img){
	document.getElementById('fotost').innerHTML = img.getAttribute('alt');
	img.onmouseout = function(){
		document.getElementById('fotost').innerHTML = '';
	}
}


function ProximaMin(tipo){
    console.info('Load More');
	if(parseInt(tipo) == 1){
		fotoAtual = fotoAtual + 15;
		if(fotoAtual > totalFotos){
			fotoAtual = 0;
		}
	}else{
		fotoAtual = fotoAtual - 15;
		if(fotoAtual < 0){
			fotoAtual = 0;
		}
	}

	 var limite = fotoAtual + 15;
	 if(limite > totalFotos){
		limite = totalFotos;
	 }

	//document.getElementById('miniaturas').innerHTML = '';
	for(var i = fotoAtual; i < limite; i++){
        var fotoMin = diretorio+pastaMiniatura+fotos.fotos[i].img;
        var foto = diretorio+fotos.fotos[i].img;
        // var link = document.createElement('a');
        // link.setAttribute('href',foto);
        // link.setAttribute('class','fancybox-thumbs');
        // link.setAttribute('data-fancybox','thumb');
        var link = $('#miniaturas > a:nth-child('+i+')');
        var img = document.createElement('img');
        img.setAttribute('src',link.data('mini'));
        img.setAttribute('alt','Visualizar');

        link.append(img);


		// document.getElementById('miniaturas').appendChild(link);
	}

    if(fotoAtual+15 >= totalFotos){
        document.getElementById('btnCarregarMaisFotos').remove();
    }

	return true;
}



$(".barraperfil a").click(function() {
	$(".barraperfil a").toggleClass("linkbarra")
	$(".barraperfil a").toggleClass("linkbarradesat")
});

$(document).ready(function () {
	// $.fancybox.defaults.hash = false;
	$("[data-fancybox]").fancybox({
		slideShow  : true,
		fullScreen : true,
		thumbs     : true,
		loop       : false,

		onComplete: function (instance, slide) {
			if ((this.index  >= $('#miniaturas > a:not(#miniaturas > a:empty)').length - 5) && ($('#btnCarregarMaisFotos').length > 0)) {
				ProximaMin(1);
				var index = this.index+1;
			}
		},

        caption : function( instance, item ) {
            var caption, link, filename;
            filename = (item.src.split('/')[item.src.split('/').length-2])+'_._'+(item.src.split('/')[item.src.split('/').length-1]);

            if ( item.type === 'image' ) {
                caption = $(this).data('caption');
                link    = '<a href="/dl/'+filename+'" class="btn">Baixar esta foto</a>';

                return (caption ? caption + '<br />' : '') + link;
            }
        }
	});
});
