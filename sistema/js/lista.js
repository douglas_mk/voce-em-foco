function Detalhes(id){
	var elem = document.getElementById('list' +id);
	
	if(elem.style.display == 'none'){
		elem.style.display = 'block';
		OcultaListas(0);
		document.getElementById('borderlist' +id).style.display = 'block';
		document.getElementById('detalhedados').style.display = 'block';
		document.getElementById('detalhedados').innerHTML = 'Voc� est� visualizando detalhes do registro n�mero '+id+', para fechar clique em Exibir/Ocultar detalhes';
	}else{
		elem.style.display = 'none';
		OcultaListas(1);
		document.getElementById('detalhedados').style.display = 'none';
		document.getElementById('detalhedados').innerHTML = '&nbsp;';
	}
}

function OcultaListas(tipo){
	tipo = parseInt(tipo);
	var listas = document.getElementById('pailista');
	for (var i = 0; i < listas.childNodes.length; i++) {
		node = listas.childNodes[i];
		if (node.nodeName == 'DIV'){
			if(tipo == 0){
				node.style.display = 'none';
			}else{
				node.style.display = 'block';	
			}
		}
	}
}

function ClasseOver(obj){
	var classe = obj.className;
	obj.className = classe + 'over';
	obj.onmouseout = function(){
		obj.className = classe;	
	}
	
}

function Acao(acao,id,ids,pagina){
	if(!document.getElementById('confirma')){
		var aviso = document.createElement('div');

		aviso.setAttribute('id','confirma'); 
		document.body.appendChild(aviso);
		
		document.getElementById('confirma').className = 'bordaconfirma';
	}	
	
	var topo = document.documentElement.scrollTop;
	var tamy = document.documentElement.clientHeight;
	var tamx = document.documentElement.clientWidth;
	
	if(window.opera){
		var topo = document.body.scrollTop;
		var tamy = document.body.clientHeight;
		var tamx = document.body.clientWidth;
	}
	
	var insere = '';
	insere += '<form name="formexcluir" method="post" action="' +pagina+ '" style="margin: 0px;">Confirma opera��o?<br><br>';
	insere += '<input type="submit" name="'+acao+'" value="Sim" class="form">&nbsp;&nbsp;&nbsp;';
	insere += '<input type="hidden" name="acao" value="' +acao+ '">';
	insere += '<input type="hidden" name="id" value="' +id+ '">';
	insere += '<input type="hidden" name="ids" value="' +ids+ '">';
	insere += '<input type="button" name="'+acao+'" value="N�o" class="form" onClick="FechaAcao()" id="botf"></form>';
	
	document.getElementById('confirma').innerHTML = insere;
	document.getElementById('confirma').style.left = (posx + 10) + 'px';		
	document.getElementById('confirma').style.top = (posy + topo) + 'px';
	
	if((posx + 220) > tamx ){
		document.getElementById('confirma').style.left = (posx - 190) + 'px';		
	}
	
	if(((posy + topo) + 80) > (tamy + topo)){
		document.getElementById('confirma').style.top = ((posy + topo) - 80) + 'px';		
	}

	document.getElementById('confirma').style.display = 'block';
	document.getElementById('botf').focus();
}

function FechaAcao(){
	document.getElementById('confirma').style.display = 'none';
}

function ExibeTodos(){
	var elem = document.getElementById('corpo');		
	var div = elem.getElementsByTagName('table');
	for(i = 0; i < div.length; i++){
		if(div[i].id.substr(0,4) == 'list'){
			div[i].style.display = 'block';
		}
	}
	document.getElementById('linkexibe').innerHTML = '<a href="javascript:OcultaTodos();">Ocultar todos</a>';
}

function OcultaTodos(){
	var elem = document.getElementById('corpo');		
	var div = elem.getElementsByTagName('table');
	for(i = 0; i < div.length; i++){
		if(div[i].id.substr(0,4) == 'list'){
			div[i].style.display = 'none';
		}
	}
	document.getElementById('linkexibe').innerHTML = '<a href="javascript:ExibeTodos();">Exibir todos</a>';
}