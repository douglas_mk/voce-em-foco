<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('destaques');
$acesso->getAcesso('cadastrar',true);

$aviso = false;
$erro = false;

$sql = new Mysql;
$data = new Data;
$tpl = new  Template('template/destaque_cadastra.html');

$diretorio = '../../arquivos/destaques/';

if(!isset($_POST['descricao'])){ $_POST['descricao'] = NULL; }

if(isset($_POST['Submit'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['descricao'],'texto','Descri��o');

	$erro = $form->getErro();
	if(!$erro){
		$upload = new Upload($_FILES['arquivo']);
		//image/jpeg,image/pjpeg,image/gif,image/x-png,image/png
		$upload->setTipo('application/x-shockwave-flash');
		$upload->setNome(date('dmyhis'));
		
		if(!$upload->Envia($diretorio)){
			$erro = $upload->getErro();
		}
		
		if(!$erro){
			$tipo = $upload->getTipo();
			$arquivo = $upload->getNome();
			
			$sql->Consulta("INSERT INTO destaques
			(estado,tipo,arquivo,descricao,data_cadastro,link)
			VALUES
			('1','$tipo','$arquivo','$post[descricao]',NOW(),NULL)");
			
			$aviso = 'Destaque cadastrado com sucesso';
			$_POST = NULL;
		}
	}	
}

$tpl->associa('DESCRICAO',$_POST['descricao']);
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>