<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('destaques');
$acesso->getAcesso('listar',true);

$aviso = false;
$erro = false;
$and = NULL;

$sql = new Mysql;
$data = new Data;
$tpl = new Template('template/destaque_lista.html');
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);

$diretorio = '../../arquivos/destaques/';

//busca	
if(isset($_GET['busca'])){
	$and = " AND (destaques.descricao LIKE '%".$_GET['busca']."%' OR destaques.link LIKE '%".$_GET['busca']."%')";
	$pag->AddLink('busca',$_GET['busca']);
}	

//acoes
if(isset($_POST['acao'])){
	$id_destaque = $_POST['id'];
	$id_s = $_POST['ids'];

	switch($_POST['acao']){
		case 'ativar':
			if(!$acesso->getAcesso('editar',false)){
			$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE destaques SET estado='1' WHERE id='$id_destaque' LIMIT 1");
				$aviso = 'Destaque ativado com sucesso';
			}
		break;
		case 'desativar':
			if(!$acesso->getAcesso('editar',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE destaques SET estado='0' WHERE id='$id_destaque' LIMIT 1");
				$aviso = 'Destaque desativado com sucesso';
			}
		break;
		case 'excluir':
			if(!$acesso->getAcesso('excluir',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE destaques SET estado='9' WHERE id='$id_destaque' LIMIT 1");
				$aviso = 'Destaque exclu�do com sucesso';
			}
		break;
	}
}

//paginacao		
$pag->setRegistro(5);
$pag->setTotal($sql->Totalreg("SELECT * FROM destaques WHERE estado!='9' $and")); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('Nenhum registro encontrado');

//lista
$tpl->loop('lista');
$query = $sql->Consulta("SELECT * FROM destaques WHERE estado!='9' $and ORDER BY id DESC LIMIT $min,$max");
while($linha = mysql_fetch_array($query)){
	$tpl->associaloop('ID',$linha['id']);
	$tpl->associaloop('DE',Vazio($linha['link']));
	$arquivo = Banner($diretorio.$linha['arquivo'],255,210,$linha['tipo']);
	$tpl->associaloop('ARQUIVO',$arquivo);
	$tpl->associaloop('DESCRICAO',$linha['descricao']);
	$tpl->associaloop('DATA',$data->MysqlData($linha['data_cadastro']));

	$estado = 'Desativar'; $acao = 'desativar'; $class = 'titulolista';
	if($linha['estado'] == '0'){ $estado = 'Ativar'; $acao = 'ativar'; $class = 'titulolistad'; }
	
	$tpl->associaloop('ACAO',$acao);
	$tpl->associaloop('ESTADO',$estado);
	$tpl->associaloop('CLASSE',$class);
	$tpl->processaloop();
}
$tpl->fechaloop();

$tpl->associa('PAGINACAO',$paginas);
$tpl->associa('TOTALREG',$pag->getTotal());
$tpl->associa('MIN',($min + 1));
$tpl->associa('MAX',($min + $max));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>