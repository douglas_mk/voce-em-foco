<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('agenda');
$acesso->getAcesso('cadastrar',true);

$aviso = false;
$erro = false;

$sql = new Mysql;
$data = new Data;
$tpl = new  Template('template/agenda_cadastra.html');

$diretorio = '../../arquivos/agenda/';

if(!isset($_POST['evento'])){ $_POST['evento'] = NULL; }
if(!isset($_POST['uf'])){ $_POST['uf'] = NULL; }
if(!isset($_POST['cidade'])){ $_POST['cidade'] = NULL; }
if(!isset($_POST['animacao'])){ $_POST['animacao'] = NULL; }
if(!isset($_POST['data'])){ $_POST['data'] = NULL; }
if(!isset($_POST['duracao'])){ $_POST['duracao'] = NULL; }
if(!isset($_POST['local'])){ $_POST['local'] = NULL; }
if(!isset($_POST['realizacao'])){ $_POST['realizacao'] = NULL; }
if(!isset($_POST['descricao'])){ $_POST['descricao'] = NULL; }

if(isset($_POST['Submit'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['evento'],'texto','Evento');
	$form->Valida($post['local'],'texto','Local');
	$form->Valida($post['cidade'],'texto','Cidade');
	$form->Valida($post['uf'],'texto','Estado');
	$form->Valida($post['data'],'data','Data');
	$form->Valida($post['duracao'],'texto','Dura��o');
	$form->Valida($post['duracao'],'numerico','Dura��o');

	$erro = $form->getErro();
	if(!$erro){
		//verifica foto da capa
		$cartaz = NULL;
		$img = new UploadImg($_FILES['cartaz']);
		if($img->getTamanho() > 0){
			$img->setQuali(85);
			$img->setLargura(550);
			$img->setAltura(413);
			$img->setFixa('altura');
			$img->setNome(date('dmYhis'));
			$maior = $img->Gera($diretorio);	
			
			$img->setLimite(true);
			$img->setLargura(100);
			$img->setAltura(75);
			$mini = $img->Gera($diretorio.'mini/');	
			
			if((!$maior) || (!$mini)){
				$erro = $img->getErro();
			}else{
				$cartaz =  $img->getNome();	
			}
		}
		
		//cadastra
		if(!$erro){
			$post['data'] = $data->DataMysql($post['data']);
		
			$sql->Consulta("INSERT INTO agenda
			(id_estado,estado,evento,local,cidade,animacao,realizacao,descricao,data,hora,duracao,cartaz)
			VALUES
			('$post[uf]','1','$post[evento]','$post[local]','$post[cidade]','$post[animacao]','$post[realizacao]','$post[descricao]','$post[data]',
			NULL,'$post[duracao]','$cartaz')");
			
			$aviso = 'Evento cadastrado com sucesso';
			$_POST = NULL;
		}
	}
}

//estados
$estado = NULL;
$query = $sql->Consulta("SELECT * FROM estados WHERE estado='1' ORDER BY nome_estado ASC");
while($linha = mysql_fetch_array($query)){
	$sel = NULL;
	if($_POST['uf'] == $linha['id']){ $sel = ' selected="selected"'; }
	$estado .= '<option value="'.$linha['id'].'"'.$sel.'>'.$linha['nome_estado'].'</option>';
}

$tpl->associa('ESTADOS',$estado);
$tpl->associa('EVENTO',$_POST['evento']);
$tpl->associa('LOCAL',$_POST['local']);
$tpl->associa('CIDADE',$_POST['cidade']);
$tpl->associa('ANIMACAO',$_POST['animacao']);
$tpl->associa('REALIZACAO',$_POST['realizacao']);
$tpl->associa('DURACAO',Marcado($_POST['duracao'],false,'1',$_POST['duracao']));
$tpl->associa('DATA',Marcado($_POST['data'],false,date('d/m/Y'),$_POST['data']));
$tpl->associa('DESCRICAO',$_POST['descricao']);
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();

?>