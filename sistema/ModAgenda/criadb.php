<?php
//require('../includes/class.mysql.php');

//$sql = new Mysql;

//agenda
$sql->Consulta("DROP TABLE IF EXISTS agenda");
$sql->Consulta("CREATE TABLE agenda(
	id INT AUTO_INCREMENT PRIMARY KEY,
	id_estado INT (11) NULL,
	estado INT (1) NOT NULL DEFAULT '0',
	evento VARCHAR (255) NOT NULL,
	local VARCHAR (100) NOT NULL,
	cidade VARCHAR (50) NOT NULL,
	animacao VARCHAR (100),
	realizacao VARCHAR (100),
	descricao TEXT NULL,
	data DATE NOT NULL,
	hora TIME NULL,
	duracao INT (2) NOT NULL,
	cartaz VARCHAR (25) NULL)");
?>