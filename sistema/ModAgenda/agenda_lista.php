<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('agenda');
$acesso->getAcesso('listar',true);

$aviso = false;
$erro = false;
$and = NULL;

$sql = new Mysql;
$data = new Data;
$tpl = new Template('template/agenda_lista.html');
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);

$diretorio = '../../arquivos/agenda/';

//busca	
if(isset($_GET['busca'])){
	$and = " AND (agenda.evento LIKE '%".$_GET['busca']."%' OR agenda.cidade LIKE '%".$_GET['busca']."%')";
	$pag->AddLink('busca',$_GET['busca']);
}	

//acoes
if(isset($_POST['acao'])){
	$id_evento = $_POST['id'];
	$id_s = $_POST['ids'];

	switch($_POST['acao']){
		case 'ativar':
			if(!$acesso->getAcesso('editar',false)){
			$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE agenda SET estado='1' WHERE id='$id_evento' LIMIT 1");
				$aviso = 'Evento ativado com sucesso';
			}
		break;
		case 'desativar':
			if(!$acesso->getAcesso('editar',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE agenda SET estado='0' WHERE id='$id_evento' LIMIT 1");
				$aviso = 'Evento desativado com sucesso';
			}
		break;
		case 'excluir':
			if(!$acesso->getAcesso('excluir',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE agenda SET estado='9' WHERE id='$id_evento' LIMIT 1");
				$aviso = 'Evento exclu�do com sucesso';
			}
		break;
	}
}

//paginacao		
$pag->setRegistro(5);
$pag->setTotal($sql->Totalreg("SELECT * FROM agenda WHERE estado!='9' $and")); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('Nenhum registro encontrado');

//lista
$tpl->loop('lista');
$query = $sql->Consulta("SELECT agenda.*,estados.nome_estado
FROM agenda
LEFT JOIN estados ON estados.id=agenda.id_estado
WHERE agenda.estado!='9' $and ORDER BY agenda.id DESC LIMIT $min,$max");
while($linha = mysql_fetch_array($query)){
	$tpl->associaloop('ID',$linha['id']);
	$tpl->associaloop('EVENTO',$linha['evento']);
	$tpl->associaloop('LOCAL',Vazio($linha['local']));
	$tpl->associaloop('CIDADE',Vazio($linha['cidade']));
	$tpl->associaloop('UF',Vazio($linha['nome_estado']));
	$tpl->associaloop('ANIMACAO',Vazio($linha['animacao']));
	$tpl->associaloop('REALIZACAO',Vazio($linha['realizacao']));
	$tpl->associaloop('DESCRICAO',Vazio(nl2br($linha['descricao'])));
	$tpl->associaloop('CARTAZ',Vazio(LinkFoto($diretorio,$linha['cartaz'],1)));
	
	
	//data	
	$linha['data'] = $data->MysqlData($linha['data']);
	if($linha['duracao'] > 1){
		$linha['data'] = 'De '.$linha['data'].' at� '.$data->SomaData($linha['data'],($linha['duracao'] - 1));
	}
	$tpl->associaloop('DATA',$linha['data']);
	
	$infodata = NULL;
	$difdata = $data->Diferenca($linha['data'],date('d/m/Y'));
	if($difdata < 0){
		$infodata = '(Evento j� realizado)';
	}
	$tpl->associaloop('INFODATA',$infodata);

	$estado = 'Desativar'; $acao = 'desativar'; $class = 'titulolista';
	if($linha['estado'] == '0'){ $estado = 'Ativar'; $acao = 'ativar'; $class = 'titulolistad'; }
	
	$tpl->associaloop('ACAO',$acao);
	$tpl->associaloop('ESTADO',$estado);
	$tpl->associaloop('CLASSE',$class);
	$tpl->processaloop();
}
$tpl->fechaloop();

$tpl->associa('PAGINACAO',$paginas);
$tpl->associa('TOTALREG',$pag->getTotal());
$tpl->associa('MIN',($min + 1));
$tpl->associa('MAX',($min + $max));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>