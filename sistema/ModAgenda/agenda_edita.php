<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('agenda');
$acesso->getAcesso('editar',true);

$aviso = false;
$erro = false;

$id_evento = (int) $_GET['id'];

$sql = new Mysql;
$data = new Data;
$tpl = new  Template('template/agenda_edita.html');

$diretorio = '../../arquivos/agenda/';

if(isset($_POST['Submit'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['evento'],'texto','Evento');
	$form->Valida($post['local'],'texto','Local');
	$form->Valida($post['cidade'],'texto','Cidade');
	$form->Valida($post['uf'],'texto','Estado');
	$form->Valida($post['data'],'data','Data');
	$form->Valida($post['duracao'],'texto','Dura��o');
	$form->Valida($post['duracao'],'numerico','Dura��o');
	$id_evento = $post['id'];

	$erro = $form->getErro();
	if(!$erro){
		//verifica foto da capa
		$cartaz = NULL;
		$ins = NULL;
		$img = new UploadImg($_FILES['cartaz']);
		if($img->getTamanho() > 0){
			$img->setQuali(85);
			$img->setLargura(550);
			$img->setAltura(413);
			$img->setFixa('altura');
			$img->setNome(date('dmYhis'));
			$maior = $img->Gera($diretorio);	
			
			$img->setLimite(true);
			$img->setLargura(100);
			$img->setAltura(75);
			$mini = $img->Gera($diretorio.'mini/');	
			
			if((!$maior) || (!$mini)){
				$erro = $img->getErro();
			}else{
				$cartaz =  $img->getNome();	
				$ins = "cartaz='$cartaz',";
			}
		}
		
		//cadastra
		if(!$erro){
			$post['data'] = $data->DataMysql($post['data']);
		
			$sql->Consulta("UPDATE agenda SET
			id_estado='$post[uf]',
			evento='$post[evento]',
			local='$post[local]',
			cidade='$post[cidade]',
			animacao='$post[animacao]',
			realizacao='$post[realizacao]',
			descricao='$post[descricao]',
			data='$post[data]',
			$ins
			duracao='$post[duracao]'
			WHERE id='$id_evento' LIMIT 1");
			
			$aviso = 'Evento atualizado com sucesso';
		}
	}
}

//dados
$query = $sql->Consulta("SELECT * FROM agenda WHERE id='$id_evento' LIMIT 1");
$agenda = mysql_fetch_array($query);

//estados
$estado = NULL;
$query = $sql->Consulta("SELECT * FROM estados WHERE estado='1' ORDER BY nome_estado ASC");
while($linha = mysql_fetch_array($query)){
	$sel = NULL;
	if($agenda['id_estado'] == $linha['id']){ $sel = ' selected="selected"'; }
	$estado .= '<option value="'.$linha['id'].'"'.$sel.'>'.$linha['nome_estado'].'</option>';
}

$tpl->associa('CARTAZ',Vazio(LinkFoto($diretorio,$agenda['cartaz'],0)));
$tpl->associa('ID',$id_evento);
$tpl->associa('SUBTITULO',FormataTitulo($agenda['evento']));
$tpl->associa('ESTADOS',$estado);
$tpl->associa('EVENTO',$agenda['evento']);
$tpl->associa('LOCAL',$agenda['local']);
$tpl->associa('CIDADE',$agenda['cidade']);
$tpl->associa('ANIMACAO',$agenda['animacao']);
$tpl->associa('REALIZACAO',$agenda['realizacao']);
$tpl->associa('DURACAO',$agenda['duracao']);
$tpl->associa('DATA',$data->MysqlData($agenda['data']));
$tpl->associa('DESCRICAO',$agenda['descricao']);
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();

?>