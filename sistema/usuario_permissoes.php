<?php
require('inc.includes.php');
require('permissoes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('usuarios');
$acesso->getAcesso('aplicar_permissoes',true);

$aviso = false;
$erro = false;

$id_usuario = (int) $_GET['id'];

//modulos
require('modulos.php');
foreach($modulos as $dir){
	if(is_file($dir.'/permissoes.php')){
		include($dir.'/permissoes.php');
	}
}

$sql = new Mysql;
$tpl = new  Template('template/usuario_permissoes.html');

if(isset($_POST['Submit'])){
	$id_usuario = (int) $_POST['id'];
	$per = $_POST['permissoes'];
	
	//deleta permissoes
	$sql->Consulta("DELETE FROM permissoes WHERE id_usuario='$id_usuario'");
		
	if(sizeof($per) > 0){
		//cadastra permissoes
		foreach($per as $indice => $valor){
			$insere = implode(',',$valor);
			$sql->Consulta("INSERT INTO permissoes (id_usuario,local,acessos) VALUES ('$id_usuario','$indice','$insere')");
		}
	}
	$aviso = 'Permissões atualizadas com sucesso. Efetue login para que as alterações sejam aplicadas';
}

//seleciona permissoes
$aplicada = array();
$query = $sql->Consulta("SELECT local,acessos FROM permissoes WHERE id_usuario='$id_usuario'"); 
while($linha = mysql_fetch_array($query)){
	$aplicada[$linha['local']] = $linha['acessos'];
}
			
//permissoes
$adiciona = NULL;
$tpl->loop('lista');
foreach($localp as $indice => $valor){
	$tpl->associaloop('ID',$indice);
	$tpl->associaloop('NOME',$localp[$indice]['area']);
	
	$copia = array();
	if(isset($aplicada[$indice])){
		$copia = explode(',',$aplicada[$indice]);
	}
	foreach($valor as $chave => $val){
		if($chave != 'area'){
			if(array_search($chave,$copia) !== false){ $sel = ' checked'; }else{ $sel = NULL; }
			$adiciona .= '<input type="checkbox" name="permissoes['.$indice.'][]" value="'.$chave.'" '.$sel.'> '.$val.'<br />';
		}
	}

	$tpl->associaloop('PERMISSOES',$adiciona);
	$tpl->processaloop();
	$adiciona = NULL;
}
$tpl->fechaloop();

//dados
$query = $sql->Consulta("SELECT * FROM usuario WHERE id='$id_usuario' LIMIT 1");
$linha = mysql_fetch_array($query);

$tpl->associa('ID',$id_usuario);
$tpl->associa('SUBTITULO',FormataTitulo($linha['nome']));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>