<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('equipe');
$acesso->getAcesso('listar',true);

$aviso = false;
$erro = false;
$and = NULL;

$sql = new Mysql;
$data = new Data;
$tpl = new Template('template/equipe_lista.html');
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);

$diretorio = '../../arquivos/equipe/';

//busca	
if(isset($_GET['busca'])){
	$and = " AND (equipe.nome LIKE '%".$_GET['busca']."%' OR equipe.email LIKE '%".$_GET['busca']."%')";
	$pag->AddLink('busca',$_GET['busca']);
}	

//acoes
if(isset($_POST['acao'])){
	$id_membro = $_POST['id'];
	$id_s = $_POST['ids'];

	switch($_POST['acao']){
		case 'ativar':
			if(!$acesso->getAcesso('editar',false)){
			$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE equipe SET estado='1' WHERE id='$id_membro' LIMIT 1");
				$aviso = 'Membro ativado com sucesso';
			}
		break;
		case 'desativar':
			if(!$acesso->getAcesso('editar',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE equipe SET estado='0' WHERE id='$id_membro' LIMIT 1");
				$aviso = 'Membro desativado com sucesso';
			}
		break;
		case 'excluir':
			if(!$acesso->getAcesso('excluir',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE equipe SET estado='9' WHERE id='$id_membro' LIMIT 1");
				$aviso = 'Membro exclu�do com sucesso';
			}
		break;
	}
}

//paginacao		
$pag->setRegistro(5);
$pag->setTotal($sql->Totalreg("SELECT * FROM equipe WHERE estado!='9' $and")); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('Nenhum registro encontrado');

//lista
$tpl->loop('lista');
$query = $sql->Consulta("SELECT equipe.*
FROM equipe
WHERE equipe.estado!='9' $and ORDER BY equipe.id DESC LIMIT $min,$max");
while($linha = mysql_fetch_array($query)){
	$tpl->associaloop('ID',$linha['id']);
	$tpl->associaloop('NOME',$linha['nome']);
	$tpl->associaloop('MSN',Vazio($linha['msn']));
	$tpl->associaloop('TELEFONER',Vazio($linha['telefoner']));
	$tpl->associaloop('TELEFONEC',Vazio($linha['telefonec']));
	$tpl->associaloop('CELULAR',Vazio($linha['celular']));
	$tpl->associaloop('EMAIL',Vazio($linha['email']));
	$tpl->associaloop('CARGO',Vazio($linha['cargo']));
	$tpl->associaloop('INFO',Vazio(nl2br($linha['informacoes'])));
	$tpl->associaloop('FOTO',Vazio(LinkFoto($diretorio,$linha['foto'],1)));
	$tpl->associaloop('DATACAD',$data->MysqlData($linha['data_cadastro']));

	$estado = 'Desativar'; $acao = 'desativar'; $class = 'titulolista';
	if($linha['estado'] == '0'){ $estado = 'Ativar'; $acao = 'ativar'; $class = 'titulolistad'; }
	
	$tpl->associaloop('ACAO',$acao);
	$tpl->associaloop('ESTADO',$estado);
	$tpl->associaloop('CLASSE',$class);
	$tpl->processaloop();
}
$tpl->fechaloop();

$tpl->associa('PAGINACAO',$paginas);
$tpl->associa('TOTALREG',$pag->getTotal());
$tpl->associa('MIN',($min + 1));
$tpl->associa('MAX',($min + $max));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>