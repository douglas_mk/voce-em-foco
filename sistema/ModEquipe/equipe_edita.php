<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('equipe');
$acesso->getAcesso('editar',true);

$aviso = false;
$erro = false;

$id_membro = (int) $_GET['id'];

$sql = new Mysql;
$data = new Data;
$tpl = new  Template('template/equipe_edita.html');

$diretorio = '../../arquivos/equipe/';

if(isset($_POST['Submit'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['nome'],'texto','Nome');
	if($post['telefoner']){
		$form->Valida($post['telefoner'],'texto','Telefone residencial');
		$form->Valida($post['telefoner'],'numerico','Telefone residencial');
	}
	if($post['telefonec']){
		$form->Valida($post['telefonec'],'texto','Telefone comercial');
		$form->Valida($post['telefonec'],'numerico','Telefone comercial');
	}
	if($post['celular']){
		$form->Valida($post['celular'],'texto','Telefone celular');
		$form->Valida($post['celular'],'numerico','Telefone celular');
	}
	$id_membro = $post['id'];

	$erro = $form->getErro();
	if(!$erro){
		//verifica foto
		$foto = NULL;
		$ins = NULL;
		$img = new UploadImg($_FILES['foto']);
		if($img->getTamanho() > 0){
			$img->setQuali(85);
			$img->setLargura(400);
			$img->setAltura(300);
			$img->setFixa('altura');
			$img->setNome(date('dmYhis'));
			$maior = $img->Gera($diretorio);	
			
			$img->setLimite(true);
			$img->setLargura(100);
			$img->setAltura(75);
			$mini = $img->Gera($diretorio.'mini/');	
			
			if((!$maior) || (!$mini)){
				$erro = $img->getErro();
			}else{
				$foto =  $img->getNome();	
				$ins = "foto='$foto',";
			}
		}
		
		//cadastra
		if(!$erro){
			$sql->Consulta("UPDATE equipe SET
			nome='$post[nome]',
			email='$post[email]',
			msn='$post[msn]',
			telefoner='$post[telefoner]',
			telefonec='$post[telefonec]',
			celular='$post[celular]',
			cargo='$post[cargo]',
			$ins
			informacoes='$post[informacoes]'
			WHERE id='$id_membro' LIMIT 1");
			
			$aviso = 'Membro atualizado com sucesso';
		}
	}
}

//dados
$query = $sql->Consulta("SELECT * FROM equipe WHERE id='$id_membro' LIMIT 1");
$membro = mysql_fetch_array($query);

$tpl->associa('FOTO',Vazio(LinkFoto($diretorio,$membro['foto'],0)));
$tpl->associa('ID',$id_membro);
$tpl->associa('SUBTITULO',FormataTitulo($membro['nome']));
$tpl->associa('NOME',$membro['nome']);
$tpl->associa('EMAIL',$membro['email']);
$tpl->associa('MSN',$membro['msn']);
$tpl->associa('TELEFONER',$membro['telefoner']);
$tpl->associa('TELEFONEC',$membro['telefonec']);
$tpl->associa('CELULAR',$membro['celular']);
$tpl->associa('CARGO',$membro['cargo']);
$tpl->associa('INFORMACOES',$membro['informacoes']);
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>