<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('equipe');
$acesso->getAcesso('cadastrar',true);

$aviso = false;
$erro = false;

$sql = new Mysql;
$data = new Data;
$tpl = new  Template('template/equipe_cadastra.html');

$diretorio = '../../arquivos/equipe/';

if(!isset($_POST['nome'])){ $_POST['nome'] = NULL; }
if(!isset($_POST['email'])){ $_POST['email'] = NULL; }
if(!isset($_POST['msn'])){ $_POST['msn'] = NULL; }
if(!isset($_POST['telefoner'])){ $_POST['telefoner'] = NULL; }
if(!isset($_POST['telefonec'])){ $_POST['telefonec'] = NULL; }
if(!isset($_POST['celular'])){ $_POST['celular'] = NULL; }
if(!isset($_POST['cargo'])){ $_POST['cargo'] = NULL; }
if(!isset($_POST['informacoes'])){ $_POST['informacoes'] = NULL; }

if(isset($_POST['Submit'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['nome'],'texto','Nome');
	$form->Valida($post['cargo'],'texto','Cargo');
	if($post['telefoner']){
		$form->Valida($post['telefoner'],'texto','Telefone residencial');
		$form->Valida($post['telefoner'],'numerico','Telefone residencial');
	}
	if($post['telefonec']){
		$form->Valida($post['telefonec'],'texto','Telefone comercial');
		$form->Valida($post['telefonec'],'numerico','Telefone comercial');
	}
	if($post['celular']){
		$form->Valida($post['celular'],'texto','Telefone celular');
		$form->Valida($post['celular'],'numerico','Telefone celular');
	}

	$erro = $form->getErro();
	if(!$erro){
		//verifica foto
		$foto = NULL;
		$img = new UploadImg($_FILES['foto']);
		if($img->getTamanho() > 0){
			$img->setQuali(85);
			$img->setLargura(400);
			$img->setAltura(300);
			$img->setFixa('altura');
			$img->setNome(date('dmYhis'));
			$maior = $img->Gera($diretorio);	
			
			$img->setLimite(true);
			$img->setLargura(100);
			$img->setAltura(75);
			$mini = $img->Gera($diretorio.'mini/');	
			
			if((!$maior) || (!$mini)){
				$erro = $img->getErro();
			}else{
				$foto =  $img->getNome();	
			}
		}
		
		//cadastra
		if(!$erro){
			$sql->Consulta("INSERT INTO equipe
			(estado,nome,email,msn,telefoner,telefonec,celular,cargo,informacoes,foto,data_cadastro)
			VALUES
			('1','$post[nome]','$post[email]','$post[msn]','$post[telefoner]','$post[telefonec]','$post[celular]','$post[cargo]','$post[informacoes]','$foto',NOW())");
			
			$aviso = 'Membro cadastrado com sucesso';
			$_POST = NULL;
		}
	}
}

$tpl->associa('NOME',$_POST['nome']);
$tpl->associa('EMAIL',$_POST['email']);
$tpl->associa('MSN',$_POST['msn']);
$tpl->associa('TELEFONER',$_POST['telefoner']);
$tpl->associa('TELEFONEC',$_POST['telefonec']);
$tpl->associa('CELULAR',$_POST['celular']);
$tpl->associa('CARGO',$_POST['cargo']);
$tpl->associa('INFORMACOES',$_POST['informacoes']);
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>