<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('enquetes');
$acesso->getAcesso('listar',true);

$aviso = false;
$erro = false;
$and = NULL;

$sql = new Mysql;
$data = new Data;
$tpl = new Template('template/enquete_lista.html');
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);

//busca	
if(isset($_GET['busca'])){
	$and = " AND enquete.titulo LIKE '%".$_GET['busca']."%'";
	$pag->AddLink('busca',$_GET['busca']);
}	

//acoes
if(isset($_POST['acao'])){
	$id_enquete = $_POST['id'];
	$id_s = $_POST['ids'];

	switch($_POST['acao']){
		case 'ativar':
			if(!$acesso->getAcesso('editar',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE enquete SET estado='1' WHERE id='$id_enquete' LIMIT 1");
				$aviso = 'Enquete ativada com sucesso';
			}
		break;
		case 'desativar':
			if(!$acesso->getAcesso('editar',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE enquete SET estado='0' WHERE id='$id_enquete' LIMIT 1");
				$aviso = 'Enquete desativada com sucesso';
			}
		break;
		case 'excluir':
			if(!$acesso->getAcesso('excluir',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE enquete SET estado='9' WHERE id='$id_enquete' LIMIT 1");
				$aviso = 'Enquete exclu�da com sucesso';
			}
		break;
	}
}

//paginacao		
$pag->setRegistro(5);
$pag->setTotal($sql->Totalreg("SELECT * FROM enquete WHERE estado!='9' $and")); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('Nenhum registro encontrado');

//lista
$tpl->loop('lista');
$query = $sql->Consulta("SELECT * FROM enquete WHERE estado!='9' $and ORDER BY id DESC LIMIT $min,$max");
while($linha = mysql_fetch_array($query)){
	$tpl->associaloop('ID',$linha['id']);
	$tpl->associaloop('TITULO',$linha['titulo']);
	$tpl->associaloop('DATA',$data->MysqlData($linha['data_cadastro']));
	
	//validade
	if($linha['data_cadastro'] == $linha['data_validade']){
		$linha['data_validade'] = 'Indeterminada';
	}else{
		$linha['data_validade'] = $data->MysqlData($linha['data_validade']);
		$linha['data_validade'] .= ' ('.$data->Diferenca($linha['data_validade'],$data->MysqlData($linha['data_cadastro'])).' dias)';
	}
	$tpl->associaloop('VALIDADE',$linha['data_validade']);
	
	//opcoes
	$op = NULL;
	$query1 = $sql->Consulta("SELECT * FROM opcoes_enquete WHERE id_enquete='".$linha['id']."'ORDER BY id ASC");
	while($linha1 = mysql_fetch_array($query1)){
		$op .= $linha1['opcao'].' ('.$linha1['votos'].' votos)<br />';
	}
	$tpl->associaloop('OPCOES',$op);
	
	$estado = 'Desativar'; $acao = 'desativar'; $class = 'titulolista';
	if($linha['estado'] == '0'){ $estado = 'Ativar'; $acao = 'ativar'; $class = 'titulolistad'; }
	
	$tpl->associaloop('ACAO',$acao);
	$tpl->associaloop('ESTADO',$estado);
	$tpl->associaloop('CLASSE',$class);
	$tpl->processaloop();
}
$tpl->fechaloop();

$tpl->associa('PAGINACAO',$paginas);
$tpl->associa('TOTALREG',$pag->getTotal());
$tpl->associa('MIN',($min + 1));
$tpl->associa('MAX',($min + $max));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>