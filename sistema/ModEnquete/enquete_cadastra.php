<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('enquetes');
$acesso->getAcesso('cadastrar',true);

$aviso = false;
$erro = false;

$data = new Data;
$tpl = new  Template('template/enquete_cadastra.html');

if(!isset($_POST['titulo'])){ $_POST['titulo'] = NULL; }
if(!isset($_POST['validade'])){ $_POST['validade'] = NULL; }
if(!isset($_POST['qtd'])){ $_POST['qtd'] = NULL; }
if(!isset($_POST['opcao'])){ $_POST['opcao'] = NULL; }

if(isset($_POST['Submit'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['titulo'],'texto','Titulo');
	$form->Valida($post['validade'],'numerico','Validade');
	$form->Valida($post['qtd'],'texto','Quantidade de op��es');
	$opc = $_POST['opcao'];
	
	$erro = $form->getErro();
	
	//valida op��es
	if(!$erro){
		$i = 1;
		foreach($opc as $dados){
			$form->Valida($dados,'texto','Op��o '.$i);
			$i++;
		}
		$erro = $form->getErro();
	}
	
	if(!$erro){
		$sql = new Mysql;
		
		$validade = $data->SomaData($data->getData(),$post['validade']);
		$validade = $data->DataMysql($validade);
		
		//cadastra enquete
		$id_enquete = $sql->Insere("INSERT INTO enquete
		(estado,titulo,data_cadastro,data_validade)
		VALUES
		('1','$post[titulo]',NOW(),'$validade')");
		
		//cadastra opcoes
		foreach($opc as $dados){
			$sql->Insere("INSERT INTO opcoes_enquete
			(id_enquete,opcao,icone,votos)
			VALUES
			('$id_enquete','$dados',NULL,'0')");
		}
		
		$aviso = 'Enquete cadastrada com sucesso';
		$_POST = NULL;
	}
}
//quantidade
$qtd = 0;
for($i = 2; $i <= 15; $i++){
	$sel = NULL;
	if($_POST['qtd'] == $i){ $sel = ' selected="selected"'; }
	$qtd .= '<option value="'.$i.'"'.$sel.'>'.$i.'</option>';
}

//opcoes
$opcoes = NULL;
if(sizeof($_POST['opcao']) == 0){
	$opcoes = 'Quantidade de op��es n�o selecionada';
}else{
	$i = 1;
	foreach($_POST['opcao'] as $dados){
		$opcoes .= 'Op��o '.$i.':';
		$opcoes .= '<br /><textarea name="opcao[]" style="width: 300px; height: 30px; margin-bottom: 8px;" class="form">'.$dados.'</textarea>';
		$opcoes .= '<br />';
		$i++;
	}
}

$tpl->associa('DATA',$data->getData());
$tpl->associa('OPCOES',$opcoes);
$tpl->associa('QTD',$qtd);
$tpl->associa('TITULO',$_POST['titulo']);
$tpl->associa('VALIDADE',Marcado((int) $_POST['validade'],false,0,$_POST['validade']));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>