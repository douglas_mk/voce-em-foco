<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('enquetes');
$acesso->getAcesso('editar',true);

$aviso = false;
$erro = false;

$id_enquete = (int) $_GET['id'];

$data = new Data;
$sql = new Mysql;
$tpl = new  Template('template/enquete_edita.html');

if(isset($_POST['Submit'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['titulo'],'texto','Titulo');
	$form->Valida($post['validade'],'numerico','Validade');
	$opc = $_POST['opcao'];
	$id_enquete = $post['id'];
	
	$erro = $form->getErro();
	
	//valida op��es
	if(!$erro){
		$i = 1;
		foreach($opc as $dados){
			$form->Valida($dados,'texto','Op��o '.$i);
			$i++;
		}
		$erro = $form->getErro();
	}
	
	if(!$erro){
			
		$validade = $data->SomaData($post['datac'],$post['validade']);
		$validade = $data->DataMysql($validade);
		
		//atualiza enquete
		$sql->Consulta("UPDATE enquete SET
		titulo='$post[titulo]',
		data_validade='$validade'
		WHERE id='$id_enquete' LIMIT 1");
		
		$aviso = 'Banner atualizado com sucesso';
		
		//atualiza opcoes
		foreach($opc as $indice => $dados){
			$sql->Insere("UPDATE opcoes_enquete SET opcao='$dados' WHERE id='$indice' LIMIT 1");
		}
		
		$aviso = 'Enquete atualizada com sucesso';
		$_POST = NULL;
	}
}

//opcoes
$i = 1;
$opcoes = NULL;
$query = $sql->Consulta("SELECT * FROM opcoes_enquete WHERE id_enquete='$id_enquete' ORDER BY id ASC");
while($linha = mysql_fetch_array($query)){
	$opcoes .= 'Op��o '.$i.':';
	$opcoes .= '<br /><textarea name="opcao['.$linha['id'].']" style="width: 300px; height: 30px; margin-bottom: 8px;" class="form">'.$linha['opcao'].'</textarea>';
	$opcoes .= '<br />';
	$i++;
}

//seleciona dados
$query = $sql->Consulta("SELECT * FROM enquete WHERE id='$id_enquete' LIMIT 1");
$enquete = mysql_fetch_array($query);
$validade = $data->Diferenca($data->MysqlData($enquete['data_validade']),$data->MysqlData($enquete['data_cadastro']));

$tpl->associa('ID',$id_enquete);
$tpl->associa('VALIDADE',$validade);
$tpl->associa('DATAC',$data->MysqlData($enquete['data_cadastro']));
$tpl->associa('SUBTITULO',FormataTitulo($enquete['titulo']));
$tpl->associa('OPCOES',$opcoes);
$tpl->associa('TITULO',$enquete['titulo']);
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>