<?php
require('inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('usuarios');
$acesso->getAcesso('cadastrar',true);

$aviso = false;
$erro = false;

$tpl = new  Template('template/usuario_cadastra.html');

if(!isset($_POST['nome'])){ $_POST['nome'] = NULL; }
if(!isset($_POST['email'])){ $_POST['email'] = NULL; }
if(!isset($_POST['usuario'])){ $_POST['usuario'] = NULL; }

if(isset($_POST['Submit'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['nome'],'texto','Nome');
	$form->Valida($post['email'],'email','E-mail');
	$form->Valida($post['usuario'],'usuario','Usu�rio');
	$form->Tamanho($post['usuario'],4,12,'Usu�rio');
	$form->Valida($post['senha'],'senha','Nova senha');
	$form->Tamanho($post['senha'],4,12,'Nova senha');
	$form->Compara($post['senha'],$post['rsenha'],'Nova senha','Confirme nova senha');

	$erro = $form->getErro();
	if(!$erro){
		$sql = new Mysql;
		$total = $sql->Totalreg("SELECT * FROM usuario WHERE usuario='$post[usuario]' LIMIT 1");
		if($total == 0){
		
			$post['senha'] = md5($post['senha']);
			
			$sql->Consulta("INSERT INTO usuario 
			(estado,nome,email,usuario,senha,data_cadastro)
			VALUES
			('1','$post[nome]','$post[email]','$post[usuario]','$post[senha]',NOW())");
			
			$aviso = 'Usu�rio cadastrado com sucesso';
			$_POST = NULL;
			
		}else{
			$erro = 'Este usu�rio j� existe, por favor informe outro';
		}
	}
}

$tpl->associa('NOME',$_POST['nome']);
$tpl->associa('EMAIL',$_POST['email']);
$tpl->associa('USUARIO',$_POST['usuario']);
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();

?>