<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('galeranet');
$acesso->getAcesso('editar',true);

$aviso = false;
$erro = false;

$id_cadastro = (int) $_GET['id'];

$sql = new Mysql;
$data = new Data;
$tpl = new  Template('template/galeranet_edita.html');

$diretorio = '../../arquivos/galeranet/';

if(isset($_POST['Submit'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['nome'],'texto','Nome');
	$form->Valida($post['email'],'email','E-mail');
	$form->Valida($post['cidade'],'texto','Cidade');
	$form->Valida($post['uf'],'texto','Estado');
	$form->Valida($post['nascimento'],'data','Data de nascimento');
	$id_cadastro = $post['id'];

	$erro = $form->getErro();
	if(!$erro){
		//verifica foto da capa
		$foto = NULL;
		$ins = NULL;
		$img = new UploadImg($_FILES['foto']);
		if($img->getTamanho() > 0){
			$img->setQuali(85);
			$img->setLargura(400);
			$img->setAltura(300);
			$img->setFixa('altura');
			$img->setNome(date('dmYhis'));
			$maior = $img->Gera($diretorio);	
		
			$img->setLimite(true);
			$img->setLargura(100);
			$img->setAltura(75);
			$mini = $img->Gera($diretorio.'/mini/');	
			
			if((!$maior) || (!$mini)){
				$erro = $img->getErro();
			}else{
				$foto =  $img->getNome();	
				$ins = "foto='$foto',";
			}
		}
		
		//atualiza
		if(!$erro){
			$post['nascimento'] = $data->DataMysql($post['nascimento']);
			$sql->Consulta("UPDATE galeranet SET
			nome='$post[nome]',
			nascimento='$post[nascimento]',
			id_estado='$post[uf]',
			email='$post[email]',
			msn='$post[msn]',
			siter='$post[siter]',
			$ins
			cidade='$post[cidade]'
			WHERE id='$id_cadastro' LIMIT 1");
			
			$aviso = 'Cadastro atualizado com sucesso';
		}
	}
}

//dados
$query = $sql->Consulta("SELECT * FROM galeranet WHERE id='$id_cadastro' LIMIT 1");
$cadastro = mysql_fetch_array($query);

//estados
$estado = NULL;
$query = $sql->Consulta("SELECT * FROM estados WHERE estado='1' ORDER BY nome_estado ASC");
while($linha = mysql_fetch_array($query)){
	$sel = NULL;
	if($cadastro['id_estado'] == $linha['id']){ $sel = ' selected="selected"'; }
	$estado .= '<option value="'.$linha['id'].'"'.$sel.'>'.$linha['nome_estado'].'</option>';
}

$tpl->associa('FOTOCAPA',Vazio(LinkFoto($diretorio,$cadastro['foto'],0)));
$tpl->associa('ID',$id_cadastro);
$tpl->associa('SUBTITULO',FormataTitulo($cadastro['nome']));
$tpl->associa('ESTADOS',$estado);
$tpl->associa('NOME',$cadastro['nome']);
$tpl->associa('ORKUT',$cadastro['siter']);
$tpl->associa('NASCIMENTO',$data->MysqlData($cadastro['nascimento']));
$tpl->associa('CIDADE',$cadastro['cidade']);
$tpl->associa('EMAIL',$cadastro['email']);
$tpl->associa('MSN',$cadastro['msn']);
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>