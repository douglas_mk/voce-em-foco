<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('galeranet');
$acesso->getAcesso('listar',true);

$aviso = false;
$erro = false;
$and = NULL;

$sql = new Mysql;
$data = new Data;
$tpl = new Template('template/galeranet_lista.html');
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);

$diretorio = '../../arquivos/galeranet/';

//busca	
if(isset($_GET['busca'])){
	$and = " AND (galeranet.nome LIKE '%".$_GET['busca']."%' OR galeranet.cidade LIKE '%".$_GET['busca']."%' OR galeranet.email LIKE '%".$_GET['busca']."%')";
	$pag->AddLink('busca',$_GET['busca']);
}	

//acoes
if(isset($_POST['acao'])){
	$id_cadastro = $_POST['id'];
	$id_s = $_POST['ids'];

	switch($_POST['acao']){
		case 'ativar':
			if(!$acesso->getAcesso('editar',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE galeranet SET estado='1' WHERE id='$id_cadastro' LIMIT 1");
				$aviso = 'Cadastro ativado com sucesso';
			}
		break;
		case 'desativar':
			if(!$acesso->getAcesso('editar',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE galeranet SET estado='0' WHERE id='$id_cadastro' LIMIT 1");
				$aviso = 'Cadastro desativado com sucesso';
			}
		break;
		case 'excluir':
			if(!$acesso->getAcesso('excluir',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE galeranet SET estado='9' WHERE id='$id_cadastro' LIMIT 1");
				$aviso = 'Cadastro exclu�do com sucesso';
			}
		break;
	}
}

//paginacao		
$pag->setRegistro(5);
$pag->setTotal($sql->Totalreg("SELECT galeranet.* FROM galeranet WHERE galeranet.estado!='9' $and")); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('Nenhum registro encontrado');

//lista
$tpl->loop('lista');
$query = $sql->Consulta("SELECT galeranet.*,estados.uf 
FROM galeranet 
LEFT JOIN estados ON estados.id=galeranet.id_estado
WHERE galeranet.estado!='9' $and ORDER BY galeranet.id DESC LIMIT $min,$max");
while($linha = mysql_fetch_array($query)){
	$tpl->associaloop('ID',$linha['id']);
	$tpl->associaloop('NOME',$linha['nome']);
	$tpl->associaloop('CIDADE',$linha['cidade']);
	$tpl->associaloop('UF',$linha['uf']);
	$tpl->associaloop('EMAIL',$linha['email']);
	$tpl->associaloop('MSN',Vazio($linha['msn']));
	$tpl->associaloop('ORKUT',Vazio($linha['siter']));
	$tpl->associaloop('FOTO',Vazio(LinkFoto($diretorio,$linha['foto'],1)));
	$tpl->associaloop('DATA',$data->MysqlData($linha['data']));
	$tpl->associaloop('NASCIMENTO',$data->MysqlData($linha['nascimento']));
	$tpl->associaloop('HORA',$linha['hora']);
	$tpl->associaloop('IP',$linha['ip']);

	$estado = 'Desativar'; $acao = 'desativar'; $class = 'titulolista';
	if($linha['estado'] == '0'){ $estado = 'Ativar'; $acao = 'ativar'; $class = 'titulolistad'; }
	
	$tpl->associaloop('ACAO',$acao);
	$tpl->associaloop('ESTADO',$estado);
	$tpl->associaloop('CLASSE',$class);
	$tpl->processaloop();
}
$tpl->fechaloop();

$tpl->associa('PAGINACAO',$paginas);
$tpl->associa('TOTALREG',$pag->getTotal());
$tpl->associa('MIN',($min + 1));
$tpl->associa('MAX',($min + $max));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>