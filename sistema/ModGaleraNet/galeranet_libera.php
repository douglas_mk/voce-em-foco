<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('galeranet');
$acesso->getAcesso('listar',true);

$aviso = false;
$erro = false;

$sql = new Mysql;
$data = new Data;
$tpl = new Template('template/galeranet_libera.html');
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);

$diretorio = '../../arquivos/galeranet/';

if(!isset($_POST['idcadastro'])){ $_POST['idcadastro'] = NULL; }

//excluir
if(isset($_POST['excluir'])){
	$idcadastro = $_POST['idcadastro'];
	
	if(sizeof($idcadastro) == 0){
		$erro = 'Nenhum registro selecionado';
	}else{
		if(!$acesso->getAcesso('excluir',false)){
			$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
		}else{
			//exclui recados
			foreach($idcadastro as $id){
				$sql->Consulta("UPDATE galeranet SET estado='9' WHERE id='$id' LIMIT 1");	
			}
			$aviso = 'Cadastro(s) exclu�do(s) com sucesso';
		}
	}	
}

//ativar
if(isset($_POST['liberar'])){
	$idcadastro = $_POST['idcadastro'];
	
	if(sizeof($idcadastro) == 0){
		$erro = 'Nenhum registro selecionado';
	}else{
		if(!$acesso->getAcesso('editar',false)){
			$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
		}else{
			//exclui recados
			foreach($idcadastro as $id){
				$sql->Consulta("UPDATE galeranet SET estado='1' WHERE id='$id' LIMIT 1");	
			}
			$aviso = 'Cadastro(s) liberado(s) com sucesso';
		}
	}	
}

//paginacao		
$pag->setRegistro(8);
$pag->setTotal($sql->Totalreg("SELECT galeranet.* FROM galeranet WHERE galeranet.estado='0'")); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('Nenhum registro encontrado');

//lista
$tpl->loop('lista');
$query = $sql->Consulta("SELECT galeranet.*,estados.uf 
FROM galeranet 
LEFT JOIN estados ON estados.id=galeranet.id_estado
WHERE galeranet.estado='0' ORDER BY galeranet .id DESC LIMIT $min,$max");
while($linha = mysql_fetch_array($query)){
	$tpl->associaloop('ID',$linha['id']);
	$tpl->associaloop('NOME',$linha['nome']);
	$tpl->associaloop('CIDADE',$linha['cidade']);
	$tpl->associaloop('UF',$linha['uf']);
	$tpl->associaloop('EMAIL',$linha['email']);
	$tpl->associaloop('MSN',Vazio($linha['msn']));
	$tpl->associaloop('ORKUT',Vazio($linha['siter']));
	$tpl->associaloop('FOTO',Vazio(LinkFoto($diretorio,$linha['foto'],1)));
	$tpl->associaloop('DATA',$data->MysqlData($linha['data']));
	$tpl->associaloop('NASCIMENTO',$data->MysqlData($linha['nascimento']));
	$tpl->associaloop('HORA',$linha['hora']);
	$tpl->processaloop();
}
$tpl->fechaloop();

$tpl->associa('PAGINACAO',$paginas);
$tpl->associa('TOTALREG',$pag->getTotal());
$tpl->associa('MIN',($min + 1));
$tpl->associa('MAX',($min + $max));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>