<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('gatafoco');
$acesso->getAcesso('listar',true);

$aviso = false;
$erro = false;
$and = NULL;

$sql = new Mysql;
$data = new Data;
$tpl = new Template('template/gata_lista.html');
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);

$diretorio = '../../arquivos/emfoco/foto_capa';

//busca	
if(isset($_GET['busca'])){
	$and = " AND (gataemfoco.nome LIKE '%".$_GET['busca']."%' OR gataemfoco.cidade LIKE '%".$_GET['busca']."%')";
	$pag->AddLink('busca',$_GET['busca']);
}	

//acoes
if(isset($_POST['acao'])){
	$id_gata = $_POST['id'];
	$id_s = $_POST['ids'];

	switch($_POST['acao']){
		case 'ativar':
			if(!$acesso->getAcesso('editar',false)){
			$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE gataemfoco SET estado='1' WHERE id='$id_gata' LIMIT 1");
				$aviso = 'Gata ativada com sucesso';
			}
		break;
		case 'desativar':
			if(!$acesso->getAcesso('editar',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE gataemfoco SET estado='0' WHERE id='$id_gata' LIMIT 1");
				$aviso = 'Gata desativada com sucesso';
			}
		break;
		case 'excluir':
			if(!$acesso->getAcesso('excluir',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE gataemfoco SET estado='9' WHERE id='$id_gata' LIMIT 1");
				$aviso = 'Galta exclu�da com sucesso';
			}
		break;
	}
}

//paginacao		
$pag->setRegistro(5);
$pag->setTotal($sql->Totalreg("SELECT * FROM gataemfoco WHERE estado!='9' $and")); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('Nenhum registro encontrado');

//lista
$tpl->loop('lista');
$query = $sql->Consulta("SELECT gataemfoco.*,estados.nome_estado,EXTRACT(YEAR FROM nascimento) AS ano,EXTRACT(MONTH FROM nascimento) AS mes,EXTRACT(DAY FROM nascimento) AS dia
FROM gataemfoco
LEFT JOIN estados ON estados.id=gataemfoco.id_estado
WHERE gataemfoco.estado!='9' $and ORDER BY gataemfoco.id DESC LIMIT $min,$max");
while($linha = mysql_fetch_array($query)){
	$tpl->associaloop('ID',$linha['id']);
	$tpl->associaloop('NOME',$linha['nome']);
	$tpl->associaloop('SIGNO',Vazio($linha['signo']));
	$tpl->associaloop('ALTURA',$linha['altura'].' m');
	$tpl->associaloop('QUADRIL',$linha['quadril'].' cm');
	$tpl->associaloop('CINTURA',$linha['cintura'].' cm');
	$tpl->associaloop('BUSTO',$linha['busto'].' cm');
	$tpl->associaloop('PES',Vazio($linha['pes']));
	$tpl->associaloop('LOCAL',Vazio($linha['local_fotos']));
	$tpl->associaloop('CIDADE',Vazio($linha['cidade']));
	$tpl->associaloop('UF',Vazio($linha['nome_estado']));
	$tpl->associaloop('ACESSOS',$linha['acessos']);
	$tpl->associaloop('FOTOGRAFO',Vazio($linha['fotografo']));
	$tpl->associaloop('AGRADEC',Vazio(nl2br($linha['agradecimentos'])));
	$tpl->associaloop('APOIO',Vazio(nl2br($linha['apoio'])));
	$tpl->associaloop('FOTOCAPA',Vazio(LinkFoto($diretorio,$linha['foto_capa'],1)));
	
	$dataatual = date('d/m/Y') ;
	$dataatual = explode('/',$dataatual);
	
	$idade = $dataatual[2] - $linha['ano'];
	if($dataatual[1] <= $linha['mes']){
		$idade--;
		if(($dataatual[1] == $linha['mes']) && ($dataatual[0] >= $linha['dia'])){
			$idade++;
		}
	}
	
	$tpl->associaloop('IDADE',$idade);
	

	$tpl->associaloop('DATA',$data->MysqlData($linha['data']));

	$estado = 'Desativar'; $acao = 'desativar'; $class = 'titulolista';
	if($linha['estado'] == '0'){ $estado = 'Ativar'; $acao = 'ativar'; $class = 'titulolistad'; }
	
	$tpl->associaloop('ACAO',$acao);
	$tpl->associaloop('ESTADO',$estado);
	$tpl->associaloop('CLASSE',$class);
	$tpl->processaloop();
}
$tpl->fechaloop();

$tpl->associa('PAGINACAO',$paginas);
$tpl->associa('TOTALREG',$pag->getTotal());
$tpl->associa('MIN',($min + 1));
$tpl->associa('MAX',($min + $max));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>