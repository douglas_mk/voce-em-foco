<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('gatafoco');
$acesso->getAcesso('listar',true);

$aviso = false;
$erro = false;


$id_gata = (int) $_GET['id'];

$sql = new Mysql;
$tpl = new Template('template/gata_lista_foto.html');
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);
$pag->AddLink('id',$id_gata);

$diretorio = '../../arquivos/emfoco/';

//dados
$query = $sql->Consulta("SELECT * FROM gataemfoco WHERE id='$id_gata' LIMIT 1");
$gata = mysql_fetch_array($query);
$diretorio .= $gata['pasta'].'/';

//acoes
if(isset($_POST['acao'])){
	$id_galeria = $_POST['id'];
	$fotoex = $_POST['ids'];

	switch($_POST['acao']){
		case 'excluir':
			if(!$acesso->getAcesso('excluir',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				@unlink($diretorio.$fotoex);
				@unlink($diretorio.'mini/'.$fotoex);
				$aviso = 'Foto exclu�da com sucesso';
			}
		break;
	}
}

//ler fotos
$fotos = array();

$aberto = @opendir($diretorio.'mini/'); 
while($arq = @readdir($aberto)) {
	if(($arq != '.') && ($arq != '..')) {
		$ext = explode('.',$arq); 
		$ext = strtolower($ext[1]);
		if(($ext == 'jpg') || ($ext == 'png') || ($ext == 'gif')){
			$fotos[] = $arq;
		}
	}
}
@closedir($aberto);
@array_multisort($fotos);

//paginacao
$totalr = sizeof($fotos);
$pag->setRegistro(8);
$pag->setTotal($totalr); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('Nenhum registro encontrado');

//lista
$max = $min + $max;
if($max > $totalr){ $max = $totalr; }

$tpl->loop('lista');
for($i = $min; $i < $max; $i++){
	$tpl->associaloop('FOTO',LinkFoto($diretorio,$fotos[$i],1));
	$tpl->associaloop('IDGATA',$id_gata);
	$tpl->associaloop('NOMEFOTO',$fotos[$i]);
	$tpl->processaloop();
}
$tpl->fechaloop();

$tpl->associa('ID',$id_gata);
$tpl->associa('SUBTITULO',FormataTitulo($gata['nome']));
$tpl->associa('PAGINACAO',$paginas);
$tpl->associa('TOTALREG',$pag->getTotal());
$tpl->associa('MIN',($min + 1));
$tpl->associa('MAX',$max);
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>