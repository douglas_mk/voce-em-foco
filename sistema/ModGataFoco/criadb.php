<?php
require('../includes/class.mysql.php');

$sql = new Mysql;

//gata em foco
$sql->Consulta("DROP TABLE IF EXISTS gataemfoco");
$sql->Consulta("CREATE TABLE gataemfoco(
	id INT AUTO_INCREMENT PRIMARY KEY,
	tipo INT(11) NOT NULL DEFAULT 1,
	id_estado INT (11) NULL,
	estado INT (1) NOT NULL DEFAULT '0',
	exibe_capa INT (1) NOT NULL DEFAULT '1', 
	pasta VARCHAR (25) NOT NULL,
	nome VARCHAR (255) NOT NULL,
	signo VARCHAR (40) NULL, 
	altura FLOAT NULL,
	quadril FLOAT NULL,
	cintura FLOAT NULL,
	busto FLOAT NULL,
	pes INT(2) NULL,
	nascimento DATE NOT NULL,
	cidade VARCHAR (100) NULL,
	esporte TEXT NULL,
	hobby TEXT NULL,
	musica TEXT NULL,
	comida TEXT NULL,
	bebida TEXT NULL,
	ama TEXT NULL,
	odeia TEXT NULL,
	fantasia TEXT NULL,
	homem_ideal TEXT NULL,
	parte_corpo VARCHAR (50) NULL,
	desejo TEXT NULL,
	conselho TEXT NULL,	
	local_fotos VARCHAR (100),
	fotografo VARCHAR (60) NULL,
	agradecimentos TEXT NULL,
	apoio TEXT NULL,
	data DATE,
	foto_capa VARCHAR (25) NULL,
	acessos INT (6) NULL)");
	
$sql->Consulta("DROP TABLE IF EXISTS banner_gata");
$sql->Consulta("CREATE TABLE banner_gata(
	id INT AUTO_INCREMENT PRIMARY KEY,
	id_gataemfoco INT (11) NOT NULL,
	estado INT (1) NOT NULL DEFAULT '0',
	pagina VARCHAR (25) NOT NULL,
	local INT (2) NOT NULL,
	tipo VARCHAR (40) NOT NULL,
	banner VARCHAR (25) NOT NULL,
	resx INT (4) NOT NULL,
	resy INT (4) NOT NULL,
	data_cadastro DATE NOT NULL,
	data_validade DATE NOT NULL,
	descricao VARCHAR (100) NULL,
	link VARCHAR (255) NULL)");
?>