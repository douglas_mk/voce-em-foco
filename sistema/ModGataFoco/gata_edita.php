<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('gatafoco');
$acesso->getAcesso('editar',true);

$aviso = false;
$erro = false;


$id_gata = (int) $_GET['id'];

$sql = new Mysql;
$data = new Data;
$tpl = new  Template('template/gata_edita.html');

$diretorio = '../../arquivos/emfoco/';
$dirfotocapa =  $diretorio.'foto_capa/';

if(isset($_POST['Submit'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['nome'],'texto','Nome');
	$form->Valida($post['nascimento'],'data','Data de nascimento');
	$form->Valida($post['cidade'],'texto','Cidade');
	$form->Valida($post['uf'],'texto','Estado');
	if($post['altura']){ $form->Valida($post['altura'],'numerico','Altura'); }
	if($post['quadril']){ $form->Valida($post['quadril'],'numerico','Quadril'); }
	if($post['cintura']){ $form->Valida($post['cintura'],'numerico','Cintura'); }
	if($post['busto']){ $form->Valida($post['busto'],'numerico','Busto'); }
	if($post['pes']){ $form->Valida($post['pes'],'numerico','P�s'); }
	$id_gata = $post['id'];

	$erro = $form->getErro();
	if(!$erro){
		//verifica foto da capa
		$fotocapa = NULL;
		$ins = NULL;
		$img = new UploadImg($_FILES['fotocapa']);
		if($img->getTamanho() > 0){
			$img->setQuali(85);
			$img->setLargura(268);
			$img->setAltura(201);
			$img->setFixa('altura');
			$img->setLimite(true);
			$img->setNome(date('dmYhis'));
			$maior = $img->Gera($dirfotocapa);	
		
			$img->setLargura(120);
			$img->setAltura(90);
			$mini = $img->Gera($dirfotocapa.'mini/');	
			
			if((!$maior) || (!$mini)){
				$erro = $img->getErro();
			}else{
				$fotocapa =  $img->getNome();
				$ins = "foto_capa='$fotocapa',";	
			}
		}
		
		//cadastra
		if(!$erro){
			$post['nascimento'] = $data->DataMysql($post['nascimento']);
			if(!$post['altura']){ $post['altura'] = 'NULL'; }
			if(!$post['quadril']){ $post['quadril'] = 'NULL'; }
			if(!$post['cintura']){ $post['cintura'] = 'NULL'; }
			if(!$post['busto']){ $post['busto'] = 'NULL'; }
			if(!$post['pes']){ $post['pes'] = 'NULL'; }
			
			
			$sql->Consulta("UPDATE gataemfoco SET
			id_estado='$post[uf]',
			nome='$post[nome]',
			signo='$post[signo]',
			altura=$post[altura],
			quadril=$post[quadril],
			cintura=$post[cintura],
			busto=$post[busto],
			pes=$post[pes],
			nascimento='$post[nascimento]',
			cidade='$post[cidade]',
			esporte='$post[esporte]',
			hobby='$post[hobby]',
			musica='$post[musica]',
			comida='$post[comida]',
			bebida='$post[bebida]',
			ama='$post[ama]',
			odeia='$post[odeia]',
			fantasia='$post[fantasia]',
			homem_ideal='$post[homemideal]',
			parte_corpo='$post[partecorpo]',
			desejo='$post[desejo]',
			conselho='$post[conselho]',
			local_fotos='$post[local]',
			fotografo='$post[fotografo]',
			agradecimentos='$post[agradec]',
			$ins
			apoio='$post[apoio]'
			WHERE id='$id_gata' LIMIT 1");
			
			$aviso = 'Gata atualizada com sucesso';
		}
	}
}

//dados
$query = $sql->Consulta("SELECT * FROM gataemfoco WHERE id='$id_gata' LIMIT 1");
$gata = mysql_fetch_array($query);

//estados
$estado = NULL;
$query = $sql->Consulta("SELECT * FROM estados WHERE estado='1' ORDER BY nome_estado ASC");
while($linha = mysql_fetch_array($query)){
	$sel = NULL;
	if($gata['id_estado'] == $linha['id']){ $sel = ' selected="selected"'; }
	$estado .= '<option value="'.$linha['id'].'"'.$sel.'>'.$linha['nome_estado'].'</option>';
}

$tpl->associa('FOTOCAPA',Vazio(LinkFoto($dirfotocapa,$gata['foto_capa'],0)));
$tpl->associa('ID',$id_gata);
$tpl->associa('SUBTITULO',FormataTitulo($gata['nome']));
$tpl->associa('ESTADOS',$estado);
$tpl->associa('NOME',$gata['nome']);
$tpl->associa('SIGNO',$gata['signo']);
$tpl->associa('QUADRIL',$gata['quadril']);
$tpl->associa('CINTURA',$gata['cintura']);
$tpl->associa('BUSTO',$gata['busto']);
$tpl->associa('ALTURA',$gata['altura']);
$tpl->associa('PES',$gata['pes']);
$tpl->associa('NASCIMENTO',$data->MysqlData($gata['nascimento']));
$tpl->associa('ESPORTE',$gata['esporte']);
$tpl->associa('HOBBY',$gata['hobby']);
$tpl->associa('MUSICA',$gata['musica']);
$tpl->associa('COMIDA',$gata['comida']);
$tpl->associa('BEBIDA',$gata['bebida']);
$tpl->associa('AMA',$gata['ama']);
$tpl->associa('ODEIA',$gata['odeia']);
$tpl->associa('FANTASIA',$gata['fantasia']);
$tpl->associa('HOMEMIDEAL',$gata['homem_ideal']);
$tpl->associa('PARTECORPO',$gata['parte_corpo']);
$tpl->associa('DESEJO',$gata['desejo']);
$tpl->associa('CONSELHO',$gata['conselho']);
$tpl->associa('LOCAL',$gata['local_fotos']);
$tpl->associa('CIDADE',$gata['cidade']);
$tpl->associa('FOTOGRAFO',$gata['fotografo']);
$tpl->associa('AGRADEC',$gata['agradecimentos']);
$tpl->associa('APOIO',$gata['apoio']);
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>