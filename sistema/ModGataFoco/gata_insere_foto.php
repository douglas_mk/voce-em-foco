<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('gatafoco');
$acesso->getAcesso('cadastrar',true);

$aviso = false;
$erro = false;

$id_gata = (int) $_GET['id'];

$sql = new Mysql;
$data = new Data;
$tpl = new  Template('template/gata_insere_foto.html');

$diretorio = '../../arquivos/emfoco/';
$quali = 85;
$qualidade = 0;
$fotose = NULL;
$enviadas = array();

//dados
$query = $sql->Consulta("SELECT * FROM gataemfoco WHERE id='$id_gata' LIMIT 1");
$gata = mysql_fetch_array($query);
$diretorio .= $gata['pasta'].'/';

if(isset($_POST['Submit'])){

	$id_galeria = (int) $_POST['id'];
	$qualidade = (int) $_POST['quali'];
	$fotos = $_FILES['foto'];
	
	if($quali == 0){  $quali = $qualidade; }
	
	for($i = 0;  $i < sizeof($fotos['name']); $i++){
		if($fotos['size'][$i] > 0){
		
			$foto['size'] = $fotos['size'][$i];
			$foto['name'] = $fotos['name'][$i];
			$foto['tmp_name'] = $fotos['tmp_name'][$i];
			$foto['type'] = $fotos['type'][$i];
		
			$img = new UploadImg($foto);
			
			//gera maior
			$img->setQuali($qualidade);
			$img->setLargura(550);
			$img->setAltura(413);
			$img->setFixa('altura');
			$img->setNome(date('dmyhis').$i);
			$maior = $img->Gera($diretorio);	
			
			//gera miniatura
			$img->setLargura(70);
			$img->setAltura(53);
			$mini = $img->Gera($diretorio.'mini/');		
			
			if((!$maior) || (!$mini)){
				$aviso .= 'Erro ao enviar foto '.($i + 1).': '.$img->getErro().'<br />';
			}else{
				$enviadas[] = $img->getNome();
				$aviso  .= 'Foto '.($i + 1).' enviada com sucesso<br />';
			}
		}else{
			$aviso  .= 'Foto '.($i + 1).' n�o enviada<br />';
		}
	}

}

//exibe enviadas
if(sizeof($enviadas) > 0){
	foreach($enviadas as $nome_foto){
		$fotose .= LinkFoto($diretorio,$nome_foto,1);
	}
}else{
	$fotose = 'Nenhuma foto enviada';
}

$tpl->associa('ID',$id_gata);
$tpl->associa('FOTOSE',$fotose);
$tpl->associa('GATA',$gata['nome']);
$tpl->associa('SUBTITULO',FormataTitulo($gata['nome']));
$tpl->associa('QUALIDADE',Marcado($qualidade,false,$quali,$qualidade));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>