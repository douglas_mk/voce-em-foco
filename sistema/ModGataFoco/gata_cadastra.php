<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('gatafoco');
$acesso->getAcesso('cadastrar',true);

$aviso = false;
$erro = false;

$sql = new Mysql;
$data = new Data;
$tpl = new  Template('template/gata_cadastra.html');

$diretorio = '../../arquivos/emfoco/';
$dirfotocapa =  $diretorio.'foto_capa/';

if(!isset($_POST['uf'])){ $_POST['uf'] = NULL; }
if(!isset($_POST['nome'])){ $_POST['nome'] = NULL; }
if(!isset($_POST['nascimento'])){ $_POST['nascimento'] = NULL; }
if(!isset($_POST['cidade'])){ $_POST['cidade'] = NULL; }
if(!isset($_POST['pasta'])){ $_POST['pasta'] = NULL; }
if(!isset($_POST['altura'])){ $_POST['altura'] = NULL; }
if(!isset($_POST['quadril'])){ $_POST['quadril'] = NULL; }
if(!isset($_POST['cintura'])){ $_POST['cintura'] = NULL; }
if(!isset($_POST['busto'])){ $_POST['busto'] = NULL; }
if(!isset($_POST['pes'])){ $_POST['pes'] = NULL; }
if(!isset($_POST['ama'])){ $_POST['ama'] = NULL; }
if(!isset($_POST['bebida'])){ $_POST['bebida'] = NULL; }
if(!isset($_POST['comida'])){ $_POST['comida'] = NULL; }
if(!isset($_POST['signo'])){ $_POST['signo'] = NULL; }
if(!isset($_POST['esporte'])){ $_POST['esporte'] = NULL; }
if(!isset($_POST['hobby'])){ $_POST['hobby'] = NULL; }
if(!isset($_POST['musica'])){ $_POST['musica'] = NULL; }
if(!isset($_POST['odeia'])){ $_POST['odeia'] = NULL; }
if(!isset($_POST['fantasia'])){ $_POST['fantasia'] = NULL; }
if(!isset($_POST['desejo'])){ $_POST['desejo'] = NULL; }
if(!isset($_POST['conselho'])){ $_POST['conselho'] = NULL; }
if(!isset($_POST['local'])){ $_POST['local'] = NULL; }
if(!isset($_POST['fotografo'])){ $_POST['fotografo'] = NULL; }
if(!isset($_POST['apoio'])){ $_POST['apoio'] = NULL; }
if(!isset($_POST['agradec'])){ $_POST['agradec'] = NULL; }
if(!isset($_POST['homemideal'])){ $_POST['homemideal'] = NULL; }
if(!isset($_POST['partecorpo'])){ $_POST['partecorpo'] = NULL; }

if(isset($_POST['Submit'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['tipo'],'texto','Tipo');
	$form->Valida($post['nome'],'texto','Nome');
	$form->Valida($post['nascimento'],'data','Data de nascimento');
	$form->Valida($post['cidade'],'texto','Cidade');
	$form->Valida($post['uf'],'texto','Estado');
	$form->Valida($post['pasta'],'pasta','Pasta');
	if($post['altura']){ $form->Valida($post['altura'],'numerico','Altura'); }
	if($post['quadril']){ $form->Valida($post['quadril'],'numerico','Quadril'); }
	if($post['cintura']){ $form->Valida($post['cintura'],'numerico','Cintura'); }
	if($post['busto']){ $form->Valida($post['busto'],'numerico','Busto'); }
	if($post['pes']){ $form->Valida($post['pes'],'numerico','P�s'); }

	$erro = $form->getErro();
	if(!$erro){
		//verifica foto da capa
		$fotocapa = NULL;
		$img = new UploadImg($_FILES['fotocapa']);
		//if($img->getTamanho() > 0){
			$img->setQuali(85);
			$img->setLargura(268);
			$img->setAltura(201);
			$img->setFixa('altura');
			$img->setLimite(true);
			$img->setNome(date('dmYhis'));
			$maior = $img->Gera($dirfotocapa);	
		
			$img->setLargura(120);
			$img->setAltura(90);
			$mini = $img->Gera($dirfotocapa.'mini/');	
			
			if((!$maior) || (!$mini)){
				$erro = $img->getErro();
			}else{
				$fotocapa =  $img->getNome();	
			}
		//}
		
		//verifica diretorio
		if(!$erro){
			if(file_exists($diretorio.$post['pasta'])){
				$erro = 'Este diret�rio j� existe, por favor informe outro';
			}elseif(!@mkdir($diretorio.$post['pasta'],0777)){
				$erro = 'Erro ao criar diret�rio';
			}elseif(!@mkdir($diretorio.$post['pasta'].'mini/',0777)){
				$erro = 'Erro ao criar diret�rio das miniaturas';
			}
		}

		//cadastra
		if(!$erro){
			$post['nascimento'] = $data->DataMysql($post['nascimento']);
			if(!$post['altura']){ $post['altura'] = 'NULL'; }
			if(!$post['quadril']){ $post['quadril'] = 'NULL'; }
			if(!$post['cintura']){ $post['cintura'] = 'NULL'; }
			if(!$post['busto']){ $post['busto'] = 'NULL'; }
			if(!$post['pes']){ $post['pes'] = 'NULL'; }
			
			
			$sql->Consulta("INSERT INTO gataemfoco
			(tipo,id_estado,estado,exibe_capa,pasta,nome,signo,altura,quadril,cintura,busto,pes,nascimento,cidade,esporte,hobby,musica,comida,bebida,ama,
			odeia,fantasia,homem_ideal,parte_corpo,desejo,conselho,local_fotos,fotografo,agradecimentos,apoio,data,foto_capa,acessos)
			VALUES
			('$post[tipo]','$post[uf]','1','1','$post[pasta]','$post[nome]','$post[signo]',$post[altura],$post[quadril],$post[cintura],$post[busto],$post[pes],
			'$post[nascimento]','$post[cidade]','$post[esporte]','$post[hobby]','$post[musica]','$post[comida]','$post[bebida]','$post[ama]',
			'$post[odeia]','$post[fantasia]','$post[homemideal]','$post[partecorpo]','$post[desejo]','$post[conselho]','$post[local]',
			'$post[fotografo]','$post[agradec]','$post[apoio]',NOW(),'$fotocapa','0')");
			
			$aviso = 'Gata/gato cadastrada com sucesso';
			$_POST = NULL;
		}
	}
}

//estados
$estado = NULL;
$query = $sql->Consulta("SELECT * FROM estados WHERE estado='1' ORDER BY nome_estado ASC");
while($linha = mysql_fetch_array($query)){
	$sel = NULL;
	if($_POST['uf'] == $linha['id']){ $sel = ' selected="selected"'; }
	$estado .= '<option value="'.$linha['id'].'"'.$sel.'>'.$linha['nome_estado'].'</option>';
}

$tpl->associa('ESTADOS',$estado);
$tpl->associa('NOME',$_POST['nome']);
$tpl->associa('SIGNO',$_POST['signo']);
$tpl->associa('QUADRIL',$_POST['quadril']);
$tpl->associa('CINTURA',$_POST['cintura']);
$tpl->associa('BUSTO',$_POST['busto']);
$tpl->associa('ALTURA',$_POST['altura']);
$tpl->associa('PES',$_POST['pes']);
$tpl->associa('NASCIMENTO',$_POST['nascimento']);
$tpl->associa('ESPORTE',$_POST['esporte']);
$tpl->associa('HOBBY',$_POST['hobby']);
$tpl->associa('MUSICA',$_POST['musica']);
$tpl->associa('COMIDA',$_POST['comida']);
$tpl->associa('BEBIDA',$_POST['bebida']);
$tpl->associa('AMA',$_POST['ama']);
$tpl->associa('ODEIA',$_POST['odeia']);
$tpl->associa('FANTASIA',$_POST['fantasia']);
$tpl->associa('HOMEMIDEAL',$_POST['homemideal']);
$tpl->associa('PARTECORPO',$_POST['partecorpo']);
$tpl->associa('DESEJO',$_POST['desejo']);
$tpl->associa('CONSELHO',$_POST['conselho']);
$tpl->associa('LOCAL',$_POST['local']);
$tpl->associa('CIDADE',$_POST['cidade']);
$tpl->associa('FOTOGRAFO',$_POST['fotografo']);
$tpl->associa('PASTA',$_POST['pasta']);
$tpl->associa('AGRADEC',$_POST['agradec']);
$tpl->associa('APOIO',$_POST['apoio']);
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>