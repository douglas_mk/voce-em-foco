<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('muralr');
$acesso->getAcesso('editar',true);

$aviso = false;
$erro = false;

$id_recado = (int) $_GET['id'];

$sql = new Mysql;
$tpl = new  Template('template/mural_edita.html');

if(isset($_POST['Submit'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['de'],'texto','De');
	$form->Valida($post['para'],'texto','Para');
	$form->Valida($post['email'],'email','E-mail');
	$form->Valida($post['recado'],'texto','Recado');
	$form->Tamanho($post['recado'],2,500,'Recado');
	$id_recado = $post['id'];

	$erro = $form->getErro();
	if(!$erro){
			
		$sql->Consulta("UPDATE muralr SET
		de='$post[de]',
		para='$post[para]',
		email='$post[email]',
		recado='$post[recado]'
		WHERE id='$id_recado' LIMIT 1");
		
		$aviso = 'Recado atualizado com sucesso';
	}
}

//seleciona dados
$query = $sql->Consulta("SELECT * FROM muralr WHERE id='$id_recado' LIMIT 1");
$linha = mysql_fetch_array($query);

$tpl->associa('ID',$id_recado);
$tpl->associa('SUBTITULO',FormataTitulo($linha['de']));
$tpl->associa('DE',$linha['de']);
$tpl->associa('PARA',$linha['para']);
$tpl->associa('EMAIL',$linha['email']);
$tpl->associa('RECADO',$linha['recado']);
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>