<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('muralr');
$acesso->getAcesso('listar',true);

$aviso = false;
$erro = false;

$sql = new Mysql;
$data = new Data;
$tpl = new Template('template/mural_libera.html');
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);

if(!isset($_POST['idrecado'])){ $_POST['idrecado'] = NULL; }

//excluir
if(isset($_POST['excluir'])){
	$idrecado = $_POST['idrecado'];
	
	if(sizeof($idrecado) == 0){
		$erro = 'Nenhum registro selecionado';
	}else{
		if(!$acesso->getAcesso('excluir',false)){
			$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
		}else{
			//exclui recados
			foreach($idrecado as $id){
				$sql->Consulta("UPDATE muralr SET estado='9' WHERE id='$id' LIMIT 1");	
			}
			$aviso = 'Recado(s) exclu�do(s) com sucesso';
		}
	}	
}

//ativar
if(isset($_POST['liberar'])){
	$idrecado = $_POST['idrecado'];
	
	if(sizeof($idrecado) == 0){
		$erro = 'Nenhum registro selecionado';
	}else{
		if(!$acesso->getAcesso('editar',false)){
			$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
		}else{
			//exclui recados
			foreach($idrecado as $id){
				$sql->Consulta("UPDATE muralr SET estado='1' WHERE id='$id' LIMIT 1");	
			}
			$aviso = 'Recado(s) liberado(s) com sucesso';
		}
	}	
}

//paginacao		
$pag->setRegistro(5);
$pag->setTotal($sql->Totalreg("SELECT * FROM muralr WHERE estado='0'")); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('Nenhum registro encontrado');

//lista
$tpl->loop('lista');
$query = $sql->Consulta("SELECT * FROM muralr WHERE estado='0' ORDER BY id DESC LIMIT $min,$max");
while($linha = mysql_fetch_array($query)){
	$tpl->associaloop('ID',$linha['id']);
	$tpl->associaloop('DE',$linha['de']);
	$tpl->associaloop('PARA',$linha['para']);
	$tpl->associaloop('EMAIL',$linha['email']);
	$tpl->associaloop('DATA',$data->MysqlData($linha['data']));
	$tpl->associaloop('HORA',$linha['hora']);
	$tpl->associaloop('RECADO',nl2br($linha['recado']));
	$tpl->processaloop();
}
$tpl->fechaloop();

$tpl->associa('PAGINACAO',$paginas);
$tpl->associa('TOTALREG',$pag->getTotal());
$tpl->associa('MIN',($min + 1));
$tpl->associa('MAX',($min + $max));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>