<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('noticias');
$acesso->getAcesso('cadastrar',true);

$aviso = false;
$erro = false;

$id_noticia = (int) $_GET['id'];

$sql = new Mysql;
$data = new Data;
$tpl = new  Template('template/noticia_insere_foto.html');

$diretorio = '../../arquivos/noticias/';
$quali = 85;
$qualidade = 0;
$fotose = NULL;
$enviadas = array();

if(isset($_POST['Submit'])){

	$id_noticia = (int) $_POST['id'];
	$qualidade = (int) $_POST['quali'];
	$legendaenv = array();
	$fotos = $_FILES['foto'];
	$legenda = $_POST['legenda'];
	
	if($qualidade == 0){ $qualidade = $quali; }
	
	for($i = 0;  $i < sizeof($fotos['name']); $i++){
		if($fotos['size'][$i] > 0){
		
			$foto['size'] = $fotos['size'][$i];
			$foto['name'] = $fotos['name'][$i];
			$foto['tmp_name'] = $fotos['tmp_name'][$i];
			$foto['type'] = $fotos['type'][$i];
		
			$img = new UploadImg($foto);
			
			//gera maior
			$img->setQuali($qualidade);
			$img->setLargura(550);
			$img->setAltura(413);
			$img->setFixa('altura');
			$img->setNome(date('dmyhis').$i);
			$maior = $img->Gera($diretorio);	
			
			//gera miniatura
			$img->setLargura(100);
			$img->setAltura(75);
			$mini = $img->Gera($diretorio.'mini/');		
			
			if((!$maior) || (!$mini)){
				$aviso .= 'Erro ao enviar foto '.($i + 1).': '.$img->getErro().'<br />';
			}else{
				$enviadas[] = $img->getNome();
				$legendaenv[] = $legenda[$i];
				$aviso  .= 'Foto '.($i + 1).' enviada com sucesso<br />';
			}
		}else{
			$aviso  .= 'Foto '.($i + 1).' n�o enviada<br />';
		}
	}
	
	//cadastra fotos
	if(sizeof($enviadas) > 0){
		foreach($enviadas as $indice => $nome_foto){
			$sql->Consulta("INSERT INTO fotos_noticia (id_noticia,foto,legenda) VALUES ('$id_noticia','$nome_foto','$legendaenv[$indice]')");	
		}
	}
}

//exibe enviadas
if(sizeof($enviadas) > 0){
	foreach($enviadas as $nome_foto){
		$fotose .= LinkFoto($diretorio,$nome_foto,1);
	}
}else{
	$fotose = 'Nenhuma foto enviada';
}

//dados
$query = $sql->Consulta("SELECT * FROM noticias WHERE id='$id_noticia' LIMIT 1");
$noticia = mysql_fetch_array($query);

$tpl->associa('ID',$id_noticia);
$tpl->associa('FOTOSE',$fotose);
$tpl->associa('NOTICIA',$noticia['titulo']);
$tpl->associa('SUBTITULO',FormataTitulo($noticia['titulo']));
$tpl->associa('QUALIDADE',Marcado($qualidade,false,$quali,$qualidade));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>