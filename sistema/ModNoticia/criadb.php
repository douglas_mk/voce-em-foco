<?php
//require('../includes/class.mysql.php');

//$sql = new Mysql;
	
//categorias
$sql->Consulta("DROP TABLE IF EXISTS cat_noticia");
$sql->Consulta("CREATE TABLE cat_noticia(
	id INT AUTO_INCREMENT PRIMARY KEY,
	estado INT (1) NOT NULL DEFAULT '0',
	categoria VARCHAR (50) NOT NULL,
	icone VARCHAR (25) NULL)");

//noticias
$sql->Consulta("DROP TABLE IF EXISTS noticias");
$sql->Consulta("CREATE TABLE noticias(
	id INT AUTO_INCREMENT PRIMARY KEY,
	id_categoria INT (11) NOT NULL,
	estado INT (1) NOT NULL DEFAULT '0',
	destaque INT (1) NOT NULL DEFAULT '0',
	titulo VARCHAR (255) NOT NULL,
	noticia MEDIUMTEXT NOT NULL,
	fonte VARCHAR (150) NULL,
	autor VARCHAR (60),
	data DATE NOT NULL,
	hora TIME NOT NULL,
	acessos INT (6) NOT NULl)");
	
$sql->Consulta("DROP TABLE IF EXISTS fotos_noticia");
$sql->Consulta("CREATE TABLE fotos_noticia(
	id INT AUTO_INCREMENT PRIMARY KEY,
	id_noticia INT (11) NOT NULL,
	estado INT (1) NOT NULL DEFAULT '0',
	foto VARCHAR (25) NOT NULL,
	legenda TEXT NOT NULL)");
?>