<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('noticias');
$acesso->getAcesso('cadastrar',true);

$aviso = false;
$erro = false;

$sql = new Mysql;
$data = new Data;
$tpl = new  Template('template/noticia_cadastra.html');

if(!isset($_POST['categoria'])){ $_POST['categoria'] = NULL; }
if(!isset($_POST['titulo'])){ $_POST['titulo'] = NULL; }
if(!isset($_POST['noticia'])){ $_POST['noticia'] = NULL; }
if(!isset($_POST['destaque'])){ $_POST['destaque'] = NULL; }
if(!isset($_POST['fonte'])){ $_POST['fonte'] = NULL; }
if(!isset($_POST['autor'])){ $_POST['autor'] = NULL; }

if(isset($_POST['Submit'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['titulo'],'texto','T�tulo');
	$form->Valida($post['noticia'],'texto','Not�cia');
	$form->Valida($post['categoria'],'texto','Categoria');

	$erro = $form->getErro();
	if(!$erro){
		$sql = new Mysql;
		if(!$post['destaque']){ $post['destaque'] = '0'; }
		
		$id_noticia = $sql->Insere("INSERT INTO noticias
		(id_categoria,estado,destaque,titulo,noticia,fonte,autor,data,hora,acessos)
		VALUES
		('$post[categoria]','1','$post[destaque]','$post[titulo]','$post[noticia]','$post[fonte]','$post[autor]',NOW(),NOW(),'0')");
		
		$aviso = 'Not�cia cadastrada com sucesso';
		$_POST = NULL;
	}
}

//categorias
$cat = NULL;
$query = $sql->Consulta("SELECT * FROM cat_noticia WHERE estado!='9' ORDER BY categoria ASC");
while($linha = mysql_fetch_array($query)){
	$sel = NULL;
	if($_POST['categoria'] == $linha['id']){ $sel = ' selected="selected"'; }
	$cat .= '<option value="'.$linha['id'].'"'.$sel.'>'.$linha['categoria'].'</option>';
}


//editor
$oFCKeditor = new FCKeditor('noticia');
$oFCKeditor->BasePath = '../fckeditor/';
$oFCKeditor->Value = $_POST['noticia'];
$editor = $oFCKeditor->Create();

$tpl->associa('CATEGORIAS',$cat);
$tpl->associa('DESTAQUE',Marcado($_POST['destaque'],'1','checked="checked"',false));
$tpl->associa('EDITOR',$editor);
$tpl->associa('TITULO',$_POST['titulo']);
$tpl->associa('FONTE',$_POST['fonte']);
$tpl->associa('AUTOR',$_POST['autor']);
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>