<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('noticias');
$acesso->getAcesso('listar',true);

$aviso = false;
$erro = false;

$id_noticia = (int) $_GET['id'];

$sql = new Mysql;
$tpl = new Template('template/noticia_lista_foto.html');
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);
$pag->AddLink('id',$id_noticia);

$diretorio = '../../arquivos/noticias/';

//acoes
if(isset($_POST['acao'])){
	$id_foto = $_POST['id'];
	$id_noticia = $_POST['ids'];

	switch($_POST['acao']){
		case 'excluir':
			if(!$acesso->getAcesso('excluir',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$query = $sql->Consulta("SELECT * FROM fotos_noticia WHERE id='$id_foto' LIMIT 1");
				$linha = mysql_fetch_array($query);
				@unlink($diretorio.$linha['foto']);
				@unlink($diretorio.'mini/'.$linha['foto']);
				
				$sql->Consulta("DELETE FROM fotos_noticia WHERE id='$id_foto' LIMIT 1");
				
				$aviso = 'Foto exclu�da com sucesso';
			}
		break;
	}
}

//dados
$query = $sql->Consulta("SELECT * FROM noticias WHERE id='$id_noticia' LIMIT 1");
$noticia = mysql_fetch_array($query);

//paginacao
$pag->setRegistro(8);
$pag->setTotal($sql->Totalreg("SELECT * FROM fotos_noticia WHERE id_noticia='$id_noticia'")); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('Nenhum registro encontrado');

//lista
$tpl->loop('lista');
$query = $sql->Consulta("SELECT * FROM fotos_noticia WHERE id_noticia='$id_noticia' ORDER BY id DESC LIMIT $min,$max");
while($linha = mysql_fetch_array($query)){
	$tpl->associaloop('FOTO',LinkFoto($diretorio,$linha['foto'],1));
	$tpl->associaloop('IDNOTICIA',$id_noticia);
	$tpl->associaloop('IDFOTO',$linha['id']);
	$tpl->associaloop('LEGENDA',nl2br($linha['legenda']));
	$tpl->processaloop();
}
$tpl->fechaloop();

$tpl->associa('ID',$id_noticia);
$tpl->associa('SUBTITULO',FormataTitulo($noticia['titulo']));
$tpl->associa('PAGINACAO',$paginas);
$tpl->associa('TOTALREG',$pag->getTotal());
$tpl->associa('MIN',($min + 1));
$tpl->associa('MAX',($min + $max));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>