<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('noticias');
$acesso->getAcesso('editar',true);

$aviso = false;
$erro = false;

$id_noticia = (int) $_GET['id'];

$sql = new Mysql;
$data = new Data;
$tpl = new  Template('template/noticia_edita.html');

if(isset($_POST['Submit'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['titulo'],'texto','T�tulo');
	$form->Valida($post['noticia'],'texto','Not�cia');
	$form->Valida($post['categoria'],'texto','Categoria');
	$id_noticia = $post['id'];

	$erro = $form->getErro();
	if(!$erro){
		$sql = new Mysql;
		if(!isset($post['destaque'])){ $post['destaque'] = '0'; }
		
		 $sql->Consulta("UPDATE noticias SET
		id_categoria='$post[categoria]',
		destaque='$post[destaque]',
		titulo='$post[titulo]',
		noticia='$post[noticia]',
		fonte='$post[fonte]',
		autor='$post[autor]'
		WHERE id='$id_noticia' LIMIT 1");
		
		$aviso = 'Not�cia atualizada com sucesso';
		unset($_POST);
	}
}

//seleciona dados
$query = $sql->Consulta("SELECT * FROM noticias WHERE id='$id_noticia' LIMIT 1");
$noticia = mysql_fetch_array($query);

//categorias
$cat = NULL;
$query = $sql->Consulta("SELECT * FROM cat_noticia WHERE estado!='9' ORDER BY categoria ASC");
while($linha = mysql_fetch_array($query)){
	$sel = NULL;
	if($noticia['id_categoria'] == $linha['id']){ $sel = ' selected="selected"'; }
	$cat .= '<option value="'.$linha['id'].'"'.$sel.'>'.$linha['categoria'].'</option>';
}

//editor
$oFCKeditor = new FCKeditor('noticia');
$oFCKeditor->BasePath = '../fckeditor/';
$oFCKeditor->Value = $noticia['noticia'];
$editor = $oFCKeditor->Create();

$tpl->associa('ID',$id_noticia);
$tpl->associa('SUBTITULO',FormataTitulo($noticia['titulo']));
$tpl->associa('CATEGORIAS',$cat);
$tpl->associa('DESTAQUE',Marcado($noticia['destaque'],'1','checked="checked"',false));
$tpl->associa('EDITOR',$editor);
$tpl->associa('TITULO',$noticia['titulo']);
$tpl->associa('FONTE',$noticia['fonte']);
$tpl->associa('AUTOR',$noticia['autor']);
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>