<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('noticias');
$acesso->getAcesso('listar',true);

$aviso = false;
$erro = false;
$and = NULL;

$sql = new Mysql;
$data = new Data;
$tpl = new Template('template/noticia_lista.html');
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);

//busca	
if(isset($_GET['busca'])){
	$and = " AND (noticias.titulo LIKE '%".$_GET['busca']."%' OR noticias.noticia LIKE '%".$_GET['busca']."%')";
	$pag->AddLink('busca',$_GET['busca']);
}	

//acoes
if(isset($_POST['acao'])){
	$id_noticia = $_POST['id'];
	$id_s = $_POST['ids'];

	switch($_POST['acao']){
		case 'ativar':
			if(!$acesso->getAcesso('editar',false)){
			$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE noticias SET estado='1' WHERE id='$id_noticia' LIMIT 1");
				$aviso = 'Not�cia ativada com sucesso';
			}
		break;
		case 'desativar':
			if(!$acesso->getAcesso('editar',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE noticias SET estado='0' WHERE id='$id_noticia' LIMIT 1");
				$aviso = 'Not�cia desativada com sucesso';
			}
		break;
		case 'excluir':
			if(!$acesso->getAcesso('excluir',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE noticias SET estado='9' WHERE id='$id_noticia' LIMIT 1");
				$aviso = 'Not�cia exclu�da com sucesso';
			}
		break;
	}
}

//paginacao		
$pag->setRegistro(5);
$pag->setTotal($sql->Totalreg("SELECT * FROM noticias WHERE estado!='9' $and")); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('Nenhum registro encontrado');

//lista
$tpl->loop('lista');
$query = $sql->Consulta("SELECT noticias.*,cat_noticia.categoria 
FROM noticias 
LEFT JOIN cat_noticia ON cat_noticia.id=noticias.id_categoria
WHERE noticias.estado!='9' $and ORDER BY noticias.id DESC LIMIT $min,$max");
while($linha = mysql_fetch_array($query)){
	$tpl->associaloop('ID',$linha['id']);
	$tpl->associaloop('TITULO',$linha['titulo']);
	$tpl->associaloop('CATEGORIA',$linha['categoria']);
	$tpl->associaloop('FONTE',Vazio($linha['fonte']));
	$tpl->associaloop('AUTOR',Vazio($linha['autor']));
	$tpl->associaloop('DESTAQUE',Marcado($linha['destaque'],'1','Sim','N�o'));
	$tpl->associaloop('DATA',$data->MysqlData($linha['data']));
	$tpl->associaloop('ACESSOS',$linha['acessos']);
	$tpl->associaloop('HORA',$linha['hora']);

	$estado = 'Desativar'; $acao = 'desativar'; $class = 'titulolista';
	if($linha['estado'] == '0'){ $estado = 'Ativar'; $acao = 'ativar'; $class = 'titulolistad'; }
	
	$tpl->associaloop('ACAO',$acao);
	$tpl->associaloop('ESTADO',$estado);
	$tpl->associaloop('CLASSE',$class);
	$tpl->processaloop();
}
$tpl->fechaloop();

$tpl->associa('PAGINACAO',$paginas);
$tpl->associa('TOTALREG',$pag->getTotal());
$tpl->associa('MIN',($min + 1));
$tpl->associa('MAX',($min + $max));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>