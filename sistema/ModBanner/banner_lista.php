<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('banners');
$acesso->getAcesso('listar',true);

$aviso = false;
$erro = false;
$and = NULL;

$sql = new Mysql;
$data = new Data;
$tpl = new Template('template/banner_lista.html');
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);

$diretorio = '../../arquivos/banners/';

//busca	
if(isset($_GET['busca'])){
	$and = " AND (banners.descricao LIKE '%".$_GET['busca']."%' OR banners.link LIKE '%".$_GET['busca']."%')";
	$pag->AddLink('busca',$_GET['busca']);
}	

//acoes
if(isset($_POST['acao'])){
	$id_banner = $_POST['id'];
	$id_s = $_POST['ids'];

	switch($_POST['acao']){
		case 'ativar':
			if(!$acesso->getAcesso('editar',false)){
			$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE banners SET estado='1' WHERE id='$id_banner' LIMIT 1");
				$aviso = 'Banner ativado com sucesso';
			}
		break;
		case 'desativar':
			if(!$acesso->getAcesso('editar',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE banners SET estado='0' WHERE id='$id_banner' LIMIT 1");
				$aviso = 'Banner desativado com sucesso';
			}
		break;
		case 'excluir':
			if(!$acesso->getAcesso('excluir',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE banners SET estado='9' WHERE id='$id_banner' LIMIT 1");
				$aviso = 'Banner exclu�do com sucesso';
			}
		break;
	}
}

//paginacao		
$pag->setRegistro(5);
$pag->setTotal($sql->Totalreg("SELECT * FROM banners WHERE estado!='9' $and")); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('Nenhum registro encontrado');

//lista
$tpl->loop('lista');
$query = $sql->Consulta("SELECT * FROM banners WHERE estado!='9' $and ORDER BY id DESC LIMIT $min,$max");
while($linha = mysql_fetch_array($query)){
	$tpl->associaloop('ID',$linha['id']);
	$tpl->associaloop('TITULO',$linha['descricao'].'&nbsp;&nbsp;( '.$linha['resx'].' x '.$linha['resy'].' - '.$linha['pagina'].' - '.$linha['local'].' )');
	$tpl->associaloop('LINK',Vazio($linha['link']));
	$banner = Banner($diretorio.$linha['banner'],$linha['resx'],$linha['resy'],$linha['tipo']);
	$tpl->associaloop('BANNER',$banner);
	$tpl->associaloop('DESCRICAO',Vazio($linha['descricao']));
	$tpl->associaloop('PAGINAB',$linha['pagina']);
	$tpl->associaloop('LOCAL',$linha['local']);
	$tpl->associaloop('RESOL',$linha['resx'].' x '.$linha['resy']);
	$tpl->associaloop('DATA',$data->MysqlData($linha['data_cadastro']));
	
	//validade
	if($linha['data_cadastro'] == $linha['data_validade']){
		$linha['data_validade'] = 'Indeterminada';
	}else{
		$linha['data_validade'] = $data->MysqlData($linha['data_validade']);
		$linha['data_validade'] .= ' ('.$data->Diferenca($linha['data_validade'],$data->MysqlData($linha['data_cadastro'])).' dias)';
	}
	$tpl->associaloop('VALIDADE',$linha['data_validade']);

	$estado = 'Desativar'; $acao = 'desativar'; $class = 'titulolista';
	if($linha['estado'] == '0'){ $estado = 'Ativar'; $acao = 'ativar'; $class = 'titulolistad'; }
	
	$tpl->associaloop('ACAO',$acao);
	$tpl->associaloop('ESTADO',$estado);
	$tpl->associaloop('CLASSE',$class);
	$tpl->processaloop();
}
$tpl->fechaloop();

$tpl->associa('PAGINACAO',$paginas);
$tpl->associa('TOTALREG',$pag->getTotal());
$tpl->associa('MIN',($min + 1));
$tpl->associa('MAX',($min + $max));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>