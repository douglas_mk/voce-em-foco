<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('banners');
$acesso->getAcesso('cadastrar',true);

$aviso = false;
$erro = false;

$sql = new Mysql;
$data = new Data;
$tpl = new  Template('template/banner_cadastra.html');

$diretorio = '../../arquivos/banners/';

if(!isset($_POST['pagina'])){ $_POST['pagina'] = NULL; }
if(!isset($_POST['descricao'])){ $_POST['descricao'] = NULL; }
if(!isset($_POST['validade'])){ $_POST['validade'] = NULL; }
if(!isset($_POST['linkb'])){ $_POST['linkb'] = NULL; }
if(!isset($_POST['local'])){ $_POST['local'] = NULL; }

if(isset($_POST['Submit'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['validade'],'numerico','Validade');
	$form->Valida($post['pagina'],'texto','P�gina');
	$form->Valida($post['local'],'texto','Local');
	$form->Valida($post['descricao'],'texto','Descri��o');

	$erro = $form->getErro();
	if(!$erro){
		$upload = new Upload($_FILES['arquivo']);
		$upload->setTipo('image/jpeg,image/pjpeg,image/gif,image/x-png,image/png,application/x-shockwave-flash');
		$upload->setNome(date('dmyhis'));
		
		if(!$upload->Envia($diretorio)){
			$erro = $upload->getErro();
		}
		
		if(!$erro){
			$tipo = $upload->getTipo();
			$validade = $data->SomaData($data->getData(),$post['validade']);
			$validade = $data->DataMysql($validade);
			$banner = $upload->getNome();
			$codigo = explode('-',$post['local']);
			$res = explode('x',$codigo[1]);
			$resx = $res[0];
			$resy = $res[1];
			
			$sql->Consulta("INSERT INTO banners
			(estado,pagina,local,tipo,banner,resx,resy,data_cadastro,data_validade,descricao,link)
			VALUES
			('1','$post[pagina]','$codigo[0]','$tipo','$banner','$resx','$resy',NOW(),'$validade','$post[descricao]','$post[linkb]')");
			
			$aviso = 'Banner cadastrado com sucesso';
			$_POST = NULL;
		}
	}	
}

$tpl->associa('DATA',$data->getData());
$tpl->associa('LINKB',$_POST['linkb']);
$tpl->associa('DESCRICAO',$_POST['descricao']);
$tpl->associa('VALIDADE',Marcado((int) $_POST['validade'],false,0,$_POST['validade']));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>