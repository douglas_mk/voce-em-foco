<?php
require('inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$menu = NULL;

$data = new Data;
$tpl = new  Template('template/painel.html');

//modulos
require('modulos.php');

foreach($modulos as $dir){
	if(is_file($dir.'/menu.php')){
		$menu .= file_get_contents($dir.'/menu.php');
	}
}

$tpl->associa('MENU',$menu);
$tpl->associa('USUARIO',$acesso->getParametro('nome'));
$tpl->associa('NOMESITE',$config['nome']);
$tpl->associa('TITULO',$config['nome']);
$tpl->associa('DATA',$data->DataExtenso(false));
$tpl->exibe();
?>