<?php
require('inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('usuarios');
$acesso->getAcesso('listar',true);

$aviso = false;
$erro = false;
$where = NULL;

$sql = new Mysql;
$tpl = new Template('template/usuario_lista.html');
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);

//busca	
if(isset($_GET['busca'])){
	$where = " WHERE (usuario.nome LIKE '%".$_GET['busca']."%' OR usuario.usuario LIKE '%".$_GET['busca']."%')";
	$pag->AddLink('busca',$_GET['busca']);
}	

//acoes
if(isset($_POST['acao'])){
	$id_usuario = $_POST['id'];
	$id_s = $_POST['ids'];

	switch($_POST['acao']){
		case 'ativar':
			if(!$acesso->getAcesso('editar',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE usuario SET estado='1' WHERE id='$id_usuario' LIMIT 1");
				$aviso = 'Usu�rio ativado com sucesso';
			}
		break;
		case 'desativar':
			if(!$acesso->getAcesso('editar',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE usuario SET estado='0' WHERE id='$id_usuario' LIMIT 1");
				$aviso = 'Usu�rio desativado com sucesso';
			}
		break;
		case 'excluir':
			if(!$acesso->getAcesso('excluir',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("DELETE FROM usuario WHERE id='$id_usuario' LIMIT 1");
				$aviso = 'Usu�rio exclu�do com sucesso';
			}
		break;
	}
}

//paginacao		
$pag->setRegistro(8);
$pag->setTotal($sql->Totalreg("SELECT * FROM usuario $where")); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('Nenhum registro encontrado');

//lista
$tpl->loop('lista');
$query = $sql->Consulta("SELECT * FROM usuario $where ORDER BY id DESC LIMIT $min,$max");
while($linha = mysql_fetch_array($query)){
	$tpl->associaloop('ID',$linha['id']);
	$tpl->associaloop('NOME',$linha['nome']);
	$tpl->associaloop('EMAIL',$linha['email']);
	$tpl->associaloop('USUARIO',$linha['usuario']);
	
	$estado = 'Desativar'; $acao = 'desativar'; $class = 'titulolista';
	if($linha['estado'] == '0'){ $estado = 'Ativar'; $acao = 'ativar'; $class = 'titulolistad'; }
	
	$tpl->associaloop('ACAO',$acao);
	$tpl->associaloop('ESTADO',$estado);
	$tpl->associaloop('CLASSE',$class);
	$tpl->processaloop();
}
$tpl->fechaloop();

$tpl->associa('PAGINACAO',$paginas);
$tpl->associa('TOTALREG',$pag->getTotal());
$tpl->associa('MIN',$min);
$tpl->associa('MAX',($min + $max));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>