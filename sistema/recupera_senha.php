<?php
require('inc.includes.php');

$aviso = false;
$erro = false;

$tpl = new  Template('template/recupera_senha.html');

if(isset($_POST['Submit'])){
	
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['usuario'],'usuario','Usu�rio');
	$form->Valida($post['email'],'email','E-mail');

	$erro = $form->getErro();
	if(!$erro){
		$sql = new Mysql;
		$query = $sql->Consulta("SELECT * FROM usuario WHERE usuario='$post[usuario]' LIMIT 1"); 
		$linha = mysql_fetch_array($query);
		if($linha){
		
			$nova_senha = GeraSenha(6);
						
			$email = new Email;
			$email->setPara($linha['email']);
			$email->setDe(false);
			$email->setAssunto('Solicita��o de senha de acesso');
			
			$email->Associa('NOME',$linha['nome']);
			$email->Associa('DATA',date('d/m/Y'));
			$email->Associa('HORA',date('h:i:s'));
			$email->Associa('NOVASENHA',$nova_senha);
			
			$email->setEmail('email/recupera_senha.txt');
	
			if(!$email->Envia()){
				$erro = 'Falha ao enviar email, tente novamente mais tarde';
			}else{
				$nova_senha = md5($nova_senha);
				$query = $sql->Consulta("UPDATE usuario SET senha='$nova_senha' WHERE usuario='$post[usuario]' LIMIT 1"); 
				
				$aviso = 'Uma nova senha foi gerada e enviada para o e-mail cadastrado no sistema';
			}
		
		}else{
			$erro = 'Nenhum registro encontrado para o usu�rio e e-mail fornecidos';
		}	
	}
}

$data = new Data;

$tpl->associa('PAGINA',Pagina());
$tpl->associa('TITULO',$config['nome']);
$tpl->associa('RODAPE',$config['rodape']);
$tpl->associa('DATA',$data->DataExtenso(false));
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();

?>