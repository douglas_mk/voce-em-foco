<?php
//require('../includes/class.mysql.php');

//$sql = new Mysql;

//parceiros
$sql->Consulta("DROP TABLE IF EXISTS parceiros");
$sql->Consulta("CREATE TABLE parceiros(
	id INT AUTO_INCREMENT PRIMARY KEY,
	id_estado INT (11) NULL,
	estado INT (1) NOT NULL DEFAULT '0',
	nome VARCHAR (100) NOT NULL,
	email VARCHAR (60) NULL,
	site VARCHAR (150) NULL,
	telefone VARCHAR (11) NULL,
	endereco  VARCHAR (200) NULL,
	cidade VARCHAR (60) NOT NULL,
 	informacoes TEXT NULL,
	foto VARCHAR (25) NULL,
	data_cadastro DATE NOT NULL)");
?>