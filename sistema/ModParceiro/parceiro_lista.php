<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('parceiro');
$acesso->getAcesso('listar',true);

$aviso = false;
$erro = false;
$and = NULL;

$sql = new Mysql;
$data = new Data;
$tpl = new Template('template/parceiro_lista.html');
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);

$diretorio = '../../arquivos/parceiros/';

//busca	
if(isset($_GET['busca'])){
	$and = " AND (parceiros.nome LIKE '%".$_GET['busca']."%' OR parceiros.cidade LIKE '%".$_GET['busca']."%')";
	$pag->AddLink('busca',$_GET['busca']);
}	

//acoes
if(isset($_POST['acao'])){
	$id_parceiro = $_POST['id'];
	$id_s = $_POST['ids'];

	switch($_POST['acao']){
		case 'ativar':
			if(!$acesso->getAcesso('editar',false)){
			$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE parceiros SET estado='1' WHERE id='$id_parceiro' LIMIT 1");
				$aviso = 'Parceiro ativado com sucesso';
			}
		break;
		case 'desativar':
			if(!$acesso->getAcesso('editar',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE parceiros SET estado='0' WHERE id='$id_parceiro' LIMIT 1");
				$aviso = 'Parceiro desativado com sucesso';
			}
		break;
		case 'excluir':
			if(!$acesso->getAcesso('excluir',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE parceiros SET estado='9' WHERE id='$id_parceiro' LIMIT 1");
				$aviso = 'Parceiro exclu�do com sucesso';
			}
		break;
	}
}

//paginacao		
$pag->setRegistro(5);
$pag->setTotal($sql->Totalreg("SELECT * FROM parceiros WHERE estado!='9' $and")); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('Nenhum registro encontrado');

//lista
$tpl->loop('lista');
$query = $sql->Consulta("SELECT parceiros.*,estados.nome_estado
FROM parceiros
LEFT JOIN estados ON estados.id=parceiros.id_estado
WHERE parceiros.estado!='9' $and ORDER BY parceiros.id DESC LIMIT $min,$max");
while($linha = mysql_fetch_array($query)){
	$tpl->associaloop('ID',$linha['id']);
	$tpl->associaloop('NOME',$linha['nome']);
	$tpl->associaloop('ENDERECO',Vazio($linha['endereco']));
	$tpl->associaloop('CIDADE',Vazio($linha['cidade']));
	$tpl->associaloop('UF',Vazio($linha['nome_estado']));
	$tpl->associaloop('TELEFONE',Vazio($linha['telefone']));
	$tpl->associaloop('EMAIL',Vazio($linha['email']));
	$tpl->associaloop('SITE',Vazio($linha['site']));
	$tpl->associaloop('INFO',Vazio(nl2br($linha['informacoes'])));
	$tpl->associaloop('FOTO',Vazio(LinkFoto($diretorio,$linha['foto'],1)));
	$tpl->associaloop('DATACAD',$data->MysqlData($linha['data_cadastro']));

	$estado = 'Desativar'; $acao = 'desativar'; $class = 'titulolista';
	if($linha['estado'] == '0'){ $estado = 'Ativar'; $acao = 'ativar'; $class = 'titulolistad'; }
	
	$tpl->associaloop('ACAO',$acao);
	$tpl->associaloop('ESTADO',$estado);
	$tpl->associaloop('CLASSE',$class);
	$tpl->processaloop();
}
$tpl->fechaloop();

$tpl->associa('PAGINACAO',$paginas);
$tpl->associa('TOTALREG',$pag->getTotal());
$tpl->associa('MIN',($min + 1));
$tpl->associa('MAX',($min + $max));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>