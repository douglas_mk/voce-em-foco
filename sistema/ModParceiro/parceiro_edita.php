<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('parceiro');
$acesso->getAcesso('editar',true);

$aviso = false;
$erro = false;

$id_parceiro = (int) $_GET['id'];

$sql = new Mysql;
$data = new Data;
$tpl = new  Template('template/parceiro_edita.html');

$diretorio = '../../arquivos/parceiros/';

if(isset($_POST['Submit'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['nome'],'texto','Nome');
	$form->Valida($post['cidade'],'texto','Cidade');
	$form->Valida($post['uf'],'texto','Estado');
	if($post['telefone']){
		$form->Valida($post['telefone'],'texto','Telefone');
		$form->Valida($post['telefone'],'numerico','Telefone');
	}
	$id_parceiro = $post['id'];

	$erro = $form->getErro();
	if(!$erro){
		//verifica foto da capa
		$foto = NULL;
		$ins = NULL;
		$img = new UploadImg($_FILES['foto']);
		if($img->getTamanho() > 0){
			$img->setQuali(85);
			$img->setLargura(550);
			$img->setAltura(413);
			$img->setFixa('altura');
			$img->setNome(date('dmYhis'));
			$maior = $img->Gera($diretorio);	
			
			$img->setLimite(true);
			$img->setLargura(100);
			$img->setAltura(75);
			$mini = $img->Gera($diretorio.'mini/');	
			
			if((!$maior) || (!$mini)){
				$erro = $img->getErro();
			}else{
				$foto =  $img->getNome();	
				$ins = "foto='$foto',";
			}
		}
		
		//cadastra
		if(!$erro){
			$sql->Consulta("UPDATE parceiros SET
			id_estado='$post[uf]',
			nome='$post[nome]',
			email='$post[email]',
			site='$post[site]',
			telefone='$post[telefone]',
			endereco='$post[endereco]',
			$ins
			cidade='$post[cidade]',
			informacoes='$post[informacoes]'
			WHERE id='$id_parceiro' LIMIT 1");
			
			$aviso = 'Parceiro atualizado com sucesso';
		}
	}
}

//dados
$query = $sql->Consulta("SELECT * FROM parceiros WHERE id='$id_parceiro' LIMIT 1");
$parceiro = mysql_fetch_array($query);

//estados
$estado = NULL;
$query = $sql->Consulta("SELECT * FROM estados WHERE estado='1' ORDER BY nome_estado ASC");
while($linha = mysql_fetch_array($query)){
	$sel = NULL;
	if($parceiro['id_estado'] == $linha['id']){ $sel = ' selected="selected"'; }
	$estado .= '<option value="'.$linha['id'].'"'.$sel.'>'.$linha['nome_estado'].'</option>';
}

$tpl->associa('FOTO',Vazio(LinkFoto($diretorio,$parceiro['foto'],0)));
$tpl->associa('ID',$id_parceiro);
$tpl->associa('SUBTITULO',FormataTitulo($parceiro['nome']));
$tpl->associa('ESTADOS',$estado);
$tpl->associa('NOME',$parceiro['nome']);
$tpl->associa('EMAIL',$parceiro['email']);
$tpl->associa('SITE',$parceiro['site']);
$tpl->associa('TELEFONE',$parceiro['telefone']);
$tpl->associa('ENDERECO',$parceiro['endereco']);
$tpl->associa('CIDADE',$parceiro['cidade']);
$tpl->associa('INFORMACOES',$parceiro['informacoes']);
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();

?>