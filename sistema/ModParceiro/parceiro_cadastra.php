<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('parceiro');
$acesso->getAcesso('cadastrar',true);

$aviso = false;
$erro = false;

$sql = new Mysql;
$data = new Data;
$tpl = new  Template('template/parceiro_cadastra.html');

$diretorio = '../../arquivos/parceiros/';

if(!isset($_POST['nome'])){ $_POST['nome'] = NULL; }
if(!isset($_POST['uf'])){ $_POST['uf'] = NULL; }
if(!isset($_POST['cidade'])){ $_POST['cidade'] = NULL; }
if(!isset($_POST['email'])){ $_POST['email'] = NULL; }
if(!isset($_POST['site'])){ $_POST['site'] = NULL; }
if(!isset($_POST['telefone'])){ $_POST['telefone'] = NULL; }
if(!isset($_POST['endereco'])){ $_POST['endereco'] = NULL; }
if(!isset($_POST['informacoes'])){ $_POST['informacoes'] = NULL; }

if(isset($_POST['Submit'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['nome'],'texto','Nome');
	$form->Valida($post['cidade'],'texto','Cidade');
	$form->Valida($post['uf'],'texto','Estado');
	if($post['telefone']){
		$form->Valida($post['telefone'],'texto','Telefone');
		$form->Valida($post['telefone'],'numerico','Telefone');
	}

	$erro = $form->getErro();
	if(!$erro){
		//verifica foto da capa
		$foto = NULL;
		$img = new UploadImg($_FILES['foto']);
		if($img->getTamanho() > 0){
			$img->setQuali(85);
			$img->setLargura(550);
			$img->setAltura(413);
			$img->setFixa('altura');
			$img->setNome(date('dmYhis'));
			$maior = $img->Gera($diretorio);	
			
			$img->setLimite(true);
			$img->setLargura(100);
			$img->setAltura(75);
			$mini = $img->Gera($diretorio.'mini/');	
			
			if((!$maior) || (!$mini)){
				$erro = $img->getErro();
			}else{
				$foto =  $img->getNome();	
			}
		}
		
		//cadastra
		if(!$erro){
			$sql->Consulta("INSERT INTO parceiros
			(id_estado,estado,nome,email,site,telefone,endereco,cidade,informacoes,foto,data_cadastro)
			VALUES
			('$post[uf]','1','$post[nome]','$post[email]','$post[site]','$post[telefone]','$post[endereco]','$post[cidade]','$post[informacoes]','$foto',NOW())");
			
			$aviso = 'Parceiro cadastrado com sucesso';
			$_POST = NULL;
		}
	}
}

//estados
$estado = NULL;
$query = $sql->Consulta("SELECT * FROM estados WHERE estado='1' ORDER BY nome_estado ASC");
while($linha = mysql_fetch_array($query)){
	$sel = NULL;
	if($_POST['uf'] == $linha['id']){ $sel = ' selected="selected"'; }
	$estado .= '<option value="'.$linha['id'].'"'.$sel.'>'.$linha['nome_estado'].'</option>';
}

$tpl->associa('ESTADOS',$estado);
$tpl->associa('NOME',$_POST['nome']);
$tpl->associa('EMAIL',$_POST['email']);
$tpl->associa('SITE',$_POST['site']);
$tpl->associa('TELEFONE',$_POST['telefone']);
$tpl->associa('ENDERECO',$_POST['endereco']);
$tpl->associa('CIDADE',$_POST['cidade']);
$tpl->associa('INFORMACOES',$_POST['informacoes']);
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();

?>