<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('videos');
$acesso->getAcesso('listar',true);

$aviso = false;
$erro = false;
$and = NULL;

$id_video = (int) $_GET['id'];

$sql = new Mysql;
$data = new Data;
$tpl = new Template('template/video_lista_com.html');
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);
$pag->AddLink('id',$id_video);

$diretorio = '../../arquivos/muralfotos/';

//dados
$query = $sql->Consulta("SELECT * FROM videos WHERE id='$id_video' LIMIT 1");
$video = mysql_fetch_array($query);

//busca	
if(isset($_GET['busca'])){
	$and = " AND (coment_video.nome LIKE '%".$_GET['busca']."%' OR coment_video.comentario LIKE '%".$_GET['busca']."%' OR coment_video.email LIKE '%".$_GET['busca']."%')";
	$pag->AddLink('busca',$_GET['busca']);
}	

//acoes
if(isset($_POST['acao'])){
	$id_com = $_POST['id'];
	$id_s = $_POST['ids'];

	switch($_POST['acao']){
		case 'ativar':
			if(!$acesso->getAcesso('editar',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE coment_video SET estado='1' WHERE id='$id_com' LIMIT 1");
				$aviso = 'Coment�rio ativado com sucesso';
			}
		break;
		case 'desativar':
			if(!$acesso->getAcesso('editar',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE coment_video SET estado='0' WHERE id='$id_com' LIMIT 1");
				$aviso = 'Comet�rio desativado com sucesso';
			}
		break;
		case 'excluir':
			if(!$acesso->getAcesso('excluir',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE coment_video SET estado='9' WHERE id='$id_com' LIMIT 1");
				$aviso = 'Coment�rio exclu�do com sucesso';
			}
		break;
	}
}

//paginacao		
$pag->setRegistro(8);
$pag->setTotal($sql->Totalreg("SELECT * FROM coment_video WHERE estado!='9' AND id_video='$id_video' $and")); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('Nenhum registro encontrado');

//lista
$tpl->loop('lista');
$query = $sql->Consulta("SELECT * FROM coment_video WHERE estado!='9' AND id_video='$id_video' $and ORDER BY id DESC LIMIT $min,$max");
while($linha = mysql_fetch_array($query)){
	$tpl->associaloop('ID',$linha['id']);
	$tpl->associaloop('NOME',$linha['nome']);
	$tpl->associaloop('CIDADE',$linha['cidade']);
	$tpl->associaloop('EMAIL',$linha['email']);
	$tpl->associaloop('FOTO',Vazio(LinkFoto($diretorio,$linha['foto'],1)));
	$tpl->associaloop('DATA',$data->MysqlData($linha['data']));
	$tpl->associaloop('HORA',$linha['hora']);
	$tpl->associaloop('COMENTARIO',nl2br($linha['comentario']));
	$tpl->associaloop('IP',$linha['ip']);
	
	$estado = 'Desativar'; $acao = 'desativar'; $class = 'titulolista';
	if($linha['estado'] == '0'){ $estado = 'Ativar'; $acao = 'ativar'; $class = 'titulolistad'; }
	
	$tpl->associaloop('ACAO',$acao);
	$tpl->associaloop('ESTADO',$estado);
	$tpl->associaloop('CLASSE',$class);
	$tpl->processaloop();
}
$tpl->fechaloop();

$tpl->associa('ID',$id_video);
$tpl->associa('SUBTITULO',FormataTitulo($video['titulo']));
$tpl->associa('PAGINACAO',$paginas);
$tpl->associa('TOTALREG',$pag->getTotal());
$tpl->associa('MIN',($min + 1));
$tpl->associa('MAX',($min + $max));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>