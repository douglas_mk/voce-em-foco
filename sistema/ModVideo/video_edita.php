<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('videos');
$acesso->getAcesso('editar',true);

$aviso = false;
$erro = false;

$id_video = (int) $_GET['id'];

$sql = new Mysql;
$data = new Data;
$tpl = new  Template('template/video_edita.html');

$diretorio = '../../arquivos/videos/';

if(isset($_POST['Submit'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['titulo'],'texto','Titulo');
	$form->Valida($post['data'],'data','Data');
	$form->Valida($post['duracao'],'hora','Dura��o');
	$form->Valida($post['codigo'],'texto','C�digo');
	$id_video = $post['id'];

	$erro = $form->getErro();
	if(!$erro){
		//verifica miniatura
		$miniatura = NULL;
		$ins = NULL;
		$img = new UploadImg($_FILES['miniatura']);
		if($img->getTamanho() > 0){
			$img->setQuali(85);
			$img->setLargura(100);
			$img->setAltura(75);
			$img->setFixa('altura');
			$img->setNome(date('dmYhis'));
			$mini = $img->Gera($diretorio.'mini/');	
			
			if(!$mini){
				$erro = $img->getErro();
			}
			$miniatura =  $img->getNome();	
			$ins = "miniatura='$miniatura',";
		}

		//cadastra
		if(!$erro){
			if(!isset($post['exibe_capa'])){ $post['exibe_capa'] = 0; }
			if(!isset($post['destaque'])){ $post['destaque'] = 0; }
			$post['data'] = $data->DataMysql($post['data']);
		
			$sql->Consulta("UPDATE videos SET
			exibe_capa='$post[exibe_capa]',
			destaque='$post[destaque]',
			titulo='$post[titulo]',
			descricao='$post[descricao]',
			codigo='$post[codigo]',
			$ins
			duracao='$post[duracao]',
			data='$post[data]'
			WHERE id='$id_video' LIMIT 1");
			
			$aviso = 'V�deo atualizado com sucesso';
			$_POST = NULL;
		}
	}
}

//seleciona dados
$query = $sql->Consulta("SELECT * FROM videos WHERE id='$id_video' LIMIT 1");
$linha = mysql_fetch_array($query);
$linha['data'] = $data->MysqlData($linha['data']);

$tpl->associa('MINIATURA',Vazio(LinkFoto($diretorio,$linha['miniatura'],0)));
$tpl->associa('TITULO',$linha['titulo']);
$tpl->associa('ID',$id_video);
$tpl->associa('SUBTITULO',FormataTitulo($linha['titulo']));
$tpl->associa('DATA',Marcado($linha['data'],false,date('d/m/Y'),$linha['data']));
$tpl->associa('DURACAO',Marcado($linha['duracao'],false,'00:00:00',$linha['duracao']));
$tpl->associa('DESTAQUE',Marcado($linha['destaque'],'1','checked="checked"',false));
$tpl->associa('EXIBECAPA',Marcado($linha['exibe_capa'],'1','checked="checked"',false));
$tpl->associa('DESCRICAO',$linha['descricao']);
$tpl->associa('CODIGO',$linha['codigo']);
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>