<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('videos');
$acesso->getAcesso('cadastrar',true);

$aviso = false;
$erro = false;

$sql = new Mysql;
$data = new Data;
$tpl = new  Template('template/video_cadastra.html');

$diretorio = '../../arquivos/videos/';

if(!isset($_POST['titulo'])){ $_POST['titulo'] = NULL; }
if(!isset($_POST['data'])){ $_POST['data'] = NULL; }
if(!isset($_POST['duracao'])){ $_POST['duracao'] = NULL; }
if(!isset($_POST['destaque'])){ $_POST['destaque'] = NULL; }
if(!isset($_POST['videox'])){ $_POST['videox'] = NULL; }
if(!isset($_POST['videoy'])){ $_POST['videoy'] = NULL; }
if(!isset($_POST['descricao'])){ $_POST['descricao'] = NULL; }
if(!isset($_POST['codigo'])){ $_POST['codigo'] = NULL; }

if(isset($_POST['Submit'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['titulo'],'texto','Titulo');
	$form->Valida($post['data'],'data','Data');
	$form->Valida($post['duracao'],'hora','Dura��o');

	$erro = $form->getErro();
	if(!$erro){
		//verifica miniatura
		$miniatura = NULL;
		$img = new UploadImg($_FILES['miniatura']);
		if($img->getTamanho() > 0){
			$img->setQuali(85);
			$img->setLargura(100);
			$img->setAltura(75);
			$img->setFixa('altura');
			$img->setNome(date('dmYhis'));
			$mini = $img->Gera($diretorio.'mini/');	
			
			if(!$mini){
				$erro = $img->getErro();
			}else{
				$miniatura =  $img->getNome();	
			}
		}
		
		//verifica tipo
		//1 - enviar v�deo ao servidor
		if($post['tipo'] == '1'){
			$form->Valida($post['videox'],'numerico','Largura do v�deo');
			$form->Valida($post['videoy'],'numerico','Altura do v�deo');
			
			$erro = $form->getErro();
			if(!$erro){
				$codigo = '<object width="{LARGURA}" height="{ALTURA}" type="application/x-shockwave-flash" data="{BASE}{VIDEO}">';
				$codigo .= '<param name="movie" value="{BASE}{VIDEO}" />';
				$codigo .= '<param name="quality" value="high" />';
				$codigo .= '<param name="wmode" value="transparent" />';
				//<embed src="videoplay.swf?videoplay='.$pasta[videos].$video.'&autop=0" quality="high" pluginspage="http://www.macromedia.com/go/getflashplayer"
				//type="application/x-shockwave-flash" width="240" height="225"  wmode="transparent"></embed>
				$codigo .= '</object>';
			
				$upload = new Upload($_FILES['video']);
				$upload->setTipo('application/octet-stream');
				$upload->setNome(date('dmyhis'));
				
				if(!$upload->Envia($diretorio)){
					$erro = $upload->getErro();
				}else{
					$codigo = str_replace('{VIDEO}',$upload->getNome(),$codigo);
					$codigo = str_replace('{ALTURA}',$post['videoy'],$codigo);
					$codigo = str_replace('{LARGURA}',$post['videox'],$codigo);
				}
			}
		}else{
		//2 - c�digo do v�deo
			$form->Valida($post['codigo'],'texto','C�digo');
			$erro = $form->getErro();
			$codigo = $post['codigo'];
		}

		//cadastra
		if(!$erro){
			if(!$post['exibe_capa']){ $post['exibe_capa'] = 0; }
			if(!$post['destaque']){ $post['destaque'] = 0; }
			$post['data'] = $data->DataMysql($post['data']);
		
			$sql->Consulta("INSERT INTO videos
			(estado,exibe_capa,destaque,titulo,descricao,codigo,miniatura,duracao,data,hora,acessos)
			VALUES
			('1','$post[exibe_capa]','$post[destaque]','$post[titulo]','$post[descricao]','$codigo',
			'$miniatura','$post[duracao]','$post[data]',NOW(),'0')");
			
			$aviso = 'V�deo cadastrado com sucesso';
			$_POST = NULL;
		}
	}
}

$tpl->associa('TITULO',$_POST['titulo']);
$tpl->associa('DATA',Marcado($_POST['data'],false,date('d/m/Y'),$_POST['data']));
$tpl->associa('DURACAO',Marcado($_POST['duracao'],false,'00:00:00',$_POST['duracao']));
$tpl->associa('DESTAQUE',Marcado($_POST['destaque'],'1','checked="checked"',false));
$tpl->associa('VIDEOY',Marcado($_POST['videoy'],false,225,$_POST['videoy']));
$tpl->associa('VIDEOX',Marcado($_POST['videox'],false,240,$_POST['videox']));
$tpl->associa('DESCRICAO',$_POST['descricao']);
$tpl->associa('CODIGO',$_POST['codigo']);
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>