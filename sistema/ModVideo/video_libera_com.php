<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('videos');
$acesso->getAcesso('listar',true);

$aviso = false;
$erro = false;

$sql = new Mysql;
$data = new Data;
$tpl = new Template('template/video_libera_com.html');
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);

$diretorio = '../../arquivos/videos/';

if(!isset($_POST['idcom'])){ $_POST['idcom'] = NULL; }

//excluir
if(isset($_POST['excluir'])){
	$idcom = $_POST['idcom'];
	
	if(sizeof($idcom) == 0){
		$erro = 'Nenhum registro selecionado';
	}else{
		if(!$acesso->getAcesso('excluir',false)){
			$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
		}else{
			//exclui recados
			foreach($idcom as $id){
				$sql->Consulta("UPDATE coment_video SET estado='9' WHERE id='$id' LIMIT 1");	
			}
			$aviso = 'Coment�rio(s) exclu�do(s) com sucesso';
		}
	}	
}

//ativar
if(isset($_POST['liberar'])){
	$idcom = $_POST['idcom'];
	
	if(sizeof($idcom) == 0){
		$erro = 'Nenhum registro selecionado';
	}else{
		if(!$acesso->getAcesso('editar',false)){
			$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
		}else{
			//exclui recados
			foreach($idcom as $id){
				$sql->Consulta("UPDATE coment_video SET estado='1' WHERE id='$id' LIMIT 1");	
			}
			$aviso = 'Coment�rio(s) liberado(s) com sucesso';
		}
	}	
}

//paginacao		
$pag->setRegistro(5);
$pag->setTotal($sql->Totalreg("SELECT * FROM coment_video WHERE estado='0'")); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('Nenhum registro encontrado');

//lista
$tpl->loop('lista');
$query = $sql->Consulta("SELECT coment_video.*,videos.titulo,videos.descricao,videos.miniatura
FROM coment_video
LEFT JOIN videos ON videos.id=coment_video.id_video
WHERE coment_video.estado='0'
ORDER BY coment_video.id DESC LIMIT $min,$max");
while($linha = mysql_fetch_array($query)){
	$tpl->associaloop('ID',$linha['id']);
	$tpl->associaloop('NOME',$linha['nome']);
	$tpl->associaloop('CIDADE',$linha['cidade']);
	$tpl->associaloop('EMAIL',$linha['email']);
	$tpl->associaloop('FOTO',LinkFoto($diretorio,$linha['foto'],1));
	$tpl->associaloop('DATA',$data->MysqlData($linha['data']));
	$tpl->associaloop('HORA',$linha['hora']);
	$tpl->associaloop('COMENTARIO',nl2br($linha['comentario']));
	
	$tpl->associaloop('TITULOV',$linha['titulo']);
	$tpl->associaloop('FOTOV',LinkFoto($diretorio,$linha['miniatura'],0));
	$tpl->associaloop('DESCRICAOV',nl2br($linha['descricao']));
	$tpl->processaloop();
}
$tpl->fechaloop();

$tpl->associa('PAGINACAO',$paginas);
$tpl->associa('TOTALREG',$pag->getTotal());
$tpl->associa('MIN',($min + 1));
$tpl->associa('MAX',($min + $max));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>