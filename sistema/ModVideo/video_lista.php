<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('videos');
$acesso->getAcesso('listar',true);

$aviso = false;
$erro = false;
$and = NULL;

$sql = new Mysql;
$data = new Data;
$tpl = new Template('template/video_lista.html');
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);

$diretorio = '../../arquivos/videos/';
$player = 'videoplay.swf?autop=0&amp;videoplay='.$diretorio;

//busca	
if(isset($_GET['busca'])){
	$and = " AND (videos.titulo LIKE '%".$_GET['busca']."%' OR videos.descricao LIKE '%".$_GET['busca']."%')";
	$pag->AddLink('busca',$_GET['busca']);
}	

//acoes
if(isset($_POST['acao'])){
	$id_video = $_POST['id'];
	$id_s = $_POST['ids'];

	switch($_POST['acao']){
		case 'ativar':
			if(!$acesso->getAcesso('editar',false)){
			$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE videos SET estado='1' WHERE id='$id_video' LIMIT 1");
				$aviso = 'V�deo ativado com sucesso';
			}
		break;
		case 'desativar':
			if(!$acesso->getAcesso('editar',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE videos SET estado='0' WHERE id='$id_video' LIMIT 1");
				$aviso = 'V�deo desativado com sucesso';
			}
		break;
		case 'excluir':
			if(!$acesso->getAcesso('excluir',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE videos SET estado='9' WHERE id='$id_video' LIMIT 1");
				$aviso = 'V�deo exclu�do com sucesso';
			}
		break;
	}
}

//paginacao		
$pag->setRegistro(5);
$pag->setTotal($sql->Totalreg("SELECT * FROM videos WHERE estado!='9' $and")); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('Nenhum registro encontrado');

//lista
$tpl->loop('lista');
$query = $sql->Consulta("SELECT * FROM videos WHERE estado!='9' $and ORDER BY id DESC LIMIT $min,$max");
while($linha = mysql_fetch_array($query)){
	$tpl->associaloop('ID',$linha['id']);
	$tpl->associaloop('TITULO',$linha['titulo']);
	$tpl->associaloop('DURACAO',$linha['duracao']);
	$tpl->associaloop('ACESSOS',$linha['acessos']);
	$tpl->associaloop('DESCRICAO',Vazio(nl2br($linha['descricao'])));
	$tpl->associaloop('EXIBECAPA',Marcado($linha['exibe_capa'],'1','Sim','N�o'));
	$tpl->associaloop('DESTAQUE',Marcado($linha['destaque'],'1','Sim','N�o'));
	$tpl->associaloop('MINIATURA',Vazio(LinkFoto($diretorio,$linha['miniatura'],0)));
	$tpl->associaloop('DATA',$data->MysqlData($linha['data']));
	
	$totalc = $sql->Totalreg("SELECT * FROM coment_video WHERE id_video='$linha[id]' AND estado!='9'");
	$tpl->associaloop('TOTALC',$totalc);
	
	//v�deo
	$video = $linha['codigo'];
	$video = str_replace('{BASE}',$player,$video);
	$tpl->associaloop('VIDEO',$video);
	

	$estado = 'Desativar'; $acao = 'desativar'; $class = 'titulolista';
	if($linha['estado'] == '0'){ $estado = 'Ativar'; $acao = 'ativar'; $class = 'titulolistad'; }
	
	$tpl->associaloop('ACAO',$acao);
	$tpl->associaloop('ESTADO',$estado);
	$tpl->associaloop('CLASSE',$class);
	$tpl->processaloop();
}
$tpl->fechaloop();

$tpl->associa('PAGINACAO',$paginas);
$tpl->associa('TOTALREG',$pag->getTotal());
$tpl->associa('MIN',($min + 1));
$tpl->associa('MAX',($min + $max));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>