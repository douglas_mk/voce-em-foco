<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('dicavideos');
$acesso->getAcesso('editar',true);

$aviso = false;
$erro = false;

$id_dica = (int) $_GET['id'];

$sql = new Mysql;
$data = new Data;
$tpl = new  Template('template/dicavideo_edita.html');

$diretorio = '../../arquivos/dicasvideos/';

if(isset($_POST['Submit'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['titulo'],'texto','T�tulo');
	$form->Valida($post['genero'],'texto','G�nero');
	$id_dica = $post['id'];

	$erro = $form->getErro();
	if(!$erro){
		//verifica foto da capa
		$foto =  NULL;
		$ins = NULL;	
		$img = new UploadImg($_FILES['cartaz']);
		if($img->getTamanho() > 0){
			$img->setQuali(85);
			$img->setLargura(550);
			$img->setAltura(413);
			$img->setFixa('altura');
			$img->setNome(date('dmYhis'));
			$maior = $img->Gera($diretorio);	
			
			$img->setLimite(true);
			$img->setLargura(100);
			$img->setAltura(75);
			$mini = $img->Gera($diretorio.'mini/');	
			
			if((!$maior) || (!$mini)){
				$erro = $img->getErro();
			}else{
				$foto =  $img->getNome();
				$ins = "cartaz='$foto',";	
			}
		}
		
		//cadastra
		if(!$erro){
			$sql->Consulta("UPDATE dicasvideos SET
			titulo='$post[titulo]',
			genero='$post[genero]',
			classificacao='$post[classificacao]',
			direcao='$post[direcao]',
			atores='$post[atores]',
			sinopse='$post[sinopse]',
			$ins
			locadora='$post[locadora]'
			WHERE id='$id_dica' LIMIT 1");
			
			$aviso = 'Dica de v�deo atualizada com sucesso';
		}
	}
}

//dados
$query = $sql->Consulta("SELECT * FROM dicasvideos WHERE id='$id_dica' LIMIT 1");
$video = mysql_fetch_array($query);

$tpl->associa('CARTAZ',Vazio(LinkFoto($diretorio,$video['cartaz'],0)));
$tpl->associa('ID',$id_dica);
$tpl->associa('SUBTITULO',FormataTitulo($video['titulo']));
$tpl->associa('TITULO',$video['titulo']);
$tpl->associa('GENERO',$video['genero']);
$tpl->associa('CLASSIFICACAO',$video['classificacao']);
$tpl->associa('DIRECAO',$video['direcao']);
$tpl->associa('LOCADORA',$video['locadora']);
$tpl->associa('ATORES',$video['atores']);
$tpl->associa('SINOPSE',$video['sinopse']);
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>