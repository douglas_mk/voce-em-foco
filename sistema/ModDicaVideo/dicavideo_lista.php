<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('dicavideos');
$acesso->getAcesso('listar',true);

$aviso = false;
$erro = false;
$and = NULL;

$sql = new Mysql;
$data = new Data;
$tpl = new Template('template/dicavideo_lista.html');
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);

$diretorio = '../../arquivos/dicasvideos/';

//busca	
if(isset($_GET['busca'])){
	$and = " AND (dicasvideos.titulo LIKE '%".$_GET['busca']."%' OR dicasvideos.locadora LIKE '%".$_GET['busca']."%')";
	$pag->AddLink('busca',$_GET['busca']);
}	

//acoes
if(isset($_POST['acao'])){
	$id_dica = $_POST['id'];
	$id_s = $_POST['ids'];

	switch($_POST['acao']){
		case 'ativar':
			if(!$acesso->getAcesso('editar',false)){
			$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE dicasvideos SET estado='1' WHERE id='$id_dica' LIMIT 1");
				$aviso = 'Dica ativada com sucesso';
			}
		break;
		case 'desativar':
			if(!$acesso->getAcesso('editar',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE dicasvideos SET estado='0' WHERE id='$id_dica' LIMIT 1");
				$aviso = 'Dica desativada com sucesso';
			}
		break;
		case 'excluir':
			if(!$acesso->getAcesso('excluir',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE dicasvideos SET estado='9' WHERE id='$id_dica' LIMIT 1");
				$aviso = 'Dica exclu�da com sucesso';
			}
		break;
	}
}

//paginacao		
$pag->setRegistro(5);
$pag->setTotal($sql->Totalreg("SELECT * FROM dicasvideos WHERE estado!='9' $and")); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('Nenhum registro encontrado');

//lista
$tpl->loop('lista');
$query = $sql->Consulta("SELECT dicasvideos.*
FROM dicasvideos
WHERE dicasvideos.estado!='9' $and ORDER BY dicasvideos.id DESC LIMIT $min,$max");
while($linha = mysql_fetch_array($query)){
	$tpl->associaloop('ID',$linha['id']);
	$tpl->associaloop('TITULO',$linha['titulo']);
	$tpl->associaloop('GENERO',Vazio($linha['genero']));
	$tpl->associaloop('CLASSIFICACAO',Vazio($linha['classificacao']));
	$tpl->associaloop('DIRECAO',Vazio($linha['direcao']));
	$tpl->associaloop('LOCADORA',Vazio($linha['locadora']));
	$tpl->associaloop('ATORES',Vazio(nl2br($linha['atores'])));
	$tpl->associaloop('SINOPSE',Vazio(nl2br($linha['sinopse'])));
	$tpl->associaloop('CARTAZ',Vazio(LinkFoto($diretorio,$linha['cartaz'],1)));
	$tpl->associaloop('DATACAD',$data->MysqlData($linha['data_cadastro']));

	$estado = 'Desativar'; $acao = 'desativar'; $class = 'titulolista';
	if($linha['estado'] == '0'){ $estado = 'Ativar'; $acao = 'ativar'; $class = 'titulolistad'; }
	
	$tpl->associaloop('ACAO',$acao);
	$tpl->associaloop('ESTADO',$estado);
	$tpl->associaloop('CLASSE',$class);
	$tpl->processaloop();
}
$tpl->fechaloop();

$tpl->associa('PAGINACAO',$paginas);
$tpl->associa('TOTALREG',$pag->getTotal());
$tpl->associa('MIN',($min + 1));
$tpl->associa('MAX',($min + $max));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>