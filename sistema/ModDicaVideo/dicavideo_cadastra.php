<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('dicavideos');
$acesso->getAcesso('cadastrar',true);

$aviso = false;
$erro = false;

$sql = new Mysql;
$data = new Data;
$tpl = new  Template('template/dicavideo_cadastra.html');

$diretorio = '../../arquivos/dicasvideos/';

if(!isset($_POST['titulo'])){ $_POST['titulo'] = NULL; }
if(!isset($_POST['genero'])){ $_POST['genero'] = NULL; }
if(!isset($_POST['classificacao'])){ $_POST['classificacao'] = NULL; }
if(!isset($_POST['direcao'])){ $_POST['direcao'] = NULL; }
if(!isset($_POST['locadora'])){ $_POST['locadora'] = NULL; }
if(!isset($_POST['atores'])){ $_POST['atores'] = NULL; }
if(!isset($_POST['sinopse'])){ $_POST['sinopse'] = NULL; }

if(isset($_POST['Submit'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['titulo'],'texto','T�tulo');
	$form->Valida($post['genero'],'texto','G�nero');

	$erro = $form->getErro();
	if(!$erro){
		//verifica foto da capa
		$foto = NULL;
		$img = new UploadImg($_FILES['cartaz']);
		if($img->getTamanho() > 0){
			$img->setQuali(85);
			$img->setLargura(550);
			$img->setAltura(413);
			$img->setFixa('altura');
			$img->setNome(date('dmYhis'));
			$maior = $img->Gera($diretorio);	
			
			$img->setLimite(true);
			$img->setLargura(100);
			$img->setAltura(75);
			$mini = $img->Gera($diretorio.'mini/');	
			
			if((!$maior) || (!$mini)){
				$erro = $img->getErro();
			}else{
				$foto =  $img->getNome();	
			}
		}
		
		//cadastra
		if(!$erro){
			$sql->Consulta("INSERT INTO dicasvideos
			(estado,destaque,titulo,genero,classificacao,direcao,atores,sinopse,locadora,cartaz,data_cadastro)
			VALUES
			('1','0','$post[titulo]','$post[genero]','$post[classificacao]','$post[direcao]','$post[atores]','$post[sinopse]','$post[locadora]','$foto',NOW())");
			
			$aviso = 'Dica de v�deo cadastrada com sucesso';
			$_POST = NULL;
		}
	}
}


$tpl->associa('TITULO',$_POST['titulo']);
$tpl->associa('GENERO',$_POST['genero']);
$tpl->associa('CLASSIFICACAO',$_POST['classificacao']);
$tpl->associa('DIRECAO',$_POST['direcao']);
$tpl->associa('LOCADORA',$_POST['locadora']);
$tpl->associa('ATORES',$_POST['atores']);
$tpl->associa('SINOPSE',$_POST['sinopse']);
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>