<?php
class Acesso{

	var $local;

	function Acesso(){
		session_start();
	}
	
	//inicializa sess�o
	function setSessao($variaveis){
		$_SESSION['idchave'] = 'b3939bcfe1841261d8c33449ce28fc03';
		
		foreach($variaveis as $chave => $valor){
			$_SESSION[$chave] = $valor;
		}
	}
	
	//verifica sess�o
	function getSessao(){
		if((!isset($_SESSION['idchave'])) || ($_SESSION['idchave'] != 'b3939bcfe1841261d8c33449ce28fc03')){
			header('Location:index.php');
			exit();
		}
	}
	
	//encerra sess�o
	function EncerraSessao(){
		session_unset();
		@session_destroy();
	}
	
	//seta local
	function setLocal($local){
		$this->local = $local;
	}
	
	//verifica n�vel de acesso
	function getAcesso($acao,$tipo){
		//tipo: true = redireciona, false = mensagem de erro;
		if(!$_SESSION['acessos']){
			header('Location:index.php');
			exit();
		}else{	
			$acessos = $_SESSION['acessos'];
			
			$copia = explode(',',$acessos[$this->local]);
			
			if(array_search($acao,$copia) === false){
				if($tipo == true){
					header('Location:index.php');
					exit();
				}else{
					return false;
				}
			}else{
				return true;
			}
		}
	}
	
	//retorna par�mtro
	function getParametro($local){
		return $_SESSION[$local];
	}
}
?>