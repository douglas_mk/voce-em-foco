<?php
class Categoria {
	var $menu;
	var $caminho = array();
	var $menufilho = array();
	var $menupai = array();
	var $menuarvore = array();
	
	function Categoria(){
    }
	
	function Arvore($id_cat){
		unset($this->menuarvore);
		$this->GeraArvore($id_cat);
		return $this->menuarvore;	
	}
	
	//verifica a existÍncia de filhos
	function Existe_filho($id){
		$sql = new Mysql;
		return $sql->Totalreg("SELECT * FROM categorias WHERE id_pai='$id' AND estado='1'");
	}

	//exibe os fihos
	function GeraArvore($id = 0){

		  $sql = new Mysql;
		  $query = $sql->Consulta("SELECT id,categoria,nivel,id_pai FROM categorias WHERE id_pai='$id' AND estado='1' ORDER BY categoria ASC"); 
		  while ($linha = mysql_fetch_array($query)){ 
				$id = $linha['id'];
				
				$indice = sizeof($this->menuarvore);
				
				$this->menuarvore[$indice][id] = $linha['id'];
				$this->menuarvore[$indice][id_pai] = $linha['id_pai'];
				$this->menuarvore[$indice][categoria] = $linha['categoria'];
				$this->menuarvore[$indice][nivel] = $linha['nivel'];
				
				//verifica existÍncia de filhos
				if($sql->Totalreg("SELECT * FROM categorias WHERE id_pai='$id' AND estado='1' LIMIT 1") > 0){
					$this->GeraArvore($id);
				}
		  }
	 }

	 //retorna um vetor com o caminho da categoria
	 function Caminho($id_cat){
		  if($id_cat){
		  		unset($this->caminho);
		  		$sql = new Mysql;
				$id = $id_cat;
				do{
					$query = $sql->Consulta("SELECT id,categoria,id_pai FROM categorias WHERE id='$id' AND estado='1' LIMIT 1"); 
					$linha = mysql_fetch_array($query);
					$id = $linha['id_pai'];
					$id_cat = $linha['id'];

					$this->caminho[$linha['id']] = $linha['categoria'];
				}while($id > 0);
		  }
		  return $this->caminho;
	 }
	 
	//retorna o codigo da arvore
	function getArvore($id){
		$sql = new Mysql;
		$query = $sql->Consulta("SELECT arvore FROM categorias WHERE id='$id' LIMIT 1"); 
		$linha = mysql_fetch_array($query); 
		return $linha['arvore'];	
	 }
	 
	 //retorna  um vetor com as categorias pai
	 function Menu_pai(){
		$sql = new Mysql;
		$query = $sql->Consulta("SELECT id,categoria FROM categorias WHERE id_pai='0' AND estado='1' ORDER BY categoria ASC"); 
		while ($linha = mysql_fetch_array($query)){ 
			$this->menupai[$linha['id']] = $linha['categoria'];
		}
	 	return $this->menupai;
	 }
	 
	 //retorna um vetor com as subcategorias da categoria pai
	 function Menu_filho($id){
		 if($id > 0){
			$sql = new Mysql;
			$query = $sql->Consulta("SELECT id,categoria FROM categorias WHERE id_pai='$id' AND estado='1' ORDER BY categoria ASC"); 
			while ($linha = mysql_fetch_array($query)){ 
				$this->menufilho[$linha['id']] = $linha['categoria'];
			}	 
		 }
	  	 return $this->menufilho;
	 }
}
?>