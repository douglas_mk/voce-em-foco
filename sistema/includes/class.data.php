<?php
class Data{

	function Data(){
    }
	
	function DataMysql($data){
		$data = explode("/",$data);		
		$novadata = $data[2].'-'.$data[1].'-'.$data[0];
		return $novadata;
	}
	
	function MysqlData($data){
		$data = explode("-",$data);		
		$novadata = $data[2].'/'.$data[1].'/'.$data[0];
		return $novadata;
	}
	
	function getData(){
		return date('d/m/Y');
	}
	
	function DataExtenso($data){
		if(empty($data)){
			$data = date('d/m/Y');
		}
		$dataa = explode('/',$data);
		$data = mktime(0,0,0,$dataa[1],$dataa[0],$dataa[2]);
		$dias = array('Domingo','Segunda-feira','Ter�a-feira','Quarta-feira','Quinta-feira','Sexta-feira','S�bado');
		$dia = date('w',$data);
		
		switch($dataa[1]){
			case '01': $dataa[1] = 'Janeiro'; break;
			case '02': $dataa[1] = 'Fevereiro'; break;
			case '03': $dataa[1] = 'Mar�o'; break;
			case '04': $dataa[1] = 'Abril'; break;
			case '05': $dataa[1] = 'Maio'; break;
			case '06': $dataa[1] = 'Junho'; break;
			case '07': $dataa[1] = 'Julho'; break;
			case '08': $dataa[1] = 'Agosto'; break;
			case '09': $dataa[1] = 'Setembro'; break;
			case '10': $dataa[1] = 'Outubro'; break;
			case '11': $dataa[1] = 'Novembro'; break;
			case '12': $dataa[1] = 'Dezembro'; break;
		}
		return $dias[$dia].', '.$dataa[0].' de '.$dataa[1].' de '.$dataa[2];
	}
	
	function SomaData($data,$prazo){
		if(empty($data)){
			$data = date('d/m/Y');
		}
		$data = explode('/',$data);
		$data = mktime(0,0,0,$data[1],$data[0] + $prazo,$data[2]);
		$data = date('d/m/Y',$data);
		return $data;
	}
	
	function Diferenca($inicial,$final){
		$inicial = explode('/',$inicial);
		$final = explode('/',$final);
		$inicial = mktime(0,0,0,$inicial[1],$inicial[0],$inicial[2]);
		$final = mktime(0,0,0,$final[1],$final[0],$final[2]);
		return round(($inicial - $final)/86400);  	
	}
	
	function MesExtenso($mes){
		switch($mes){
			case '01': return 'Janeiro'; break;
			case '02': return 'Fevereiro'; break;
			case '03': return 'Mar�o'; break;
			case '04': return 'Abril'; break;
			case '05': return 'Maio'; break;
			case '06': return 'Junho'; break;
			case '07': return 'Julho'; break;
			case '08': return 'Agosto'; break;
			case '09': return 'Setembro'; break;
			case '10': return 'Outubro'; break;
			case '11': return 'Novembro'; break;
			case '12': return 'Dezembro'; break;
		}
	}
}
?>