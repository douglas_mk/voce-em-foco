<?php
class Template{

	var $vars = array(); //variaveis gerais
	var $varsloop = array(); //variaveis a inserir no loop 
	var $tpl; //template
	var $loop; //copia a parte do loop a ser substituido
	var $lista = array(); //lista ja rpocessada do loop
	var $lista_inicial; //lisya original a processar
	
	function Template($pagina){
		if(!is_file($pagina)) { 
			print 'P�gina '.$pagina.' n�o � um arquivo ou n�o foi encontrada';
		}else{
			$this->tpl = file_get_contents($pagina); 
		}
    }
	
	//associa vari�veis normais
	function associa($local,$valor){
		$this->vars[$local] = $valor;
	}
	
	//inclui templates atrav�s do php
	function incluitpl($local,$pagina){
		$pagina = file_get_contents($pagina);
		$this->tpl = str_replace('{' . $local . '}',$pagina,$this->tpl); 
	}
	
	//inicia um loop
	function loop($tag){
		$inicio = explode('{LOOP:'.$tag.'}',$this->tpl);
		$fim = explode('{FLOOP:'.$tag.'}',$inicio[1]);
		
		if(sizeof($fim) == 1){ $this->Erro('Fechamento do loop '.$tag.' n�o encontrado.'); }
		
		$this->loop = '{LOOP:'.$tag.'}'.$fim[0].'{FLOOP:'.$tag.'}';
		$this->lista_inicial = $fim[0];
	}

	
	//processa cada loop associando vari�veis
	function processaloop(){
		$lista = $this->lista_inicial;
		foreach ($this->varsloop as $rotulo => $valor){
			$lista = str_replace('{' . $rotulo . '}',$valor,$lista); 
		} 
		$this->lista[] = $lista;
		unset($this->varsloop);
	}
	
	//associa vari�veis no loop a serem processadas pelo processaloop
	function associaloop($local,$valor){
		$this->varsloop[$local] = $valor;
	}
	
	//conlui o loop
	function fechaloop(){
		$lista = implode('',$this->lista);
		$this->tpl = str_replace($this->loop,$lista,$this->tpl); 
		unset($this->lista,$this->loop,$this->lista_sub);
	}
	
	//apaga loop
	function apagaloop($tag,$valor){
		$this->loop($tag);
		$this->tpl = str_replace($this->loop,$valor,$this->tpl); 
	}
	
	//exibe erro
	function Erro($erro){
		die('Classe Template: '.$erro);
	}
	
	//processa p�gina
	function exibe(){
		//processa pagina
		foreach ($this->vars as $rotulo => $valor){
			$this->tpl = str_replace('{' . $rotulo . '}',$valor,$this->tpl); 
		} 
		print $this->tpl;
	}
}
?>