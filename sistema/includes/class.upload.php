<?php
//classe para upload de imagem

class Upload{
	var $erro;
	var $tamanho;
	var $nometmp;
	var $nome;
	var $tipos;
	var $novonome;
	var $tmax = 0;
	
	function Upload($arquivo){
		if($arquivo){
			$this->tamanho = $arquivo['size'];
			$this->nometmp = $arquivo['tmp_name'];
			$this->tipo = $arquivo['type'];
			$this->nome = $arquivo['name'];
			$this->novonome = $this->LimpaNome($this->nome);
		}
    }
	
	//getters e setters
	function setNome($nome){
		$ext = explode('.',$this->nome);
		$this->novonome = $nome.'.'.$ext[sizeof($ext) - 1];
	}
	
	function setTipo($tipos){
		$this->tipos = $tipos;
	}
	
	function setTamanhoMax($tmax){
		$this->tmax = $tmax;
	}
	
	function getErro(){
		return $this->erro;
	}
	
	function getTamanho(){
		return $this->tamanho;
	}

	function getNome(){
		return $this->novonome;
	}
	
	function getTipo(){
		return $this->tipo;
	}
	
	//metodos
	function Envia($diretorio){	
	
		if (!$this->TiposAceitos()){
			$this->erro =  'Tipo de arquivo n�o aceito';
			return false;	
		}elseif($this->tamanho == 0){
			$this->erro =  'Selecione um arquivo';
			return false;
		}elseif(($this->tamanho > ($this->tmax * 1024)) && ($this->tmax > 0)){
			$this->erro =  'Arquivo maior que '.$this->tmax.' KB';
			return false;
		}elseif(!file_exists($diretorio)){
			$this->erro =  'O diret�rio '.$diretorio.' n�o existe';
			return false;
		}elseif(!copy($this->nometmp,$diretorio.$this->novonome)){
			$this->erro =  'Erro ao mover arquivo';
			return false;
		}else{
			return true;
		}
	}
	
	function LimpaNome($nome){
		$nome = strtolower($this->nome);
		$nome = ereg_replace("[��������]","a",$nome);
		$nome = ereg_replace("[������]","e",$nome);
		$nome = ereg_replace("[������]","i",$nome);
		$nome = ereg_replace("[���������]","o",$nome);
		$nome = ereg_replace("[������]","u",$nome);
		$nome = ereg_replace("[��]","c",$nome);
		$nome = ereg_replace(" ","",$nome); 
		$nome = ereg_replace("[%/|\?*:<>^~-]","",$nome);
		$nome = trim($nome);
		return $nome;
	}
	
	function TiposAceitos(){
		if(empty($this->tipos)){
			return true;
		}else{
			if(@substr_count($this->tipos,$this->tipo)){
				return true;
			}else{
				return false;
			}
		}
	}
}
?>