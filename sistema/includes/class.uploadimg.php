<?php
class UploadImg{
	var $imagem;
	var $x;
	var $y;
	var $fixa = 'largura';
	var $local = 'centro';
	var $selo = false;
	var $tamanho = 0;
	var $nometmp;
	var $tipo;
	var $nome;
	var $novonome;
	var $quali = 85;
	var $limite = false;
	var $erro;
	
	function UploadImg($imagem){
		if($imagem){
			$this->tamanho = $imagem['size'];
			$this->nometmp = $imagem['tmp_name'];
			$this->tipo = $imagem['type'];
			$this->nome = $imagem['name'];
		}
    }
	
	//getters e setters
	function setLargura($x){
		$this->x = $x;
	}
	
	function setAltura($y){
		$this->y = $y;
	}
	
	function setFixa($fixa){
		$this->fixa = $fixa;
	}
	
	function setLocal($local){
		$this->local = $local;
	}
	
	function setLimite($limite){
		$this->limite = $limite;
	}
	
	function setSelo($selo){
		$this->selo = $selo;
	}
	
	function setNome($novonome){
		$this->novonome = $novonome;
	}
	
	function setQuali($quali){
		$this->quali = $quali;
	}
	
	function getTamanho(){
		return $this->tamanho;
	}
	
	function getNovoNome(){
		return $this->novonome;
	}
	
	function getNome(){
		return $this->nome;
	}
	
	function getErro(){
		return $this->erro;
	}
	
	//metodos
	function Gera($diretorio){
		if($this->tamanho == 0){
			$this->erro =  'Selecione uma imagem';
			return false;
		}elseif(!file_exists($diretorio)){
			$this->erro =  'O diret�rio '.$diretorio.' n�o existe';
			return false;
		}else{
			$tmpx = $this->x;
			$tmpy = $this->y;
			
			//tipo da imagem (jpg,png,gif)
			switch($this->tipo){
				case 'image/jpeg': $nova = imagecreatefromjpeg($this->nometmp); $ext = '.jpg'; break;
				case 'image/pjpeg': $nova = imagecreatefromjpeg($this->nometmp); $ext = '.jpg'; break;
				case 'image/x-png': $nova = imagecreatefrompng($this->nometmp); $ext = '.png'; break;
				case 'image/gif': $nova = imagecreatefromgif($this->nometmp); $ext = '.gif'; break;
				default:
					$this->erro =  'Tipo de arquivo n�o aceito';
					return false;	
			}
		
			$imgx = imagesx($nova);
			$imgy = imagesy($nova); 
			
			//se a imagem form menor que o informado mantem tamanho
			if($this->y > $imgy){ $this->x = $imgx;};
			if($this->y > $imgy){ $this->y = $imgy; }

			//calcula propor��o
			switch($this->fixa){
				case 'altura': 
					$this->x = ($imgx * $this->y) / $imgy; 
					if(($this->x > $tmpx) && ($this->limite == true)){ $this->x = $tmpx; }
				break;
				case 'largura': 
					 $this->y = ($imgy * $this->x) / $imgx;
					if(($this->y > $tmpy) && ($this->limite == true)){ $this->y = $tmpy; }
				break;
			}
			
			//redimensiona imagem
			$gerada = imagecreatetruecolor($this->x,$this->y); 
			imagecopyresampled($gerada,$nova, 0, 0, 0, 0, $this->x, $this->y, $imgx, $imgy); 
			
			//gera selo
			if($this->selo){
				$selo = imagecreatefromjpeg($this->selo);
				$selox = imagesx($selo);
				$seloy = imagesy($selo); 
				
				switch($local){
					case 'direita': $addx = $this->x - $selox; break;
					case 'esquerda': $addx = 0; break;
					default: $addx = (($this->x - $selox) / 2);
				}
				
				imagecopymerge($gerada,$selo, $addx, ($this->y - $seloy), 0, 0, $selox,$seloy,90);
			}
			
			//gera imagem
			switch($this->tipo){
				case 'image/jpeg': imagejpeg($gerada,$diretorio.$this->novonome.$ext ,$this->quali); break;
				case 'image/pjpeg': imagejpeg($gerada,$diretorio.$this->novonome.$ext ,$this->quali); break;
				case 'image/x-png': imagepng($gerada,$diretorio.$this->novonome.$ext ,$this->quali); break;
				case 'image/gif': imagegif($gerada,$diretorio.$this->novonome.$ext ,$this->quali); break;	
			}
	
			//destroi imagem
			imagedestroy($gerada);
			imagedestroy($nova);
			
			$this->nome = $this->novonome.$ext;
			return true;
		}
	}
}