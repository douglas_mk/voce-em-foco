<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('galerias');
$acesso->getAcesso('listar',true);

$aviso = false;
$erro = false;
$and = NULL;

$id_galeria = (int) $_GET['id'];

$sql = new Mysql;
$data = new Data;
$tpl = new Template('template/galeria_lista_banner.html');
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);
$pag->AddLink('id',$id_galeria);

$diretorio = '../../arquivos/banners/';

//dados
$query = $sql->Consulta("SELECT * FROM galerias WHERE id='$id_galeria' LIMIT 1");
$galeria = mysql_fetch_array($query);

//busca	
if(isset($_GET['busca'])){
	$and = " AND (banner_galeria.local LIKE '%".$_GET['busca']."%' OR banner_galeria.descricao LIKE '%".$_GET['busca']."%')";
	$pag->AddLink('busca',$_GET['busca']);
}	

//acoes
if(isset($_POST['acao'])){
	$id_banner = $_POST['id'];
	$id_s = $_POST['ids'];

	switch($_POST['acao']){
		case 'ativar':
			if(!$acesso->getAcesso('editar',false)){
			$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE banner_galeria SET estado='1' WHERE id='$id_banner' LIMIT 1");
				$aviso = 'Banner ativado com sucesso';
			}
		break;
		case 'desativar':
			if(!$acesso->getAcesso('editar',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE banner_galeria SET estado='0' WHERE id='$id_banner' LIMIT 1");
				$aviso = 'Banner desativado com sucesso';
			}
		break;
		case 'excluir':
			if(!$acesso->getAcesso('excluir',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE banner_galeria SET estado='9' WHERE id='$id_banner' LIMIT 1");
				$aviso = 'Banner exclu�do com sucesso';
			}
		break;
	}
}

//paginacao		
$pag->setRegistro(5);
$pag->setTotal($sql->Totalreg("SELECT * FROM banner_galeria WHERE estado!='9' AND id_galeria='$id_galeria' $and")); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('Nenhum registro encontrado');

//lista
$tpl->loop('lista');
$query = $sql->Consulta("SELECT * FROM banner_galeria WHERE estado!='9' AND id_galeria='$id_galeria' $and ORDER BY id DESC LIMIT $min,$max");
while($linha = mysql_fetch_array($query)){
	$tpl->associaloop('ID',$linha['id']);
	$tpl->associaloop('TITULO',$linha['descricao'].'&nbsp;&nbsp;( '.$linha['resx'].' x '.$linha['resy'].' - '.$linha['local'].' )');
	$tpl->associaloop('LINK',Vazio($linha['link']));
	$banner = Banner($diretorio.$linha['banner'],$linha['resx'],$linha['resy'],$linha['tipo']);
	$tpl->associaloop('BANNER',$banner);
	$tpl->associaloop('DESCRICAO',Vazio($linha['descricao']));
	$tpl->associaloop('LOCAL',$linha['local']);
	$tpl->associaloop('RESOL',$linha['resx'].' x '.$linha['resy']);
	$tpl->associaloop('DATA',$data->MysqlData($linha['data_cadastro']));
	
	//validade
	if($linha['data_cadastro'] == $linha['data_validade']){
		$linha['data_validade'] = 'Indeterminada';
	}else{
		$linha['data_validade'] = $data->MysqlData($linha['data_validade']);
		$linha['data_validade'] .= ' ('.$data->Diferenca($linha['data_validade'],$data->MysqlData($linha['data_cadastro'])).' dias)';
	}
	$tpl->associaloop('VALIDADE',$linha['data_validade']);

	$estado = 'Desativar'; $acao = 'desativar'; $class = 'titulolista';
	if($linha['estado'] == '0'){ $estado = 'Ativar'; $acao = 'ativar'; $class = 'titulolistad'; }
	
	$tpl->associaloop('ACAO',$acao);
	$tpl->associaloop('ESTADO',$estado);
	$tpl->associaloop('CLASSE',$class);
	$tpl->processaloop();
}
$tpl->fechaloop();

$tpl->associa('ID',$id_galeria);
$tpl->associa('SUBTITULO',FormataTitulo($galeria['titulo']));
$tpl->associa('PAGINACAO',$paginas);
$tpl->associa('TOTALREG',$pag->getTotal());
$tpl->associa('MIN',($min + 1));
$tpl->associa('MAX',($min + $max));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>