<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('galerias');
$acesso->getAcesso('listar',true);

$aviso = false;
$erro = false;
$and = NULL;

$sql = new Mysql;
$data = new Data;
$tpl = new Template('template/galeria_lista.html');
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);

$diretorio = '../../arquivos/galerias/foto_capa/';

//busca	
if(isset($_GET['busca'])){
	$and = " AND (galerias.titulo LIKE '%".$_GET['busca']."%' OR galerias.local LIKE '%".$_GET['busca']."%')";
	$pag->AddLink('busca',$_GET['busca']);
}	

//acoes
if(isset($_POST['acao'])){
	$id_galeria = $_POST['id'];
	$id_s = $_POST['ids'];

	switch($_POST['acao']){
		case 'ativar':
			if(!$acesso->getAcesso('editar',false)){
			$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE galerias SET estado='1' WHERE id='$id_galeria' LIMIT 1");
				$aviso = 'Galeria ativada com sucesso';
			}
		break;
		case 'desativar':
			if(!$acesso->getAcesso('editar',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE galerias SET estado='0' WHERE id='$id_galeria' LIMIT 1");
				$aviso = 'Galeria desativada com sucesso';
			}
		break;
		case 'excluir':
			if(!$acesso->getAcesso('excluir',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE galerias SET estado='9' WHERE id='$id_galeria' LIMIT 1");
				$aviso = 'Galeria exclu�da com sucesso';
			}
		break;
	}
}

//paginacao		
$pag->setRegistro(5);
$pag->setTotal($sql->Totalreg("SELECT * FROM galerias WHERE estado!='9' $and")); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('Nenhum registro encontrado');

//lista
$tpl->loop('lista');
$query = $sql->Consulta("SELECT galerias.*,estados.nome_estado,cat_galeria.categoria 
FROM galerias 
LEFT JOIN estados ON estados.id=galerias.id_estado
LEFT JOIN cat_galeria ON cat_galeria.id=galerias.id_categoria
WHERE galerias.estado!='9' $and ORDER BY galerias.id DESC LIMIT $min,$max");
while($linha = mysql_fetch_array($query)){
	$tpl->associaloop('ID',$linha['id']);
	$tpl->associaloop('TITULO',$linha['titulo']);
	$tpl->associaloop('CATEGORIA',$linha['categoria']);
	$tpl->associaloop('LOCAL',Vazio($linha['local']));
	$tpl->associaloop('CIDADE',Vazio($linha['cidade']));
	$tpl->associaloop('UF',Vazio($linha['nome_estado']));
	$tpl->associaloop('ACESSOS',$linha['acessos']);
	$tpl->associaloop('FOTOGRAFO',Vazio($linha['fotografo']));
	$tpl->associaloop('DESCRICAO',Vazio(nl2br($linha['descricao'])));
	$tpl->associaloop('EXIBECAPA',Marcado($linha['exibe_capa'],'1','Sim','N�o'));
	$tpl->associaloop('DESTAQUE',Marcado($linha['destaque'],'1','Sim','N�o'));
	$tpl->associaloop('FOTOCAPA',Vazio(LinkFoto($diretorio,$linha['foto_capa'],1)));
	
	//data	
	$linha['data'] = $data->MysqlData($linha['data']);
	if($linha['duracao'] > 1){
		$linha['data'] = 'De '.$linha['data'].' at� '.$data->SomaData($linha['data'],($linha['duracao'] - 1));
	}
	$tpl->associaloop('DATA',$linha['data']);

	$estado = 'Desativar'; $acao = 'desativar'; $class = 'titulolista';
	if($linha['estado'] == '0'){ $estado = 'Ativar'; $acao = 'ativar'; $class = 'titulolistad'; }
	
	$tpl->associaloop('ACAO',$acao);
	$tpl->associaloop('ESTADO',$estado);
	$tpl->associaloop('CLASSE',$class);
	$tpl->processaloop();
}
$tpl->fechaloop();

$tpl->associa('PAGINACAO',$paginas);
$tpl->associa('TOTALREG',$pag->getTotal());
$tpl->associa('MIN',($min + 1));
$tpl->associa('MAX',($min + $max));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>