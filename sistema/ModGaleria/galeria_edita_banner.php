<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('galerias');
$acesso->getAcesso('editar',true);

$aviso = false;
$erro = false;

$id_banner = (int) $_GET['id'];

$sql = new Mysql;
$data = new Data;
$tpl = new  Template('template/galeria_edita_banner.html');

if(isset($_POST['Submit'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['descricao'],'texto','Descri��o');
	$form->Valida($post['validade'],'numerico','Validade');
	$form->Valida($post['local'],'numerico','Local');
	$form->Valida($post['local'],'texto','Local');
	$id_banner = $post['id'];

	$erro = $form->getErro();
	if(!$erro){
		
		$validade = $data->SomaData($post['datac'],$post['validade']);
		$validade = $data->DataMysql($validade);
		
		$sql->Consulta("UPDATE banner_galeria SET
		descricao='$post[descricao]',
		data_validade='$validade',
		local='$post[local]',
		link='$post[linkb]'
		WHERE id='$id_banner' LIMIT 1");
		
		$aviso = 'Banner atualizado com sucesso';
		
	}
}

//seleciona dados
$query = $sql->Consulta("SELECT banner_galeria.*,galerias.titulo
FROM banner_galeria 
INNER JOIN galerias ON galerias.id=banner_galeria.id_galeria
WHERE banner_galeria.id='$id_banner' LIMIT 1");
$banner = mysql_fetch_array($query);
$validade = $data->Diferenca($data->MysqlData($banner['data_validade']),$data->MysqlData($banner['data_cadastro']));

$tpl->associa('ID',$id_banner);
$tpl->associa('VALIDADE',$validade);
$tpl->associa('DATAC',$data->MysqlData($banner['data_cadastro']));
$tpl->associa('IDGALERIA',$banner['id_galeria']);
$tpl->associa('RESOL','( '.$banner['resx'].' x '.$banner['resy'].' - '.$banner['local'].' )');
$tpl->associa('SUBTITULO',FormataTitulo($banner['titulo']));
$tpl->associa('DESCRICAO',$banner['descricao']);
$tpl->associa('LINKB',$banner['link']);
$tpl->associa('LOCALB',$banner['local']);
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();

?>