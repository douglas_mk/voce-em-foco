<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('galerias');
$acesso->getAcesso('cadastrar',true);

$aviso = false;
$erro = false;

$id_galeria = (int) $_GET['id'];

$sql = new Mysql;
$data = new Data;
$tpl = new  Template('template/galeria_insere_banner.html');

$diretorio = '../../arquivos/banners/';

if(!isset($_POST['validade'])){ $_POST['validade'] = NULL; }
if(!isset($_POST['linkb'])){ $_POST['linkb'] = NULL; }
if(!isset($_POST['descricao'])){ $_POST['descricao'] = NULL; }

if(isset($_POST['Submit'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['validade'],'numerico','Validade');
	$form->Valida($post['local'],'texto','Local');
	$form->Valida($post['descricao'],'texto','Descri��o');
	$id_galeria = $post['id'];

	$erro = $form->getErro();
	if(!$erro){
		$upload = new Upload($_FILES['arquivo']);
		$upload->setTipo('image/jpeg,image/pjpeg,image/gif,image/x-png,image/png,application/x-shockwave-flash');
		$upload->setNome(date('dmyhis'));
		
		if(!$upload->Envia($diretorio)){
			$erro = $upload->getErro();
		}
		
		if(!$erro){
			$tipo = $upload->getTipo();
			$validade = $data->SomaData($data->getData(),$post['validade']);
			$validade = $data->DataMysql($validade);
			$banner = $upload->getNome();
			$codigo = explode('-',$post['local']);
			$res = explode('x',$codigo[1]);
			$resx = $res[0];
			$resy = $res[1];
			
			$sql->Consulta("INSERT INTO banner_galeria
			(id_galeria,estado,pagina,local,tipo,banner,resx,resy,data_cadastro,data_validade,descricao,link)
			VALUES
			('$id_galeria','1','galeria','$codigo[0]','$tipo','$banner','$resx','$resy',NOW(),'$validade','$post[descricao]','$post[linkb]')");
			
			$aviso = 'Banner cadastrado com sucesso';
			$_POST = NULL;
		}
	}	
}

//dados
$query = $sql->Consulta("SELECT * FROM galerias WHERE id='$id_galeria' LIMIT 1");
$galeria = mysql_fetch_array($query);

$tpl->associa('ID',$id_galeria);
$tpl->associa('DATA',$data->getData());
$tpl->associa('LINKB',$_POST['linkb']);
$tpl->associa('DESCRICAO',$_POST['descricao']);
$tpl->associa('VALIDADE',Marcado((int) $_POST['validade'],false,0,$_POST['validade']));
$tpl->associa('SUBTITULO',FormataTitulo($galeria['titulo']));
$tpl->associa('GALERIA',$galeria['titulo']);
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>