<?php
//require('../includes/class.mysql.php');

//$sql = new Mysql;

//galerias
$sql->Consulta("DROP TABLE IF EXISTS cat_galeria");
$sql->Consulta("CREATE TABLE cat_galeria(
	id INT AUTO_INCREMENT PRIMARY KEY,
	estado INT (1) NOT NULL DEFAULT '0',
	categoria VARCHAR (50) NOT NULL,
	icone VARCHAR (25) NULL)");

$sql->Consulta("DROP TABLE IF EXISTS galerias");
$sql->Consulta("CREATE TABLE galerias(
	id INT AUTO_INCREMENT PRIMARY KEY,
	id_categoria INT (11) NOT NULL,
	id_estado INT (11) NULL,
	estado INT (1) NOT NULL DEFAULT '0',
	exibe_capa INT (1) NOT NULL DEFAULT '1', 
	destaque INT (1) NOT NULL DEFAULT '0',
	pasta VARCHAR (25) NOT NULL,
	titulo VARCHAR (255) NOT NULL,
	local VARCHAR (100),
	cidade VARCHAR (50),
	descricao TEXT,
	fotografo VARCHAR (60) NULL,
	data DATE,
	duracao INT (2) NOT NULL,
	foto_capa VARCHAR (25) NULL,
	acessos INT (6) NULL)");

$sql->Consulta("DROP TABLE IF EXISTS banner_galeria");
$sql->Consulta("CREATE TABLE banner_galeria(
	id INT AUTO_INCREMENT PRIMARY KEY,
	id_galeria INT (11) NOT NULL,
	estado INT (1) NOT NULL DEFAULT '0',
	pagina VARCHAR (25) NOT NULL,
	local INT (2) NOT NULL,
	tipo VARCHAR (40) NOT NULL,
	banner VARCHAR (25) NOT NULL,
	resx INT (4) NOT NULL,
	resy INT (4) NOT NULL,
	data_cadastro DATE NOT NULL,
	data_validade DATE NOT NULL,
	descricao VARCHAR (100) NULL,
	link VARCHAR (255) NULL)");
?>