<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('galerias');
$acesso->getAcesso('cadastrar',true);

$aviso = false;
$erro = false;

$tpl = new  Template('template/cat_galeria_cadastra.html');

if(!isset($_POST['categoria'])){ $_POST['categoria'] = NULL; }

if(isset($_POST['Submit'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['categoria'],'texto','Categoria');

	$erro = $form->getErro();
	if(!$erro){
		$sql = new Mysql;
		
		$total = $sql->Totalreg("SELECT * FROM cat_galeria WHERE categoria='$post[categoria]' AND estado!='9' LIMIT 1");
		if($total == 0){
			$sql->Consulta("INSERT INTO cat_galeria 
			(estado,categoria,icone)
			VALUES
			('1','$post[categoria]',NULL)");
			
			$aviso = 'Categoria cadastrada com sucesso';
			$_POST = NULL;
		}else{
			$erro = 'Esta categoria j� esta cadastrada';
		}
	}
}

$tpl->associa('CATEGORIA',$_POST['categoria']);
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>