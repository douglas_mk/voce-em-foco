<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('muralf');
$acesso->getAcesso('listar',true);

$aviso = false;
$erro = false;
$and = NULL;

$id_recado = (int) $_GET['id'];

$sql = new Mysql;
$data = new Data;
$tpl = new Template('template/mural_lista_com.html');
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);
$pag->AddLink('id',$id_recado);

$diretorio = '../../arquivos/muralfotos/';

//dados
$query = $sql->Consulta("SELECT * FROM muralf WHERE id='$id_recado' LIMIT 1");
$recado = mysql_fetch_array($query);

//busca	
if(isset($_GET['busca'])){
	$and = " AND (coment_muralf.nome LIKE '%".$_GET['busca']."%' OR coment_muralf.comentario LIKE '%".$_GET['busca']."%' OR coment_muralf.email LIKE '%".$_GET['busca']."%')";
	$pag->AddLink('busca',$_GET['busca']);
}	

//acoes
if(isset($_POST['acao'])){
	$id_com = $_POST['id'];
	$id_s = $_POST['ids'];

	switch($_POST['acao']){
		case 'ativar':
			if(!$acesso->getAcesso('editar',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE coment_muralf SET estado='1' WHERE id='$id_com' LIMIT 1");
				$aviso = 'Coment�rio ativado com sucesso';
			}
		break;
		case 'desativar':
			if(!$acesso->getAcesso('editar',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE coment_muralf SET estado='0' WHERE id='$id_com' LIMIT 1");
				$aviso = 'Comet�rio desativado com sucesso';
			}
		break;
		case 'excluir':
			if(!$acesso->getAcesso('excluir',false)){
				$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
			}else{
				$sql->Consulta("UPDATE coment_muralf SET estado='9' WHERE id='$id_com' LIMIT 1");
				$aviso = 'Coment�rio exclu�do com sucesso';
			}
		break;
	}
}

//paginacao		
$pag->setRegistro(8);
$pag->setTotal($sql->Totalreg("SELECT * FROM coment_muralf WHERE estado!='9' AND id_recado='$id_recado' $and")); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('Nenhum registro encontrado');

//lista
$tpl->loop('lista');
$query = $sql->Consulta("SELECT * FROM coment_muralf WHERE estado!='9' AND id_recado='$id_recado' $and ORDER BY id DESC LIMIT $min,$max");
while($linha = mysql_fetch_array($query)){
	$tpl->associaloop('ID',$linha['id']);
	$tpl->associaloop('NOME',$linha['nome']);
	$tpl->associaloop('CIDADE',$linha['cidade']);
	$tpl->associaloop('EMAIL',$linha['email']);
	$tpl->associaloop('FOTO',Vazio(LinkFoto($diretorio,$linha['foto'],1)));
	$tpl->associaloop('DATA',$data->MysqlData($linha['data']));
	$tpl->associaloop('HORA',$linha['hora']);
	$tpl->associaloop('COMENTARIO',nl2br($linha['comentario']));
	$tpl->associaloop('IP',$linha['ip']);
	
	$estado = 'Desativar'; $acao = 'desativar'; $class = 'titulolista';
	if($linha['estado'] == '0'){ $estado = 'Ativar'; $acao = 'ativar'; $class = 'titulolistad'; }
	
	$tpl->associaloop('ACAO',$acao);
	$tpl->associaloop('ESTADO',$estado);
	$tpl->associaloop('CLASSE',$class);
	$tpl->processaloop();
}
$tpl->fechaloop();

$tpl->associa('ID',$id_recado);
$tpl->associa('SUBTITULO',FormataTitulo($recado['nome']));
$tpl->associa('PAGINACAO',$paginas);
$tpl->associa('TOTALREG',$pag->getTotal());
$tpl->associa('MIN',($min + 1));
$tpl->associa('MAX',($min + $max));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>