<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('muralf');
$acesso->getAcesso('editar',true);

$aviso = false;
$erro = false;

$id_recado = (int) $_GET['id'];

$sql = new Mysql;
$tpl = new  Template('template/mural_edita.html');

$diretorio = '../../arquivos/muralfotos/';

if(isset($_POST['Submit'])){
	$post = array_map('Sql_inject',$_POST);
	
	$form = new ValidaForm;
	$form->Valida($post['nome'],'texto','Nome');
	$form->Valida($post['cidade'],'texto','Cidade');
	$form->Valida($post['email'],'email','E-mail');
	$form->Valida($post['comentario'],'texto','Comentário');
	$form->Tamanho($post['comentario'],2,500,'Comentário');
	$id_recado = $post['id'];

	$erro = $form->getErro();
	if(!$erro){
			
		$sql->Consulta("UPDATE muralf SET
		nome='$post[nome]',
		cidade='$post[cidade]',
		email='$post[email]',
		comentario='$post[comentario]'
		WHERE id='$id_recado' LIMIT 1");
		
		$aviso = 'Recado atualizado com sucesso';
	}
}

//seleciona dados
$query = $sql->Consulta("SELECT * FROM muralf WHERE id='$id_recado' LIMIT 1");
$linha = mysql_fetch_array($query);

$tpl->associa('ID',$id_recado);
$tpl->associa('FOTO',LinkFoto($diretorio,$linha['foto'],0));
$tpl->associa('SUBTITULO',FormataTitulo($linha['nome']));
$tpl->associa('NOME',$linha['nome']);
$tpl->associa('CIDADE',$linha['cidade']);
$tpl->associa('EMAIL',$linha['email']);
$tpl->associa('COMENTARIO',$linha['comentario']);
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>