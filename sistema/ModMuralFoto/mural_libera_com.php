<?php
require('../inc.includes.php');
$acesso = new Acesso;
$acesso->getSessao();
$acesso->setLocal('muralf');
$acesso->getAcesso('listar',true);

$aviso = false;
$erro = false;

$sql = new Mysql;
$data = new Data;
$tpl = new Template('template/mural_libera_com.html');
if(!isset($_GET['inicio'])){ $_GET['inicio'] = 0; }
$pag = new Paginacao($_GET['inicio']);

$diretorio = '../../arquivos/muralfotos/';

if(!isset($_POST['idcom'])){ $_POST['idcom'] = NULL; }

//excluir
if(isset($_POST['excluir'])){
	$idcom = $_POST['idcom'];
	
	if(sizeof($idcom) == 0){
		$erro = 'Nenhum registro selecionado';
	}else{
		if(!$acesso->getAcesso('excluir',false)){
			$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
		}else{
			//exclui recados
			foreach($idcom as $id){
				$sql->Consulta("UPDATE coment_muralf SET estado='9' WHERE id='$id' LIMIT 1");	
			}
			$aviso = 'Coment�rio(s) exclu�do(s) com sucesso';
		}
	}	
}

//ativar
if(isset($_POST['liberar'])){
	$idcom = $_POST['idcom'];
	
	if(sizeof($idcom) == 0){
		$erro = 'Nenhum registro selecionado';
	}else{
		if(!$acesso->getAcesso('editar',false)){
			$erro = 'Voc� n�o tem permiss�o para executar esta opera��o';
		}else{
			//exclui recados
			foreach($idcom as $id){
				$sql->Consulta("UPDATE coment_muralf SET estado='1' WHERE id='$id' LIMIT 1");	
			}
			$aviso = 'Coment�rio(s) liberado(s) com sucesso';
		}
	}	
}

//paginacao		
$pag->setRegistro(5);
$pag->setTotal($sql->Totalreg("SELECT * FROM coment_muralf WHERE estado='0'")); 
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('Nenhum registro encontrado');

//lista
$tpl->loop('lista');
$query = $sql->Consulta("SELECT coment_muralf.*,muralf.foto AS fotor,muralf.comentario AS comentarior,muralf.nome AS nomer
FROM coment_muralf 
LEFT JOIN muralf ON muralf.id=coment_muralf.id_recado
WHERE coment_muralf.estado='0'
ORDER BY coment_muralf.id DESC LIMIT $min,$max");
while($linha = mysql_fetch_array($query)){
	$tpl->associaloop('ID',$linha['id']);
	$tpl->associaloop('NOME',$linha['nome']);
	$tpl->associaloop('CIDADE',$linha['cidade']);
	$tpl->associaloop('EMAIL',$linha['email']);
	$tpl->associaloop('FOTO',LinkFoto($diretorio,$linha['foto'],1));
	$tpl->associaloop('DATA',$data->MysqlData($linha['data']));
	$tpl->associaloop('HORA',$linha['hora']);
	$tpl->associaloop('COMENTARIO',nl2br($linha['comentario']));
	
	$tpl->associaloop('NOMER',$linha['nomer']);
	$tpl->associaloop('FOTOR',LinkFoto($diretorio,$linha['fotor'],1));
	$tpl->associaloop('COMENTARIOR',nl2br($linha['comentarior']));
	$tpl->processaloop();
}
$tpl->fechaloop();

$tpl->associa('PAGINACAO',$paginas);
$tpl->associa('TOTALREG',$pag->getTotal());
$tpl->associa('MIN',($min + 1));
$tpl->associa('MAX',($min + $max));
$tpl->associa('PAGINA',Pagina());
$tpl->associa('MSG',Msg($aviso,$erro));
$tpl->exibe();
?>