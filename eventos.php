<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new data;
include('banners.php');

$dirfoto = $patchARQ . 'galerias/foto_capa/';

if (!isset($_GET['inicio'])) {
    $_GET['inicio'] = 0;
}
$pag = new Paginacao($_GET['inicio']);

$id_categoria = (int) $_GET['cat'];

//dados da categoria
$query = $sql->Consulta("SELECT * FROM cat_galeria WHERE id='$id_categoria' LIMIT 1");
$cat = mysql_fetch_array($query);
VerificaVar($cat);

//busca
if (!isset($_GET['busca'])) {
    $_GET['busca'] = NULL;
}
$busca = Sql_inject($_GET['busca']);
$titulob = 'Todas as galerias';
$vertodos = NULL;
$and = NULL;

if ($busca) {
    $and = " AND (galerias.titulo LIKE '%$busca%' OR galerias.local LIKE '%$busca%') AND galerias.id_categoria='$id_categoria' ";
    $pag->AddLink('busca', $busca);
    $titulob = 'Resultados da busca por: ' . $busca;
}

//paginacao
$totaln = $sql->Totalreg("SELECT galerias.* FROM galerias WHERE estado='1' AND id_categoria='$id_categoria' $and");

if (($busca) && ($totaln == 0)) {
    $vertodos = '<a href="'.$host . $id_categoria . '/coberturas/" title="Ver todas as galerias em ' . $cat['categoria'] . '">Todas as galerias em ' . $cat['categoria'] . '</a>';
}


$pag->setRegistro(7);
$pag->AddLink('cat', $id_categoria);
$pag->setTotal($totaln);
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('');
$pagatual = $pag->getAtual();

$totalgaleria = NULL;
if ($totaln > 0) {
    $exmax = $min + $max;
    if ($totaln < ($min + $max)) {
        $exmax = $totaln;
    }
    $exibindo = '&nbsp;&nbsp;I&nbsp;&nbsp;Exibindo ' . ($min + 1) . ' - ' . $exmax . ' de um total de ' . $totaln;
    $totalgaleria = ' (' . $totaln . ' galeria';
    if ($totaln > 1) {
        $totalgaleria.= 's';
    }
    $totalgaleria .= ')';
} else {
    $exibindo = '&nbsp;&nbsp;I&nbsp;&nbsp;Nenhuma galeria encontrada';
}

//listagem
$query = $sql->Consulta("SELECT galerias.*,estados.uf
FROM galerias
LEFT JOIN estados ON estados.id=galerias.id_estado
WHERE galerias.estado='1' AND id_categoria='$id_categoria' $and ORDER BY galerias.id DESC LIMIT $min,$max");
?><!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title><?= $cat['categoria']; ?> - <?= $config['nomesite']; ?></title>
        <meta charset="iso-8859-1">
        <link rel="icon" href="<?= $patchIMG ?>favicon.ico" type="image/x-icon" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">        -->
        <link rel="stylesheet" href="<?= $patchCSS ?>/reset.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/text.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/960_16_col.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/style.css">
        <link href="<?= $patchCSS ?>/eventos.css" rel="stylesheet" type="text/css" />
        <link href="<?= $patchCSS ?>/paginacao.css" rel="stylesheet" type="text/css" />
        <link href="<?= $patchCSS ?>/galeriav4.css" rel="stylesheet" type="text/css" />
    </head>
    <body>

        <!-- topo -->
        <?php include('topo.php'); ?>
        <div class="container_16 box vermelho">
            <ul class="breadcrumb grid_16">
                <li><a href="<?= $host ?>" >Inicial</a> <span class="divider">></span></li>
                <li class="active"><?= $cat['categoria']; ?></li>
            </ul>
            <?php include('sidebar.php'); ?>
            <div class="grid_13">
                <h2><span class="icon25 coberturas"></span><?= $cat['categoria']; ?><i class="text14 textBranco"><?= $totalgaleria; ?></i></h2>
                <div class="content">
                    <!--- corpo -->
                    <!-- busca -->
                    <div class="fundobusca grid_12">
                        <div style="float: left;"><a href="javascript:ExibeBusca();" title="Exibir busca" class="btn left vermelho grid_2">Buscar</a>
                            <div id="formbusca" class="chatBaloom" style="display: none; width: 240px;">
                                <form method="get" name="busca" id="busca" style="margin: 0px; display: block;">
                                    <input type="text" name="busca" maxlength="25" class="formbusca" value="<?= $busca; ?>" title="Informe um termo para buscar" />
                                    <input type="button" value="Buscar galeria" class="botaobusca" onclick="location.href='<?= $host ?><?= $id_categoria; ?>/coberturas/busca/'+busca.value" style="width: 100px;">
                                </form>
                            </div>
                        </div>
                        <div class="vertodosbusca"><?= $vertodos; ?></div>
                    </div>

                    <div class="clear"></div>
                    <div class="textCinza text11 grid_12"><?= $titulob; ?><span><?= $exibindo; ?></span></div>

                    <!-- listagem -->
                    <table width="700" border="0" cellspacing="0" cellpadding="5">
                        <?php
                        while ($linha = mysql_fetch_array($query)) {
                            if ($linha['foto_capa']) {
                                $foto = $dirfoto . 'mini/' . $linha['foto_capa'];
                            } else {
                                $foto = 'icones/fotogaleria.jpg';
                            }
                            $linha['foto_capa'] = '<a href="' . $host . $linha['id'] . '/coberturas/' . titulo2url($linha['titulo']) . '" title="Ver fotos" class="bordaimg"><img src="' . $foto . '" style="float: none; margin: 10px;" /></a>';

                            if ($linha['local']) {
                                $linha['local'] = '<div class="eventodescricao">Local: ' . $linha['local'] . '</div>';
                            }
                            if ($linha['uf']) {
                                $linha['uf'] = ' - ' . $linha['uf'];
                            }
                            if ($linha['cidade']) {
                                $linha['cidade'] = '<div class="eventodescricao">Cidade: ' . $linha['cidade'] . $linha['uf'] . '</div>';
                            }
                            ?>
                            <tr>
                                <td width="90" class="bordafundofoto" align="center"><?= $linha['foto_capa']; ?></td>
                                <td align="left" valign="top" class="bordafundodados" style="padding: 10px;">
                                    <div class="eventodata"><?= $data->MYsqlData($linha['data']); ?></div>
                                    <h3><a href="<?= $host . $linha['id'] . '/coberturas/' . titulo2url($linha['titulo']); ?>" class="vermelho"><?= $linha['titulo']; ?></a></h3>

                                    <?= $linha['local']; ?>
                                    <?= $linha['cidade']; ?>
                                </td>
                            </tr>
                            <tr>
                                <td height="15" colspan="2"></td>
                            </tr>
                        <?php } ?>
                    </table>

                    <!-- paginacao -->
                    <div class="grid_16">
                        <?= $paginas; ?>
                    </div>
                    <div class="clear"></div>
                    <div class="ads largo push_2" title="Anuncie" style="margin-top: 20px;">
                        <?php
                        if (isset($banners['9'])) {
                            print $banners['9'];
                        }
                        ?>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
        <?php include('rodape.php'); ?>
        <script type="text/javascript" src="<?= $patchJS ?>geral.js"></script>
    </body>
</html>
