<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new data;
include('banners.php');

$direquipe = $arquivos . 'equipe/';

//paginacao
$totalr = $sql->Totalreg("SELECT * FROM equipe WHERE estado='1'");
if (!isset($_GET['inicio'])) {
    $_GET['inicio'] = 0;
}
$pag = new Paginacao($_GET['inicio']);
$pag->setRegistro(5);
$pag->setTotal($totalr);
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('');
$pagatual = $pag->getAtual();

if ($totalr > 0) {
    //$exibindo = 'Clique sobre o nome para mais detalhes';
    $exmax = $min + $max;
    if ($totalr < ($min + $max)) {
        $exmax = $totalr;
    }
    $exibindo = 'Exibindo ' . ($min + 1) . ' - ' . $exmax . ' de um total de ' . $totalr;
} else {
    $exibindo = 'Nenhum cadastro encontrado';
}

//listagem
$query = $sql->Consulta("SELECT equipe.*
FROM equipe
WHERE equipe.estado='1' ORDER BY equipe.nome ASC LIMIT $min,$max");
?><!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Equipe  - <?= $config['nomesite']; ?></title>
        <meta charset="iso-8859-1">
        <link rel="icon" href="<?= $patchIMG ?>favicon.ico" type="image/x-icon" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">        -->
        <link rel="stylesheet" href="<?= $patchCSS ?>/reset.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/text.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/960_16_col.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/style.css">
        <link href="<?= $patchCSS ?>/equipe.css" rel="stylesheet" type="text/css" />
        <link href="<?= $patchCSS ?>/paginacao.css" rel="stylesheet" type="text/css" />
        <link href="<?= $patchCSS ?>/amplia.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css" />
    </head>
    <body>
        <!-- topo -->
        <?php include('topo.php'); ?>
        <div class="container_16 box vermelho">
            <ul class="breadcrumb grid_16">
                <li><a href="<?= $host ?>" >Inicial</a> <span class="divider">></span></li>
                <li class="active">Equipe</li>
            </ul>
            <?php include('sidebar.php'); ?>
            <div class="grid_13">
                <h2><span class="icon25 galera"></span>Equipe</h2>
                <div class="content">
                    <!--- corpo -->

                    <div class="fundocentro">
                        <div class="fundoexibindo" style="margin-top: 6px;"><?= $exibindo; ?></div>

                        <!-- listagem -->
                        <?php
                        while ($linha = mysql_fetch_array($query)) {
                            if ($linha['foto']) {
                                $linha['foto'] = '<a href="' . $direquipe . $linha['foto'] . '" title="Ampliar foto" class="bordaimg fancybox"><img src="' . $direquipe . 'mini/' . $linha['foto'] . '" style="float: right;" /></a>';
                            }
                            if ($linha['telefonec']) {
                                $linha['telefonec'] = '<span class="equipedescricaotitulo">Telefone comercial:&nbsp;</span> ' . FormataTel($linha['telefonec']) . '<br />';
                            }
                            if ($linha['telefoner']) {
                                $linha['telefoner'] = '<span class="equipedescricaotitulo">Telefone residencial:&nbsp;</span> ' . FormataTel($linha['telefoner']) . '<br />';
                            }
                            if ($linha['celular']) {
                                $linha['celular'] = '<span class="equipedescricaotitulo">Telefone celular:&nbsp;</span> ' . FormataTel($linha['celular']) . '<br />';
                            }
                            if ($linha['email']) {
                                $linha['email'] = '<span class="equipedescricaotitulo">E-mail:&nbsp;</span> ' . $linha['email'] . '<br />';
                            }
                            if ($linha['msn']) {
                                $linha['msn'] = '<span class="equipedescricaotitulo">Msn:&nbsp;</span> ' . $linha['msn'] . '<br />';
                            }
                            if ($linha['cargo']) {
                                $linha['cargo'] = '<div class="equipecargo">' . $linha['cargo'] . '</div>';
                            }
                            ?>
                            <div class="equipebordalista">
                                <div class="equipenome grid_12 omega alpha"><?= $linha['nome']; ?></div>
                                <div class="equipefoto grid_1 omega" style="float: right"><?= $linha['foto']; ?></div>
                                <div class="equipeoculto grid_11">
                                    <div class="equipebordatitulolista">
                                        <?= $linha['cargo']; ?>
                                        <?= $linha['telefonec']; ?>
                                        <?= $linha['telefoner']; ?>
                                        <?= $linha['celular']; ?>
                                        <?= $linha['email']; ?>
                                        <?= $linha['msn']; ?>
                                        <?= nl2br($linha['informacoes']); ?>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>

                        <!-- paginacao -->
                        <div class="grid_16">
                            <?= $paginas; ?>
                        </div>
                        <div class="clear"></div>
                        <div class="ads largo push_2" title="Anuncie" style="margin-top: 20px;">
                            <?php
                            if (isset($banners['9'])) {
                                print $banners['9'];
                            }
                            ?>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('rodape.php'); ?>
        <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>
<script type="text/javascript" src="<?=$patchJS ?>amplia.js"></script>
<script type="text/javascript" src="<?=$patchJS ?>geral.js"></script>
</body>
</html>
