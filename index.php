<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new data;
include('banners.php');
//banner inicial
if (isset($banners[12])) {
    session_start();
    if (!isset($_SESSION['splash'])) {
        header('Location:splash.php');
        $_SESSION['splash'] = 1;
    }
}
//destaque
$query = $sql->Consulta("SELECT * FROM destaques WHERE estado='1' ORDER BY id DESC LIMIT 1");
$destaque = mysql_fetch_array($query);
if ($destaque) {
    $destaque = Banner($arquivos . 'destaques/' . $destaque['arquivo'], 380, 252, 'application/x-shockwave-flash');
} else {
    $destaque = 'Nenhum destaque neste momento';
}
//links
$query = $sql->Consulta("SELECT * FROM links WHERE estado='1' ORDER BY id DESC");

$links = '';
while ($link = mysql_fetch_array($query)) {
    $links .= '<a href="' . $link['site'] . '" title="' . $link['nome'] . '" class="ads links">'
            . '<img src="' . $arquivos . 'links/' . $link['foto'] . '" alt="' . $link['nome'] . '"></a>';
}
?><!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Voc� em Foco</title>
        <meta charset="iso-8859-1">
        <link rel="icon" href="<?= $patchIMG ?>favicon.ico" type="image/x-icon" />
        <!--meta name="viewport" content="width=device-width, initial-scale=1.0"-->
        <!-- FACEBOOK -->
        <meta property="og:title" content="Voc� em Foco"/>
        <meta property="og:description" content="Fotografia e coberturas de eventos"/>
        <meta property="og:image" content="<?= $patchIMG ?>logo_voceemfoco_topo.png"/>

        <link rel="stylesheet" href="<?= $patchCSS ?>/reset.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/text.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/960_16_col.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/style.css">
        <link rel="stylesheet" href="<?= $patchCSS ?>/nivo-slider.css">
        <link rel="stylesheet" href="<?= $patchCSS ?>/amplia.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css" />
        <!--link rel="stylesheet" href="<?= $patchCSS ?>/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" /-->
    </head>
    <body>
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
                var js, fjs=d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js=d.createElement(s);
                js.id=id;
                js.src="https://connect.facebook.net/pt_BR/all.js#xfbml=1&appId=251330314881360";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
        <?php
        if (isset($banners['13'])) {
            ?>
            <div id="bannerpop">
                <a href="javascript:FechaBanner()">Fechar</a>
                 <?=$banners['13']?>
            </div>
            <?php
        }
        ?>
        <!-- topo -->
        <?php include('topo.php'); ?>
        <div class="slider-wrapper theme-default container_16">
            <div id="sliderPrincipal" class="">
                <?php
                $id_evento = (int) 1616;

//dados do evento
                $query = $sql->Consulta("SELECT galerias.* FROM galerias WHERE galerias.id='$id_evento' LIMIT 1");
                $evento = mysql_fetch_array($query);
                $id_categoria = $evento['id_categoria'];

                $aberto = opendir($arquivos . 'galerias/' . $evento['pasta'] . '/'); //abre o diretorio
                $diretorio = $patchARQ . 'galerias/' . $evento['pasta'] . '/';
                while ($arq = @readdir($aberto)) {//le o diretorio
                    if (($arq != '.') && ($arq != '..')) {//desconsidera subdiretorios
                        $ext = explode('.', $arq); //verifica extens�o da imagens
                        if (($ext[1] == 'jpg') || ($ext[1] == 'JPG')) {
                            //$fotos[] = $arq;
                            print '<img src="' . $arquivos . 'galerias/' . $evento['pasta'] . '/' . $arq . '"  alt="" height="415" width="960">';
                        }
                    }
                }
                @closedir($aberto);
//ordena o vetor
//galeria v4
                ?>
            </div>
        </div>

        <div class="container_16">
            <?php include('sidebar.php'); ?>
            <div class="grid_13">

                <div class="ads largo push_2" style="margin-top: 20px;margin-bottom: 0px;height: 65px;">
                    <?php
                    if (isset($banners['8'])) {
                        print $banners['8'];
                    }
                    ?>
                </div>

                <div class="clear"></div>
                <div class="fb-like-box grid_6 box alpha" data-href="https://www.facebook.com/voceemfoco.com.br" data-width="340" data-height="332" data-show-faces="true" data-stream="false" data-show-border="true" data-header="false" style="background-color: #FFF;"></div>

                <div class="grid_7 omega box petroleo">
                    <h2><span class="icon25 destaque"></span>Destaques</h2>
                    <div class="content">
                        <?= $destaque; ?>
                    </div>
                </div>
                <?php
                /*
                <div class="grid_7 omega box laranja">
                    <h2><span class="icon25 videos"></span>V�deos</h2>
                    <div class="content">
                        <ul class="list">
                            <?php
                            //videos
                            $query = $sql->Consulta("SELECT * FROM videos WHERE estado='1' ORDER BY id DESC LIMIT 3");
                            while ($linha = mysql_fetch_array($query)) {
                                print '<li class="clearfix">';
                                print '<a href="' . $hotst . $linha['id'] . '/videos/' . titulo2url($linha['titulo']) . '" title="' . $linha['titulo'] . '">';
                                print '<img src="timthumb.php?src=' . $arquivos . 'videos/mini/' . $linha['miniatura'] . '&w=80&h=70&zc=1" alt="' . $linha['titulo'] . '" height="70" width="80">';
                                print '<h3>' . $linha['titulo'] . '</h3>';
                                print '<div class="data">' . $data->MYsqlData($linha['data']) . '</div>';
                                print '</a>';
                                print '</li>';
                            }
                            ?>
                        </ul>
                        <div class="clear"></div>
                        <a href="<?= $host ?>videos" class="btn right laranja" title="Todos os v�deos">Todos os v�deos</a>
                        <div class="clear"></div>
                    </div>
                </div>
                <div class="clear"></div>
                */
                ?>
                <div class="grid_6 alpha box verde">
                    <h2><span class="icon25 recados"></span>Recados</h2>
                    <div class="content">
                        <?php
//mural de recados
                        $query = $sql->Consulta("SELECT * FROM muralr WHERE estado='1' ORDER BY id DESC LIMIT 2");
                        while ($linha = mysql_fetch_array($query)) {
                            ?>
                            <p class="textCinza text12">De:<b><?= $linha['de'] ?></b></p>
                            <p class="textCinza text12">Para:<b><?= $linha['para'] ?></b></p>
                            <p class="data textCinza text10">Em <?= $data->MYsqlData($linha['data']) ?> as <?= $linha['hora'] ?></p>
                            <div style="position: relative">
                                <p class="chatBaloom text12" onmouseover="$('#rec<?= $linha['id'] ?>').fadeIn();"><?= substr($linha['recado'], 0, 140) ?> ...</p>
                                <p id="rec<?= $linha['id'] ?>" class="chatBaloom text12 recadoFull"  onmouseout="$('#rec<?= $linha['id'] ?>').fadeOut();"><?= $linha['recado'] ?></p>
                            </div>
                            <div class="clear"></div>
                            <?php
                        }
                        ?>
                        <a href="<?= $host ?>recados" class="btn right verde" title="Todos os recados">Todos os recados</a>
                        <div class="clear"></div>
                    </div>
                </div>

                <div class="grid_7 omega box azul">
                    <h2><span class="icon25 galera"></span>Galera net</h2>
                    <div class="content">
                        <ul class="grid">
                            <?php
//galeranet
                            $query = $sql->Consulta("SELECT * FROM galeranet WHERE estado='1' ORDER BY RAND() LIMIT 12");
                            while ($linha = mysql_fetch_array($query)) {
                                print '<li>';
                                print '<a href="galeraBusca.php?user=' . $linha['id'] . '" title="' . $linha['nome'] . '">';
                                print '<img src="timthumb.php?src=' . $arquivos . 'galeranet/' . $linha['foto'] . '&w=94&h=97&zc=1" alt="' . $linha['nome'] . '" width="94" height="">';
                                print '</a>';
                                print '</li>';
                            }
                            ?>
                        </ul>
                        <div class="clear"></div>
                        <a href="<?= $host ?>galeranet/cadastro" class="btn left azul" title="Cadastre-se">CADASTRE-SE</a>
                        <a href="<?= $host ?>galeranet" class="btn right azul" title="Toda a galera">Toda a galera</a>
                        <div class="clear"></div>
                    </div>
                </div>

                <div class="clear"></div>

                <div class="ads largo push_2" style="margin-top: 20px;margin-bottom: 0px;height: 65px;">
                    <?php
                    if (isset($banners['9'])) {
                        print $banners['9'];
                    }
                    ?>
                </div>

                <div class="grid_6 alpha box vermelho">
                    <h2><span class="icon25 coberturas"></span>Coberturas</h2>
                    <div class="content">
                        <div class="bordaeventos">
                            <div class="dadoseventos" id="eventos">
                                <?php
                                $lkev = NULL;
                                $cont = 1;
                                $query = $sql->Consulta("SELECT * FROM galerias WHERE estado='1' AND exibe_capa='1' AND id_categoria='1' ORDER BY id DESC LIMIT 5");
                                while ($evento = mysql_fetch_array($query)) {
                                    print '<div id="evento' . $cont . '"><a href="' . $host . $evento['id'] . '/coberturas/' . titulo2url($evento['titulo']) . '"><img src="timthumb.php?src=' . $arquivos . 'galerias/foto_capa/' . $evento['foto_capa'] . '&w=260&h=205&zc=1&q=100"  alt="' . $evento['titulo'] . '" ></a>';
                                    print '<br ><a href="' . $host . $evento['id'] . '/coberturas/' . titulo2url($evento['titulo']) . '" class="linkCoberturas">' . $evento['titulo'] . '</a></div>';
                                    $lkev .= '<a href="javascript:MostraEvento(' . $cont . ');" id="lkev' . $cont . '" class="lkevover" title="' . $evento['titulo'] . '">' . $cont . '</a>';
                                    $cont++;
                                } if ($cont == 1) {
                                    print 'Nenhum evento encontrado';
                                }
                                ?>
                            </div>
                            <?= $lkev; ?>
                        </div>
                        <div class="clear"></div>
                        <a href="<?= $host ?>1/coberturas" class="btn right vermelho" title="Todos as coberturas">Todas as coberturas</a>
                        <div class="clear"></div>
                    </div>
                </div>
                <?php
                /*
                <div class="clear"></div>
                <div class="grid_6 alpha box preto">
                    <h2><span class="icon25 filmes"></span>Dicas de Filmes</h2>
                    <div class="content" style="padding: 9px;">
                        <ul class="grid">
                            <?php
//dicas de v�deos
                            $i = 0;
                            $query = $sql->Consulta("SELECT * FROM dicasvideos WHERE estado='1' ORDER BY id DESC LIMIT 3");
                            while ($linha = mysql_fetch_array($query)) {
                                if ($i == 0) {
                                    $class = "alpha";
                                }
                                if ($i == 1) {
                                    $class = "alpha omega";
                                }
                                if ($i == 2) {
                                    $class = "omega";
                                }
                                print '<li class="grid_2 ' . $class . '">';
                                if ($linha['cartaz']) {
                                    print '<a href="' . $host . $linha['id'] . '/dicas-de-filmes/' . titulo2url($linha['titulo']) . '" title="Ver detalhes" class="bordaimg">
                                        <img src="timthumb.php?src=' . $arquivos . 'dicasvideos/' . $linha['cartaz'] . '&w=100&h=146&zc=1" width="100" height="146" alt="' . ucfirst(strtolower($linha['titulo'])) . '">
                                            </a>';
                                }
                                print '<h3 class="text12">' . ucfirst(strtolower($linha['titulo'])) . '</h3>';
                                print '</li>';
                                $i++;
                            }
                            ?>
                        </ul>
                        <div class="clear"></div>
                        <a href="<?= $host ?>dicas-de-filmes" class="btn right preto" title="Todos as dicas">Todas as dicas</a>
                        <div class="clear"></div>
                    </div>
                </div>
                */
                ?>
                <div class="grid_7 omega box ciano">
                    <h2><span class="icon25 aniversarios"></span>Aniversariantes</h2>
                    <div class="content">

                        <ul class="grid">
                            <?php
                            $mes = date('m');
                            $query = $sql->Consulta("SELECT galeranet.*,estados.uf,EXTRACT(DAY FROM galeranet.nascimento) AS dia
                                FROM galeranet
                                LEFT JOIN estados ON estados.id=galeranet.id_estado
                                WHERE galeranet.estado='1' AND EXTRACT(MONTH FROM galeranet.nascimento)='$mes' ORDER BY galeranet.nascimento ASC LIMIT 6");
                            while ($linha = mysql_fetch_array($query)) {
                                print '<li class="grid_2">';
                                print '<div class="data text10">Dia ' . $linha['dia'] . '</div>';
                                print '<a href="' . $arquivos . 'galeranet/' . $linha['foto'] . '" title="Ampliar foto" class="bordaimg fancybox">
                                    <img src="timthumb.php?src=' . $arquivos . 'galeranet/' . $linha['foto'] . '&w=100&h=60&zc=1" width="100" height="60" style="margin-bottom: 5px;" alt="' . ucfirst(strtolower($nome[0])) . '" >
                                        </a>';
                                $nome = explode(" ", $linha['nome']);
                                print '<h3 class="text12">' . ucfirst(strtolower($nome[0])) . '</h3>';
                                print '</li>';
                            }
                            ?>
                        </ul>
                        <div class="clear"></div>
                        <a href="<?= $host ?>aniversarios" class="btn right ciano" title="Todos os aniversariantes">Todos os aniversariantes</a>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </div>
        <?php include('rodape.php'); ?>
        <script type="text/javascript" src="http://code.jquery.com/jquery-latest.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js"></script>
        <script type="text/javascript" src="<?= $patchJS ?>jquery.nivo.slider.pack.js"></script>
        <script type="text/javascript" src="<?= $patchJS ?>inicial.js"></script>
        <script type="text/javascript" src="<?= $patchJS ?>amplia.js"></script>
        <script type="text/javascript">
            $(window).load(function() {
                $('#sliderPrincipal').nivoSlider({
                    controlNav: false,
                    effect: 'fade',
                    animSpeed: 1000
                });
            });
            $(document).ready(function() {
                $(".fancybox").fancybox();
            });
        </script>
    </body>
</html>