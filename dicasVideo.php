<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new Data;
include('banners.php');

$dirdica = $arquivos . 'dicasvideos/';

//paginacao
$totalr = $sql->Totalreg("SELECT * FROM dicasvideos WHERE estado='1'");
if (!isset($_GET['inicio'])) {
    $_GET['inicio'] = 0;
}
$pag = new Paginacao($_GET['inicio']);
$pag->setRegistro(12);
$pag->setTotal($totalr);
$max = $pag->getMax();
$min = $pag->getMin();
$paginas = $pag->getPaginas('');
$pagatual = $pag->getAtual();

if ($totalr > 0) {
    $exmax = $min + $max;
    if ($totalr < ($min + $max)) {
        $exmax = $totalr;
    }
    $exibindo = 'Exibindo ' . ($min + 1) . ' - ' . $exmax . ' de um total de ' . $totalr;
} else {
    $exibindo = 'Nenhuma dica encontrada';
}

//listagem
$query = $sql->Consulta("SELECT * FROM dicasvideos WHERE estado='1' ORDER BY id DESC LIMIT $min,$max");
?><!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>Dicas de v�deos  - <?= $config['nomesite']; ?></title>
        <meta charset="iso-8859-1">
        <link rel="icon" href="<?= $patchIMG ?>favicon.ico" type="image/x-icon" />
        <!--<meta name="viewport" content="width=device-width, initial-scale=1.0">        -->
        <link rel="stylesheet" href="<?= $patchCSS ?>/reset.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/text.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/960_16_col.css" />
        <link rel="stylesheet" href="<?= $patchCSS ?>/style.css">
        <link href="<?= $patchCSS ?>/dicasvideos.css" rel="stylesheet" type="text/css" />
        <link href="<?= $patchCSS ?>/paginacao.css" rel="stylesheet" type="text/css" />
        <link href="<?= $patchCSS ?>/galeriav4.css" rel="stylesheet" type="text/css" />
    </head>
    <body>

        <!-- topo -->
        <?php include('topo.php'); ?>
        <div class="container_16 box preto">

            <ul class="breadcrumb grid_16">
                <li><a href="<?= $host ?>" >Inicial</a> <span class="divider">></span></li>
                <li class="active">Dicas de v�deos</li>
            </ul>

            <?php include('sidebar.php'); ?>
            <div class="grid_13">
                <h2><span class="icon25 filmes"></span>Dicas de v�deos</h2>
                <div class="content">
                    <!--- corpo -->

                    <div class="fundocentro">
                        <div class="fundoexibindo" style="margin-top: 6px;"><span class="exibindo"><?= $exibindo; ?></span></div>
                        <a href="http://casablancavideos.com.br/" target="blank" title="Casablanca Videolocadora"><img src="<?=$patchIMG?>casablanca.png" alt="Casablanca Videolocadora" style="top: 35px;right: 20px; position: absolute;"></a>
                        <!-- listagem -->
                        <?php
                        $cont = 1;
                        while ($linha = mysql_fetch_array($query)) {
                            if ($linha['cartaz']) {
                                $linha['cartaz'] = '<a href="' . $host . $linha['id'] . '/dicas-de-filmes/' . titulo2url($linha['titulo']) . '" title="Ver detalhes" class="bordacartaz"><img src="' . $dirdica . 'mini/' . $linha['cartaz'] . '" border="0" /></a>';
                            }
                            if ($cont == 5) {
                                $cont = 1;
                                print '<div style="clear: both;"></div>';
                            }
                            $cont++;
                            ?>
                            <div class="dicaborda">
                                <?= $linha['cartaz']; ?>
                                <a href="<?= $host . $linha['id']; ?>/dicas-de-filmes/<?= titulo2url($linha['titulo']) ?>" title="Ver detalhes" class="titulo"><h3 class="text12"><?= $linha['titulo']; ?></h3></a>
                            </div>
                            <?php
                        }
                        ?>
                        <div class="clear"></div>
                        <!-- paginacao -->
                        <div class="grid_16" style="margin-top: 20px;">
                            <?= $paginas; ?>
                        </div>
                        <div class="clear"></div>
                        <div class="ads largo push_2" title="Anuncie" style="margin-top: 20px;">
                            <?php
                            if (isset($banners['9'])) {
                                print $banners['9'];
                            }
                            ?>
                            <div class="clear"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php include('rodape.php'); ?>
        <script type="text/javascript" src="<?= $patchJS ?>geral.js"></script>
    </body>
</html>
