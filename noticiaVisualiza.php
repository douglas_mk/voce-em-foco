<?php
include('inc.includes.php');
$sql = new Mysql;
$data = new data;
include('banners.php');

$id_noticia = (int) $_GET['noticia'];

$dirfoto = 'arquivos/noticias/';

//dados
$query = $sql->Consulta("SELECT noticias.*,cat_noticia.categoria,fotos_noticia.foto,fotos_noticia.legenda
FROM noticias 
LEFT JOIN cat_noticia ON cat_noticia.id=noticias.id_categoria
LEFT JOIN fotos_noticia ON fotos_noticia.id_noticia=noticias.id
WHERE noticias.id='$id_noticia' AND noticias.estado='1' LIMIT 1");
$linha = mysql_fetch_array($query);
VerificaVar($linha);

//atualiza acesso
$sql->Consulta("UPDATE noticias SET acessos=(acessos + 1) WHERE id='$id_noticia' LIMIT 1");

if($linha['foto']){
	$linha['foto'] = '<img src="'.$dirfoto.'mini/'.$linha['foto'].'" class="bordaminiatura" title="'.$linha['legenda'].'" />';
}
if($linha['autor']){
		$linha['autor'] = 'Autor: '.$linha['autor'];
}
if($linha['fonte']){
		$linha['fonte'] = 'Fonte: '.$linha['fonte'];
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>Not�cias - <?=$config['nomesite'];?></title>
<link href="<?= $patchCSS ?>/geral.css" rel="stylesheet" type="text/css" />
<link href="css/noticias.css" rel="stylesheet" type="text/css" />
<link href="<?= $patchCSS ?>/paginacao.css" rel="stylesheet" type="text/css" />
<link href="<?= $patchCSS ?>/amplia.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<?=$patchJS ?>geral.js"></script>
<script type="text/javascript" src="<?=$patchJS ?>amplia.js"></script>
</head>

<body>
<table width="750" border="0" cellspacing="0" cellpadding="0" align="center" class="fundosite">
  <tr>
    <td height="180" colspan="2">
		<!-- topo -->
		<?php @include('topo.php'); ?>
		</td>
  </tr>
  <tr>
    <td width="180" align="center" valign="top">
		<!-- menu -->
		<?php include('menu.php'); ?>
	</td>
    <td width="560" align="center" valign="top" class="corpo">
		<!--- corpo -->
		<div class="bannercentral"><?php if(isset($banners['8'])) { print $banners['8']; } ?></div>
		<div class="caixatopointerno">
			<div class="logodiariosapezal"><img src="<?=$patchICONES?>diariosapezal.jpg" alt="Di&aacute;rio de Sapezal" /></div>
			Not&iacute;cias - Visualizar not&iacute;cia 
		</div>
		
		<div class="caminho">
			<a href="<?= $host ?>" class="linkcaminho">Inicial</a><img src="<?=$patchICONES?>setacaminho.jpg" />
			<a href="noticias.php" class="linkcaminho">Not�cias</a><img src="<?=$patchICONES?>setacaminho.jpg" />
			Visualizar not�cia
		</div>

		<div class="fundocentro">
			<div class="topobotaointerno" style="height: 22px;">
				<div style="float:left;">
					<a href="javascript:Fonte(1);"><img src="<?=$patchICONES?>fontemais.jpg" alt="Aumentar tamanho da fonte" border="0" /></a>
					<a href="javascript:Fonte(0);"><img src="<?=$patchICONES?>fontemenos.jpg" alt="Diminuir tamanho da fonte" border="0" /></a> 
				</div>
				<div class="descricaobotao"><a href="noticias.php">Voltar para not�cias</a></div>
			</div>
			
			<div class="noticiaborda">
				<?=$linha['foto'];?>
				<span class="noticiadescricao"><?=$linha['categoria'];?></span><br />
				<span class="titulovisualiza"><?=$linha['titulo'];?></a></span> <br />
				<div class="noticiadata"><?=$data->MYsqlData($linha['data']);?> - <?=$linha['hora'];?></div>
				<div style="margin-top: 15px;" id="texto"><?=$linha['noticia'];?>&nbsp;</div>
				<div class="noticiaautor">
					<?=$linha['autor'];?><br />	
					<?=$linha['fonte'];?>
				</div>
				<div class="bordaimpressao"><a href="noticiaPrint.php?noticia=<?=$id_noticia;?>" title="Vers�o para impress�o desta not�cia" target="_blank">Vers�o para impress�o</a></div>
				
				<!-- fotos na not�cia -->
				<?php
				$totalf = $sql->Totalreg("SELECT * FROM fotos_noticia WHERE id_noticia='$id_noticia'");
				if($totalf > 0){
				?>
				<div class="fundoexibindo">Fotos nesta not�cia: (Clique na foto para ampliar)</div>
				<?php 
					//fotos
					$query = $sql->Consulta("SELECT fotos_noticia.* FROM fotos_noticia WHERE id_noticia='$id_noticia'");
					while($linha = mysql_fetch_array($query)){ 
			
						print '<div class="bordafoto">';
						print '<a href="javascript:Amplia(\''.$dirfoto.$linha['foto'].'\');" title="Ampliar foto" class="bordaimg"><img src="'.$dirfoto.'mini/'.$linha['foto'].'" /></a>';
						print $linha['legenda'];
						print '</div>';
					}
				} 
				?>
			</div>
			
			<div class="bannercentral" style="margin-top: 15px;"><?php if(isset($banners['9'])) { print $banners['9']; } ?></div>
		</div>
	</td>
  </tr>
  <tr>
    <td colspan="2"><?php include('rodape.php'); ?></td>
  </tr>
</table>
</body>
</html>